### PC扫码登陆-生成二维码

- **接口说明：** PC扫码登陆-生成二维码
- **接口地址：** /api/oauth/wechat/pc/qrcode
- **请求方式：** Get

#### 请求参数

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| type  | string  | 生成某类型二维码  | 默认不传是pc_login 登陆码、可传pc_relation 绑定/换绑码  |

#### 请求示例

`{{host}}/api/oauth/wechat/pc/qrcode?type=pc_relation`

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| session_token | string  | token值  | 轮询的token值  |
| base64_img    | string  | 图片     |  base64 |

#### 返回示例

```json
{
    "data": {
        "type": "wechat_pc_code",
        "id": "1",
        "attributes": {
            "session_token": "r0m6TbKtWC3tBqL2WYKIUw7KhJ7ED3l1ulgG07Lm",
            "base64_img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFACAYAAADNkKWqAAAACXBIWXMAAA7EAAAOxAGVKw4bA... ...=="
        }
    }
}
```
