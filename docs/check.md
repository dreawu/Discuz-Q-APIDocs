### 检查更新

- **接口说明：** 检查更新
- **接口地址：** /api/check
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述 |
| :------- | :--: | :------: | :--- |


#### 请求示例

```

```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 非 200

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                  | 类型   | 出现要求 | 描述       |
| :------------------------ | :----- | :------- | :--------- |
| attributes.qcloud_version | string |          | 远程版本号 |
| attributes.client_version | string |          | 本地版本号 |
| attributes.need_update    | int    |          | 是否有更新 |

#### 返回示例

```json
{
  "data": {
    "type": "qcloud-check",
    "id": "1",
    "attributes": {
      "qcloud_version": "5.0",
      "client_version": "5.0",
      "need_update": false
    }
  }
}
```
