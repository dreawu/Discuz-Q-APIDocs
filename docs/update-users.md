### 批量修改用户的用户组

- **权限说明：** user.edit.group
- **接口说明：** 批量修改用户的用户组
- **接口地址：** /api/users
- **请求方式：** PATCH

#### 请求参数

| 参数名称 | 类型   | 描述      |
| :------- | :----- | :-------- |
| id       | string | 用户 id   |
| groupId  | string | 用户组 id |

请求示例：

```json
{
  "data": [
    {
      "attributes": {
        "id": 1,
        "groupId": 3
      }
    },
    {
      "attributes": {
        "id": 2,
        "groupId": 3
      }
    }
  ]
}
```

#### 返回结果

| 参数名称 | 类型 | 出现要求 | 描述 |
| :------- | :--- | :------- | :--- |


#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 非 200

#### 返回示例

```json
{
  "data": [
    {
      "type": "users",
      "id": "7",
      "attributes": {
        "id": 7,
        "succeed": false,
        "error": "Permission Denied"
      }
    },
    {
      "type": "users",
      "id": "8",
      "attributes": {
        "id": 8,
        "succeed": false,
        "error": "Permission Denied"
      }
    }
  ]
}
```
