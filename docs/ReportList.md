### 举报列表数据

- **接口说明：** 举报列表数据
- **接口地址：** /api/reports
- **请求方式：** Get

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
|:--|:--|:--|:--|:--
| filter[username]    | int  | 否  | 用户ID  | 筛选举报人的用户名
| filter[start_time] | string  | 否  | 开始时间  | 筛选要大于创建时间
| filter[end_time]   | string  | 否  | 结束时间  | 筛选要小于创建时间
| filter[status]     | int  | 否  | 举报状态  | 0未处理 1已处理
| filter[type]       | int  | 否  | 举报类型  | 0个人主页 1主题 2评论&回复  |
| page[number]       | int  | 否  | 页码  |   |
| page[limit]        | int  | 否  | 条数  |   |

#### 请求示例

```json
{
    "data": {
        "type": "reports",
        "attributes": {
        	"user_id": 2,
        	"start_time": "2020-04-28 00:00",
        	"end_time": "2020-04-28 23:00"
        }
    }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| **data** | object  |   |  数据类型
| type     | string  |   |  数据类型
| id       | int     |   |  数据 id
**attributes**  | object  |   |
| user_id    | int  | 用户ID  |   举报人的用户ID
| thread_id  | int  | 主题ID  |  被举报的主题ID
| post_id    | int  | 回复ID  |  被举报的回复内容ID
| type       | int  | 举报类型  |  0个人主页 1主题 2评论/回复
| reason     | string  | 举报理由  |
| status     | int  | 举报状态  |  0未处理 1已处理
| created_at | string  | 举报创建时间  |
| updated_at | string  | 举报修改时间  |
| **included**  | object  |   | 数据关联
| username  | string  | 用户名称  | 举报人的用户名  |

#### 返回示例

```json
{
    "data": [
        {
            "type": "reports",
            "id": "4",
            "attributes": {
                "user_id": 2,
                "thread_id": 830,
                "post_id": 458,
                "type": 0,
                "reason": "Ford",
                "status": 0,
                "created_at": "2020-04-28T15:39:25+08:00",
                "updated_at": "2020-04-28T15:39:25+08:00"
            },
            "relationships": {
                "user": {
                    "data": {
                        "type": "users",
                        "id": "2"
                    }
                }
            }
        }
    ],
    "included": [
        {
            "type": "users",
            "id": "2",
            "attributes": {
                "id": 2,
                "username": "username",
                "avatarUrl": "",
                "isReal": false,
                "threadCount": 0,
                "followCount": 0,
                "fansCount": 0,
                "likedCount": 0,
                "signature": "",
                "usernameBout": 0,
                "follow": 0,
                "status": 0,
                "loginAt": "2020-04-16T19:26:23+08:00",
                "joinedAt": "2020-04-16T19:26:23+08:00",
                "expiredAt": null,
                "createdAt": "2020-04-16T19:26:23+08:00",
                "updatedAt": "2020-04-16T19:26:28+08:00",
                "canEdit": true,
                "canDelete": true,
                "showGroups": false,
                "registerReason": "",
                "banReason": "",
                "originalMobile": "",
                "registerIp": "127.0.0.1",
                "lastLoginIp": "127.0.0.1",
                "identity": "",
                "realname": "",
                "mobile": ""
            }
        }
    ]
}
```
