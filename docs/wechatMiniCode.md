### 小程序码

- **接口说明：** 微信小程序 - 小程序码
- **接口地址：** /api/oauth/wechat/miniprogram/code
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型  | 是否必须 | 描述       |
| :------- | :---:  | :------: | :--------- |
| path  | string |    否    | 跳转小程序路由路径  |
| width |  int   |    否    | 图片宽度   |
| r     |  int   |    否    | 图片的 RGB |
| g     |  int   |    否    | 图片的 RGB |
| b     |  int   |    否    | 图片的 RGB |

#### 请求示例

Query Params：
```
{{host}}/api/oauth/wechat/miniprogram/code?path=pages/home/index&width=100&r=231&g=121&b=232
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 非 200

#### 返回结果

| 参数名称 | 类型 | 出现要求 | 描述 |
| :------- | :--- | :------- | :--- |


#### 返回示例

```img
Headers:
Content-Type:image/jpeg
二进制:小程序码图片
```
