### 用户刷新 token 接口

- **接口说明：** 刷新 token 接口
- **接口地址：** /api/refresh-token
- **请求方式：** POST

#### 请求参数

| 参数名称      | 类型   | 是否必须 | 描述                  |
| :------------ | :----- | :------: | :-------------------- |
| refresh_token | string |    是    | 刷新令牌              |
| grant_type    | string |    是    | 固定值：refresh_token |
| scope         | array  |    否    | 范围权限列表          |
| client_secret | string |    否    | 客户的秘钥            |
| client_id     | int    |    否    | 客户的 ID             |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "grant_type": "refresh_token",
      "refresh_token": "def502003840......3c420090c5a7",
      "client_id": 2
    }
  }
}
```

#### 返回结果

| 参数名称      | 类型   | 出现要求 | 描述         |
| :------------ | :----- | :------- | :----------- |
| token_type    | string | 刷新成功 | token 类型   |
| expires_in    | int    | 刷新成功 | 时间         |
| access_token  | token  | 刷新成功 | 用户标识     |
| refresh_token | token  | 刷新成功 | 刷新用户标识 |

#### 返回说明

- 登录成功， http 状态码： 200
- 登录失败， http 状态码： 400

#### 返回示例

```json
// 成功示例
{
    "data": {
        "type": "token",
        "id": "0",
        "attributes": {
            "token_type": "Bearer",
            "expires_in": 86400,
            "access_token": "eyJ0eXAiOiJKV1QiLC......BQHUssAOfMLQMiA",
            "refresh_token": "def50200ba012c992......fb3db9aff71eb00"
        }
    }
}

//失败示例
{
    "errors": [
        {
            "status": 401,
            "code": "invalid_request",
            "detail": {
                "error": "invalid_request",
                "error_description": "The refresh token is invalid.",
                "hint": "Cannot decrypt the refresh token",
                "message": "The refresh token is invalid."
            }
        }
    ]
}

//失败示例
{
    "error": "invalid_request",
    "error_description": "The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.",
    "hint": "Check the `client_id` parameter",
    "message": "The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed."
}
```
