### 创建分类接口[多条]

- **接口说明：** 创建分类接口[多条]
- **接口地址：** /api/categories/batch
- **请求方式：** POST

#### 请求参数

| 参数名称    |  类型  | 是否必须 | 描述                                      |
| :---------- | :----: | :------: | :---------------------------------------- |
| name        | string |    是    | 分类名称                                  |
| description | string |    是    | 分类说明                                  |
| icon        | string |    否    | 分类图标 URL[可通过上传图片接口上传图标]  |
| sort        |  int   |    否    | 显示顺序                                  |
| property    |  int   |    否    | 属性：0:正常 1:首页展示                   |
| parentid    |  int   |    是    | 父级ID，父级默认为0，子级为对应的父级ID值 |

#### 请求示例

```json
{
  "data": [
    {
      "type": "categories",
      "attributes": {
        "name": "{{$randomWord}}",
        "description": "{{$randomWords}}",
        "sort": "{{$randomInt}}",
        "icon": "{{$randomAvatarImage}}",
		"parentid": "{{$randomInt}}"
      }
    },
    {
      "type": "categories",
      "attributes": {
        "name": "South Carolina",
        "description": "{{$randomWords}}",
        "sort": "{{$randomInt}}",
        "icon": "{{$randomAvatarImage}}",
		"parentid": "{{$randomInt}}"
      }
    },
    {
      "type": "categories",
      "attributes": {
        "name": "",
        "description": "{{$randomWords}}",
        "sort": "{{$randomInt}}",
        "icon": "{{$randomAvatarImage}}",
		"parentid": "{{$randomInt}}"
      }
    }
  ]
}
```

#### 返回结果

| 参数名称               | 类型   | 出现要求 | 描述                    |
| :--------------------- | :----- | :------- | :---------------------- |
| **data**               | object |          | 基础数据                |
| type                   | string |          | 数据类型                |
| id                     | int    |          | 数据 id                 |
| **attributes**         | object |          | 数据属性                |
| attributes.id          | int    |          | 分类 id                 |
| attributes.name        | string |          | 分类名称                |
| attributes.icon        | string |          | 分类图标 URL            |
| attributes.description | string |          | 分类说明                |
| attributes.property    | int    |          | 属性：0:正常 1:首页展示 |
| attributes.sort        | int    |          | 显示顺序                |
| attributes.threadCount | int    |          | 主题总数                |
| attributes.parentid    | int    |          | 所属父类ID              |

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回示例

```json
{
  "data": [
    {
      "type": "categories",
      "id": "18",
      "attributes": {
        "name": "AGP",
        "description": "connecting deposit hacking Practical Metal Salad",
        "icon": "https://s3.amazonaws.com/uifaces/faces/twitter/imammuht/128.jpg",
        "sort": "184",
        "property": null,
        "thread_count": 0,
        "ip": "127.0.0.1",
		"parentid": 0,
        "created_at": "2019-12-02T19:59:10+08:00",
        "updated_at": "2019-12-02T19:59:10+08:00"
      }
    }
  ],
  "meta": [
    {
      "name": "South Carolina",
      "message": {
        "name": ["名称 不能大于 10 个字符。"]
      }
    },
    {
      "name": "",
      "message": {
        "name": ["名称 不能为空。"]
      }
    }
  ]
}
```
