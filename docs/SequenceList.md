### 获取智能排序信息[列表]

- **接口说明：** 获取智能排序信息列表]
- **接口地址： **/api/sequence
- **请求方式：** GET

#### 请求示例

```
/api/sequence
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                    | 类型   | 出现要求 | 描述           |
| :-------------------------- | :----- | :------- | :------------- |
| **data**                    | object |          | 基础数据       |
| type                        | string |          | 数据类型       |
| id                          | int    |          | 数据 id        |
| **attributes**              | object |          | 数据属性       |
| attributes.category_ids     | string |          | 分类id         |
| attributes.group_ids        | string |          | 用户角色id     |
| **attributes.users**        | object |          | 被选择的用户们 |
| users.id                    | int    |          | 用户ID         |
| users.username              | string |          | 用户昵称       |
| **attributes.topics**       | object |          | 被选择的话题们 |
| topics.id                   | int    |          | 话题ID         |
| topics.content              | string |          | 话题名称       |
| attributes.threads          | string |          | 被选择的帖子ID |
| **attributes.block_users**  | object |          | 被排除的用户们 |
| **attributes.block_topics** | object |          | 被排除的话题们 |
| block_threads               | string |          | 被排除的帖子ID |

#### 返回示例

```json
{
    "data": {
        "type": "sequence",
        "id": "",
        "attributes": {
            "category_ids": "2,3",
            "group_ids": "1,6,7,10",
            "users": [
                {
                    "id": 1,
                    "username": "admincjw"
                },
                {
                    "id": 2,
                    "username": "cjw"
                },
                {
                    "id": 3,
                    "username": "newcjw"
                },
                {
                    "id": 4,
                    "username": "haha"
                }
            ],
            "topics": [],
            "threads": "5,6",
            "block_users": [],
            "block_topics": [
                {
                    "id": 7,
                    "user_id": 3,
                    "content": "333333"
                }
            ],
            "block_threads": "7"
        }
    }
}
```
