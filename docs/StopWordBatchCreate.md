### 创建敏感词接口[批量]

- **接口说明：** 修改敏感词[批量]
- **接口地址：** /api/stop-words/batch
- **请求方式：** POST

#### 请求参数

> **按敏感词批量插入规则：**  
> 「敏感词」=「替换词（或处理方式）」  
> 如需分别设置 ugc 与 username 可使用 | 分隔  
> 主题与回复（ugc）可使用的处理方式：  
>  忽略 {IGNORE}、审核 {MOD}、禁止 {BANNED}、替换 {REPLACE} （可直接写替换内容，默认为 \*\*）  
> 用户名（username）可使用的处理方式：  
>  忽略 {IGNORE}、禁止 {BANNED}
>
> **正确示例**  
> abc  
> abc=123  
> abc={BANNED}  
> abc={MOD}|{IGNORE}  
> abc=456|{BANNED}  
> abc={IGNORE}|{BANNED}  
> abc={BANNED}|{IGNORE}
>
> **错误示例**  
> =content

| 参数名称  |  类型  | 是否必须 | 描述                                               |
| :-------- | :----: | :------: | :------------------------------------------------- |
| **data**  | object |    是    | 基础数据                                           |
| type      | string |    是    | 数据类型，固定值 stop-words                        |
| words     | array  |    是    | 数组中每个元素都是按敏感词批量插入规则编写的字符串 |
| overwrite |  bool  |    否    | 是否覆盖原有敏感词                                 |

#### 请求示例

```json
{
  "data": {
    "type": "stop-words",
    "overwrite": true,
    "words": [
      "abc=123",
      "法西斯",
      "不良言论={MOD}|{IGNORE},",
      "what={BANNEN}|{BANNED}"
    ]
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型   | 出现要求 | 描述     |
| :------- | :----- | :------- | :------- |
| **data** | object |          | 基础数据 |
| type     | string |          | 数据类型 |
| created  | int    |          | 创建数量 |
| updated  | int    |          | 修改数量 |
| unique   | int    |          | 重复数量 |

#### 返回示例

```json
{
  "data": {
    "type": "stop-words",
    "created": 2,
    "updated": 0,
    "unique": 2
  }
}
```
