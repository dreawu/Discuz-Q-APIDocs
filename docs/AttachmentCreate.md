### 上传附件接口

- **接口说明：** 上传附件接口
- **接口地址：** /api/attachments
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述                                                   |
| :------- | :--: | :------: | :----------------------------------------------------- |
| file     | file |    是    | 上传的文件                                             |
| type     | int  |    否    | 0 帖子附件 1 帖子图片 2 帖子音频 3 帖子视频 4 消息图片 5 回答图片 |

#### 请求示例

```
# form-data
file: 选择的文件
type: 1
```

#### 返回说明

- 上传成功， http 状态码： 201
- 上传失败， http 状态码： 500

#### 返回结果

| 参数名称       | 类型   | 出现要求 | 描述                                                               |
| :------------- | :----- | :------- | :----------------------------------------------------------------- |
| **data**       | object |          | 基础数据                                                           |
| type           | string |          | 数据类型                                                           |
| id             | int    |          | 数据 id                                                            |
| **attributes** | object |          | 数据属性                                                           |
| order          | int    |          | 排序                                                               |
| type           | int    |          | 0 帖子附件<br>1 帖子图片<br>2 帖子音频<br>3 帖子视频<br>4 消息图片<br>5 回答图片 |
| type_id        | int    |          | 关联数据 id                                                        |
| isRemote       | bool   |          | 是否是远程附件                                                     |
| isApproved     | bool   |          | 是否合法                                                           |
| url            | string |          | 附件地址                                                           |
| attachment     | string |          | 文件系统生成的名称                                                 |
| extension      | string |          | 文件扩展名                                                         |
| fileName       | string |          | 文件原名称                                                         |
| filePath       | string |          | 文件路径                                                           |
| fileSize       | int    |          | 文件大小                                                           |
| fileType       | string |          | 文件类型                                                           |
| thumbUrl       | string | type=1   | 缩略图地址                                                         |

#### 返回示例

```json
// 成功示例
{
    "data": {
        "type": "attachments",
        "id": "21",
        "attributes": {
            "order": 0,
            "type": 1,
            "type_id": null,
            "isRemote": false,
            "isApproved": 1,
            "url": "http://discuz.test/storage/attachments/2020/07/30/wJVSfE4xaMkjehM4kDGByh5ZpLOmI51Ta9OaNnhJ.png",
            "attachment": "wJVSfE4xaMkjehM4kDGByh5ZpLOmI51Ta9OaNnhJ.png",
            "extension": "png",
            "fileName": "logomark-orange@2x.png",
            "filePath": "public/attachments/2020/07/30/",
            "fileSize": 10179,
            "fileType": "image/png",
            "thumbUrl": "http://discuz.test/storage/attachments/2020/07/30/wJVSfE4xaMkjehM4kDGByh5ZpLOmI51Ta9OaNnhJ_thumb.png"
        }
    }
}

// 失败示例
{
    "errors": [
        {
            "status": "404",
            "code": "upload_error"
        }
    ]
}
```
