### 公众号创建多个菜单接口[多条]

- **接口说明：** 公众号创建多个菜单接口[多条]
- **接口地址：** /api/offiaccount/menu
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述 |
| :--- | :--- | :--- | :--- |
| attributes  | array  | 是  | 一级菜单数组，个数应为1~3个  |
| attributes.name  | string  | 是  | 一级菜单名称  |
| attributes.sub_button  | array  | 否  |  二级菜单数组，个数应为1~5个  |
| sub_button.name  | string  | 是  | 二级菜单名称  |
| type  | string  | 是  | 菜单类型有：click点击、view(跳转网页)、text(返回文本)、img、photo、video、voice、miniprogram(小程序) |
| key  | string  | click等点击类型必须  | 菜单KEY值，用于消息接口推送  |
| url  | string  | view、miniprogram类型必须  | 网页 链接，用户点击菜单可打开链接，<br>type为miniprogram时，不支持小程序的老版本客户端将打开本url。  |
| appid  | int  | miniprogram类型必须  | 小程序的appid（仅认证公众号可配置）  |
| pagepath  | string  | miniprogram类型必须  | 小程序的页面路径  |

#### 请求示例

```json
{
  "data": [
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "今日歌曲",
        "type": "click",
        "key": "keyword"
      }
    },
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "菜单",
        "sub_button": [
          {
            "name": "搜索",
            "type": "view",
            "url": "http://www.soso.com/"
          },
          {
            "name": "小程序跳转",
            "type": "miniprogram",
            "url": "http://mp.weixin.qq.com",
            "appid": "wxbf6a27188sdswq",
            "pagepath": "pages/home/index"
          },
          {
            "name": "media_id赞一下",
            "type": "media_id",
            "media_id": "ryUzCNaipU-eN3pYfRrEdsadsaqwe-dsadqewq"
          },
          {
            "name": "image图片消息",
            "type": "media_id",
            "media_id": "ryUzCNaipU-dwq1d81gegrrt8-fdg1e89weqq3"
          },
          {
            "name": "limited图文消息",
            "type": "view_limited",
            "media_id": "ryUzCNaipU-ieooIHfwfsw556SDQW1fswdqdqd"
          }
        ]
      }
    },
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "加试",
        "sub_button": [
          {
            "name": "发送位置",
            "type": "location_select",
            "key": "rselfmenu_2_0"
          },
          {
            "name": "拍照或者相册发图",
            "type": "pic_photo_or_album",
            "key": "rselfmenu_1_1",
            "sub_button": []
          },
          {
            "name": "扫码带提示",
            "type": "scancode_waitmsg",
            "key": "rselfmenu_0_0",
            "sub_button": []
          },
          {
            "name": "扫码推事件",
            "type": "scancode_push",
            "key": "rselfmenu_0_1",
            "sub_button": []
          }
        ]
      }
    }
  ]
}
```

#### 其他新增按钮类型的请求示例

```json
{
  "data": [
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "扫码",
        "sub_button": [
          {
            "name": "扫码带提示",
            "type": "scancode_waitmsg",
            "key": "rselfmenu_0_0",
            "sub_button": []
          },
          {
            "name": "扫码推事件",
            "type": "scancode_push",
            "key": "rselfmenu_0_1",
            "sub_button": []
          }
        ]
      }
    },
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "发图",
        "sub_button": [
          {
            "name": "系统拍照发图",
            "type": "pic_sysphoto",
            "key": "rselfmenu_1_0",
            "sub_button": []
          },
          {
            "name": "拍照或者相册发图",
            "type": "pic_photo_or_album",
            "key": "rselfmenu_1_1",
            "sub_button": []
          },
          {
            "name": "微信相册发图",
            "type": "pic_weixin",
            "key": "rselfmenu_1_2",
            "sub_button": []
          }
        ]
      }
    },
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "发送位置",
        "type": "location_select",
        "key": "rselfmenu_2_0"
      }
    },
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "图片",
        "type": "media_id",
        "media_id": "MEDIA_ID1"
      }
    },
    {
      "type": "offiaccount_menu",
      "attributes": {
        "name": "图文消息",
        "type": "view_limited",
        "media_id": "MEDIA_ID2"
      }
    }
  ]
}
```

#### 返回示例

- 成功 http 200

| 参数名称 | 类型 | 描述 |
| :--- | :--- | :--- |
| errcode  | int  | 0成功，其余都是失败。（微信返回的数据）  |
| errmsg  | string  | 成功返回“ok”  |

```json
{
    "errcode": 0,
    "errmsg": "ok"
}
```

- 失败 http 500

| 参数名称 | 类型 | 描述 |
| :--- | :--- | :--- |
| errors  | array  | 微信返回数据格式  |
| status  | string  | 状态码  |
| code  | string  | invalid weapp appid hint (类似这种小程序appid不正确)  |

```json
{
    "errors": [
        {
            "status": "500",
            "code": "invalid weapp appid hint: [tvxOua020184]"
        }
    ]
}
```
