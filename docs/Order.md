### 创建订单

- **接口说明：** 创建订单
- **接口地址：** /api/orders
- **请求方式：** POST

#### 请求参数

| 字段名      | 变量名       | 必填 | 类型  | 描述                                                         |
| :---------- | :----------- | :--- | :---- | :----------------------------------------------------------- |
| 订单类型    | type         | 是   | int   | 1：注册，2：打赏，3：付费主题，4：付费用户组，5：问答提问支付，6：问答围观付费, 7: 付费附件，20：文字帖红包，21：长文帖红包 |
| 打赏数据 ID | thread_id    | 否   | int   | 打赏主题填写，填写主题 id                                    |
| 用户组 ID   | group_id     | 否   | int   | 购买用户组时填写                                             |
| 订单金额    | amount       | 否   | float | 订单金额，打赏时填写                                         |
| 是否匿名    | is_anonymous | 否   | int   | 是否匿名，默认不匿名。匿名订单不在前台展示                   |
| 收款人 ID   | payee_id     | 否   | int   | 当订单类型为 5 问答提问时，该字段必传                        |

```json
{
  "data": {
    "attributes": {
      "type": "1",
      "thread_id": "1",
      "group_id": "null",
      "amount": "1",
      "is_anonymous": "1",
      "payee_id": 4
    }
  }
}
```

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                | 必填 | 类型     | 描述                                                         |
| :--------------------- | :-------------------- | :--- | :------- | :----------------------------------------------------------- |
| **data.attributes**    | object                | 是   | object   | 数据属性                                                     |
| 订单编号               | attributes.order_sn   | 是   | string   | 订单唯一编号                                                 |
| 订单金额               | attributes.amount     | 是   | float    | 订单付款金额                                                 |
| 付款状态               | attributes.status     | 是   | int      | 付款状态，0 待付款，1 已付款 ，2 取消订单，3 支付失败，4 订单已过期，10 已退款订单，11 不处理的异常订单 |
| 订单类型               | attributes.type       | 是   | int      | 1：注册，2：打赏，3：付费主题，4：付费用户组，5：问答提问支付，6：问答围观付费, 7: 付费附件，20：文字帖红包，21：长文帖红包 |
| 主题 ID                | attributes.thread_id  | 是   | int      | 打赏的主题                                                   |
| 用户组 ID              | attributes.group_id   | 是   | int      | 付费用户组 id                                                |
| 更新时间               | attributes.updated_at | 是   | datetime | 更新时间                                                     |
| 创建时间               | attributes.created_at | 是   | datetime | 创建时间                                                     |
| **data.relationships** | object                | 是   | object   | 关联关系                                                     |
| **included**           | object                | 是   | object   | 关联数据（包含 user，thread，thread.firstPost，group）       |

#### 返回示例

```json
{
  "data": {
    "type": "orders",
    "id": "9",
    "attributes": {
      "order_sn": "2019112210453097539952",
      "amount": "2.50",
      "status": 0,
      "type": 2,
      "thread_id": "2",
      "updated_at": "2019-11-22T10:45:30+08:00",
      "created_at": "2019-11-22T10:45:30+08:00"
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      },
      "thread": {
        "data": {
          "type": "threads",
          "id": "2"
        }
      }
    }
  },
  "relationships": {
    "user": {
      "data": {
        "type": "users",
        "id": "1"
      }
    },
    "thread": {
      "data": {
        "type": "threads",
        "id": "2"
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "mobile": "mobile",
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    },
    {
      "type": "threads",
      "id": "2",
      "attributes": {
        "title": "test",
        "price": "0.50",
        "viewCount": 0,
        "postCount": 0,
        "likeCount": 0,
        "createdAt": null,
        "updatedAt": null,
        "isApproved": true,
        "isSticky": false,
        "isEssence": false,
        "canApprove": false,
        "canSticky": false,
        "canEssence": false,
        "canDelete": false
      }
    }
  ]
}
```
