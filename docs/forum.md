### 前端获取配置接口

- **接口说明：** 前端获取配置接口
- **接口地址：** /api/forum
- **请求方式：** GET

#### 请求参数

#### 返回结果

- 站点设置 set_site

| 字段名                | 功能对应           | tag            | 类型         | 值说明                                                                 | 参数值示例                              | 管理员<br>可见    |
| --------------------- | ------------------ | -------------- | ------------ | ---------------------------------------------------------------------- | --------------------------------------- | ----------------- |
| site_name             | 站点名称           | default        | string       | 展示在站点信息和 title                                                 | Discuz!Q                                |                   |
| site_title            | 站点标题           | default        | string       | 展示在站点 title                                                       | Discuz!Q                                |                   |
| site_keywords         | 站点关键词         | default        | string       | 展示在站点信息                                                         | 畅言,无阻                               |                   |
| site_introduction     | 站点介绍           | default        | string       | 展示在站点信息                                                         | 畅言无阻                                |                   |
| site_mode             | 站点模式           | default        | string       | public 公开模式 pay 付费模式                                           | pay                                     |                   |
| site_logo             | 站点 Logo          | default        | string       |                                                                        |                                         |                   |
| site_close            | 关闭站点           | default        | bool         | false 开启站点 true 关闭站点                                           | 0                                       |                   |
| site_url              | 站点地址           | default        | 无需<br>配置 | 初始化站点时 C 端填写<br>末尾不要加斜线                                | discuz.com                              |                   |
| site_record           | 备案信息           | default        | string       | 站点的 ICP 备案编号                                                    | WQ12DISCUZ1                             |                   |
| site_stat             | 第三方统计         | default        | string       | 网站的第三方统计代码                                                   |                                         ||
| site_install          | 站点信息           | default        | string       | 站点安装时间                                                           | 2020-01-01 00:00:00                     |                   |
| site_pay_time         | 付费模式开启时间   | default        |              |                                                                        |                                         | ✓                 |
| site_price            | 加入价格           | default        | float        | 付费后加站价格                                                         | 39.9                                    | site_mode='pay'时 |
| site_expire           | 到期时间           | default        |              |                                                                        |                                         | site_mode='pay'时 |
| site_onlooker_price   | 站点围观价格       | default        | float        | 围观单价                                                               | 1                                       |                   |
| site_author_scale     | 作者比例           | default        | int          | 主题打赏分成比例,和站长比例加起来必须为 10,不填时默认为作者 10、平台 0 | 7                                       | ✓                 |
| site_master_scale     | 站长比例           | default        | int          | 主题打赏分成比例,和作者比例加起来必须为 10,不填时默认为作者 10、平台 0 | 3                                       |                   |
| site_close_msg        | 关闭站点时提示信息 | default        | string       | 关闭后网站提示语                                                       | 该论坛已关闭。                          | site_close=1 时   |
| site_author.id        | 站长用户 ID        | default        | int          | 站长的用户 id                                                          | 1                                       |                   |
| site_author.username  | 站长用户 名称      | default        | string       | 站长的用户 名称                                                        | admin                                   |                   |
| site_author.avatar    | 站长用户 头像      | default        | string       | 站长的用户 头像                                                        | https://xxxxx.com/storage/avatars/1.png |                   |
| username_bout         | 用户名修改次数     | default        | int          | 允许每个用户的修改次数(管理员不受限制)默认值是 1                       |                                         |                   |
| site_header_logo      | 首页头部 LOGO      | default        |              |                                                                        |                                         |                   |
| site_background_image | 首页头部背景图     | default        |              |                                                                        |                                         |                   |
| miniprogram_video     | 微信小程序视频开关 | wx_miniprogram | bool         | 开启后小程序允许发布视频、展示视频主题（0 关闭 1 开启）                | true                                    | ✓                 |
| site_minimum_amount   | 最小自定义支付金额 | default        | float        |                                                                        |                                         |                   |
| site_open_sort | 首页智能排序 | default | int | 0关闭，1开启 | 0 | |
| site_create_thread0 | 允许站点发布文字帖 | default | int | 0不允许，1允许 | 1 | |
| site_create_thread1   | 允许站点发布帖子   | default        | int          | 0不允许，1允许                                               | 1                                       | |
| site_create_thread2   | 允许站点发布视频帖 | default        | int          | 0不允许，1允许                                               | 1                                       | |
| site_create_thread3   | 允许站点发布图片帖 | default        | int          | 0不允许，1允许                                               | 1                                       | |
| site_create_thread4   | 允许站点发布语音帖 | default        | int          | 0不允许，1允许                                               | 1                                       | |
| site_create_thread5   | 允许站点发布问答帖 | default        | int          | 0不允许，1允许                                               | 1                                       | |
| site_create_thread6   | 允许站点发布商品帖 | default        | int          | 0不允许，1允许                                               | 1                                       | |
| site_skin             | 站点所在主题       | default        | int          | 1蓝版、2红版                                                 | 1                                       | |

- 注册设置 set_reg

| 字段名            | 功能对应         | tag     | 类型 | 值说明                                   | 参数值示例 | 管理员可见 |
| ----------------- | ---------------- | ------- | ---- | ---------------------------------------- | ---------- | ---------- |
| register_close    | 是否允许注册     | default | bool | false 不允许 true 允许                   | true       |            |
| register_type     | 注册模式         | default | int  | 0 用户名模式、1 手机号模式<br>2 无感模式 | 1          |            |
| register_captcha  | 注册验证码       | default | bool | false 关闭 true 开启                     | 0          |            |
| register_validate | 是否开启注册审核 | default | bool | false 关闭 true 开启                     | 0          |            |
| password_length   | 密码长度         | default | int  | 默认不填时是 6 位密码                    | 10         |            |
| password_strength | 密码强度         | default | int  | 0 数字 1 小写字母<br>2 符号 3 大写字母   | 0,1,2      |            |

- 第三方设置 passport

| 字段名                          | 功能对应                        | tag            | 类型   | 值说明                   | 参数值示例 | 管理员可见 |
| ------------------------------- | ------------------------------- | -------------- | ------ | ------------------------ | ---------- | ---------- |
| offiaccount_close               | 微信 h5 登陆开关                | wx_offiaccount | bool   | false 关闭<br> true 开启 | false      |            |
| offiaccount_app_id              | 微信 h5 登陆                    | wx_offiaccount | 加密   | 使用                     |            | ✓          |
| offiaccount_app_secret          | 微信 h5 登陆                    | wx_offiaccount | 加密   | 使用                     |            | ✓          |
| offiaccount_server_config_token | 微信公众号服务器配置令牌(token) | wx_offiaccount | 加密   | 使用中                   | g3G9Xo4jSM | ✓          |
| miniprogram_close               | 微信小程序登陆开关              | wx_miniprogram | bool   | false 关闭<br> true 开启 | true       |            |
| miniprogram_app_id              | 微信小程序登陆                  | wx_miniprogram | 加密   | 使用                     |            | ✓          |
| miniprogram_app_secret          | 微信小程序登陆                  | wx_miniprogram | 加密   | 使用                     |            | ✓          |
| oplatform_close                 | 微信 pc 登陆开关                | wx_oplatform   | bool   | false 关闭<br> true 开启 | true       |            |
| oplatform_app_id                | 微信开放平台                    | wx_oplatform   | 加密   | 使用                     |            | ✓          |
| oplatform_app_secret            | 微信开放平台                    | wx_oplatform   | 加密   | 使用                     |            | ✓          |
| oplatform_url                   | 微信 pc 登陆                    | wx_oplatform   | 不加密 | 使用                     |            | ✓          |
| oplatform_app_token             | 微信 pc 登陆                    | wx_oplatform   | 不加密 | 使用                     |            | ✓          |
| oplatform_app_aes_key           | 微信 pc 登陆                    | wx_oplatform   | 不加密 | 使用                     |            | ✓          |
| wx_work                         | 企业微信登陆开关                | wx_work        | bool   | false 关闭<br> true 开启 | false      |            |
| corpid                          | 企业微信企业 ID                 | wx_work        | 加密   | 使用                     |            | ✓          |
| secret                          | 企业微信应用密钥                | wx_work        | 加密   | 使用                     |            | ✓          |
| agentid                         | 企业微信应用 ID                 | wx_work        | 加密   | 使用                     |            | ✓          |

- 支付设置 paycenter

| 字段名             | 功能对应               | tag   | 类型 | 值说明               | 参数值示例 | 管理员可见 |
| ------------------ | ---------------------- | ----- | ---- | -------------------- | ---------- | ---------- |
| wxpay_close        | 微信支付开关           | wxpay | bool | true 开启 false 关闭 | true       |            |
| wxpay_ios          | IOS 微信支付开关       | wxpay | bool | true 开启 false 关闭 | true       |            |
| mch_id             | 微信支付商户号         | wxpay |      | v1 使用              |            | ✓          |
| app_id             | 微信支付 app_id        | wxpay | 加密 | v1 使用              |            | ✓          |
| api_key            | 微信支付 api_key       | wxpay | 加密 | v1 使用              |            | ✓          |
| app_secret         | 微信支付 app_secret    | wxpay | 加密 | v1 使用              |            | ✓          |
| wxpay_mch_id       | 微信支付商户号         | wxpay | 加密 | 暂无使用             |            | ✓          |
| wxpay_app_id       | 微信支付 app_id        | wxpay | 加密 | 暂无使用             |            | ✓          |
| wxpay_api_key      | 微信支付 api_key       | wxpay | 加密 | 暂无使用             |            | ✓          |
| wxpay_app_secret   | 微信支付 app_secret    | wxpay | 加密 | 暂无使用             |            | ✓          |
| wxpay_mchpay_close | 微信企业付款到零钱开关 | wxpay | bool | true 开启 false 关闭 |            |            |

- 附件设置 set_attach

| 字段名           | 功能对应         | tag     | 类型   | 值说明  | 参数值示例       | 管理员可见 |
| ---------------- | ---------------- | ------- | ------ | ------- | ---------------- | ---------- |
| support_img_ext  | 支持的图片扩展名 | default | string | ,隔开   | png,gif,jpg      |            |
| support_file_ext | 支持的附件扩展名 | default | string | ,隔开   | doc,docx,pdf,zip |            |
| support_max_size | 支持最大大小     | default | int    | MB 单位 | 1                |            |

- 腾讯云设置 qcloud

| 字段名                    | 功能对应               | tag    | 类型    | 值说明                                                                                                                                                                                                                  | 参数值示例    | 管理员可见 |
| ------------------------- | ---------------------- | ------ | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | ---------- |
| qcloud_close              | 云 api 开关            | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    | true          | ✓          |
| qcloud_app_id             | 云 api-app_id          | qcloud | string  |                                                                                                                                                                                                                         |               |            |
| qcloud_secret_id          | 云 api-secret_id       | qcloud | 加密    |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_secret_key         | 云 api-secret_key      | qcloud | 加密    |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_cms_image          | 云 api 图片安全开关    | qcloud | bool    | true 开启,false 关闭                                                                                                                                                                                                    |               | ✓          |
| qcloud_cms_text           | 云 api 内容安全开关    | qcloud | bool    | true 开启,false 关闭                                                                                                                                                                                                    |               | ✓          |
| qcloud_sms                | 短信开关               | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    | true          |            |
| qcloud_sms_app_id         | 短信 app_id            | qcloud | 加密    |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_sms_app_key        | 短信 app_key           | qcloud | 加密    |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_sms_template_id    | 短信模板 id            | qcloud | 加密    |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_sms_sign           | 短信签名               | qcloud | 加密    |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_cos                | cos 开关               | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    |               |            |
| qcloud_cos_cdn_url        | cos cdn 域名           | qcloud | string  |                                                                                                                                                                                                                         |               | ✓          |
| qcloud_cos_bucket_name    | 名称                   | qcloud | string  | test-1251011534                                                                                                                                                                                                         |               | ✓          |
| qcloud_cos_bucket_area    | 地域                   | qcloud | string  | ap-beijing                                                                                                                                                                                                              |               | ✓          |
| qcloud_cos_sign_url       | 开启 url 签名          | qcloud | bool    | 开启时将返回带有有效期签名的 url                                                                                                                                                                                        | true          | ✓          |
| qcloud_ci_url             | 数据万象处理域名       | qcloud | string  | test-.picbj.myqcloud.com                                                                                                                                                                                                |               |            |
| qcloud_vod                | 云点播开关             | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    | 1             |            |
| qcloud_vod_transcode      | 转码模板               | qcloud | int     | https://console.cloud.tencent.com/vod/video-process/template 中的模板名称 ID                                                                                                                                            |               |            |
| qcloud_vod_ext            | 视频扩展名             | qcloud | string  | WMV、RM、MOV、MPEG、MP4、3GP、FLV、AVI、RMVB 等                                                                                                                                                                         | mp4,wmv       |            |
| qcloud_vod_size           | 视频大小               | qcloud | MB 单位 |                                                                                                                                                                                                                         | 1             |            |
| qcloud_vod_cover_template | 截图模板               | qcloud | int     | https://console.cloud.tencent.com/vod/video-process/template/screenshot 中的模板 ID，创建模板类型为”时间点截图“                                                                                                         |               | ✓          |
| qcloud_vod_url_key        | 云点播防盗链 Key       | qcloud | string  | https://console.cloud.tencent.com/vod/distribute-play/domain 域名设置中“Key 防盗链”                                                                                                                                     |               | ✓          |
| qcloud_vod_url_expire     | 云点播防盗链签名有效期 | qcloud | int     | 单位秒。过期后该 URL 将不再有效，返回 403 响应码。考虑到机器之间可能存在时间差，防盗链 URL 的实际过期时间一般比指定的过期时间长 5 分钟，即额外给出 300 秒的容差时间。建议过期时间戳不要过短，确保视频有足够时间完整播放 | 3600          | ✓          |
| qcloud_vod_taskflow_gif   | 动图封面任务流名称     | qcloud | string  | https://console.cloud.tencent.com/vod/video-process/taskflow中创建的任务流                                                                                                                                              | GifVideoCover | ✓          |
| qcloud_vod_sub_app_id     | 云点播子应用 appid     | qcloud | int     | 云点播子应用 appid                                                                                                                                                                                                      |               |            |
| qcloud_vod_watermark      | 视频水印模板 ID        | qcloud | int     | https://console.cloud.tencent.com/vod/video-process/template/watermark 中的模板 ID                                                                                                                                      |               |            |
| qcloud_faceid             | 实名认证开关           | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    |               |            |
| qcloud_faceid_region      | 实名认证地域           | qcloud | string  |                                                                                                                                                                                                                         |               |            |
| qcloud_token              |                        | qcloud | string  |                                                                                                                                                                                                                         |               |            |
| qcloud_captcha            | 验证码开关             | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    |               |            |
| qcloud_captcha_app_id     | 验证码 appid           | qcloud | string  |                                                                                                                                                                                                                         |               |            |
| qcloud_captcha_secret_key | 验证码 secret_key      | qcloud | string  |                                                                                                                                                                                                                         |               |            |
| qcloud_cos_doc_preview    | 是否开启文档预览       | qcloud | bool    | true 开启 false 关闭                                                                                                                                                                                                    |               |            |

- 提现设置 set_cash

| 字段名             | 功能对应           | tag  | 类型  | 值说明                                               | 参数值示例 | 管理员可见 |
| ------------------ | ------------------ | ---- | ----- | ---------------------------------------------------- | ---------- | ---------- |
| cash_interval_time | 提现间隔时间       | cash | int   | 每次提现间隔时间<br>1 天为 24 小时，0 或不填则不限制 | 1          | ✓          |
| cash_rate          | 提现手续费率       | cash | float | 提现手续费率（百分之）                               | 0.3        |            |
| cash_min_sum       | 单次提现最小金额   | cash | float | 用户每次提现的最小金额                               | 100        | ✓          |
| cash_max_sum       | 单次提现最大金额   | cash | float | 用户每次提现的最大金额                               | 5000       | ✓          |
| cash_sum_limit     | 每日提现总金额上限 | cash | float | 所有用户提现的每日上限总金额                         | 5000       | ✓          |

- 水印设置 watermark

| 字段名             | 功能对应     | tag       | 类型   | 值说明     | 参数值示例          | 管理员可见 |
| ------------------ | ------------ | --------- | ------ | ---------- | ------------------- | ---------- |
| watermark          | 水印开关     | watermark | bool   | true/false | true                | ✓          |
| watermark_image    | 水印图       | watermark | string | 水印图路径 | watermark_image.png | ✓          |
| position           | 水印位置     | watermark | int    | 0 - 9      | 3                   | ✓          |
| horizontal_spacing | 水印水平边距 | watermark | int    | 0 - 9999   | 10                  | ✓          |
| vertical_spacing   | 水印垂直边距 | watermark | int    | 0 - 9999   | 10                  | ✓          |

- 隐私政策 & 用户协议

| 字段名           | 功能对应 | tag       | 类型   | 值说明       | 参数值示例 | 管理员可见 |
| ---------------- | -------- | --------- | ------ | ------------ | ---------- | ---------- |
| privacy          | 隐私政策 | agreement | bool   | 隐私政策开关 | true       |            |
| privacy_content  | 隐私政策 | agreement | string | 隐私政策内容 |            |            |
| register         | 用户协议 | agreement | bool   | 用户协议开关 | true       |            |
| register_content | 用户协议 | agreement | string | 用户协议内容 |            |            |

## 非 setting 设置的值

- 其他信息 other

| 字段名                        | 功能对应                         | tag | 类型 | 值说明                                           | 参数值示例 | 管理员可见 |
| ----------------------------- | -------------------------------- | --- | ---- | ------------------------------------------------ | ---------- | ---------- |
| count_threads                 | 统计所有主题数                   |     | int  | 所有的合法的主题数                               |
| count_posts                   | 统计所有回复数                   |     | int  | 所有的合法的回复数                               |
| count_users                   | 统计所有的用户                   |     | int  | 所有的正常的用户                                 |
| can_upload_attachments        | 判断上传附件的权限               |     | bool | 是否可以上传附件                                 |
| can_upload_images             | 判断上传图片的权限               |     | bool | 是否可以上传图片                                 |
| can_create_thread             | 判断发布主题的权限               |     | bool | 是否可以发布主题                                 |
| can_view_threads              | 判断是否可以浏览主题列表页       |     | bool | 是否可以获取主题列表                             |
| can_batch_edit_threads        | 判断是否可以批量管理主题         |     | bool | 是否可以批量编辑主题                             |
| can_view_user_list            | 判断是否可以浏览用户列表         |     | bool | 是否可以获取用户列表                             |
| can_edit_user_group           | 判断是否可以编辑用户分组         |     | bool | 是否可以编辑用户分组                             |
| can_edit_user_status          | 判断是否可以编辑用户状态         |     | bool | 是否可以编辑用户状态                             |
| can_create_invite             | 判断是否可以发起邀请             |     | bool | 是否可以发起邀请                                 |
| can_create_thread_video       | 判断发视频帖权限                 |     | bool | 是否可以上传视频                                 |
| can_create_thread_long        | 判断发长文权限                   |     | bool | 是否可以发布长文                                 |
| create_thread_with_captcha    | 判断发帖启用验证码               |     | bool | 是否启用发帖验证码                               |
| initialized_pay_password      | 判断初始化支付密码               |     | bool | 是否初始化支付密码                               |
| can_create_audio              | 是否有权发布音频                 |     | bool | 是否有权发布音频                                 |
| can_create_thread_in_category | 判断至少在某个分类下有发帖权限   |     | bool |                                                  |
| can_create_dialog             | 是否能够创建站内信会话、会话消息 |     | bool | 是否能够创建站内信会话、会话消息                 |
| publish_need_real_name        | 发布是否需要实名认证             |     | bool |                                                  |
| publish_need_bind_phone       | 发布是否需要手机号               |     | bool |                                                  |
| can_create_thread_paid        | 是否能够发布付费贴和被支付       |     | bool | 是否能够发布付费贴和被支付                       |
| can_invite_user_scale         | 是否可以邀请用户分成             |     | bool | 当用户所在用户组有分成比例时，该字段用于前端判断 |
| can_create_thread_goods       | 是否允许发布商品帖               |     | bool | 是否允许发布商品帖                               |
| can_create_thread_question    | 是否允许发布问答帖               |     | bool | 是否允许发布问答帖                               |
| can_be_asked                  | 是否允许被提问                   |     | bool | 是否允许被提问                                   |
| can_be_onlooker               | 是否允许被围观                   |     | bool | 是否允许被围观                                   |

- 登陆者的信息 user

| 字段名        | 功能对应         | tag | 类型   | 值说明           | 参数值示例 | 管理员可见 |
| ------------- | ---------------- | --- | ------ | ---------------- | ---------- | ---------- |
| groups        | 用户所属的用户组 |     | array  | 用户的所有用户组 |
| register_time | 用户注册时间     |     | string | 用户注册的时间   |

- 位置服务

| 字段名     | 功能对应     | tag | 类型   | 值说明           | 参数值示例  | 管理员可见 |
| ---------- | ------------ | --- | ------ | ---------------- | ----------- | ---------- |
| lbs        | 位置服务     | lbs | bool   | 位置服务开关     | true        | x          |
| qq_lbs_key | 腾讯位置服务 | lbs | string | 腾讯位置服务 key | ABC-123-XYZ | x          |

- UCenter

| 字段名      | 功能对应     | tag     | 类型   | 值说明             | 参数值示例    | 管理员可见 |
| ----------- | ------------ | ------- | ------ | ------------------ | ------------- | ---------- |
| ucenter_url | UCenter 地址 | ucenter | string | UCenter 配置的地址 | https://...   | ✓          |
| ucenter_key | 通信秘钥     | ucenter | string | UCenter 配置的 key | uonSweqDdqsda | ✓          |

#### 返回说明

- 返回空，http 状态码：200

#### 返回示例

```json
{
  "data": {
    "attributes": {
      "set_site": {
        "site_name": "Discuz Q",
        "site_introduction": "站点介绍修改",
        "site_mode": "public",
        "site_close": false,
        "site_logo": "http://discuz.com/storage/logo.png?1580713768",
        "site_close_msg": "关闭原因",
        "site_price": "0.01",
        "site_expire": "5",
        "site_author_scale": "6",
        "site_master_scale": "4",
        "site_icp": null,
        "site_stat": null,
        "site_author": {
          "id": 1,
          "username": "username"
        },
        "site_install": "2019-12-25 17:22:52",
		"site_open_sort": 0,
        "site_create_thread0": 1,
        "site_create_thread1": 1,
        "site_create_thread2": 1,
        "site_create_thread3": 1,
        "site_create_thread4": 1,
        "site_create_thread5": 1,
        "site_create_thread6": 1,
        "site_skin": 1
      },
      "set_reg": {
        "register_close": true,
        "register_validate": false,
        "password_length": 6,
        "password_strength": "0,1,3"
      },
      "passport": {
        "offiaccount_close": "1",
        "offiaccount_app_id": null,
        "offiaccount_app_secret": null,
        "miniprogram_close": null,
        "miniprogram_app_id": null,
        "miniprogram_app_secret": null,
        "oplatform_close": null,
        "oplatform_app_id": null,
        "oplatform_app_secret": null
      },
      "paycenter": {
        "wxpay_close": "1",
        "mch_id": "mch_id",
        "app_id": "app_id",
        "api_key": "api_key",
        "app_secret": null,
        "wxpay_mch_id": null,
        "wxpay_app_id": null,
        "wxpay_api_key": null,
        "wxpay_app_secret": null
      },
      "set_attach": {
        "support_img_ext": "jpg,png,gif,jpeg",
        "support_file_ext": "doc,docx,pdf,zip,jpg,png,gif,jpeg",
        "support_max_size": "5"
      },
      "qcloud": {
        "qcloud_close": false,
        "qcloud_app_id": null,
        "qcloud_secret_id": "qcloud_secret_id",
        "qcloud_secret_key": "qcloud_secret_key",
        "qcloud_token": null,
        "qcloud_cms_image": false,
        "qcloud_cms_text": false,
        "qcloud_sms_app_id": "qcloud_sms_app_id",
        "qcloud_sms_app_key": "qcloud_sms_app_key",
        "qcloud_sms_template_id": "451143",
        "qcloud_sms_sign": "zixunicom"
      },
      "set_cash": {
        "cash_interval_time": "1",
        "cash_rate": "100",
        "cash_min_sum": "50",
        "cash_max_sum": "50",
        "cash_sum_limit": "50"
      },
      "other": {
        "count_threads": 286,
        "count_users": 137,
        "can_upload_attachments": true,
        "can_upload_images": true,
        "can_create_thread": true,
        "can_view_threads": true,
        "can_batch_edit_threads": true,
        "can_viewUser_list": true,
        "can_editUser_group": true,
        "can_create_invite": true
      },
      "user": {
        "groups": [
          {
            "id": 1,
            "name": "管理员",
            "type": "",
            "color": "",
            "icon": "",
            "default": 0,
            "pivot": {
              "user_id": 1,
              "group_id": 1
            }
          },
          {
            "id": 10,
            "name": "普通会员",
            "type": "",
            "color": "",
            "icon": "",
            "default": 1,
            "pivot": {
              "user_id": 1,
              "group_id": 10
            }
          }
        ],
        "register_time": "2019-12-25T17:22:52+08:00"
      }
    }
  }
}
```
