### 公众号获取单条回复

- **接口说明：** 公众号获取回复[单条]
- **接口地址：** /api/offiaccount/reply/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 名称 | 描述 | 是否必须 | 传输类型 |
| :--- | :--- | :--- | :--- | :--- | :--- |
| query/{id}  | int  | 回复的自增ID | | 是 | query |

#### 请求示例

```
{{host}}/api/offiaccount/reply/6
```

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述 |
| :--- | :--- | :--- | :--- |
| name | string | 规则名 |  |
| keyword | string | 关键词 |  |
| match_type | int | 匹配规则 | 0全匹配1半匹配 |
| reply_type | int | 消息回复类型 | 1文本2图片3语音4视频5图文 |
| content | string | 回复文本内容 |  |
| media_id | string | 素材ID |  |
| media_type | int | 素材类型 | 1图片2视频3语音4图文 |
| type | int | 数据类型 | 0被关注回复1消息回复2关键词回复 |
| status | int | 是否开启 | 0关闭1开启 |
| created_at | string | 创建时间 |  |
| updated_at | string | 更新时间 |  |

#### 返回说明

- 成功，http 状态码 200
- 失败，http 状态码 500

#### 返回示例

```json
{
    "data": {
        "type": "offiaccount_reply",
        "id": "2",
        "attributes": {
            "name": "Kory",
            "keyword": "Katrina3",
            "match_type": 1,
            "reply_type": 4,
            "media_id": "ryUzCNaipU-iHQGbMFvy4uPewhXeiePQtQ5SBXz0aiE",
            "media_type": 1,
            "type": 2,
            "updated_at": "2020-06-16T17:19:56+08:00",
            "created_at": "2020-06-16T17:19:56+08:00"
        }
    }
}
```
