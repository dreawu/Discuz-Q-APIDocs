### 公众号素材管理上传接口

- **接口说明：** 公众号素材管理上传接口
- **接口地址：** /api/offiaccount/asset
- **请求方式：** POST

#### 请求参数

- get参数:

`{{host}}/api/offiaccount/asset?filter[type]=video`

- body参数:

| 参数名称 | 类型 | 名称 | 描述 | 是否<br>必须 | 传输类型 | 关联字段 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| filter[type]  | string  | 素材的类型 | 图片（image）<br>视频（video）<br>语音（voice）<br>图文（news）<br>缩略图（thumbnail） | 是 | query | |
| file  | file  | 文件  | 图片/音乐/视频 文件  | 否  | body | 如果是视频多传两个参数：video_title、video_info |
| video_title  | string  | 视频标题  | 在file等于视频时传输  | 否  | body  | file  |
| video_info  | string  | 视频介绍  | 在file等于视频时传输  | 否  | body  | file  |
| media_id  | string  | 图文封面图  | 图文类型时传  | 是  | body  | news  |
| **news**  | json  |   |   |   | body  |   |
| news.title  | string  | 标题  | 图文类型时传  | 是  | json  | news  |
| news.thumb_media_id  | string  | 封面图  | 图文类型时传  | 是  | json  | news  |
| news.content  | string  | 内容  | 图文类型时传  | 是  | json  | news  |
| news.authod  | string  | 作者  | 图文类型时传  | 否  | json  | news  |
| news.digest  | string  | 摘要  | 图文类型时传  | 否  | json  | news  |
| news.show_cover  | int  | 是否显示封面  | 0为false，即不显示，1为true，即显示  | 否  | json  | news  |
| news.need_open_comment  | int  | 是否打开评论  | 0不打开，1打开  | 否  | json  | news  |
| news.only_fans_can_comment  | int  | 是否粉丝才可评论  | 0所有人可评论，1粉丝才可评论  | 否  | json  | news  |

#### 上传图文卡片消息

[上传图文消息素材 - 微信接口文档地址](https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#1)

在图文消息中插入 小程序卡片/卡卷/图片等

- 小程序卡片跳转小程序，代码示例：
```html
<mp-miniprogram data-miniprogram-appid="wx123123123" data-miniprogram-path="pages/index/index" data-miniprogram-title="小程序示例" data-progarm-imageurl="http://mmbizqbic.cn/demo.jpg"></mp-miniprogram>
```
> 注意：纠错微信Demo代码 (插入卡片小程序却是一直报错errcode=45166, errmsg = invalid content hint)
>> 1. `data-progarm-imageurl` 改为 `data-miniprogram-imageurl`
>> 2. `appid`如果不对或者不写、图片的`image`和卡片的`data-progarm-imageurl`参数写错了或不写都会报系统异常的错误

- 文字跳转小程序，代码示例：
```html
<p><a data-miniprogram-appid="wx123123123" data-miniprogram-path="pages/index" href="">点击文字跳转小程序</a></p>
```

- 图片跳转小程序，代码示例：
```html
<p><a data-miniprogram-appid="wx123123123" data-miniprogram-path="pages/index" href=""><img src="https://mmbiz.qpic.cn/mmbiz_jpg/demo/0?wx_fmt=jpg" alt="" data-width="null" data-ratio="NaN"></a></p>
```

#### 请求示例

`form-data` 格式按照请求参数规则传输

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :--- | :--- | :--- |
| media_id  | string  | 成功返回素材ID  |
| errcode  | int  | 0成功，其余都是失败。（微信返回的数据）  |
| errmsg  | string  | 成功返回“ok”  |

#### 返回说明

>注意：不能依据状态码判断，要根据 `errcode` != 0 去判断

#### 返回示例

- 成功，http 状态码 200

```json
{
    "data": {
        "type": "offiaccount_asset",
        "id": "1",
        "attributes": {
            "media_id": "ryUzCNaipU-iHQGbMFvy4jilfgFuC7JC9-1qBujn9s4",
            "item": []
        }
    }
}
```

- 失败，http 状态码 500

```json
{
    "data": {
        "type": "offiaccount_asset",
        "id": "1",
        "attributes": {
            "errcode": 40007,
            "errmsg": "invalid media_id hint: [1K9QiA0017d234]"
        }
    }
}
```
