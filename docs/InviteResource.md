### 邀请码（单条）

- **接口说明：** 查询邀请码（单条）
- **接口地址：** /api/invite/{code}
- **请求方式：** GET

#### 请求参数

| 字段名 | 必填 | 类型   | 描述         |
| :----- | :--- | :----- | :----------- |
| code   | 是   | string | 邀请码字符串 |

#### 请求示例

`{{host}}/api/invite/B84DQBWJm9R4TUi9d8clfJKNuefzV034`

#### 返回说明

- 返回数据， http 状态码： 200

#### 返回结果

| 参数名称              | 类型   | 出现要求 | 描述                                               |
| :-------------------- | :----- | :------- | :------------------------------------------------- |
| type                  | string |          | 数据模型的类型                                     |
| id                    | int    |          |  id                                            |
| attributes            | object |          | 数据模型的属性                                     |
| attributes.group_id   | int    |          | 用户组 id                                          |
| attributes.code       | string |          | 邀请码                                             |
| attributes.dateline   | int    |          | 生效时间                                           |
| attributes.endtime    | int    |          | 失效时间                                           |
| attributes.user_id    | int    |          | 邀请人 id                                          |
| attributes.to_user_id | int    |          | 被邀请人 id                                        |
| attributes.status     | int    |          | 状态<br>0 失效<br>1 未使用<br>2 已使用<br>3 已过期 |

#### 返回示例

```json
{
  "data": {
    "type": "invite",
    "id": "39",
    "attributes": {
      "group_id": 10,
      "type": 2,
      "code": "B84DQBWJm9R4TUi9d8clfJKNuefzV034",
      "dateline": 1578400713,
      "endtime": 1579005513,
      "user_id": 7,
      "to_user_id": 0,
      "status": 1
    },
    "relationships": {
      "group": {
        "data": {
          "type": "groups",
          "id": "10"
        }
      }
    }
  },
  "included": [
    {
      "type": "groups",
      "id": "10",
      "attributes": {
        "name": "普通会员",
        "type": "",
        "color": "",
        "icon": "",
        "default": true
      },
      "relationships": {
        "permission": {
          "data": [
            {
              "type": "permissions",
              "id": "fa4e5911"
            },
            {
              "type": "permissions",
              "id": "8d496987"
            },
            {
              "type": "permissions",
              "id": "13952a0d"
            },
            {
              "type": "permissions",
              "id": "9d88dcf4"
            },
            {
              "type": "permissions",
              "id": "d1dccdee"
            },
            {
              "type": "permissions",
              "id": "75c885d7"
            },
            {
              "type": "permissions",
              "id": "ac9cf0fa"
            },
            {
              "type": "permissions",
              "id": "520795f5"
            },
            {
              "type": "permissions",
              "id": "121bb7c3"
            },
            {
              "type": "permissions",
              "id": "77e9747b"
            },
            {
              "type": "permissions",
              "id": "7d2a4ec3"
            }
          ]
        }
      }
    },
    {
      "type": "permissions",
      "id": "fa4e5911",
      "attributes": {
        "group_id": 10,
        "permission": "attachment.create.0"
      }
    },
    {
      "type": "permissions",
      "id": "8d496987",
      "attributes": {
        "group_id": 10,
        "permission": "attachment.create.1"
      }
    },
    {
      "type": "permissions",
      "id": "13952a0d",
      "attributes": {
        "group_id": 10,
        "permission": "createThread"
      }
    },
    {
      "type": "permissions",
      "id": "9d88dcf4",
      "attributes": {
        "group_id": 10,
        "permission": "order.create"
      }
    },
    {
      "type": "permissions",
      "id": "d1dccdee",
      "attributes": {
        "group_id": 10,
        "permission": "thread.favorite"
      }
    },
    {
      "type": "permissions",
      "id": "75c885d7",
      "attributes": {
        "group_id": 10,
        "permission": "thread.likePosts"
      }
    },
    {
      "type": "permissions",
      "id": "ac9cf0fa",
      "attributes": {
        "group_id": 10,
        "permission": "thread.reply"
      }
    },
    {
      "type": "permissions",
      "id": "520795f5",
      "attributes": {
        "group_id": 10,
        "permission": "thread.viewPosts"
      }
    },
    {
      "type": "permissions",
      "id": "121bb7c3",
      "attributes": {
        "group_id": 10,
        "permission": "trade.pay.order"
      }
    },
    {
      "type": "permissions",
      "id": "77e9747b",
      "attributes": {
        "group_id": 10,
        "permission": "viewThreads"
      }
    },
    {
      "type": "permissions",
      "id": "7d2a4ec3",
      "attributes": {
        "group_id": 10,
        "permission": "viewUserList"
      }
    }
  ]
}
```
