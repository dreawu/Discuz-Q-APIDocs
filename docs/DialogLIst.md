### 获取会话列表

- **接口说明：** 获取会话列表
- **接口地址：** /api/dialog
- **请求方式：** GET

#### 请求参数

| 参数名称     | 类型   | 是否必须 | 描述     |
| :----------- | :----- | :------: | :------- |
| include      | string |    否    | 关联数据 |
| page[number] | int    |    否    | 页码     |
| page[limit]  | int    |    否    | 单页数量 |
| sort         | string |    否    | 默认按照创建时间倒序，可选值：最新消息编号dialogMessageId、创建时间createdAt。添加标记‘-’为倒序，如：‘-dialogMessageId’； |

#### include 可关联的数据

| 关联名称         | 模型           | 类型   | 是否默认 | 描述         |
| :--------------- | :------------- | :----- | :------: | :----------- |
| sender           | users          | object |    否    | 发送人       |
| recipient        | users          | object |    否    | 收信人       |
| sender.groups    | groups         | object |    否    | 发送人用户组 |
| recipient.groups | groups         | object |    否    | 收信人用户组 |
| dialogMessage    | dialog_message | object |    否    | 最新消息     |

#### 请求示例

```
/api/dialog?include=sender,recipient,dialogMessage&page[number]=1&page[limit]=10
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 字段名                 | 变量名                       | 必填 | 类型     | 描述                       |
| :--------------------- | :--------------------------- | :--- | :------- | :------------------------- |
| **links**              | object                       | 是   | object   | 接口链接                   |
| **data.type**          | data.type                    | 是   | string   | 固定值 dialog              |
| **data.id**            | data.id                      | 是   | int      | 会话 id                    |
| **data.attributes**    | object                       | 是   | object   | 数据属性                   |  |
| 最新会话消息 ID        | attribute. dialog_message_id | 是   | int      | 最新会话消息 ID            |
| 发送人 uid             | attribute. sender_user_id    | 是   | int      | 发送人 uid                 |
| 收信人 uid             | attribute. recipient_user_id | 是   | int      | 收信人 uid                 |
| 发送人阅读时间         | attribute. sender_read_at    | 是   | datetime | 发送人阅读时间,null 时未读 |
| 收信人阅读时间         | attribute. recipient_read_at | 是   | datetime | 收信人阅读时间,null 时未读 |
| 更新时间               | attributes.updated_at        | 是   | datetime | 更新时间                   |
| 创建时间               | attributes.created_at        | 是   | datetime | 创建时间                   |
| **data.relationships** | object                       | 否   | object   | 关联关系                   |
| **included**           | object                       | 否   | object   | 关联数据                   |

#### 返回示例

```json
{
  "links": {
    "first": "DummySiteUrl/api/dialog?include=sender%2Crecipient%2CdialogMessage",
    "last": "DummySiteUrl/api/dialog?include=sender%2Crecipient%2CdialogMessage"
  },
  "data": [
    {
      "type": "dialog",
      "id": "1",
      "attributes": {
        "dialog_message_id": 1,
        "sender_user_id": 1,
        "recipient_user_id": 2,
        "sender_read_at": null,
        "recipient_read_at": "2020-04-21T11:27:24+08:00",
        "updated_at": "2020-02-17T10:14:28+08:00",
        "created_at": "2020-02-17T10:14:31+08:00"
      },
      "relationships": {
        "sender": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "recipient": {
          "data": {
            "type": "users",
            "id": "2"
          }
        },
        "dialogMessage": {
          "data": {
            "type": "dialog_message",
            "id": "1"
          }
        }
      }
    },
    {
      "type": "dialog",
      "id": "2",
      "attributes": {
        "dialog_message_id": 2,
        "sender_user_id": 1,
        "recipient_user_id": 3,
        "sender_read_at": null,
        "recipient_read_at": "2020-04-21T11:27:24+08:00",
        "updated_at": "2020-02-17T10:14:43+08:00",
        "created_at": "2020-02-17T10:14:46+08:00"
      },
      "relationships": {
        "sender": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "recipient": {
          "data": {
            "type": "users",
            "id": "3"
          }
        },
        "dialogMessage": {
          "data": {
            "type": "dialog_message",
            "id": "2"
          }
        }
      }
    },
    {
      "type": "dialog",
      "id": "3",
      "attributes": {
        "dialog_message_id": 0,
        "sender_user_id": 1,
        "recipient_user_id": 4,
        "sender_read_at": null,
        "recipient_read_at": "2020-04-21T11:27:24+08:00",
        "updated_at": "2020-02-17T10:14:56+08:00",
        "created_at": "2020-02-17T10:14:57+08:00"
      },
      "relationships": {
        "sender": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "recipient": {
          "data": {
            "type": "users",
            "id": "4"
          }
        }
      }
    },
    {
      "type": "dialog",
      "id": "5",
      "attributes": {
        "dialog_message_id": 0,
        "sender_user_id": 1,
        "recipient_user_id": 5,
        "sender_read_at": null,
        "recipient_read_at": "2020-04-21T11:27:24+08:00",
        "updated_at": "2020-02-17T11:17:08+08:00",
        "created_at": "2020-02-17T11:17:08+08:00"
      },
      "relationships": {
        "sender": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "recipient": {
          "data": {
            "type": "users",
            "id": "5"
          }
        }
      }
    },
    {
      "type": "dialog",
      "id": "9",
      "attributes": {
        "dialog_message_id": 0,
        "sender_user_id": 1,
        "recipient_user_id": 6,
        "sender_read_at": null,
        "recipient_read_at": "2020-04-21T11:27:24+08:00",
        "updated_at": "2020-02-17T18:12:45+08:00",
        "created_at": "2020-02-17T18:12:45+08:00"
      },
      "relationships": {
        "sender": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "recipient": {
          "data": {
            "type": "users",
            "id": "6"
          }
        }
      }
    },
    {
      "type": "dialog",
      "id": "10",
      "attributes": {
        "dialog_message_id": 0,
        "sender_user_id": 1,
        "recipient_user_id": 8,
        "sender_read_at": null,
        "recipient_read_at": "2020-04-21T11:27:24+08:00",
        "updated_at": "2020-02-17T18:13:18+08:00",
        "created_at": "2020-02-17T18:13:18+08:00"
      },
      "relationships": {
        "sender": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "recipient": {
          "data": {
            "type": "users",
            "id": "8"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "username",
        "mobile": "",
        "avatarUrl": "DummySiteUrl/storage/avatars/1.png?1578395431",
        "threadCount": 0,
        "followCount": 4,
        "fansCount": 2,
        "follow": null,
        "status": 0,
        "loginAt": "2020-02-18T15:17:13+08:00",
        "joinedAt": "2019-12-19T13:51:19+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-19T13:51:20+08:00",
        "updatedAt": "2020-02-18T15:17:13+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "2",
      "attributes": {
        "id": 2,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 1,
        "fansCount": 1,
        "follow": null,
        "status": 0,
        "loginAt": "2020-02-06T19:48:34+08:00",
        "joinedAt": "2019-12-20T03:48:09+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-20T03:48:09+08:00",
        "updatedAt": "2020-02-06T19:52:14+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "3",
      "attributes": {
        "id": 3,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-20T03:53:54+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-20T03:53:55+08:00",
        "updatedAt": "2019-12-20T03:53:55+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "4",
      "attributes": {
        "id": 4,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-19T15:33:23+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-19T15:33:23+08:00",
        "updatedAt": "2020-01-02T16:37:51+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "5",
      "attributes": {
        "id": 5,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 1,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-27T18:19:21+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-27T18:19:21+08:00",
        "updatedAt": "2020-01-20T15:53:11+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "6",
      "attributes": {
        "id": 6,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-27T18:21:39+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-27T18:21:39+08:00",
        "updatedAt": "2020-01-20T16:25:50+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "8",
      "attributes": {
        "id": 8,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-31T12:03:15+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-31T12:03:15+08:00",
        "updatedAt": "2019-12-31T12:03:15+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "dialog_message",
      "id": "1",
      "attributes": {
        "id": 1,
        "user_id": 2,
        "dialog_id": 1,
        "message_text": "11111",
        "updated_at": "2020-02-17T10:15:20+08:00",
        "created_at": "2020-02-17T10:15:22+08:00"
      }
    },
    {
      "type": "dialog_message",
      "id": "2",
      "attributes": {
        "id": 2,
        "user_id": 1,
        "dialog_id": 2,
        "message_text": "22222",
        "updated_at": "2020-02-17T10:15:43+08:00",
        "created_at": "2020-02-17T10:15:45+08:00"
      }
    }
  ],
  "meta": {
    "total": 6,
    "size": 20
  }
}
```
