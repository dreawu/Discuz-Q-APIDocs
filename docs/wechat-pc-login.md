### PC扫码登陆-轮询查询接口

- **接口说明：** PC扫码登陆-轮询查询接口
- **接口地址：** /api/oauth/wechat/pc/login/{session_token}
- **请求方式：** Get

#### 请求示例

`{{host}}/api/oauth/wechat/pc/login/SFTzUKoJUvCnIj2xRKjqM9B6JGwsI`

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| pc_login  | bool  | 登陆状态  | true扫码登陆成功，false反之  |
| access_token | string  | token值  | 登录态  |

#### 返回示例

```json
{
    "data": {
        "type": "token",
        "id": "1",
        "attributes": {
            "token_type": "Bearer",
            "expires_in": 2592000,
            "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIiLCJqdGkiOiIyNTY1YTA0ODdmMGJlM2QwMGIwYTQwZwTiA... ...",
            "refresh_token": "def502007fd8dba9c87cc7a430d465c1cd25bbbfa599730ebe69f282cd9e39436a9d8664f202f3cfef864772071... ...",
            "pc_login": true
        }
    }
}
```
