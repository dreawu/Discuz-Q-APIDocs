### 创建问答的回答接口

- **接口说明：** 创建问答的回答
- **接口地址：** /api/questions/{question_id}/answer
- **请求方式：** POST

#### 请求参数

| 参数名称                  |  类型  | 是否必须 | 描述                                                      |
| :------------------------ | :----: | :------: | :-------------------------------------------------------- |
| content                   | string |    是    | 内容（发布帖子时支持 Markdown 语法）                      |
| type                      |  int   |    是    | 文章类型（0:文字 1:帖子 2:视频 3:图片 4:语音 5:问答 6:商品）    |
| relationships.attachments | object |    否    | 主题图片、附件关联关系                                    |

#### 请求示例

```json
{
    "data": {
        "type": "answer",
        "attributes": {
            "content": "回答内容 {{$randomWords}} %%% {{$randomColor}} %%% {{$randomWords}}",
            "type": 5
        },
        "relationships": {
            "attachments": {
                "data": [
                    {
                        "type": "attachments",
                        "id": 12
                    },
                    {
                        "type": "attachments",
                        "id": 13
                    }
                ]
            }
        }
    }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| **data** | object  |   |  数据类型
| type     | string  |   |  数据类型
| id       | int     |   |  数据 id
**attributes**  | object |
| thread_id             | int  | 主题ID  |  被举报的主题ID
| user_id               | int  | 用户ID  |  提问人的用户ID
| be_user_id            | int  | 用户ID  |  回答人的用户ID
| content               | string   | 内容        |
| content_html          | string   | html 内容   |
| ip                    | string  | IP  | 回答人的IP |
| port                  | int  | 端口  | 回答人的端口 |
| price                 | float  | 问答单价  |
| onlooker_unit_price   | float  | 围观单价  |
| onlooker_price        | float  | 围观总价格  |
| onlooker_number       | int  | 围观总人数  |
| is_onlooker           | bool  | 是否允许围观  | true允许 false不允许 |
| is_answer             | int  | 是否已回答  | 0未回答 1已回答 2已过期  |
| ~~is_approved~~       | int  | 是否合法(暂未使用)  | 0不合法 1合法 2忽略  |
| created_at            | string  | 创建时间  |
| updated_at            | string  | 修改时间  |
| expired_at            | string  | 过期时间  |

#### 返回示例

- 正确返回格式

  ```json
  {
      "data": {
          "type": "question_answer",
          "id": "1",
          "attributes": {
              "thread_id": 192,
              "user_id": 4,
              "be_user_id": 1,
              "content": "回答内容 invoice pink %%% lime %%% Chair Plaza override",
              "content_html": "回答内容 invoice pink %%% lime %%% Chair Plaza override",
              "ip": "127.0.0.1",
              "port": 62449,
              "price": "10.00",
              "onlooker_unit_price": "5.00",
              "onlooker_price": "0.00",
              "onlooker_number": 0,
              "is_onlooker": true,
              "is_answer": 1,
              "is_approved": true,
              "created_at": "2020-09-07T16:16:55+08:00",
              "updated_at": "2020-09-09T11:13:31+08:00",
              "expired_at": "2020-09-14T00:00:00+08:00"
          }
      }
  }
  ```

- 错误触发敏感词返回格式

  ```json
  {
      "errors": [
          {
              "status": 500,
              "code": "content_banned",
              "detail": [
                  "垃圾",
                  "菜"
              ]
          }
      ]
  }
  ```
