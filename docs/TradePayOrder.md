### 支付订单

- **接口说明：** 支付订单
- **接口地址：** /api/trade/pay/order/{order_sn}
- **请求方式：** POST

#### 请求参数

| 参数名称     |  类型  | 是否必须 | 描述                                                                                                   |
| :----------- | :----: | :------: | :----------------------------------------------------------------------------------------------------- |
| order_sn     | string |    是    | 订单号                                                                                                 |
| payment_type |  int   |    是    | 支付方式：<br>10 微信扫码支付<br>11 微信 h5 支付<br>12 微信网页、公众号 <br>13 小程序支付<br>20 钱包支付 |
| pay_password | string |    否    | 支付密码（6 位数字，钱包支付时必须）                                                                   |

#### 请求示例

```json
{
	"data": {
		"attributes":{
    		"payment_type": 20,
    		"pay_password": "123456"
		}
	}
}
```

#### 返回说明

- 成功，http 状态码 200
- 失败，http 状态码非 200

#### 返回结果

| 参数名称           | 类型   | 出现要求     | 描述                                          |
| :----------------- | :----- | :----------- | :-------------------------------------------- |
| **links**          | object |              | 接口链接                                      |
| **data**           | object |              | 基础数据                                      |
| type               | string |              | 数据类型                                      |
| id                 | int    |              | 数据 id                                       |
| **attributes**     | object |              | 数据属性                                      |
| wechat_qrcode      | string | 微信扫码支付   | 经过 Base64 编码的二维码 （支付方式：10）                     |
| wechat_h5_link     | string | 微信 h5 支付 | h5 支付链接 （支付方式：11）                                 |
| wechat_js          | string | 微信内支付   | 微信内支付参数 （支付方式：12、13）                 |
| wallet_pay         | object | 钱包支付     | 钱包支付直接返回支付结果                      |
| wallet_pay.result  | string | 钱包支付     | 钱包支付结果：success 支付成功，fail 支付失败 |
| wallet_pay.message | string | 钱包支付     | 钱包支付提示语                                |

#### 返回示例

微信二维码支付

```json
{
  "data": {
    "type": "payorder",
    "id": "8",
    "attributes": {
      "wechat_qrcode": "xxx"
    }
  }
}
```

钱包支付成功

```json
{
  "data": {
    "type": "payorder",
    "id": "43",
    "attributes": {
      "wallet_pay": {
        "result": "success",
        "message": "支付成功。"
      }
    }
  }
}
```

钱包支付失败

```json
{
  "data": {
    "type": "payorder",
    "id": "43",
    "attributes": {
      "wallet_pay": {
        "result": "failed",
        "message": "钱包可用金额不足"
      }
    }
  }
}
```
