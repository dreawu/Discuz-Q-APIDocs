### 查询回复接口[列表]

- **接口说明：** 查询回复[列表]
- **接口地址：** /api/posts
- **请求方式：** GET

#### 请求参数

| 参数名称                |  类型  | 是否必须 | 描述                                              |
| :---------------------- | :----: | :------: | :------------------------------------------------ |
| include                 | string |    否    | 关联数据                                          |
| filter[q]               | string |    否    | 关键词                                            |
| filter[userId]          |  int   |    否    | 作者 ID                                           |
| filter[username]        | string |    否    | 作者用户名                                        |
| filter[deletedUserId]   |  int   |    否    | 操作删除者 ID                                     |
| filter[deletedUsername] | string |    否    | 操作删除者用户名                                  |
| filter[createdAtBegin]  | string |    否    | 发表于（开始时间）                                |
| filter[createdAtEnd]    | string |    否    | 发表于（结束时间）                                |
| filter[deletedAtBegin]  | string |    否    | 删除于（开始时间）                                |
| filter[deletedAtEnd]    | string |    否    | 删除于（结束时间）                                |
| filter[categoryId]      |  int   |    否    | 分类 ID                                           |
| filter[thread]          |  int   |    否    | 主题 ID                                           |
| filter[reply]           |  int   |    否    | 回复 ID                                           |
| filter[isApproved]      | string |    否    | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| filter[isDeleted]       | string |    否    | 是否删除（yes/no）                                |
| filter[isComment]       | string |    否    | 是否是回复的评论（yes/no）                        |
| filter[highlight]       | string |    否    | 是否高亮敏感词（yes/no）                          |

#### include 可关联的数据

| 关联名称                    | 模型           | 类型   | 是否默认 | 描述                     |
| :-------------------------- | :------------- | :----- | :------: | :----------------------- |
| user                        | users          | object |    是    | 发表用户                 |
| replyUser                   | users          | object |    是    | 所回复的用户             |
| commentUser                 | users          | object |    是    | 楼中楼关联的用户信息      |
| images                      | attachments    | array  |    是    | 回复下的图片             |
| thread                      | threads        | object |    是    | 所属主题                 |
| user.groups                 | groups         | object |    否    | 用户所在群组             |
| thread.category             | categories     | object |    否    | 所属主题的分类           |
| thread.firstPost            | posts          | object |    否    | 所属主题的首贴           |
| lastThreeComments           | posts          | object |    否    | 最后三条回复             |
| lastThreeComments.user      | posts          | object |    否    | 最后三条回复的作者       |
| lastThreeComments.replyUser | posts          | object |    否    | 最后三条回复被回复的用户 |
| lastThreeComments.commentUser| posts         | object |    否    | 最后三条楼中楼用户      |
| lastThreeComments.images    | attachments    | object |    否    | 最后三条回复的图片       |
| deletedUser                 | users          | object |    否    | 删除用户                 |
| lastDeletedLog              | operation-logs | object |    否    | 最后一次被删除的操作日志 |

#### 请求示例

```
// 用户 ID
/api/posts?filter[userId]=1

// 主题 ID
/api/posts?filter[thread]=1

// 回复 ID
/api/posts?filter[reply]=1

// 查看待审核 filter[isApproved]=0
/api/posts?filter[isApproved]=0

// 查看回收站 filter[isDeleted]=yes
/api/posts?filter[isDeleted]=yes
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称          | 类型      | 出现要求   | 描述                                              |
| :---------------- | :-------- | :--------- | :------------------------------------------------ |
| **links**         | object    |            | 接口链接                                          |
| **data**          | object    |            | 基础数据                                          |
| type              | string    |            | 数据类型                                          |
| id                | int       |            | 数据 id                                           |
| **attributes**    | object    |            | 数据属性                                          |
| replyUserId       | int\|null |            | 所回复的用户 ID                                   |
| summary           | string    |            | 摘要                                              |
| content           | string    |            | 内容                                              |
| contentHtml       | string    |            | html 内容                                         |
| ip                | string    |            | 发布 ip                                           |
| replyCount        | int       |            | 回复数                                            |
| likeCount         | int       |            | 喜欢数                                            |
| createdAt         | datetime  |            | 创建时间                                          |
| updatedAt         | datetime  |            | 修改时间                                          |
| deletedAt         | datetime  | 在回收站时 | 删除时间                                          |
| isFirst           | bool      |            | 是否首帖                                          |
| isComment         | bool      |            | 是否是评论                                        |
| isApproved        | bool      |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isLiked           | bool      |            | 是否喜欢                                          |
| canEdit           | bool      |            | 是否有权编辑                                      |
| canApprove        | bool      |            | 是否有权审核                                      |
| canDelete         | bool      |            | 是否有权永久删除                                  |
| canHide           | bool      |            | 是否有权放入回收站                                |
| canLike           | bool      |            | 是否有权点赞                                      |
| isLiked           | bool      |            | 是否喜欢                                          |
| rewards           | int       |            | 评论获取到的悬赏金额，0为没有                     |
| redPacketAmount   | int       |            | 用户评论获得的红包总金额                          |
| **relationships** | object    |            | 关联关系                                          |
| **included**      | object    |            | 关联数据                                          |

#### 返回示例

```json
{
  "data": [
    {
      "type": "Posts",
      "id": "1",
      "attributes": {
        "content": "<span>asdaasd</span><i>hahahahh</i> :ku: Idaho Yuan Renminbi Gorgeous Soft <span class=\"highlight\">Car</span> eco-centric == purple == neural Orchard Computer fuchsia optimizing",
        "contentHtml": "<span>asdaasd</span>&lt;i&gt;hahahahh&lt;/i&gt; <img src=\"emoji/qq/ku.gif\" alt=\":ku:\"> Idaho Yuan Renminbi Gorgeous Soft <span class=\"highlight\">Car</span> eco-centric == purple == neural Orchard Computer fuchsia optimizing",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:23+08:00",
        "updatedAt": "2019-11-12T17:10:23+08:00",
        "isFirst": true,
        "isApproved": true,
        "isLiked": false
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    },
    {
      "type": "Posts",
      "id": "2",
      "attributes": {
        "content": "Berkshire Legacy Public-key Bedfordshire Auto Loan Account == lime == Group incentivize exploit backing up port",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:27+08:00",
        "updatedAt": "2019-11-12T17:10:27+08:00",
        "isFirst": true,
        "isApproved": true,
        "isLiked": false,
		"rewards": 0,
        "redPacketAmount": 1,
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "nickname": null,
        "mobile": "",
        "unionId": "",
        "lastLoginIp": "127.0.0.1",
        "createdAt": "2019-10-11T00:00:00+08:00",
        "updatedAt": "2019-11-11T14:51:34+08:00"
      }
    }
  ]
}
```
