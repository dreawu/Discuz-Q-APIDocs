### 创建关注关系接口[单条]

- **接口说明：** 创建关注关系[单条]
- **接口地址：** /api/follow
- **请求方式：** POST

#### 请求参数

| 参数名称               |  类型  | 是否必须 | 描述                         |
| :--------------------- | :----: | :------: | :--------------------------- |
| **data**               | object |    是    | 基础数据                     |
| type                   | string |    是    | 数据类型，固定值 user_follow |
| **attributes**         | object |    是    | 数据属性                     |
| attributes. to_user_id | string |    是    | 被关注人 uid                 |

#### 请求示例

```json
{
  "data": {
    "type": "user_follow",
    "attributes": {
      "to_user_id": "2"
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

> 关联数据模型字段释义请参见相应文档

| 参数名称                | 类型     | 出现要求 | 描述                |
| :---------------------- | :------- | :------- | :------------------ |
| **data**                | object   |          | 基础数据            |
| type                    | string   |          | 数据类型            |
| id                      | int      |          | 数据 id             |
| **attributes**          | object   |          | 数据属性            |
| attributes.id           | int      |          | 自增 ID             |
| attributes.from_user_id | int      |          | 关注人 Uid          |
| attributes.to_user_id   | int      |          | 被关注人 Uid        |
| attributes.is_mutual    | int      |          | 互相关注：0 否 1 是 |
| attributes.updated_at   | int      |          | 更新时间            |
| attributes.created_at   | datetime |          | 创建时间            |

#### 返回示例

```json
{
  "data": {
    "type": "user_follow",
    "id": "56",
    "attributes": {
      "id": 56,
      "from_user_id": 1,
      "to_user_id": 2,
      "is_mutual": 1,
      "updated_at": "2020-02-06T19:52:14+08:00",
      "created_at": "2020-02-06T19:52:14+08:00"
    }
  }
}
```
