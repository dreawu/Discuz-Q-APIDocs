### 删除话题接口[批量]

- **接口说明：** 删除会话接口，管理员可用。会同步删除话题主题关系
- **接口地址：** /api/topics/batch/{ids}
- **请求方式：** delete

#### 请求参数

| 参数名称 | 类型    | 描述      |
| :------- | :------ | :-------- |
| ids       | string  | 话题 id，用英文逗号","分隔 |

#### 请求示例
```
/api/topics/batch/1,2,3
```

#### 返回结果
```
data  被删除的分类列表
meta  出现异常的分类列表
```

#### 返回说明

- http 状态码 200

#### 返回示例


