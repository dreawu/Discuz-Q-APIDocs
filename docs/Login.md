### 用户登录接口

- **接口说明：** 用户登录接口
- **接口地址：** /api/login
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型    | 是否须 | 描述               |
| :------- | :------ | :----- | :----------------- |
| username | string | 是     | 用户名/手机号（支持手机号+密码的形式登陆）        |
| password | string | 是     | 密码               |
| token           | string |    否    | [微信公众号通过code参数换取token接口](wechat-user.md)返回的 token，用于登陆并绑定微信 |
| js_code  | string | 否     | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序登陆code，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/login/wx.login.html|
| iv       | string | 否     | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序用户数据数据加密算法的初始向量，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html|
| encryptedData    | string | 否     | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序用户包括敏感数据在内的完整用户信息的加密数据，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html |
| mobileToken | string | 否     |  手机号登录时未绑定用户返回的token，用于登陆并绑定手机号 [短信验证](sms-verify.md)|
| rebind      | int    | 否  | 重新绑定当前微信。  换绑：1，不换：0或者不传 |
#### 请求示例

```json
{
  "data": {
    "attributes": {
      "username": "username",
      "password": "password"
    }
  }
}
```

#### 返回结果

| 参数名称      | 类型   | 出现要求 | 描述         |
| :------------ | :----- | :------- | :----------- |
| token_type    | string | 登录成功 | token 类型   |
| expires_in    | int    | 登录成功 | 时间         |
| access_token  | token  | 登录成功 | 用户标识     |
| refresh_token | token  | 登录成功 | 刷新用户标识 |

#### 返回说明

- 登录成功， http 状态码： 200
- 登录失败， http 状态码： 400

#### 返回示例

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Qi......dj3H9CCSPib6MQtnaT6VNrw",
      "refresh_token": "def50200a26b6a9......10ccbf3c1694084c2d2d276"
    }
  }
}
```

#### 失败示例

```json
{
  "error": "invalid_grant",
  "error_description": "The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.",
  "hint": "",
  "message": "The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client."
}
```
