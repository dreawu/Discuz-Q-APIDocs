### 前端获取指定 Tag 配置接口

- **接口说明：** 前端获取指定 Tag 配置
- **接口地址：** /api/settings/{tags}
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型   | 描述           |
| :------- | :----- | :------------- |
| /拼接    | string | default,qcloud |

请求示例：

```
/api/settings/default,qcloud  多个的话逗号拼接
```

#### 返回结果

| 参数名称   | 类型               | 出现要求 | 描述           |
| :--------- | :----------------- | :------- | :------------- |
| attributes | tag 集合 key-value |          | key-value 形式 |

#### 返回说明

- 返回空， http 状态码： 204

#### 返回示例

```json
{
  "data": [
    {
      "type": "settings_tags",
      "id": "default",
      "attributes": {
        "allow_register": "1",
        "censor1": "true",
        "ldd": "asd",
        "password_length": "0",
        "password_strength": "0,1",
        "register_close": "1",
        "site_close": "0",
        "site_close_msg": "论坛正在维护，请稍后再试...",
        "site_name": "discuss"
      }
    },
    {
      "type": "settings_tags",
      "id": "qcloud",
      "attributes": {
        "qcloud_app_id": "12345678",
        "qcloud_secret_id": "ABCDEF",
        "qcloud_secret_key": "啦啦啦"
      }
    }
  ]
}
```
