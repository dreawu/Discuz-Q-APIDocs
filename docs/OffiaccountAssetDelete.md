### 公众号素材管理删除单条

- **接口说明：** 公众号素材管理删除[单条]
- **接口地址：** /api/offiaccount/asset/{media_id}
- **请求方式：** DELETE

#### 请求参数

| 参数名称 | 类型 | 名称 | 描述 | 是否必须 | 传输类型 |
| :--- | :--- | :--- | :--- | :--- | :--- |
| query/{media_id}  | string  | 永久素材ID | | 是 | query |

#### 请求示例

```
{{host}}/api/offiaccount/asset/ryUzCNaipU-iHQGbMFvydsadWDSA1Uf-cwhdNltI8
```

#### 返回说明

- 成功，http 状态码 200
- 服务器失败，http 状态码 500

#### 返回示例

- 成功 http 200

| 参数名称 | 类型 | 描述 |
| :--- | :--- | :--- |
| errcode  | int  | 0成功，其余都是失败。（微信返回的数据）  |
| errmsg  | string  | 成功返回“ok”  |

```json
{
    "errcode": 0,
    "errmsg": "ok"
}
```

- 失败 http 200

| 参数名称 | 类型 | 描述 |
| :--- | :--- | :--- |
| errcode  | int  | 微信返回数据格式  |
| errmsg  | string  | 微信返回数据错误内容  |

```json
{
    "errcode": 40007,
    "errmsg": "invalid media_id hint: [08712]"
}
```
