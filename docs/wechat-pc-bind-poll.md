### PC扫码绑定/换绑 - 轮询接口

- **接口说明：** PC扫码绑定/换绑 - 轮询接口
- **接口地址：** /api/oauth/wechat/pc/bind/{session_token}
- **请求方式：** GET

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述               |
| :-------- | :----- | :------: | :----------------- |
| session_token | string |    是    | 生成二维码的 token 值  |

#### 请求示例

```
{{host}}/api/oauth/wechat/pc/bind/{session_token}
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 500

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |
| bind  | bool  | true 成功，失败后直接抛出异常  |
| code  | string  | 绑定成功  |


#### 返回示例

绑定成功后

```json
{
    "bind": true,
    "code": "success_bind",
}
```

绑定失败异常

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |
| pc_qrcode_scanning_code  | code  | 扫码中  |
| pc_qrcode_time_out  | code  | 二维码已失效，扫码超时  |

```json
{
    "errors": [
        {
            "status": "500",
            "code": "pc_qrcode_time_out"
        }
    ]
}
```
