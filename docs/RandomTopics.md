### 获取随机话题接口[列表]

- **接口说明：** 获取随机话题接口[列表]
- **接口地址：** /random/topics
- **请求方式：** GET

#### 请求参数

#### 请求示例

```
/api/random/topics
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称           | 类型   | 出现要求 | 描述     |
| :----------------- | :----- | :------- | :------- |
| **data**           | object |          | 基础数据 |
| type               | string |          | 数据类型 |
| id                 | int    |          | 数据 id  |
| **attributes**     | object |          | 数据属性 |
| attributes.user_id | string |          | 话题作者 |
| attributes.content | string |          | 话题标题 |

#### 返回示例

```json
{
    "data": [
        {
            "type": "topics",
            "id": "1",
            "attributes": {
                "user_id": 4,
                "content": "话题1",
                "thread_count": 1,
                "view_count": 0,
                "recommended": 0,
                "updated_at": "2020-12-15T14:44:22+08:00",
                "created_at": "2020-12-15T14:44:22+08:00",
                "recommended_at": null
            }
        },
        {
            "type": "topics",
            "id": "2",
            "attributes": {
                "user_id": 4,
                "content": "话题2",
                "thread_count": 10,
                "view_count": 0,
                "recommended": 0,
                "updated_at": "2020-12-15T14:44:50+08:00",
                "created_at": "2020-12-15T14:44:50+08:00",
                "recommended_at": null
            }
        }
    ]
}
```

