### 查询帖子红包信息接口[单条]

- **接口说明：** 查询帖子红包信息接口[单条]
- **接口地址：** /api/redpacket/{id}
- **请求方式：** GET

#### 请求示例

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                | 类型     | 出现要求   | 描述                                              |
| :---------------------- | :------- | :--------- | :------------------------------------------------ |
| **data**                | object   |            | 基础数据                                          |
| type                    | string   |            | 数据类型                                          |
| id                      | int      |            | 数据 id                                           |
| **attributes**          | object   |            | 数据属性                                          |
| thread_id      | int       |          | 关联的threads主键ID              |
| post_id        | int       |          | 关联的posts主键ID                |
| rule | int | | 发放规则，0定额，1随机 |
| condition | int | | 领取红包条件，0回复，1集赞 |
| likenum | int | | 若红包领取条件为集赞，必填集赞数 |
| money | decimal | | 红包总金额 |
| number | int | | 红包个数 |
| remain_money | decimal | | 剩余红包总额 |
| remain_number | int | | 剩余红包个数 |
| status         | int       |          | 0:红包已过期,1:红包未过期，2:红包已领完，3:红包已退还，4:不处理的异常红包 |
| created_at     | timestamp |          | 创建时间                         |
| updated_at     | timestamp |          | 更新时间                         |

#### 返回示例

```json
{
    "data": {
        "type": "redpacket",
        "id": "7",
        "attributes": {
            "thread_id": 422,
            "post_id": 506,
            "rule": 1,
            "condition": 0,
            "likenum": 0,
            "money": "100.00",
            "number": 10,
            "remain_money": "83.83",
            "remain_number": 8,
            "status": 1,
            "created_at": "2021-01-30T17:21:02+08:00",
            "updated_at": "2021-01-30T17:24:17+08:00"
        }
    }
}
```

