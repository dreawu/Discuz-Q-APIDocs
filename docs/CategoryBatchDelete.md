### 删除主题接口[批量]

- **接口说明：** 删除主题[批量]
- **接口地址：** /api/categories/batch/{ids}
- **请求方式：** DELETE

#### 请求参数

#### 请求示例

```
/api/categories/batch/21,22,23
```

#### 返回说明

- http 状态码 200

#### 返回结果

```
data  被删除的分类列表
meta  出现异常的分类列表
```

#### 返回示例

```json
{
  "data": [
    {
      "type": "categories",
      "id": "1",
      "attributes": {
        "name": "UIC-Franc",
        "description": "Savings Account Principal maximized",
        "icon": "",
        "sort": 124,
        "property": 0,
        "thread_count": 0,
        "ip": "0",
        "created_at": "2019-10-21T00:00:00+08:00",
        "updated_at": "2019-11-30T11:08:14+08:00"
      }
    },
    {
      "type": "categories",
      "id": "2",
      "attributes": {
        "name": "circuit",
        "description": "Credit Card Account silver",
        "icon": "",
        "sort": 43,
        "property": 0,
        "thread_count": 0,
        "ip": "127.0.0.1",
        "created_at": "2019-11-22T10:32:21+08:00",
        "updated_at": "2019-12-02T20:14:40+08:00"
      }
    }
  ],
  "meta": [
    {
      "id": "3",
      "message": "model_not_found"
    }
  ]
}
```
