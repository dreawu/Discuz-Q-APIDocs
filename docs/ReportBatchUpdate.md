### 批量修改举报

- **接口说明：** 批量修改举报
- **接口地址：** /api/reports/batch
- **请求方式：** Patch

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
|:--|:--|:--|:--|:--
| status  | int  | 是 | 举报状态  | 要修改的举报状态 0未处理 1已处理

#### 请求示例

```json
{
    "data": [
        {
            "type": "report",
            "id": 1,
            "attributes": {
                "status": 1
            }
        },
        {
            "type": "report",
            "id": 2,
            "attributes": {
                "status": 1
            }
        }
    ]
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| **data** | object  |   |  数据类型
| data     | object  |   |  报错信息

#### 返回示例

```json
{
    "data": []
}
```
