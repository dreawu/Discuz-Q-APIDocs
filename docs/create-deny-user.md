### 拉黑某个用户

- **接口说明：** 拉黑某个用户
- **接口地址：** /api/users/{id}/deny
- **请求方式：** POST

#### 请求参数

```

```

#### 返回说明

- 返回当前创建成功数据， http 状态码： 201

#### 返回结果

#### 返回示例

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "id": 1,
      "username": "username",
      "mobile": "mobile",
      "avatarUrl": "",
      "threadCount": 34,
      "followCount": 0,
      "fansCount": 0,
      "follow": null,
      "status": 0,
      "loginAt": "2020-02-19T10:15:17+08:00",
      "joinedAt": "2019-12-16T19:41:17+08:00",
      "expiredAt": "2020-02-19T18:26:52+08:00",
      "createdAt": "2019-12-16T19:41:17+08:00",
      "updatedAt": "2020-02-19T10:15:17+08:00",
      "canEdit": true,
      "canDelete": true,
      "canWalletPay": false,
      "registerReason": "",
      "banReason": "",
      "originalMobile": "originalMobile",
      "registerIp": "192.168.10.1",
      "lastLoginIp": "192.168.10.1",
      "identity": "",
      "realname": "",
      "hasPassword": false,
      "walletBalance": "6.00",
      "paid": true,
      "payTime": null,
      "unreadNotifications": 8,
      "typeUnreadNotifications": {
        "replied": 8
      }
    }
  }
}
```
