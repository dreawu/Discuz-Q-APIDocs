### 获取所有用户组

- **接口说明：** 获取所有用户组
- **接口地址：** /api/groups
- **请求方式：** GET

#### 请求参数

| 参数名称          |  类型  | 是否必须 | 描述                                                       |
| :---------------- | :----: | :------: | :--------------------------------------------------------- |
| include           | string |    否    | 关联数据                                                   |
| filter[isDefault] | string |    否    | 是否只返回默认用户组                                       |
| filter[type]      | string |    否    | 传 invite 返回当前用户可见用户组（非管理员不返回游客、管理员用户组），前端用户组管理也可以使用次参数控制非管理员的展示内容 |
| filter[isPaid]      | int |    否    |  传递 0 返回非付费用户组， 传递 1 返回付费用户组。 |

#### include 可关联的数据

| 关联名称   | 模型       | 类型   | 是否默认 | 描述 |
| :--------- | :--------- | :----- | :------: | :--- |
| permission | permission | object |    是    | 权限 |

#### 请求示例

`/api/groups?filter[type]=invite`

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 参数名称           | 类型   | 出现要求 | 描述                                         |
| ------------------ | ------ | -------- | -------------------------------------------- |
| **data**           | object |          | 基础数据                                     |
| type               | string |          | 数据类型                                     |
| id                 | int    |          | 用户角色ID                                   |
| **attributes**     | object |          | 数据属性                                     |
| attributes.name    | string |          | 用户角色名称                                 |
| attributes.checked | int    |          | 用户角色是否被选为智能排序的条件，0不是，1是 |



#### 返回示例

```json
{
    "data": [
        {
            "type": "groups",
            "id": "1",
            "attributes": {
                "name": "管理员",
                "type": "",
                "color": "",
                "icon": "http://127.0.0.1/images/groups/group-1.svg",
                "default": false,
                "isDisplay": false,
                "isPaid": false,
                "fee": 0,
                "days": 0,
                "scale": 0,
                "is_subordinate": false,
                "is_commission": false,
                "checked": 0
            }
        },
        {
            "type": "groups",
            "id": "7",
            "attributes": {
                "name": "游客",
                "type": "",
                "color": "",
                "icon": "http://127.0.0.1/images/groups/group-7.svg",
                "default": false,
                "isDisplay": false,
                "isPaid": false,
                "fee": 0,
                "days": 0,
                "scale": 0,
                "is_subordinate": false,
                "is_commission": false,
                "checked": 0
            }
        },
        {
            "type": "groups",
            "id": "10",
            "attributes": {
                "name": "普通会员",
                "type": "",
                "color": "",
                "icon": "http://127.0.0.1/images/groups/group-10.svg",
                "default": true,
                "isDisplay": false,
                "isPaid": false,
                "fee": 0,
                "days": 0,
                "scale": 0,
                "is_subordinate": false,
                "is_commission": false,
                "checked": 0
            }
        }
    ]
}
```
