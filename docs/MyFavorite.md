### 查询我收藏的主题接口[列表]

- **接口说明：** 查询我收藏的主题[列表]
- **接口地址：** /api/favorites
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述     |
| :------- | :----: | :------: | :------- |
| include  | string |    否    | 关联数据 |

```
示例：
include=user,firstPost
```

#### include 可关联的数据

| 关联名称                 | 模型           | 类型   | 是否默认 | 描述                     |
| :----------------------- | :------------- | :----- | :------: | :----------------------- |
| user                     | users          | object |    是    | 发表用户                 |
| firstPost                | posts          | object |    是    | 首帖                     |
| threadVideo              | thread-video   | object |    是    | 视频                     |
| lastPostedUser           | users          | object |    是    | 最后回复用户             |
| category                 | categories     | object |    是    | 主题分类                 |
| user.groups              | groups         | object |    否    | 用户所在群组             |
| deletedUser              | users          | object |    否    | 删除用户                 |
| firstPost.images         | attachments    | array  |    否    | 首贴图片                 |
| firstPost.likedUsers     | users          | array  |    否    | 点赞首帖的用户           |
| lastThreePosts           | posts          | array  |    否    | 最后三条回复             |
| lastThreePosts.user      | users          | object |    否    | 最后三条回复的作者       |
| lastThreePosts.replyUser | users          | object |    否    | 最后三条回复所回复的用户 |
| rewardedUsers            | users          | array  |    否    | 打赏主题的用户           |

#### 请求示例

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                | 类型     | 出现要求   | 描述                                              |
| :---------------------- | :------- | :--------- | :------------------------------------------------ |
| **links**               | object   |            | 接口链接                                          |
| **data**                | object   |            | 基础数据                                          |
| type                    | string   |            | 数据类型                                          |
| id                      | int      |            | 数据 id                                           |
| **attributes**          | object   |            | 数据属性                                          |
| type                    | int      | 是         | 文章类型(0 普通 1 长文 2 视频)                    |
| title                   | string   | 长文主题   | 标题                                              |
| price                   | float    | 长文主题   | 主题价格                                          |
| viewCount               | int      |            | 查看数                                            |
| postCount               | int      |            | 帖子数                                            |
| paidCount               | int      |            | 付费数                                            |
| createdAt               | datetime |            | 创建时间                                          |
| updatedAt               | datetime |            | 修改时间                                          |
| deletedAt               | datetime | 在回收站时 | 删除时间                                          |
| isApproved              | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isSticky                | bool     |            | 是否置顶                                          |
| isEssence               | bool     |            | 是否加精                                          |
| isFavorite              | bool     | 已收藏时   | 是否收藏                                          |
| paid                    | bool     | 付费主题   | 是否付费                                          |
| canViewPosts            | bool     |            | 是否有权查看详情                                  |
| canReply                | bool     |            | 是否有权回复                                      |
| canApprove              | bool     |            | 是否有权审核                                      |
| canSticky               | bool     |            | 是否有权置顶                                      |
| canEssence              | bool     |            | 是否有权加精                                      |
| canDelete               | bool     |            | 是否有权永久删除                                  |
| canHide                 | bool     |            | 是否有权放入回收站                                |
| canFavorite             | bool     |            | 是否有权收藏                                      |
| **relationships**       | object   |            | 关联关系                                          |
| thread-video. status    | int      |            | 视频状态 0:转码中 1 转码完成 2 转码失败           |
| thread-video. reason    | string   |            | 转码失败原因                                      |
| thread-video. file_name | string   |            | 视频文件名称                                      |
| thread-video. file_id   | string   |            | 媒体文件唯一标识                                  |
| thread-video. media_url | string   |            | 媒体播放地址                                      |
| thread-video. cover_url | string   |            | 媒体封面地址                                      |
| **included**            | object   |            | 关联数据                                          |

#### 返回示例

```json
{
  "links": {
    "first": "http://discuz.test/api/threads?page%5Blimit%5D=1",
    "next": "http://discuz.test/api/threads?page%5Blimit%5D=1&page%5Boffset%5D=1",
    "last": "http://discuz.test/api/threads?page%5Blimit%5D=1&page%5Boffset%5D=4"
  },
  "data": [
    {
      "type": "threads",
      "id": "31",
      "attributes": {
        "title": "",
        "price": "0.00",
        "viewCount": 0,
        "postCount": 1,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:59+08:00",
        "updatedAt": "2019-11-12T17:10:59+08:00",
        "isApproved": 1,
        "isSticky": true,
        "isEssence": true,
        "canApprove": true,
        "canSticky": true,
        "canEssence": true,
        "canDelete": true,
        "canFavorite": true,
        "isFavorite": true,
        "favoriteAt": "2019-11-18T14:13:18+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "firstPost": {
          "data": {
            "type": "posts",
            "id": "31"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "admin",
        "mobile": "",
        "avatarUrl": null,
        "threadCount": null,
        "registerIp": null,
        "lastLoginIp": "127.0.0.1",
        "createdAt": "2019-10-11T00:00:00+08:00",
        "updatedAt": "2019-11-11T14:51:34+08:00"
      }
    },
    {
      "type": "posts",
      "id": "31",
      "attributes": {
        "content": "Pants Personal Loan Account == grey == Ergonomic Metal Tuna generating",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:59+08:00",
        "updatedAt": "2019-11-12T17:10:59+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canApprove": true,
        "canDelete": true,
        "canLike": true
      }
    }
  ],
  "meta": {
    "threadCount": 5,
    "pageCount": 5
  }
}
```
