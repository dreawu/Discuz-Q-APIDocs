### 批量更新话题

- **接口说明：** 批量更新话题
- **接口地址：** /api/topics/batch/{ids}
- **请求方式：** patch

#### 请求参数

| 参数名称        | 类型   | 是否必须 | 描述                 |
| :-------------- | :----- | :------: | :------------------- |
| recommended     | int    |    否    | 是否推荐（0/1）<br>1 推荐<br>0 不推荐 |

#### 请求示例

```json
{
    "data": [
        {
            "id": "1",
            "type": "topics",
            "attributes": {
                "recommended": true
            }
        }
        {
            "id": "2",
            "type": "topics",
            "attributes": {
                "recommended": true
            }
        }
    ]
}
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 500

#### 返回结果

| 参数名称          | 类型     | 出现要求   | 描述                                              |
| :---------------- | :------- | :--------- | :------------------------------------------------ |
| **data**          | object   |            | 基础数据                                          |
| type              | string   |            | 数据类型                                          |
| id                | int      |            | 数据 id                                           |
| **attributes**    | object   |            | 数据属性                                          |
| user_id           | int      |            | 用户id                                           |
| content           | string   |            | 话题内容                                          |
| thread_count      | int      |            | 查看数                                            |
| view_count        | int      |            | 帖子数                                            |
| recommended       | tinyInt  |            | 是否推荐（0/1）<br>0 推荐<br>1 不推荐                |
| updated_at        | datetime |            | 修改时间                                          |
| created_at        | datetime |            | 创建时间                                          |
| recommended_at    | datetime |            | 推荐时间                                          |

#### 返回示例

```json
{
    "data": [
        {
            "type": "topics",
            "id": "1",
            "attributes": {
                "user_id": 1,
                "content": "test1",
                "thread_count": 2,
                "view_count": 3,
                "recommended": 1,
                "updated_at": "2020-08-13T10:48:12+08:00",
                "created_at": "2020-08-12T16:15:25+08:00",
                "recommended_at": "2020-08-13T10:08:12+08:00"
            }
        },
        {
            "type": "topics",
            "id": "2",
            "attributes": {
                "user_id": 1,
                "content": "test2",
                "thread_count": 2,
                "view_count": 3,
                "recommended": 1,
                "updated_at": "2020-08-13T10:48:12+08:00",
                "created_at": "2020-08-12T16:15:25+08:00",
                "recommended_at": "2020-08-13T10:08:12+08:00"
            }
        }
    ]
}
```
