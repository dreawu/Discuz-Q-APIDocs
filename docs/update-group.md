### 用户组单条修改

- **接口说明：** 用户组单条修改
- **接口地址：** /api/groups/{id}
- **请求方式：** PATCH

#### 请求参数

| 参数名称 | 类型   | 是否必须 | 描述       |
| :------- | :----- | :------- | :--------- |
| name     | string | 是       | 用户组名称 |
| type     | string | 否       | 类型       |
| color    | string | 否       | 颜色       |
| icon     | string | 否       | icon       |
| is_paid  | int | 否          | 是否付费用户组，0为免费,1为付费用户组         |
| fee     | float | 否       | 付费金额       |
| days     | int | 否       | 付费获得用户组权限天数       |
| scale    | int    | 否       | 分成  |
| is_subordinate  | bool  | 否  | 是否允许推广下线(注册收入)  |
| is_commission   | bool  | 否  | 是否允许收入提成(打赏/付费)  |

请求示例：

```json
{
  "data": {
    "attributes": {
      "name": "TomAuth",
      "type": "类型",
      "color": "red",
      "icon": "Aa",
      "is_paid":1,
      "fee":10,
      "days":3,
      "scale": 2,
      "is_subordinate": true,
      "is_commission": true
    }
  }
}
```

#### 返回结果

| 参数名称 | 类型 | 出现要求 | 描述 |
| :------- | :--- | :------- | :--- |


#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 非 200

#### 返回示例

```json
{
  "data": {
    "type": "groups",
    "id": "18",
    "attributes": {
      "name": "测试哈223",
      "type": "类型",
      "color": "red",
      "icon": "Aa",
      "is_paid":1,
      "fee":10,
      "days":3,
      "scale": 2,
      "is_subordinate": true,
      "is_commission": true
    }
  }
}
```
