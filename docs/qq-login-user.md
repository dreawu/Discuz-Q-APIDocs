### QQ登录参数换取token接口

- **接口说明：** qq通过  参数换取 token 接口
- **接口地址：** /api/oauth/qq/user
- **请求方式：** GET

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述               |
| :-------- | :----- | :------: | :----------------- |
| sessionId      | string |    是    | 登录接口返回 sessionId  |
| access_token | string |    是    | 登录接口返回授权token |
| state     | string |    否    | 登录接口返回 state |

```

/api/oauth/qq/user?sessionId={sessionId}&access_token={access_token}&state={state}

```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 400

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |


#### 返回示例

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Qi......dj3H9CCSPib6MQtnaT6VNrw",
      "refresh_token": "def50200a26b6a9......10ccbf3c1694084c2d2d276"
    }
  }
}
```