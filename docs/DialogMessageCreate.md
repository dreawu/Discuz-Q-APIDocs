### 创建会话消息接口[单条]

- **接口说明：** 创建会话消息[单条]
- **接口地址：** /api/dialog/message
- **请求方式：** POST

#### 请求参数

| 参数名称                 |  类型  | 是否必须 | 描述                            |
| :----------------------- | :----: | :------: | :------------------------------ |
| **data**                 | object |    是    | 基础数据                        |
| type                     | string |    是    | 数据类型，固定值 dialog_message |
| **attributes**           | object |    是    | 数据属性                        |
| attributes. dialog_id    |  int   |    是    | 会话 ID                         |
| attributes. message_text | string |    是    | 消息内容，传消息内容时可以不传附件                        |
| attributes. attachment_id | int   |    是    | 附件 ID，传附件时可以不传消息内容                        |

#### 请求示例

```json
{
  "data": {
    "type": "dialog_message",
    "attributes": {
      "dialog_id": 1,
      "message_text": "消息内容123"
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

> 关联数据模型字段释义请参见相应文档

| 参数名称                 | 类型     | 出现要求 | 描述       |
| :----------------------- | :------- | :------- | :--------- |
| **data**                 | object   |          | 基础数据   |
| type                     | string   |          | 数据类型   |
| id                       | int      |          | 消息 id    |
| **attributes**           | object   |          | 数据属性   |
| attributes. user_id      | int      |          | 发送人 uid |
| attributes. dialog_id    | int      |          | 会话 id    |
| attributes. summary      | string   |          | 消息摘要40个字   |
| attributes. message_text | string   |          | 消息内容   |
| attributes. message_text_html | string   |          | 解析后的消息内容   |
| attributes.updated_at    | datetime |          | 更新时间   |
| attributes.created_at    | datetime |          | 创建时间   |

#### 返回示例

```json
{
  "data": {
    "type": "dialog_message",
    "id": "5",
    "attributes": {
      "user_id": 1,
      "dialog_id": 1,
      "message_text": "消息内容123",
      "updated_at": "2020-02-18T17:09:54+08:00",
      "created_at": "2020-02-18T17:09:54+08:00"
    }
  }
}
```
