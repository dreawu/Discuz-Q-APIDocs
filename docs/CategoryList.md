### 查询分类接口[列表]

- **接口说明：** 查询分类接口[列表]
- **接口地址：** /api/categories
- **请求方式：** GET

#### 请求参数
| 参数名称               | 类型   | 值 | 描述                    |
| :--------------------- | :----- | :------- | :---------------------- |
| **filter**              | array |           | 过滤条件                            |
| createThread        | int     |     1     | 可发布主题的分类                |

#### 请求示例

```
/api/categories
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                | 类型   | 出现要求 | 描述                                                         |
| :---------------------- | :----- | :------- | :----------------------------------------------------------- |
| **links**               | object |          | 接口链接                                                     |
| **data**                | object |          | 基础数据                                                     |
| type                    | string |          | 数据类型                                                     |
| id                      | int    |          | 数据 id                                                      |
| **attributes**          | object |          | 数据属性                                                     |
| attributes.name         | string |          | 分类名称                                                     |
| attributes.icon         | string |          | 分类图标 URL                                                 |
| attributes.description  | string |          | 分类说明                                                     |
| attributes.property     | int    |          | 属性：0:正常 1:首页展示                                      |
| attributes.sort         | int    |          | 显示顺序                                                     |
| attributes.threadCount  | int    |          | 主题总数                                                     |
| attributes.checked      | int    |          | 是否为智能排序中被选中的条件，1选中，0未选中                 |
| attributes.search_ids   | object |          | 当以分类进行搜索时(前端首页、后台内容管理)，前端就取search_ids的值传给对应的搜索接口 |
| **attributes.children** | object |          | 子类，里面的字段和上面的一致                                 |

#### 返回示例

```json
{
    "data": [
        {
            "type": "categories",
            "id": "1",
            "attributes": {
                "name": "星期一",
                "description": "默认分类",
                "icon": "",
                "sort": 0,
                "parentid": 0,
                "property": 0,
                "thread_count": 0,
                "ip": "127.0.0.1",
                "created_at": "2021-02-08T11:27:27+08:00",
                "updated_at": "2021-02-08T11:30:52+08:00",
                "checked": 0,
                "search_ids": "1,3,4",
                "children": [
                    {
                        "id": 3,
                        "name": "星期一上午",
                        "description": "",
                        "icon": "",
                        "sort": 1,
                        "property": 0,
                        "thread_count": 0,
                        "ip": "127.0.0.1",
                        "parentid": 1,
                        "created_at": "2021-02-08T03:37:28.000000Z",
                        "updated_at": "2021-02-08T03:37:28.000000Z",
                        "search_ids": 3,
                        "checked": 0
                    },
                    {
                        "id": 4,
                        "name": "星期一中午",
                        "description": "",
                        "icon": "",
                        "sort": 2,
                        "property": 0,
                        "thread_count": 0
                        "ip": "127.0.0.1",
                        "parentid": 1,
                        "created_at": "2021-02-08T03:37:38.000000Z",
                        "updated_at": "2021-02-08T03:37:38.000000Z",
                        "search_ids": 4,
                        "checked": 0
                    }
                ]
            }
        },
        {
            "type": "categories",
            "id": "2",
            "attributes": {
                "name": "星期二",
                "description": "",
                "icon": "",
                "sort": 1,
                "parentid": 0,
                "property": 0,
                "thread_count": 0,
                "ip": "127.0.0.1",
                "created_at": "2021-02-08T11:31:33+08:00",
                "updated_at": "2021-02-08T11:31:33+08:00",
                "checked": 0,
                "search_ids": "2",
                "children": []
            }
        }
    ]
}
```
