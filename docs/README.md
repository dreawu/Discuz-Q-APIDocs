## 总览

<center><img src="https://dl.discuz.chat/jsonapi.png" /></center>

Discuz! Q 采用 JSON 构建 API 的标准指南

JSON API 是数据交互规范，用以定义客户端如何获取与修改资源，以及服务器如何响应对应请求。

JSON API 设计用来最小化请求的数量，以及客户端与服务器间传输的数据量。在高效实现的同时，无需牺牲可读性、灵活性和可发现性。

JSON API 需要使用 JSON API 媒体类型(application/vnd.api+json) 进行数据交互。

JSON API 服务器支持通过 GET 方法获取资源。而且必须独立实现 HTTP POST, PUT 和 DELETE 方法的请求响应，以支持资源的创建、更新和删除。

JSON API 服务器也可以选择性支持 HTTP PATCH 方法 [RFC5789]和 JSON Patch 格式 [RFC6902]，进行资源修改。JSON Patch 支持是可行的，因为理论上来说，JSON API 通过单一 JSON 文档，反映域下的所有资源，并将 JSON 文档作为资源操作介质。在文档顶层，依据资源类型分组。每个资源都通过文档下的唯一路径辨识。

具体可参考 [jsonapi.org](https://jsonapi.org/format/1.1/)

## REST API v1

## 当前版本

默认情况下，所有 `https://discuz.chat/api` 接收 `v1` 版本的 `REST API` 的请求。我们建议您通过 `Accept` `header` 明确请求此版本。所有数据都以 JSON 的形式发送和接收。

```
  Accept: application/vnd.api+json
```

## 认证方式

目前所有的接口使用 Oauth2 Password 方式，也就是 JWT `Authorization` `Bearer` `header` 进认证。

## 目录

- [通用参数释义](通用参数释义.md)
- [语言包说明](LanguagePack.md)

### 站点

- [站点信息](forum.md)
- [站点基本信息](siteinfo.md)
- [上传站点 Logo](upload-sitelogo.md)
- [删除站点 Logo](delete-sitelogo.md)
- [检查新版](check.md)

### 用户

- [批量删除用户](delete-users.md)
- [上传头像](upload-avatar.md)
- [删除用户头像](delete-user-avatar.md)
- [修改用户信息](update-user.md)
- [修改支付密码 - 获取验证 token](ResetPayPassword.md)
- [批量修改用户信息](update-users.md)
- [用户信息](userprofile.md)
- [用户列表](userslist.md)
- [用户实名认证](update-user-real.md)
- [导出用户](UserExports.md)
- [解绑微信](delete-user-wechat.md)
- [拉黑用户](create-deny-user.md)
- [取消拉黑](delete-deny-user.md)
- [拉黑列表](list-deny-user.md)
- [绑定微信小程序手机号](BindWchatMiniprogramMobile.md)
- [推荐用户](UserRecommended.md)

### 用户认证

- [注册接口](register.md)
- [登录接口](Login.md)
- ~~[微信登录](wechat-login.md)~~
- ~~[微信登录 pc](wechat-login-pc.md)~~
- [微信小程序登录、注册、绑定](wechat-login-miniprogram.md)
- [微信公众号网页登陆、注册、绑定](wechat-user.md)
- [刷新 token](RefreshToken.md)
- [Ucenter 登录](UcenterLogin.md)
- [PC 扫码登录-有状态登录](wechat-pc-qrcode-login.md)
- [PC 扫码登录-生成二维码](wechat-pc-code.md)
- [PC 扫码登录-轮询查询接口](wechat-pc-login.md)
- [PC 扫码绑定/换绑](wechat-pc-bind.md)
- [PC 扫码绑定/换绑-轮询接口](wechat-pc-bind-poll.md)

### 公众号授权方式 - 微信 pc 登录

- [微信服务通信地址 ](wechat-pc-user-event.md)
- [生成 pc 微信登录二维码](wechat-pc-user-qrcode.md)
- [用户登录状态查询](wechat-pc-user-search.md)
- 公众号授权
  - [素材管理上传接口](OffiaccountAssetCreate.md)
  - [素材管理删除](OffiaccountAssetDelete.md)
  - [素材管理获取列表](OffiaccountAssetList.md)
  - [素材管理获取单条回复](OffiaccountAssetResource.md)
  - [素材管理修改图文消息](OffiaccountAssetUpdate.md)
  - [创建多个菜单接口](OffiaccountMenuCreate.md)
  - [获取菜单接口](OffiaccountMenuList.md)
  - [创建自动回复](OffiaccountReplyCreate.md)
  - [删除自动回复](OffiaccountReplyDelete.md)
  - [获取自动回复列表](OffiaccountReplyList.md)
  - [获取单条自动回复](OffiaccountReplyResource.md)
  - [站内帖子转公众号图文素材](OffIAccountThreadsReprint.md)
  - [公众号图文素材转站内帖子](OffIAccountThreadsTransform.md)

### 用户组

- [创建用户组](create-group.md)
- [上传用户组图标](GroupUploadIcon.md)
- [删除用户组](delete-group.md)
- [批量删除用户组](delete-groups.md)
- [更新用户组](update-group.md)
- [批量更新用户组](update-groups.md)
- [查看单个用户组](GroupResource.md)
- [用户组列表](groups.md)

### 权限

- [权限处理](GroupPermissionUpdate.md)
- [设置权限用户组](PermissionSet.md)

### 通知

- [删除通知](NotificationDelete.md)
- [通知列表](NotificationList.md)
- [系统消息模版列表](notification-tpl-list.md)
- [更新系统消息模版](update-notification-tpl.md)
- [获取单条系统消息模版](notification-tpl.md)

### 分类

- [创建单条分类](CategoryCreate.md)
- [创建多条分类](CategoryBatchCreate.md)
- [删除单条分类](CategoryDelete.md)
- [删除多条分类](CategoryBatchDelete.md)
- [更新单条分类](CategoryUpdate.md)
- [更新多条分类](CategoryBatchUpdate.md)
- [查看所有分类](CategoryList.md)

### 主题

- [发表主题](ThreadCreate.md)
- [删除主题](ThreadDelete.md)
- [批量删除主题](ThreadBatchDelete.md)
- [更新主题](ThreadUpdate.md)
- [批量更新主题](ThreadBatchUpdate.md)
- [查看主题](ThreadResource.md)
- [主题列表](ThreadList.md)
- [我的收藏](MyFavorite.md)
- [分享主题](ThreadShare.md)
- [用户点赞的主题列表](ThreadLikesList.md)
- [创建主题视频](ThreadVideoCreate.md)
- [视频主题云点播上传签名](SignatureCreate.md)
- [相关主题](ThreadRelate.md)
- [创建问答的回答](QuestionAnswerCreate.md)

### 回复

- [发表回复](PostCreate.md)
- [删除回复](PostDelete.md)
- [批量删除回复](PostBatchDelete.md)
- [更新回复](PostUpdate.md)
- [批量更新回复](PostBatchUpdate.md)
- [查看回复](PostResource.md)
- [回复列表](PostList.md)
- [我的点赞](MyLikes.md)

### 附件

- [上传附件](AttachmentCreate.md)
- [删除附件](AttachmentDelete.md)
- [下载/预览单个附件](AttachmentResource.md)</del>

### 敏感词

- [创建敏感词](StopWordCreate.md)
- [批量创建敏感词](StopWordBatchCreate.md)
- [删除敏感词](StopWordDelete.md)
- [更改敏感词](StopWordUpdate.md)
- [查看敏感词](StopWordResource.md)
- [敏感词列表](StopWordList.md)
- [导出敏感词](StopWordExport.md)

### 表情

- [表情包](EmojiList.md)

### 邀请

- [管理员创建创建邀请码](InviteCreate.md)
- [普通用户创建邀请码](InviteUserCode.md)
- [删除邀请码[单个]](InviteDelete.md)
- [管理员邀请码[列表]](InviteList.md)
- [被邀请用户列表](InviteUserList.md)

### 订单

- [生成支付订单](TradePayOrder.md)
- [订单接口](Order.md)
- [订单列表](OrderList.md)
- [查看订单](OrderResource.md)

### 钱包

- [申请提现](CashUserWalletCreate.md)
- [提现审核](CashReview.md)
- [提现列表](CashUserWalletList.md)
- [更改钱包](UserWalletUpdate.md)
- [查看单条钱包记录](UserWalletResource.md)
- [钱包记录](UserWalletLogList.md)

### 短信

- [发送短信](sms-send.md)
- [验证短信](sms-verify.md)

### 设置

- [设置配置](settings.md)
- [获取配置](settingsTags.md)

### 财务

- [获取盈利图表数据](StatisticFinanceChart.md)
- [获取资金概况](StatisticFinanceProfile.md)

### 关注

- [创建关注关系](UserFollowCreate.md)
- [删除关注关系](UserFollowDelete.md)
- [获取关注列表](UserFollowList.md)

### 私信

- [创建会话接口[多条]](DialogBatchCreate.md)
- [创建会话接口[单条]](DialogCreate.md)
- [获取会话列表](DialogLIst.md)
- [创建会话消息接口[单条]](DialogMessageCreate.md)
- [获取会话消息列表](DialogMessageLIst.md)
- [删除会话接口](DialogDelete.md)


### 话题

- [话题列表](TopicList.md)
- [创建话题](TopicCreate.md)
- [删除话题](TopicDelete.md)
- [批量删除话题](TopicBatchDelete.md)
- [话题详情](TopicResource.md)
