### 用户组批量删除接口

- **接口说明：** 用户组批量删除接口
- **接口地址：** /api/groups
- **请求方式：** delete

#### 请求参数

| 参数名称 | 类型    | 描述      |
| :------- | :------ | :-------- |
| id       | id 数组 | 用户组 id |

#### 请求示例

```json
{
  "data": {
    "id": [11, 12]
  }
}
```

#### 返回结果

| 参数名称 | 类型 | 描述        |
| :------- | :--- | :---------- |
| id       | int  | 已删除的 id |
| succeed  | bool | 删除成功    |

#### 返回说明

- 成功， http 状态码： 204

#### 返回示例

```json
{
  "data": [
    {
      "type": null,
      "id": "10",
      "attributes": {
        "succeed": true,
        "error": null
      }
    },
    {
      "type": null,
      "id": "11",
      "attributes": {
        "succeed": true,
        "error": null
      }
    }
  ]
}
```
