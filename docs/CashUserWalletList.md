### 提现记录列表

- **接口说明：** 提现记录列表
- **接口地址：** /api/wallet/cash
- **请求方式：** GET

#### 请求参数

| 字段名   | 变量名              | 必填 | 类型     | 描述                                                                                                 |
| :------- | :------------------ | :--- | :------- | :--------------------------------------------------------------------------------------------------- |
| 排序参数 | sort                | 否   | string   | 可选值：created_at、updated_at。                                                                     |
| 筛选参数 | filter[cash_status] | 否   | int      | 筛选订单付款状态，可选值：1：待审核，2：审核通过，3：审核不通过，4：待打款， 5，已打款， 6：打款失败 |
| 筛选参数 | filter[user]        | 否   | int      | 传递 user_id,筛选某用户数据                                                                          |
| 筛选参数 | filter[cash_sn]     | 否   | string   | 按流水号筛选                                                                                         |
| 筛选参数 | filter[username]    | 否   | string   | 按提现申请人筛选                                                                                     |
| 筛选参数 | filter[start_time]  | 否   | datetime | 按提现申请时间范围筛选：开始时间                                                                     |
| 筛选参数 | filter[end_time]    | 否   | datetime | 按提现申请时间范围筛选：最后时间                                                                     |
| 关联参数 | include             | 否   | string   | 可选值:user 用户信息、userWallet 用户钱包信息、wechat 用户微信。                                     |

#### 请求示例

```
/api/wallet/cash?include=user,userWallet&filter[cash_status]=1&page[number]=1&page[size]=10&filter[cash_sn]=&filter[username]=&filter[start_time]=2020-04-19-00-00-00&filter[end_time]=2020-04-26-24-00-00
```

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                        | 必填 | 类型   | 描述                                                                                 |
| :--------------------- | :---------------------------- | :--- | :----- | :----------------------------------------------------------------------------------- |
| **data.attributes**    | object                        | 是   | object | 数据属性                                                                             |
| 记录 ID                | attributes.id                 | 是   | bigint | 提现记录唯一编号                                                                     |
| 提现编号               | attributes.cash_sn            | 是   | bigint | 提现订单唯一编号                                                                     |
| 手续费                 | attributes.cash_charge        | 是   | float  | 提现手续费                                                                           |
| 实际提现金额           | attributes.cash_actual_amount | 是   | float  | 用户到账金额                                                                         |
| 提现申请金额           | attributes.cash_apply_amount  | 是   | float  | 用户提现申请金额                                                                     |
| 提现状态               | attributes.cash_status        | 是   | int    | 提现状态，1：待审核，2：审核通过，3：审核不通过，4：待打款， 5，已打款， 6：打款失败 |
| 提现方式               | cash_type                     | 是   | int    | 提现方式：0：人工打款，1：付款到零钱                                                 |
| 提现手机               | cash_mobile                   | 否   | string | 当 cash_type = 1 时填写提现打款到的手机号码                                          |
| 审核原因               | attributes.remark             | 是   | sring  | 审核不通过原因或备注,默认为空                                                        |
| 交易号                 | attributes.trade_no           | 是   | sring  | 平台交易号                                                                           |
| 错误代码               | attributes.error_code         | 是   | sring  | 提现交易错误代码，正常时为空                                                         |
| 提现错误信息           | attributes.error_message      | 是   | sring  | 提现错误信息，正常时为空                                                             |
| 返款状态               | attributes.refunds_status     | 是   | int    | 0 为返款，1 已返款                                                                   |
| **data.relationships** | object                        | 否   | object | 关联关系                                                                             |
| **included**           | object                        | 否   | object | 关联数据                                                                             |

#### 返回示例

```json
{
  "links": {
    "first": "http://discuz.test/api/wallet/cash?page%5Blimit%5D=1&include=userWallet&filter%5Bstart_time%5D=2019-11-22",
    "next": "http://discuz.test/api/wallet/cash?page%5Blimit%5D=1&page%5Boffset%5D=1&include=userWallet&filter%5Bstart_time%5D=2019-11-22",
    "last": "http://discuz.test/api/wallet/cash?page%5Blimit%5D=1&page%5Boffset%5D=4&include=userWallet&filter%5Bstart_time%5D=2019-11-22"
  },
  "data": [
    {
      "type": "user_wallet_cash",
      "id": "40",
      "attributes": {
        "id": 40,
        "user_id": 1,
        "cash_sn": "201911228652521021",
        "cash_charge": "0.01",
        "cash_actual_amount": "0.99",
        "cash_apply_amount": "1.00",
        "cash_status": 1,
        "cash_type": 0,
        "cash_mobile": "",
        "remark": null,
        "trade_no": "",
        "error_code": "",
        "error_message": "",
        "refunds_status": 0,
        "updated_at": "2019-11-22T11:07:32+08:00",
        "created_at": "2019-11-22T11:07:32+08:00"
      },
      "relationships": {
        "userWallet": {
          "data": {
            "type": "user_wallet",
            "id": "1"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "user_wallet",
      "id": "1",
      "attributes": {
        "user_id": 1,
        "available_amount": "1.00",
        "freeze_amount": "33.00",
        "wallet_status": 0
      }
    }
  ]
}
```
