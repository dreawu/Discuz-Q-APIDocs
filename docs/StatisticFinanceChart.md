### 获取盈利图表数据

- **接口说明：** 获取盈利图表数据
- **接口地址：** /api//statistic/financeChart
- **请求方式：** GET

#### 请求参数

| 参数名称               | 类型     | 是否必须 | 描述                       |
| :--------------------- | :------- | :------: | :------------------------- |
| filter[type]           | datetime |    否    | 统计方式（1 日 2 周 3 月） |
| filter[createdAtBegin] | datetime |    否    | 时间大于                   |
| filter[createdAtEnd]   | datetime |    否    | 时间小于                   |

#### 请求示例

```
/api//statistic/financeChart?filter[createdAtBegin]=2020-01-01&filter[createdAtEnd]=2020-02-01&page[number]=1
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                     | 类型   | 出现要求 | 描述                  |
| :--------------------------- | :----- | :------- | :-------------------- |
| **data**                     | object |          | 基础数据              |
| type                         | string |          | 数据类型              |
| id                           | int    |          | 数据 id               |
| **attributes**               | object |          | 数据属性              |
| attributes.date              | float  |          | 图表 X 轴使用的时间点 |
| attributes.order_count       | int    |          | 订单数量              |
| attributes.order_amount      | float  |          | 订单数量              |
| attributes.total_profit      | float  |          | 平台总盈利            |
| attributes.register_profit   | float  |          | 注册加入收入          |
| attributes.master_portion    | float  |          | 打赏提成收入          |
| attributes.withdrawal_profit | float  |          | 提现手续费收入        |

#### 返回示例

```json
{
  "data": [
    {
      "type": "finance",
      "id": "",
      "attributes": {
        "date": "2019-52",
        "order_count": "48",
        "order_amount": "0.00",
        "total_profit": "767.00",
        "register_profit": "565.00",
        "master_portion": "767.00",
        "withdrawal_profit": "767.00"
      }
    },
    {
      "type": "finance",
      "id": "",
      "attributes": {
        "date": "2019-53",
        "order_count": "22",
        "order_amount": "330.00",
        "total_profit": "333.00",
        "register_profit": "444.00",
        "master_portion": "444.00",
        "withdrawal_profit": "444.00"
      }
    },
    {
      "type": "finance",
      "id": "",
      "attributes": {
        "date": "2020-01",
        "order_count": "88",
        "order_amount": "330.00",
        "total_profit": "1110.00",
        "register_profit": "1110.00",
        "master_portion": "1221.00",
        "withdrawal_profit": "1221.00"
      }
    },
    {
      "type": "finance",
      "id": "",
      "attributes": {
        "date": "2020-02",
        "order_count": "118",
        "order_amount": "220.00",
        "total_profit": "1756.00",
        "register_profit": "1574.00",
        "master_portion": "1867.00",
        "withdrawal_profit": "1867.00"
      }
    },
    {
      "type": "finance",
      "id": "",
      "attributes": {
        "date": "2020-03",
        "order_count": "59",
        "order_amount": "1106.03",
        "total_profit": "1059.62",
        "register_profit": "777.02",
        "master_portion": "1110.00",
        "withdrawal_profit": "837.60"
      }
    }
  ]
}
```
