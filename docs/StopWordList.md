### 查询敏感词接口[列表]

- **接口说明：** 查询敏感词[列表]
- **接口地址：** /api/stop-words
- **请求方式：** GET

#### 请求参数

| 参数名称  |  类型  | 是否必须 | 描述                   |
| :-------- | :----: | :------: | :--------------------- |
| filter[q] | string |    否    | 搜索带有关键词的敏感词 |

#### 请求示例

```
/api/stop-words?filter[q]=bad
```

#### 返回说明

> 敏感词标识释义  
> {IGNORE} 忽略、不处理  
> {MOD} 审核  
> {BANNED} 禁用、禁止使用  
> {REPLACE} 替换

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义请参见相应文档

| 参数名称               | 类型     | 出现要求 | 描述                   |
| :--------------------- | :------- | :------- | :--------------------- |
| **links**              | object   |          | 接口链接               |
| **data**               | object   |          | 基础数据               |
| type                   | string   |          | 数据类型               |
| id                     | int      |          | 数据 id                |
| **attributes**         | object   |          | 数据属性               |
| attributes.ugc         | string   |          | 针对用户内容的处理方式 |
| attributes.username    | string   |          | 针对用户名的处理方式   |
| attributes.find        | string   |          | 敏感词或敏感词查找正则 |
| attributes.replacement | string   |          | 待替换的内容           |
| attributes.created_at  | datetime |          | 创建时间               |
| attributes.updated_at  | datetime |          | 修改时间               |
| **relationships**      | object   |          | 关联关系               |
| **included**           | object   |          | 关联数据               |

#### 返回示例

```json
{
  "links": {
    "first": "http://discuz.test/api/stop-words?page%5Blimit%5D=2",
    "next": "http://discuz.test/api/stop-words?page%5Blimit%5D=2&page%5Boffset%5D=2"
  },
  "data": [
    {
      "type": "stop-words",
      "id": "31",
      "attributes": {
        "ugc": "{MOD}",
        "username": "{REPLACE}",
        "find": "reciprocal invoice",
        "replacement": "sky blue implement",
        "created_at": "2019-10-24T15:16:33+08:00",
        "updated_at": "2019-11-07T14:30:44+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    },
    {
      "type": "stop-words",
      "id": "32",
      "attributes": {
        "ugc": "{REPLACE}",
        "username": "{REPLACE}",
        "find": "MOD",
        "replacement": "111",
        "created_at": "2019-10-24T15:16:33+08:00",
        "updated_at": "2019-10-24T15:16:33+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "nickname": null,
        "mobile": "",
        "unionId": "",
        "lastLoginIp": "127.0.0.1",
        "createdAt": "2019-10-11T00:00:00+08:00",
        "updatedAt": "2019-11-11T14:51:34+08:00"
      }
    }
  ]
}
```
