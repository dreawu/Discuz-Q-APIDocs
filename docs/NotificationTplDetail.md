### 通知详情

- **接口说明：** 通知详情接口
- **接口地址：** /api/notification/tpl/detail
- **请求方式：** GET

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述         |
| --------- | ------ | -------- | ------------ |
| type_name | string | 是       | 通知模板名称 |

**请求示例**

`{{host}}/api/notification/tpl/detail?type_name`

#### 返回说明

- 返回通知模板列表， http 状态码： 200

#### 返回结果

| 参数名称                      | 类型    | 出现要求 | 描述          |
| :---------------------------- | :------ | :------- | :------------ |
| data                          | object  |          | 数据          |
| type                          | string  |          | 数据类型      |
| id                            | string  |          | 数据id        |
| attributes                    | object  |          | 数据          |
| attributes.tpl_id             | bingint |          | 记录唯一标识  |
| attributes.status             | tinyint |          | 模板状态      |
| attributes.type               | tinyint |          | 通知类型      |
| attributes.type_name          | varchar |          | 类型名称      |
| attributes.title              | varchar |          | 标题          |
| attributes.content            | text    |          | 内容          |
| attributes.template_id        | varchar |          | 模板ID        |
| attributes.template_variables | object  |          |               |
| attributes.first_data         | varchar |          | first.DATA    |
| attributes.keywords_data      | text    |          | keywords.DATA |
| attributes.remark_data        | varchar |          | remark.DATA   |
| attributes.color              | varchar |          | data color    |
| attributes.redirect_type      | tinyint |          | 跳转类型      |
| attributes.redirect_url       | varchar |          | 跳转地址      |
| attributes.page_path          | varchar |          | 跳转路由      |
| attributes.disabled           | bool    |          | 是否显示      |

示例：

```json
{
    "data": [
        {
            "type": "notification_tpls",
            "id": "0",
            "attributes": {
                "tpl_id": 1,
                "status": 1,
                "type": 0,
                "type_name": "新用户注册通知",
                "title": "欢迎加入{sitename}",
                "content": "{username}你好，你已经成为{sitename} 的{groupname} ，请你在发表言论时，遵守当地法律法规。祝你在这里玩的愉快。",
                "template_id": "",
                "template_variables": {
                    "{username}": "用户名",
                    "{sitename}": "站点名称",
                    "{groupname}": "用户组"
                },
                "first_data": "",
                "keywords_data": [],
                "remark_data": "",
                "color": [],
                "redirect_type": 0,
                "redirect_url": "",
                "page_path": "",
                "disabled": false
            }
        },
        {
            "type": "notification_tpls",
            "id": "1",
            "attributes": {
                "tpl_id": 13,
                "status": 0,
                "type": 1,
                "type_name": "新用户注册通知",
                "title": "微信注册通知",
                "content": null,
                "template_id": "",
                "template_variables": {
                    "{$user_id}": "注册人 ID（用于站点第几名注册）",
                    "{$user_name}": "用户名（注册人）",
                    "{$user_mobile}": "注册人手机号",
                    "{$user_mobile_encrypt}": "注册人手机号（带 * 的）",
                    "{$user_group}": "注册人用户组",
                    "{$joined_at}": "付费加入时间",
                    "{$expired_at}": "付费到期时间",
                    "{$site_name}": "站点名称",
                    "{$site_title}": "站点标题",
                    "{$site_introduction}": "站点介绍",
                    "{$site_mode}": "站点模式（付费/免费 (用于提示用户\"付费加入该站点\")）",
                    "{$notify_time}": "通知时间（时间格式: 20xx-xx-xx xx:xx:xx）",
                    "{$site_domain}": "站点域名（https://xxxxxx.com）"
                },
                "first_data": "",
                "keywords_data": [],
                "remark_data": "",
                "color": [],
                "redirect_type": 0,
                "redirect_url": "",
                "page_path": "",
                "disabled": false
            }
        }
    ]
}
```