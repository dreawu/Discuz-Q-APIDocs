### 用户资料展示

- **权限：** user.view 默认所有用户可看
- **接口说明：** 用户资料展示
- **接口地址：** /api/users/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述                                                      |
| :------- | :----: | :------: | :-------------------------------------------------------- |
| include  | string |    否    | 关联数据，可选值：groups,wechat(只有当前登录用户可以使用) ,dialog|

#### 请求示例

```php
api/users/1?include=groups
```

#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 非 200

#### 返回结果

| 参数名称                         | 类型     | 出现要求 | 描述                                                                                   |
| :------------------------------- | :------- | :------- | :------------------------------------------------------------------------------------- |
| **data**                         | object   |          | 基础数据        |
| type                             | string   |          | 数据类型        |
| id                               | int      |          | 数据 id        |
| **attributes**                   | object   |          | 数据属性        |
| id                               | int      |          | 用户 id        |
| username                         | string   |          | 用户名         |
| mobile                           | string   |          | 手机号（脱敏）   |
| avatarUrl                        | string   |          | 头像地址        |
| threadCount                      | int      |          | 主题数          |
| followCount                      | int      |          | 关注数          |
| fansCount                        | int      |          | 粉丝数          |
| likedCount                       | int      |          | 主题点赞数       |
| signature                        | string   |          | 签名            |
| usernameBout                     | int      |          | 用户名修改次数    |
| follow                           | object   |          | 关注状态：<br>null 未登录/登陆用户与查询用户相同<br>0 未关注<br>1 已关注<br>2 互相关注 |
| status                           | int      |          | 状态               |
| loginAt                          | datetime |          | 登录时间            |
| joinedAt                         | datetime |          | 加入时间            |
| expiredAt                        | datetime |          | 到期时间            |
| createdAt                        | datetime |          | 创建时间            |
| updatedAt                        | datetime |          | 修改时间            |
| canEdit                          | bool     |          | 是否可以编辑         |
| canDelete                        | bool     |          | 是否可以删除         |
| canWalletPay                     | bool     |          | 是否可以使用钱包支付   |
| showGroups                       | bool     |          | 是否显示群组         |
| registerReason                   | string   |          | 注册理由            |
| banReason                        | string   |          | 禁用理由            |
| originalMobile                   | string   |          | 完整手机号          |
| registerIp                       | string   |          | 注册 ip            |
| lastLoginIp                      | string   |          | 最后登录 ip         |
| identity                         | string   |          | 注册理由            |
| realname                         | string   |          | 实际姓名            |
| walletBalance                    | float    | 当前用户  | 用户余额             |
| walletFreeze                     | float    | 当前用户  | 用户冻结余额          |
| paid                             | bool     |          | 是否是付费用户                        |
| hasPassword                      | bool     |          | 是否有密码（小程序注册用户无初始密码）|
| payTime                          | datetime |          | 支付时间            |
| unreadNotifications              | int      |          | 未读消息数          |
| typeUnreadNotifications          | array    |          | 未读消息数明细      |
| typeUnreadNotifications.replied  | array    |          | 未读回复消息数      |
| typeUnreadNotifications.liked    | array    |          | 未读点赞消息数      |
| typeUnreadNotifications.rewarded | array    |          | 未读打赏消息数      |
| typeUnreadNotifications.system   | array    |          | 未读系统消息数      |
| typeUnreadNotifications.related  | array    |          | 未读@通知消息数     |
| typeUnreadNotifications.withdrawal | array  |          | 未读提现通知消息数   |

#### 返回示例

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "id": 1,
      "username": "username",
      "avatarUrl": "",
      "isReal": false,
      "threadCount": 106,
      "followCount": 12,
      "fansCount": 7,
      "likedCount": 28,
      "signature": "这个是签名",
      "usernameBout": 9,
      "follow": null,
      "status": 0,
      "loginAt": "2020-05-16T11:42:56+08:00",
      "joinedAt": null,
      "expiredAt": "2020-06-07T15:49:55+08:00",
      "createdAt": "2020-04-16T19:19:17+08:00",
      "updatedAt": "2020-05-16T14:06:06+08:00",
      "canEdit": true,
      "canDelete": true,
      "showGroups": true,
      "registerReason": "",
      "banReason": "",
      "denyStatus": false,
      "originalMobile": "186....7177",
      "registerIp": "139.55.189.130",
      "lastLoginIp": "101.32.28.125",
      "identity": "",
      "realname": "",
      "mobile": "mobile",
      "hasPassword": true,
      "canWalletPay": true,
      "walletBalance": "836.91",
      "walletFreeze": "77.00",
      "paid": true,
      "payTime": null,
      "unreadNotifications": 1,
      "typeUnreadNotifications": {
        "liked": 1
      }
    }
  }
}
```
