### 获取关注列表

- **接口说明：** 获取关注列表
- **接口地址：** /api/follow
- **请求方式：** GET

#### 请求参数

| 参数名称         | 类型   | 是否必须 | 描述                              |
| :--------------- | :----- | :------: | :-------------------------------- |
| include          | string |    否    | 关联数据                          |
| filter[type]     | int    |    否    | 类型（1 我的关注 2 我的粉丝）     |
| filter[user_id]  | int    |    否    | 用户 ID，不传默认获取当前登陆用户 |
| filter[username] | string |    否    | 用户名模糊查询                    |
| page[number]     | int    |    否    | 页码                              |
| page[limit]      | int    |    否    | 单页数量                          |
| sort             | string |    否    | 排序，默认为关注时间倒序。关注时间：createdAt，用户的创建时间：users.createdAt。添加标记‘-’为倒序，如：‘-createdAt’；|

#### include 可关联的数据

| 关联名称        | 模型   | 类型   | 是否默认 | 描述                       |
| :-------------- | :----- | :----- | :------: | :------------------------- |
| toUser          | users  | object |    否    | 关注用户，配合 type=1 使用 |
| fromUser        | users  | object |    否    | 粉丝用户，配合 type=2 使用 |
| fromUser.groups | groups | object |    否    | 粉丝的用户组               |
| toUser.groups   | groups | object |    否    | 关注的用户组               |

#### 请求示例

```
/api/follow?filter[type]=1&page[number]=1&page[limit]=10&include=fromUser&sort=createdAt
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 字段名                 | 变量名                 | 必填 | 类型     | 描述                    |
| :--------------------- | :--------------------- | :--- | :------- | :---------------------- |
| **links**              | object                 | 是   | object   | 接口链接                |
| **data.attributes**    | object                 | 是   | object   | 数据属性                |
| 自增 id                | attribute.id           | 是   | string   | 自增 id                 |
| 关注人                 | attribute.from_user_id | 是   | int      | 关注人 uid              |
| 被关注人               | attribute.to_user_id   | 是   | int      | 被关注人 uid            |
| 互相关注               | attribute.is_mutual    | 是   | int      | 是否互相关注：0 否 1 是 |
| 更新时间               | attributes.updated_at  | 是   | datetime | 更新时间                |
| 创建时间               | attributes.created_at  | 是   | datetime | 创建时间                |
| **data.relationships** | object                 | 否   | object   | 关联关系                |
| **included**           | object                 | 否   | object   | 关联数据                |

#### 返回示例

```json
{
  "links": {
    "first": "DummySiteUrl/api/follow?filter%5Btype%5D=1&page%5Blimit%5D=10&include=toUser",
    "last": "DummySiteUrl/api/follow?filter%5Btype%5D=1&page%5Blimit%5D=10&include=toUser"
  },
  "data": [
    {
      "type": "user_follow",
      "id": "37",
      "attributes": {
        "id": 37,
        "from_user_id": 1,
        "to_user_id": 3,
        "is_mutual": 0,
        "updated_at": "2020-02-06T19:52:14+08:00",
        "created_at": "2020-01-10T14:12:49+08:00"
      },
      "relationships": {
        "toUser": {
          "data": {
            "type": "users",
            "id": "3"
          }
        }
      }
    },
    {
      "type": "user_follow",
      "id": "38",
      "attributes": {
        "id": 38,
        "from_user_id": 1,
        "to_user_id": 4,
        "is_mutual": 0,
        "updated_at": "2020-02-06T19:52:14+08:00",
        "created_at": "2020-01-10T14:12:52+08:00"
      },
      "relationships": {
        "toUser": {
          "data": {
            "type": "users",
            "id": "4"
          }
        }
      }
    },
    {
      "type": "user_follow",
      "id": "39",
      "attributes": {
        "id": 39,
        "from_user_id": 1,
        "to_user_id": 5,
        "is_mutual": 0,
        "updated_at": "2020-02-06T19:52:14+08:00",
        "created_at": "2020-01-10T14:12:55+08:00"
      },
      "relationships": {
        "toUser": {
          "data": {
            "type": "users",
            "id": "5"
          }
        }
      }
    },
    {
      "type": "user_follow",
      "id": "56",
      "attributes": {
        "id": 56,
        "from_user_id": 1,
        "to_user_id": 2,
        "is_mutual": 1,
        "updated_at": "2020-02-06T19:52:14+08:00",
        "created_at": "2020-02-06T19:52:14+08:00"
      },
      "relationships": {
        "toUser": {
          "data": {
            "type": "users",
            "id": "2"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "3",
      "attributes": {
        "id": 3,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-20T03:53:54+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-20T03:53:55+08:00",
        "updatedAt": "2019-12-20T03:53:55+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "4",
      "attributes": {
        "id": 4,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-19T15:33:23+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-19T15:33:23+08:00",
        "updatedAt": "2020-01-02T16:37:51+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "5",
      "attributes": {
        "id": 5,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 0,
        "fansCount": 1,
        "follow": null,
        "status": 0,
        "loginAt": null,
        "joinedAt": "2019-12-27T18:19:21+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-27T18:19:21+08:00",
        "updatedAt": "2020-01-20T15:53:11+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "2",
      "attributes": {
        "id": 2,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 1,
        "fansCount": 1,
        "follow": null,
        "status": 0,
        "loginAt": "2020-02-06T19:48:34+08:00",
        "joinedAt": "2019-12-20T03:48:09+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-20T03:48:09+08:00",
        "updatedAt": "2020-02-06T19:52:14+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    }
  ],
  "meta": {
    "total": 4,
    "size": "10"
  }
}
```
