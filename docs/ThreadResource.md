### 查询主题接口[单条]

- **接口说明：** 查询主题接口[单条]
- **接口地址：** /api/threads/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述     |
| :------- | :----: | :------: | :------- |
| include  | string |    否    | 关联数据 |

#### include 可关联的数据

| 关联名称               | 模型         | 类型   | 是否默认 | 描述                     |
| :--------------------- | :----------- | :----- | :------: | :----------------------- |
| user                   | users        | object |    是    | 主题作者                 |
| firstPost              | posts        | object |    是    | 首帖                     |
| threadVideo            | thread-video | object |    是    | 视频                     |
| firstPost.images       | attachments  | array  |    是    | 首贴图片                 |
| firstPost.attachments  | attachments  | array  |    是    | 首贴附件                 |
| firstPost.postGoods    | post_goods   | object |    否    | 商品信息                |
| posts                  | posts        | array  |    是    | 回复列表                 |
| posts.user             | users        | object |    是    | 回复的作者               |
| posts.commentUser      | users        | object |    是    | 回复评论的关联用户  |
| posts.thread           | threads      | object |    是    | 回复所在的主题即当前主题 |
| posts.images           | attachments  | array  |    是    | 回复的图片               |
| user.groups            | groups       | object |    否    | 用户所在群组             |
| category               | categories   | object |    否    | 主题分类                 |
| firstPost.likedUsers   | users        | array  |    否    | 点赞首贴的用户           |
| posts.likedUsers       | users        | array  |    否    | 点赞回复的用户           |
| rewardedUsers          | users        | array  |    否    | 打赏主题的用户           |
| paidUsers              | users        | array  |    否    | 付费主题的已购用户       |
| firstPost.mentionUsers | users        | array  |    否    | 主题@用户                |
| posts.mentionUsers     | users        | array  |    否    | 回复@用户                |
| user.groups.permissionWithoutCategories  | permission  | array  | 否  | 查询用户除了分类的权限列表  |
| question               | question     | object | 否  | 查询问答信息  |
| question.beUser        | question     | object | 否  | 查询被提问人用户信息  |
| question.images        | question     | array  | 否  | 查询回答的图片内容  |
| onlookers              | users        | array  | 否  | 查询围观人  |

#### 请求示例

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                | 类型     | 出现要求   | 描述                                              |
| :---------------------- | :------- | :--------- | :------------------------------------------------ |
| **data**                | object   |            | 基础数据                                          |
| type                    | string   |            | 数据类型                                          |
| id                      | int      |            | 数据 id                                           |
| **attributes**          | object   |            | 数据属性                                          |
| type                    | int      |            | 类型：0 普通 1 长文 2 视频 3 图片 4 语音 5 问答帖 6 商品帖 |
| title                   | string   | 长文主题   | 标题                                              |
| price                   | float    | 长文主题   | 主题价格                                          |
| attachment_price          | float  |          | 附件价格                  |
| viewCount               | int      |            | 查看数                                            |
| postCount               | int      |            | 帖子数                                            |
| paidCount               | int      |            | 付费数                                            |
| rewardedCount           | int      |            | 打赏数                                            |
| createdAt               | datetime |            | 创建时间                                          |
| updatedAt               | datetime |            | 修改时间                                          |
| deletedAt               | datetime | 在回收站时 | 删除时间                                          |
| isApproved              | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isSticky                | bool     |            | 是否置顶                                          |
| isEssence               | bool     |            | 是否加精                                          |
| isFavorite              | bool     | 已收藏时   | 是否收藏                                          |
| isSite            | bool     |            | 是否推荐到站点信息页                                          |
| paid                    | bool     | 付费主题   | 是否付费                                          |
| isPaidAttachment        | bool     | 付费附件   | 附件是否付费                                          |
| canViewPosts            | bool     |            | 是否有权查看详情                                  |
| canReply                | bool     |            | 是否有权回复                                      |
| canApprove              | bool     |            | 是否有权审核                                      |
| canSticky               | bool     |            | 是否有权置顶                                      |
| canEssence              | bool     |            | 是否有权加精                                      |
| canDelete               | bool     |            | 是否有权永久删除                                  |
| canHide                 | bool     |            | 是否有权放入回收站                                |
| canFavorite             | bool     |            | 是否有权收藏                                      |
| isRedPacket | int | | 是否为红包帖，0不是，1是 |
| redPacket | object | | 该帖关联的红包信息 |
| redPacket.id | int | | 红包ID |
| redPacket.thread_id | int | | 关联的threads主键ID |
| redPacket.post_id | int | | 关联的posts主键ID |
| redPacket.rule | int | | 发放规则，0定额，1随机 |
| redPacket.condition | int | | 领取红包条件，0回复，1集赞 |
| redPacket.likenum | int | | 若红包领取条件为集赞，必填集赞数 |
| redPacket.money | float | | 红包总金额 |
| redPacket.number | int | | 红包个数 |
| redPacket.remain_money | float | | 剩余红包总额 |
| redPacket.remain_number | int | | 剩余红包个数 |
| redPacket.status | int | | 0:红包已过期,1:红包未过期，2:红包已领完，3:红包已退还，4:不处理的异常红包 |
| redPacket.created_at | timestamp | | 创建时间 |
| redPacket.updated_at | timestamp | | 更新时间 |
| questionTypeAndMoney | object | | 问答帖相关内容 |
| questionTypeAndMoney.id | int | | 问答扩展表主键ID |
| questionTypeAndMoney.thread_id | int | | 帖子ID |
| questionTypeAndMoney.post_id | int | | 帖子关联的posts表ID |
| questionTypeAndMoney.type | int | | 问答帖类型，0为所有人可回答的悬赏帖，1为指定人问答帖(原有的问答帖) |
| questionTypeAndMoney.user_id | int | | 作者ID |
| questionTypeAndMoney.answer_id | int | | 回答者ID，当为悬赏帖时，这里为空 |
| questionTypeAndMoney.money | float | | 问答金额/悬赏金额 |
| questionTypeAndMoney.remain_money | float | | 剩余金额 |
| questionTypeAndMoney.created_at | datetime | | 创建时间 |
| questionTypeAndMoney.updated_at | datetime | | 更新时间 |
| questionTypeAndMoney.expired_at | datetime | | 过期时间 |
| **relationships**       | object   |            | 关联关系                                          |
| thread-video. status    | int      |            | 视频状态 0:转码中 1 转码完成 2 转码失败           |
| thread-video. reason    | string   |            | 转码失败原因                                      |
| thread-video. file_name | string   |            | 视频文件名称                                      |
| thread-video. file_id   | string   |            | 媒体文件唯一标识                                  |
| thread-video. media_url | string   |            | 媒体播放地址                                      |
| thread-video. cover_url | string   |            | 媒体封面地址                                      |
| **included**            | object   |            | 关联数据                                          |
| question_answer.be_user_id            | int       | | 回答人的用户ID  |
| question_answer.content               | string    | | 回答的内容        |
| question_answer.content_html          | string    | | 回答的html 内容   |
| question_answer.ip                    | string    | | 回答人的IP  |
| question_answer.port                  | int       | | 回答人的端口 |
| question_answer.price                 | float     | | 问答单价  |
| question_answer.onlooker_unit_price   | float     | | 围观单价  |
| question_answer.onlooker_price        | float     | | 围观总价格  |
| question_answer.onlooker_number       | int       | | 围观总人数  |
| question_answer.is_onlooker           | bool      | | 是否允许围观 true允许 false不允许 |
| question_answer.is_answer             | int       | | 是否已回答 0未回答 1已回答 2已过期 |
| question_answer.expired_at            | string    | | 问答过期时间  |

#### 返回示例

```json
{
  "data": {
    "type": "threads",
    "id": "25",
    "attributes": {
      "type": 2,
      "title": "",
      "price": "0.00",
      "attachment_price": "0.00",
      "viewCount": 1,
      "postCount": 1,
      "createdAt": "2020-03-10T16:31:54+08:00",
      "updatedAt": "2020-03-10T16:31:54+08:00",
      "isApproved": 1,
      "isSticky": false,
      "isEssence": false,
      "canViewPosts": true,
      "canReply": false,
      "canApprove": false,
      "canSticky": false,
      "canEssence": false,
      "canDelete": false,
      "canHide": false,
      "canFavorite": false
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "3"
        }
      },
      "firstPost": {
        "data": {
          "type": "posts",
          "id": "23"
        }
      },
      "threadVideo": {
        "data": {
          "type": "thread-video",
          "id": "18"
        }
      },
      "posts": {
        "data": []
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "3",
      "attributes": {
        "id": 3,
        "username": "username",
        "avatarUrl": "",
        "threadCount": 19,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": "2020-03-10T11:20:42+08:00",
        "joinedAt": "2020-03-09T16:39:28+08:00",
        "expiredAt": null,
        "createdAt": "2020-03-09T16:39:28+08:00",
        "updatedAt": "2020-03-10T17:03:44+08:00",
        "canEdit": false,
        "canDelete": false,
        "registerReason": "",
        "banReason": ""
      }
    },
    {
      "type": "posts",
      "id": "23",
      "attributes": {
        "replyUserId": null,
        "content": "abc",
        "contentHtml": "abc",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-03-10T16:31:54+08:00",
        "updatedAt": "2020-03-10T16:31:54+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canEdit": false,
        "canApprove": false,
        "canDelete": false,
        "canHide": false,
        "canLike": false
      },
      "relationships": {
        "images": {
          "data": []
        },
        "attachments": {
          "data": []
        }
      }
    },
    {
      "type": "thread-video",
      "id": "18",
      "attributes": {
        "id": 18,
        "user_id": 3,
        "thread_id": 25,
        "status": 1,
        "reason": "source file err: file info lack width, height or color space",
        "file_name": "666",
        "file_id": "666",
        "media_url": "url",
        "cover_url": "url",
        "updated_at": "2020-03-10T18:26:31+08:00",
        "created_at": "2020-03-10T16:31:54+08:00"
      }
    }
  ]
}
```
