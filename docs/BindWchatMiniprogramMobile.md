### 获取微信小程序手机号绑定接口

- **接口说明：** 获取微信小程序手机号绑定接口
- **接口地址：** /api/mobile/bind/miniprogram
- **请求方式：** POST

#### 请求参数

| 参数名称                       |  类型  | 是否必须 | 描述                    |
| :----------------------------- | :----: | :------: | :---------------------- |
| **data**                       | object |    是    | 基础数据                |
| **attributes**                 | object |    是    | 数据属性                |
| attributes. js_code | string |    是    |  登录时获取的 code  https://developers.weixin.qq.com/miniprogram/dev/dev_wxwork/dev-doc/qywx-api/login/code2session.html               |
| attributes. iv      | string |    是    | 加密算法的初始向量   https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/getPhoneNumber.html   |
| attributes. encryptedData       | string |    否    | 加密数据  https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/getPhoneNumber.html              |

#### 请求示例

```json
{
  "data": {    
  	"attributes": {
      "js_code": "xx",
      "iv": "xx",
      "encryptedData": "hello"
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500
详见- [语言包说明](LanguagePack.md)
#### 返回结果

| 参数名称               | 类型     | 出现要求 | 描述        |
| :--------------------- | :------- | :------- | :---------- |
| **data**               | object   |          | 基础数据    |
| type                   | string   |          | 数据类型    |
| id                     | int      |          | 数据 id     |
| **attributes**         | object   |          | 数据属性    |
| attributes.username    | string   |          | 用户名      |
| attributes.nickname    | string   |          | 昵称        |
| attributes.mobile      | string   |          | 手机号      |
| attributes.unionId     | string   |          | union_id    |
| attributes.lastLoginIp | string   |          | 最后登陆 IP |
| attributes.createdAt   | datetime |          | 创建时间    |
| attributes.updatedAt   | datetime |          | 修改时间    |


