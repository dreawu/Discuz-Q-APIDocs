### 获取资金概况

- **接口说明：** 获取资金概况
- **接口地址：** /api/statistic/finance
- **请求方式：** GET

#### 请求参数

无

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名              | 变量名                         | 必填 | 类型   | 描述                                                                                               |
| :------------------ | :----------------------------- | :--- | :----- | :------------------------------------------------------------------------------------------------- |
| **data.attributes** | object                         | 是   | object | 数据属性                                                                                           |
| 用户总充值          | attributes.totalIncome         | 是   | float  | 用户通过支付接口，充值进平台的总金额，不考虑充值手续费（目前是 注册加入 和 打赏时 产生充值 ）      |
| 用户总提现          | attributes.totalWithdrawal     | 是   | float  | 用户通过提现功能，从钱包提现到个人账户的成功总金额，以用户发起提现的金额计算，不考虑用户提现手续费 |
| 用户钱包总金额      | attributes.totalWallet         | 是   | float  | 所有用户的 可提现 + 冻结中 的金额总数                                                              |
| 平台总盈利          | attributes.totalProfit         | 是   | float  | 注册加入收入+打赏提成收入+提现手续费收入                                                           |
| 提现手续费收入      | attributes.withdrawalProfit    | 是   | float  | (用户总提现 \* 提现手续费百分比)                                                                   |
| 打赏提成收入        | attributes.orderRoyalty        | 是   | float  | 打赏订单给平台分成的收入之和                                                                       |
| 注册加入收入        | attributes.totalRegisterProfit | 是   | float  | 注册加入平台，支付给平台的收入之和                                                                 |
| 用户订单总数        | attributes.orderCount          | 是   | int    | 所有交易订单的总数量                                                                               |

示例：

```json
{
  "data": {
    "type": "finance_statistic",
    "id": "1",
    "attributes": {
      "totalIncome": "691.08",
      "totalWithdrawal": "202.00",
      "totalWallet": "3,807.53",
      "totalProfit": "394.96",
      "withdrawalProfit": "60.60",
      "orderRoyalty": "334.36",
      "totalRegisterProfit": "0.36",
      "orderCount": 648
    }
  }
}
```
