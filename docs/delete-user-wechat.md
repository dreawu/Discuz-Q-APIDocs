### 删除用户微信信息

- **权限：** 仅自己
- **接口说明：** 删除用户微信信息
- **接口地址：** /api/users/{id}/wechat
- **请求方式：** DELETE

#### 请求参数

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |


#### 请求示例

```php

```

#### 返回结果

| 参数名称    | 类型   | 描述        |
| :---------- | :----- | :---------- |
| id          | int    | 用户 ID     |
| username    | string | 用户名      |
| mobile      | string | 手机号      |
| avatarUrl   | string | 头像        |
| threadCount | int    | 帖子数      |
| registerIp  | string | 注册 IP     |
| lastLoginIp | string | 最后登录 IP |
| createdAt   | date   | 注册时间    |

#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 非 200

#### 返回示例

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "id": 1,
      "username": "username",
      "mobile": "mobile",
      "avatarUrl": "",
      "threadCount": 0,
      "registerIp": "",
      "lastLoginIp": "",
      "createdAt": "2019-11-19T15:25:31+08:00",
      "updatedAt": "2019-11-26T10:09:40+08:00"
    }
  }
}
```
