### 查询我的草稿箱接口[列表]

- **接口说明：** 查询我的草稿箱[列表]
- **接口地址：** /api/thread/draft
- **请求方式：** GET

#### 请求参数

| 参数名称               | 类型     | 是否必须 | 描述                                                         |
| :--------------------- | :------- | :------: | :----------------------------------------------------------- |
| page[number]           | int      |    是    | 分页数                                                     |
| page[limit]            | int      |    是    | 数量                                                       |


#### 请求示例

```
/api/thread/draft?page[number]=1&page[limit]=10

```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                            | 类型     | 出现要求   | 描述                                                         |
| :---------------------------------- | :------- | :--------- | :----------------------------------------------------------- |
| **links**                           | object   |            | 接口链接                                                     |
| **data**                            | object   |            | 基础数据                                                     |
| type                                | string   |            | 数据类型                                                     |
| id                                  | int      |            | 数据 id                                                      |
| **attributes**                      | object   |            | 数据属性                                                     |
| type                                | int      |            | 文章类型**0普通；1长文；2视频；3图片；4音频；5问答；6商品; 7专辑** |
| title                               | string   | 长文主题   | 标题                                                         |
| price                               | float    | 长文主题   | 主题价格                                                     |
| attachment_price                    | float    |            | 附件价格                                                     |
| freeWords                           | float    |            | 附件价格                                                     |
| viewCount                           | int      |            | 查看数                                                       |
| postCount                           | int      |            | 帖子数                                                       |
| paidCount                           | int      |            | 付费数                                                       |
| rewardedCount                       | int      |            | 打赏数                                                       |
| createdAt                           | datetime |            | 创建时间                                                     |
| updatedAt                           | datetime |            | 修改时间                                                     |
| deletedAt                           | datetime | 在回收站时 | 删除时间                                                     |
| isApproved                          | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略            |
| isSticky                            | bool     |            | 是否置顶                                                     |
| isEssence                           | bool     |            | 是否加精                                                     |
| isFavorite                          | bool     | 已收藏时   | 是否收藏                                                     |
| isSite                              | bool     |            | 是否推荐到站点信息页                                         |
| paid                                | bool     | 付费主题   | 是否付费                                                     |
| isPaidAttachment                    | bool     | 付费附件   | 附件是否付费                                                 |
| canViewPosts                        | bool     |            | 是否有权查看详情                                             |
| canReply                            | bool     |            | 是否有权回复                                                 |
| canApprove                          | bool     |            | 是否有权审核                                                 |
| canSticky                           | bool     |            | 是否有权置顶                                                 |
| canEssence                          | bool     |            | 是否有权加精                                                 |
| canDelete                           | bool     |            | 是否有权永久删除                                             |
| canHide                             | bool     |            | 是否有权放入回收站                                           |
| canFavorite                         | bool     |            | 是否有权收藏                                                 |
| isRedPacket                         | int      |            | 是否为红包帖，0不是，1是                                     |
| postContent                         | string   |            | 帖子的内容字段，备用                                         |
| album_content                       | object   |            | 专辑帖的内容，存放的是专辑帖包含的主题的标题，该字段用于后台-内容管理-列表展示 |
| isDraft                             | int   |               | 是否草稿箱 (0/1) 是否草稿箱  |
| **questionTypeAndMoney**            | object   |            | 悬赏/问答帖信息                                              |
| id                                  | int      |            | 悬赏问答扩展表主键ID                                         |
| thread_id                           | int      |            | 悬赏/问答帖ID                                                |
| post_id                             | int      |            | 悬赏/问答帖主要内容ID                                        |
| type                                | int      |            | 类型，0为所有人可回答的悬赏帖，1为指定人回答的问答帖         |
| answer_id                           | int      |            | 被指定回答人的ID，可为空                                     |
| money                               | float    |            | 悬赏金额/问答金额                                            |
| remain_money                        | float    |            | 剩余金额                                                     |
| created_at                          | datetime |            | 创建时间                                                     |
| updated_at                          | datetime |            | 更新时间                                                     |
| expired_at                          | datetime |            | 过期时间                                                     |
| **relationships**                   | object   |            | 关联关系                                                     |
| **included**                        | object   |            | 关联数据                                                     |
| question_answer.be_user_id          | int      |            | 回答人的用户 ID                                              |
| question_answer.content             | string   |            | 回答的内容                                                   |
| question_answer.content_html        | string   |            | 回答的 html 内容                                             |
| question_answer.ip                  | string   |            | 回答人的 IP                                                  |
| question_answer.port                | int      |            | 回答人的端口                                                 |
| question_answer.price               | float    |            | 问答单价                                                     |
| question_answer.onlooker_unit_price | float    |            | 围观单价                                                     |
| question_answer.onlooker_price      | float    |            | 围观总价格                                                   |
| question_answer.onlooker_number     | int      |            | 围观总人数                                                   |
| question_answer.is_onlooker         | bool     |            | 是否允许围观 true 允许 false 不允许                          |
| question_answer.is_answer           | int      |            | 是否已回答 0 未回答 1 已回答 2 已过期                        |
| question_answer.expired_at          | string   |            | 问答过期时间                                                 |

#### 返回示例

```json
{
    "links": {
        "first": "http://172.16.1.34/api/threads?page%5Blimit%5D=10",
        "next": "http://172.16.1.34/api/threads?page%5Bnumber%5D=2&page%5Blimit%5D=10",
        "last": "http://172.16.1.34/api/threads?page%5Bnumber%5D=2&page%5Blimit%5D=10"
    },
    "data": [
        {
            "type": "threads",
            "id": "1243",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 4,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:57:42+08:00",
                "updatedAt": "2021-01-20T11:57:42+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 0,
                "redPacket": null,
                "postContent": "12121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2103"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "68"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1243",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 4,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:57:42+08:00",
                "updatedAt": "2021-01-20T11:57:42+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 0,
                "redPacket": null,
                "postContent": "12121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2103"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "68"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1243",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 4,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:57:42+08:00",
                "updatedAt": "2021-01-20T11:57:42+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 0,
                "redPacket": null,
                "postContent": "12121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2103"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "68"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1243",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 4,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:57:42+08:00",
                "updatedAt": "2021-01-20T11:57:42+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 0,
                "redPacket": null,
                "postContent": "12121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2103"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "68"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1241",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:43:16+08:00",
                "updatedAt": "2021-01-20T11:43:16+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 1,
                "redPacket": {
                    "id": 450,
                    "thread_id": 1241,
                    "post_id": 2093,
                    "rule": 1,
                    "condition": 0,
                    "likenum": 0,
                    "money": "1.00",
                    "number": 1,
                    "remain_money": "0.00",
                    "remain_number": 0,
                    "status": 0,
                    "created_at": "2021-01-20T03:42:52.000000Z",
                    "updated_at": "2021-01-28T17:17:13.000000Z"
                },
                "postContent": "212121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2093"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "65"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1241",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:43:16+08:00",
                "updatedAt": "2021-01-20T11:43:16+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 1,
                "redPacket": {
                    "id": 450,
                    "thread_id": 1241,
                    "post_id": 2093,
                    "rule": 1,
                    "condition": 0,
                    "likenum": 0,
                    "money": "1.00",
                    "number": 1,
                    "remain_money": "0.00",
                    "remain_number": 0,
                    "status": 0,
                    "created_at": "2021-01-20T03:42:52.000000Z",
                    "updated_at": "2021-01-28T17:17:13.000000Z"
                },
                "postContent": "212121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2093"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "65"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1241",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:43:16+08:00",
                "updatedAt": "2021-01-20T11:43:16+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 1,
                "redPacket": {
                    "id": 450,
                    "thread_id": 1241,
                    "post_id": 2093,
                    "rule": 1,
                    "condition": 0,
                    "likenum": 0,
                    "money": "1.00",
                    "number": 1,
                    "remain_money": "0.00",
                    "remain_number": 0,
                    "status": 0,
                    "created_at": "2021-01-20T03:42:52.000000Z",
                    "updated_at": "2021-01-28T17:17:13.000000Z"
                },
                "postContent": "212121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2093"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "65"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1241",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:43:16+08:00",
                "updatedAt": "2021-01-20T11:43:16+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 1,
                "redPacket": {
                    "id": 450,
                    "thread_id": 1241,
                    "post_id": 2093,
                    "rule": 1,
                    "condition": 0,
                    "likenum": 0,
                    "money": "1.00",
                    "number": 1,
                    "remain_money": "0.00",
                    "remain_number": 0,
                    "status": 0,
                    "created_at": "2021-01-20T03:42:52.000000Z",
                    "updated_at": "2021-01-28T17:17:13.000000Z"
                },
                "postContent": "212121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2093"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "65"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1241",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:43:16+08:00",
                "updatedAt": "2021-01-20T11:43:16+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 1,
                "redPacket": {
                    "id": 450,
                    "thread_id": 1241,
                    "post_id": 2093,
                    "rule": 1,
                    "condition": 0,
                    "likenum": 0,
                    "money": "1.00",
                    "number": 1,
                    "remain_money": "0.00",
                    "remain_number": 0,
                    "status": 0,
                    "created_at": "2021-01-20T03:42:52.000000Z",
                    "updated_at": "2021-01-28T17:17:13.000000Z"
                },
                "postContent": "212121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2093"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "65"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "1241",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2021-01-20T11:43:16+08:00",
                "updatedAt": "2021-01-20T11:43:16+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "isDraft": true,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": true,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": true,
                "isRedPacket": 1,
                "redPacket": {
                    "id": 450,
                    "thread_id": 1241,
                    "post_id": 2093,
                    "rule": 1,
                    "condition": 0,
                    "likenum": 0,
                    "money": "1.00",
                    "number": 1,
                    "remain_money": "0.00",
                    "remain_number": 0,
                    "status": 0,
                    "created_at": "2021-01-20T03:42:52.000000Z",
                    "updated_at": "2021-01-28T17:17:13.000000Z"
                },
                "postContent": "212121",
                "questionTypeAndMoney": null,
                "isDeleted": false,
                "canFavorite": true,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "2093"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "65"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "41"
                    }
                }
            }
        }
    ],
    "included": [
        {
            "type": "posts",
            "id": "2103",
            "attributes": {
                "replyPostId": null,
                "replyUserId": null,
                "commentPostId": null,
                "commentUserId": null,
                "summary": "<p>12121</p>",
                "summaryText": "12121",
                "content": "12121",
                "contentHtml": "<p>12121</p>",
                "replyCount": 0,
                "likeCount": 0,
                "createdAt": "2021-01-20T11:44:46+08:00",
                "updatedAt": "2021-01-20T11:44:46+08:00",
                "isApproved": 1,
                "canEdit": true,
                "canApprove": false,
                "canDelete": false,
                "canHide": false,
                "contentAttachIds": [],
                "ip": "192.168.2.20",
                "port": 49231,
                "isDeleted": false,
                "isFirst": true,
                "isComment": false,
                "rewards": null,
                "redPacketAmount": 0,
                "canLike": true,
                "isLiked": false
            }
        },
        {
            "type": "posts",
            "id": "2093",
            "attributes": {
                "replyPostId": null,
                "replyUserId": null,
                "commentPostId": null,
                "commentUserId": null,
                "summary": "<p>212121</p>",
                "summaryText": "212121",
                "content": "212121",
                "contentHtml": "<p>212121</p>",
                "replyCount": 0,
                "likeCount": 0,
                "createdAt": "2021-01-20T11:42:52+08:00",
                "updatedAt": "2021-01-20T11:42:52+08:00",
                "isApproved": 1,
                "canEdit": true,
                "canApprove": false,
                "canDelete": false,
                "canHide": false,
                "contentAttachIds": [],
                "ip": "192.168.2.20",
                "port": 65377,
                "isDeleted": false,
                "isFirst": true,
                "isComment": false,
                "rewards": null,
                "redPacketAmount": 0,
                "canLike": true,
                "isLiked": false
            }
        },
        {
            "type": "users",
            "id": "41",
            "attributes": {
                "id": 41,
                "username": "a456",
                "avatarUrl": "",
                "isReal": false,
                "threadCount": 14,
                "followCount": 0,
                "fansCount": 0,
                "likedCount": 0,
                "questionCount": 2,
                "signature": "",
                "usernameBout": 0,
                "status": 0,
                "loginAt": "2021-01-31T17:22:14+08:00",
                "joinedAt": "2021-01-16T10:52:48+08:00",
                "expiredAt": null,
                "createdAt": "2021-01-16T10:52:48+08:00",
                "updatedAt": "2021-01-31T17:52:14+08:00",
                "canEdit": false,
                "canDelete": false,
                "showGroups": false,
                "registerReason": "",
                "banReason": "",
                "denyStatus": false,
                "canBeAsked": false,
                "originalMobile": "",
                "registerIp": "192.168.2.20",
                "registerPort": 64885,
                "lastLoginIp": "192.168.2.20",
                "lastLoginPort": 62200,
                "identity": "",
                "realname": "",
                "mobile": "",
                "hasPassword": true,
                "canWalletPay": false,
                "walletBalance": "10.80",
                "walletFreeze": "0.00",
                "canEditUsername": true
            }
        },
        {
            "type": "categories",
            "id": "68",
            "attributes": {
                "name": "PC",
                "description": "",
                "icon": "",
                "sort": 0,
                "parentid": 67,
                "property": 0,
                "thread_count": 6,
                "ip": "192.168.3.178",
                "created_at": "2021-01-18T11:20:05+08:00",
                "updated_at": "2021-01-26T15:17:48+08:00",
                "canCreateThread": false,
                "checked": 1,
                "search_ids": "68",
                "children": []
            }
        },
        {
            "type": "categories",
            "id": "65",
            "attributes": {
                "name": "测试排序",
                "description": "奥术大师多撒多撒",
                "icon": "",
                "sort": 0,
                "parentid": 0,
                "property": 0,
                "thread_count": 330,
                "ip": "192.168.2.197",
                "created_at": "2021-01-13T21:15:09+08:00",
                "updated_at": "2021-01-31T17:50:42+08:00",
                "canCreateThread": true,
                "checked": 1,
                "search_ids": "65,77,89",
                "children": [
                    {
                        "id": 77,
                        "name": "测试 1",
                        "description": "",
                        "icon": "",
                        "sort": 1,
                        "property": 0,
                        "thread_count": 18,
                        "moderators": [
                            ""
                        ],
                        "ip": "192.168.2.203",
                        "parentid": 65,
                        "created_at": "2021-01-24T08:18:12.000000Z",
                        "updated_at": "2021-01-30T16:49:07.000000Z",
                        "canCreateThread": false,
                        "search_ids": 77,
                        "checked": 0
                    },
                    {
                        "id": 89,
                        "name": "测试2",
                        "description": "",
                        "icon": "",
                        "sort": 2,
                        "property": 0,
                        "thread_count": 24,
                        "moderators": [
                            ""
                        ],
                        "ip": "192.168.2.203",
                        "parentid": 65,
                        "created_at": "2021-01-24T08:46:13.000000Z",
                        "updated_at": "2021-01-30T16:45:21.000000Z",
                        "canCreateThread": false,
                        "search_ids": 89,
                        "checked": 0
                    }
                ]
            }
        }
    ],
    "meta": {
        "threadCount": 11,
        "pageCount": 2
    }
}
```
