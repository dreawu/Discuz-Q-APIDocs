### 获取话题列表

- **接口说明：** 获取话题列表
- **接口地址：** /api/topics
- **请求方式：** GET

#### 请求参数

| 参数名称        | 类型   | 是否必须 | 描述                 |
| :-------------- | :----- | :------: | :------------------- |
| include         | string |    否    | 关联数据             |
| filter[content] | string |    否    | 话题内容（模糊查询） |
| filter[username] | string |    否    | 话题作者 |
| filter[recommended] | int |    否    | 是否推荐(0未推荐,1推荐) |
| filter[createdAtBegin] | datetime |    否    | 话题创建时间范围-开始 |
| filter[createdAtEnd] | datetime |    否    | 话题创建时间范围-截止 |
| filter[threadCountBegin] | int |    否    | 话题主题数范围-起始 |
| filter[threadCountEnd] | int |    否    | 话题主题数范围-截止 |
| filter[viewCountBegin] | int |    否    | 话题热度范围-起始 |
| filter[viewCountEnd] | int |    否    | 话题热度范围-截止 |
| page[number]    | int    |    否    | 页码                 |
| page[limit]     | int    |    否    | 单页数量             |
| sort            | string |    否    | 默认按照创建时间倒序，可选值：主题量threadCount、浏览量viewCount、创建时间createdAt、是否推荐recommended、推荐时间recommended_at。添加标记‘-’为倒序，如：‘-threadCount’；             |

#### include 可关联的数据

| 关联名称 | 模型  | 类型   | 是否默认 | 描述       |
| :------- | :---- | :----- | :------: | :--------- |
| user                            | users | object |    否    | 话题创建人 |
| lastThread                      | threads | object |    否    | 最新主题 |
| lastThread.firstPost            | posts | object |    否    | 最新主题的首帖 |
| lastThread.firstPost.images     | attachments | object |    否    | 最新主题首帖的图片 |

#### 请求示例

```
/api/topics?filter[content]=1&include=user&page[number]=1&page[limit]=10&sort=-viewCount
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 字段名                 | 变量名                | 类型     | 描述           |
| :--------------------- | :-------------------- | :------- | :------------- |
| **links**              | object                | object   | 接口链接       |
| **data.attributes**    | object                | object   | 数据属性       |
| 自增 id                | attribute.id          | string   | 自增 id        |
| 创建人                 | attribute.user_id     | int      | 创建人 user_id |
| 是否推荐               | attribute.recommended | int      | 是否推荐 |
| 话题内容               | attribute.content     | string   | 话题内容       |
| 更新时间               | attributes.updated_at | datetime | 更新时间       |
| 创建时间               | attributes.created_at | datetime | 创建时间       |
| **data.relationships** | object                | object   | 关联关系       |
| **included**           | object                | object   | 关联数据       |

#### 返回示例

```json
{
  "links": {
    "first": "DummySiteUrl/api/topics?filter%5Bcontent%5D=1&include=user&page%5Blimit%5D=10",
    "last": "DummySiteUrl/api/topics?filter%5Bcontent%5D=1&include=user&page%5Blimit%5D=10"
  },
  "data": [
    {
      "type": "topics",
      "id": "1",
      "attributes": {
        "id": 1,
        "user_id": 3,
        "content": "话题1",
        "recommended": 1,
        "updated_at": "2020-04-24T14:44:05+08:00",
        "created_at": "2020-04-24T14:44:07+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "3"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "3",
      "attributes": {
        "id": 3,
        "username": "user",
        "avatarUrl": "",
        "isReal": false,
        "threadCount": 18,
        "followCount": 1,
        "fansCount": 0,
        "likedCount": 2,
        "signature": "",
        "usernameBout": 0,
        "follow": 0,
        "status": 0,
        "loginAt": "2020-04-23T17:06:18+08:00",
        "joinedAt": "2020-03-09T16:39:28+08:00",
        "expiredAt": null,
        "createdAt": "2020-03-09T16:39:28+08:00",
        "updatedAt": "2020-04-23T18:02:36+08:00",
        "canEdit": false,
        "canDelete": false,
        "showGroups": true,
        "registerReason": "",
        "banReason": ""
      }
    }
  ],
  "meta": {
    "total": 1,
    "pageCount": 1
  }
}
```
