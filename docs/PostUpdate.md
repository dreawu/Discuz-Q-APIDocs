### 修改回复接口[单条]

- **接口说明：** 修改回复[单条]
- **接口地址：** /api/posts/{id}
- **请求方式：** PATCH

#### 请求参数

| 参数名称   |  类型  | 是否必须 | 描述                                              | 示例 |
| :--------- | :----: | :------: | :------------------------------------------------ | :--- |
| content    | string |    否    | 内容                                              |
| isApproved |  int   |    否    | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isDeleted  |  bool  |    否    | 是否删除（回收站）                                |
| isLiked    |  bool  |    否    | 是否喜欢                                          |
| message    | string |    否    | 操作原因                                          |

#### 请求示例

```json
{
  "data": {
    "type": "posts",
    "attributes": {
      "isApproved": 0,
      "isDeleted": 0,
      "isLiked": 1
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称               | 类型     | 出现要求   | 描述                                              |
| :--------------------- | :------- | :--------- | :------------------------------------------------ |
| **data**               | object   |            | 基础数据                                          |
| type                   | string   |            | 数据类型                                          |
| id                     | int      |            | 数据 id                                           |
| **attributes**         | object   |            | 数据属性                                          |
| attributes.summary     | string   |            | 摘要                                              |
| attributes.content     | string   |            | 内容                                              |
| attributes.contentHtml | string   |            | html 内容                                         |
| attributes.ip          | string   |            | 发布 ip                                           |
| attributes.replyCount  | int      |            | 回复数                                            |
| attributes.likeCount   | int      |            | 喜欢数                                            |
| attributes.createdAt   | datetime |            | 创建时间                                          |
| attributes.updatedAt   | datetime |            | 修改时间                                          |
| attributes.deletedAt   | datetime | 在回收站时 | 删除时间                                          |
| attributes.isFirst     | bool     |            | 是否首帖                                          |
| attributes.isApproved  | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| attributes.isLiked     | bool     |            | 是否喜欢                                          |
| **relationships**      | object   |            | 关联关系                                          |
| **included**           | object   |            | 关联数据                                          |

#### 返回示例

```json
{
    "data": {
        "type": "Posts",
        "id": "24",
        "attributes": {
            "id": 24,
            "user_id": 2,
            "thread_id": 4,
            "reply_id": 0,
            "content": "Ruike & modity is coming!!!",
            "ip": "127.0.0.1",
            "reply_count": 0,
            "like_count": 0,
            "created_at": "2019-10-18T14:14:10+08:00",
            "updated_at": "2019-10-24T17:01:41+08:00",
            "deleted_at": null,
            "deleted_user_id": 0,
            "is_first": false,
            "is_approved": true
        }
    }
}{
    "data": {
        "type": "Posts",
        "id": "19",
        "attributes": {
            "content": "4th generation back up implementation RAM == azure == Lake Garden",
            "ip": "127.0.0.1",
            "replyCount": 0,
            "likeCount": 0,
            "createdAt": "2019-11-12T17:10:51+08:00",
            "updatedAt": "2019-11-12T17:10:51+08:00",
            "isFirst": true,
            "isApproved": false,
            "isLiked": true
        },
        "relationships": {
            "thread": {
                "data": {
                    "type": "Threads",
                    "id": "19"
                }
            }
        }
    },
    "included": [
        {
            "type": "Threads",
            "id": "19",
            "attributes": {
                "title": "",
                "price": "0.00",
                "viewCount": 0,
                "postCount": 1,
                "likeCount": 0,
                "createdAt": "2019-11-12T17:10:51+08:00",
                "updatedAt": "2019-11-12T17:10:51+08:00",
                "isApproved": true,
                "isSticky": false,
                "isEssence": false,
                "isFavorite": false
            }
        }
    ]
}
```
