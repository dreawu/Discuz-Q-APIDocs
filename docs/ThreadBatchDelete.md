### 删除主题接口[批量]

- **接口说明：** 删除主题[批量]
- **接口地址：** /api/threads/batch/{ids}
- **请求方式：** DELETE

#### 请求参数

#### 请求示例

```
/api/threads/batch/21,22,23
```

#### 返回说明

- http 状态码 200

#### 返回结果

```
data  被删除的主题列表
meta  出现异常的主题列表
```

#### 返回示例

```json
{
  "data": [
    {
      "type": "threads",
      "id": "11",
      "attributes": {
        "title": "",
        "price": "0.00",
        "viewCount": 0,
        "postCount": 1,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:45+08:00",
        "updatedAt": "2019-11-12T17:10:45+08:00",
        "isApproved": true,
        "isSticky": false,
        "isEssence": false,
        "canFavorite": true
      }
    },
    {
      "type": "threads",
      "id": "21",
      "attributes": {
        "title": "",
        "price": "0.00",
        "viewCount": 0,
        "postCount": 1,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:53+08:00",
        "updatedAt": "2019-11-12T17:10:53+08:00",
        "isApproved": true,
        "isSticky": false,
        "isEssence": false,
        "canFavorite": true
      }
    }
  ],
  "meta": [
    {
      "id": "11",
      "message": "No query results for model [App\\Models\\Post] 1000"
    },
    {
      "id": "21",
      "message": "No query results for model [App\\Models\\Post] 1000"
    }
  ]
}
```
