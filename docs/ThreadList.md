### 查询主题接口[列表]

- **接口说明：** 查询主题[列表]
- **接口地址：** /api/threads
- **请求方式：** GET

#### 请求参数

| 参数名称               | 类型     | 是否必须 | 描述                                                         |
| :--------------------- | :------- | :------: | :----------------------------------------------------------- |
| include                | string   |    否    | 关联数据                                                     |
| filter[q]              | string   |    否    | 关键词                                                       |
| filter[userId]         | int      |    否    | 作者 id                                                      |
| filter[username]       | string   |    否    | 作者用户名                                                   |
| filter[categoryId]     | int      |    否    | 分类 id                                                      |
| filter[createdAtBegin] | datetime |    否    | 发表时间大于                                                 |
| filter[createdAtEnd]   | datetime |    否    | 发表时间小于                                                 |
| filter[viewCountGt]    | int      |    否    | 浏览次数大于                                                 |
| filter[viewCountLt]    | int      |    否    | 浏览次数小于                                                 |
| filter[postCountGt]    | int      |    否    | 回复数大于                                                   |
| filter[postCountLt]    | int      |    否    | 回复数小于                                                   |
| filter[isEssence]      | string   |    否    | 是否精华（yes/no）                                           |
| filter[isSticky]       | string   |    否    | 是否置顶（yes/no）                                           |
| filter[isApproved]     | string   |    否    | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略            |
| filter[isDeleted]      | string   |    否    | 是否删除（yes/no）                                           |
| filter[likedLimit]     | int      |    否    | 点赞首帖的用户返回数量                                       |
| filter[rewardedLimit]  | int      |    否    | 打赏主题的用户返回数量                                       |
| filter[paidLimit]      | int      |    否    | 付费主题的已购用户返回数量                                   |
| filter[highlight]      | string   |    否    | 是否高亮敏感词（yes/no）                                     |
| filter[fromUserId]     | int      |    否    | 关注人 userId（会验证是否为当前登录用户）                    |
| filter[type]           | int      |    否    | 类型：0 普通 1 长文 2 视频 3 图片 4 语音 5 问答帖 6 商品帖   |
| filter[topicId]        | int      |    否    | 话题 ID                                                      |
| filter[location]       | string   |    否    | 附近的帖，格式：经度,纬度,[距离]（距离可不传，默认 5km，如 116.397469,39.908821） |
| filter[isSite]         | string   |    否    | 是否推荐到站点信息页（yes/no）                               |
| filter[isDisplay]      | string   |    否    | 是否展示允许显示的帖子（yes/no）                             |
| filter[answer]         | string   |    否    | 筛选某用户问答列表时，是否显示该用户被提问的问答帖（yes/no） |
| filter[isSort]         | int      |    是    | 是否为智能排序首页的请求，0否，1是                           |

#### include 可关联的数据

| 关联名称                 | 模型           | 类型   | 是否默认 | 描述                       |
| :----------------------- | :------------- | :----- | :------: | :------------------------- |
| user                     | users          | object |    是    | 发表用户                   |
| firstPost                | posts          | object |    是    | 首帖                       |
| threadVideo              | thread-video   | object |    是    | 视频                       |
| lastPostedUser           | users          | object |    是    | 最后回复用户               |
| category                 | categories     | object |    是    | 主题分类                   |
| user.groups              | groups         | object |    否    | 用户所在群组               |
| deletedUser              | users          | object |    否    | 删除用户                   |
| firstPost.images         | attachments    | array  |    否    | 首贴图片                   |
| firstPost.likedUsers     | users          | array  |    否    | 点赞首帖的用户             |
| firstPost.postGoods      | post_goods     | object |    否    | 商品信息                   |
| lastThreePosts           | posts          | array  |    否    | 最后三条回复               |
| lastThreePosts.user      | users          | object |    否    | 最后三条回复的作者         |
| lastThreePosts.replyUser | users          | object |    否    | 最后三条回复所回复的用户   |
| lastThreePosts.commentUser  | users       | object |    否    | 最后三条回复评论用户信息  |
| rewardedUsers            | users          | array  |    否    | 打赏主题的用户             |
| paidUsers                | users          | array  |    否    | 付费主题的已购用户         |
| lastDeletedLog           | operation-logs | object |    否    | 最后一次被删除的操作日志   |
| topic                    | topics         | object |    否    | 主题话题                   |
| question                 | question       | object |    否    | 问答内容                   |
| question.beUser          | question       | object |    否    | 关联问答内容的被回答人信息 |

#### 请求示例

```
/api/threads?include=user,firstPost,lastThreePosts,lastThreePosts.user&page[number]=1
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                            | 类型     | 出现要求   | 描述                                                 |
| :---------------------------------- | :------- | :--------- | :--------------------------------------------------- |
| **links**                           | object   |            | 接口链接                                             |
| **data**                            | object   |            | 基础数据                                             |
| type                                | string   |            | 数据类型                                             |
| id                                  | int      |            | 数据 id                                              |
| **attributes**                      | object   |            | 数据属性                                             |
| type                                | int      |            | 文章类型(0 普通 1 长文 2 视频)                       |
| title                               | string   | 长文主题   | 标题                                                 |
| price                               | float    | 长文主题   | 主题价格                                             |
| attachment_price                    | float    |            | 附件价格                                             |
| viewCount                           | int      |            | 查看数                                               |
| postCount                           | int      |            | 帖子数                                               |
| paidCount                           | int      |            | 付费数                                               |
| rewardedCount                       | int      |            | 打赏数                                               |
| createdAt                           | datetime |            | 创建时间                                             |
| updatedAt                           | datetime |            | 修改时间                                             |
| deletedAt                           | datetime | 在回收站时 | 删除时间                                             |
| isApproved                          | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略    |
| isSticky                            | bool     |            | 是否置顶                                             |
| isEssence                           | bool     |            | 是否加精                                             |
| isFavorite                          | bool     | 已收藏时   | 是否收藏                                             |
| isSite                              | bool     |            | 是否推荐到站点信息页                                 |
| paid                                | bool     | 付费主题   | 是否付费                                             |
| isPaidAttachment                    | bool     | 付费附件   | 附件是否付费                                         |
| canViewPosts                        | bool     |            | 是否有权查看详情                                     |
| canReply                            | bool     |            | 是否有权回复                                         |
| canApprove                          | bool     |            | 是否有权审核                                         |
| canSticky                           | bool     |            | 是否有权置顶                                         |
| canEssence                          | bool     |            | 是否有权加精                                         |
| canDelete                           | bool     |            | 是否有权永久删除                                     |
| canHide                             | bool     |            | 是否有权放入回收站                                   |
| canFavorite                         | bool     |            | 是否有权收藏                                         |
| isRedPacket                         | int      |            | 是否为红包帖，0不是，1是                             |
| questionTypeAndMoney                | object   |            | 悬赏/问答帖信息                                      |
| questionTypeAndMoney.id             | int      |            | 悬赏问答扩展表主键ID                                 |
| questionTypeAndMoney.thread_id      | int      |            | 悬赏/问答帖ID                                        |
| questionTypeAndMoney.post_id        | int      |            | 悬赏/问答帖主要内容ID                                |
| questionTypeAndMoney.type           | int      |            | 类型，0为所有人可回答的悬赏帖，1为指定人回答的问答帖 |
| questionTypeAndMoney.answer_id      | int      |            | 被指定回答人的ID，可为空                             |
| questionTypeAndMoney.money          | float    |            | 悬赏金额/问答金额                                    |
| questionTypeAndMoney.remain_money   | float    |            | 剩余金额                                             |
| questionTypeAndMoney.created_at     | datetime |            | 创建时间                                             |
| questionTypeAndMoney.updated_at     | datetime |            | 更新时间                                             |
| questionTypeAndMoney.expired_at     | datetime |            | 过期时间                                             |
| **relationships**                   | object   |            | 关联关系                                             |
| **included**                        | object   |            | 关联数据                                             |
| question_answer.be_user_id          | int      |            | 回答人的用户 ID                                      |
| question_answer.content             | string   |            | 回答的内容                                           |
| question_answer.content_html        | string   |            | 回答的 html 内容                                     |
| question_answer.ip                  | string   |            | 回答人的 IP                                          |
| question_answer.port                | int      |            | 回答人的端口                                         |
| question_answer.price               | float    |            | 问答单价                                             |
| question_answer.onlooker_unit_price | float    |            | 围观单价                                             |
| question_answer.onlooker_price      | float    |            | 围观总价格                                           |
| question_answer.onlooker_number     | int      |            | 围观总人数                                           |
| question_answer.is_onlooker         | bool     |            | 是否允许围观 true 允许 false 不允许                  |
| question_answer.is_answer           | int      |            | 是否已回答 0 未回答 1 已回答 2 已过期                |
| question_answer.expired_at          | string   |            | 问答过期时间                                         |

#### 返回示例

```json
{
  "links": {
    "first": "DummySiteUrl/api/threads?page%5Blimit%5D=10",
    "last": "DummySiteUrl/api/threads?page%5Blimit%5D=10"
  },
  "data": [
    {
      "type": "threads",
      "id": "26",
      "attributes": {
        "type": 0,
        "title": "",
        "price": "0.00",
        "attachment_price": "0.00",
        "viewCount": 0,
        "postCount": 1,
        "createdAt": "2020-03-10T16:33:24+08:00",
        "updatedAt": "2020-03-10T16:33:24+08:00",
        "isApproved": 1,
        "isSticky": false,
        "isEssence": false,
        "canViewPosts": true,
        "canReply": true,
        "canApprove": true,
        "canSticky": true,
        "canEssence": true,
        "canDelete": true,
        "canHide": true,
        "canFavorite": true,
		"isDraft": false,
	    "isRedPacket": 0,
        "questionTypeAndMoney": {
            "id": 5,
            "thread_id": 53,
            "post_id": 61,
            "type": 0,
            "answer_id": null,
            "money": "10.00",
            "remain_money": "10.00",
            "created_at": "2020-12-23T12:00:35.000000Z",
            "updated_at": "2020-12-23T12:00:38.000000Z",
            "expired_at": "2020-12-31T12:00:38.000000Z"
                },
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "3"
          }
        },
        "firstPost": {
          "data": {
            "type": "posts",
            "id": "24"
          }
        },
        "lastPostedUser": {
          "data": {
            "type": "users",
            "id": "3"
          }
        },
        "category": {
          "data": {
            "type": "categories",
            "id": "1"
          }
        }
      }
    },
    {
      "type": "threads",
      "id": "25",
      "attributes": {
        "type": 2,
        "title": "",
        "price": "0.00",
        "viewCount": 1,
        "postCount": 1,
        "createdAt": "2020-03-10T16:31:54+08:00",
        "updatedAt": "2020-03-10T16:31:54+08:00",
        "isApproved": 1,
        "isSticky": false,
        "isEssence": false,
        "canViewPosts": true,
        "canReply": true,
        "canApprove": true,
        "canSticky": true,
        "canEssence": true,
        "canDelete": true,
        "canHide": true,
        "canFavorite": true
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "3"
          }
        },
        "firstPost": {
          "data": {
            "type": "posts",
            "id": "23"
          }
        },
        "threadVideo": {
          "data": {
            "type": "thread-video",
            "id": "18"
          }
        },
        "lastPostedUser": {
          "data": {
            "type": "users",
            "id": "3"
          }
        },
        "category": {
          "data": {
            "type": "categories",
            "id": "1"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "3",
      "attributes": {
        "id": 3,
        "username": "username",
        "avatarUrl": "",
        "threadCount": 19,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": "2020-03-10T11:20:42+08:00",
        "joinedAt": "2020-03-09T16:39:28+08:00",
        "expiredAt": null,
        "createdAt": "2020-03-09T16:39:28+08:00",
        "updatedAt": "2020-03-10T17:03:44+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": "",
        "banReason": "",
        "originalMobile": "",
        "registerIp": "127.0.0.1",
        "lastLoginIp": "127.0.0.1",
        "identity": "",
        "realname": "",
        "mobile": "",
        "canWalletPay": false,
        "walletBalance": "0.00"
      }
    },
    {
      "type": "posts",
      "id": "24",
      "attributes": {
        "replyUserId": null,
        "content": "abc",
        "contentHtml": "abc",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-03-10T16:33:24+08:00",
        "updatedAt": "2020-03-10T16:33:24+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canEdit": true,
        "canApprove": true,
        "canDelete": true,
        "canHide": true,
        "ip": "127.0.0.1",
        "canLike": true
      }
    },
    {
      "type": "posts",
      "id": "23",
      "attributes": {
        "replyUserId": null,
        "content": "abc",
        "contentHtml": "abc",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-03-10T16:31:54+08:00",
        "updatedAt": "2020-03-10T16:31:54+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canEdit": true,
        "canApprove": true,
        "canDelete": true,
        "canHide": true,
        "ip": "127.0.0.1",
        "canLike": true
      }
    },
    {
      "type": "categories",
      "id": "1",
      "attributes": {
        "name": "默认分类",
        "description": "默认分类",
        "icon": "",
        "sort": 0,
        "property": 0,
        "thread_count": 19,
        "ip": "127.0.0.1",
        "created_at": "2020-03-05T09:41:41+08:00",
        "updated_at": "2020-03-10T17:03:44+08:00"
      }
    },
    {
      "type": "thread-video",
      "id": "18",
      "attributes": {
        "id": 18,
        "user_id": 3,
        "thread_id": 25,
        "status": 1,
        "reason": "source file err: file info lack width, height or color space",
        "file_name": "666",
        "file_id": "666",
        "width": 666,
        "height": 666,
        "media_url": "url",
        "cover_url": "url",
        "updated_at": "2020-03-10T18:26:31+08:00",
        "created_at": "2020-03-10T16:31:54+08:00"
      }
    },
    {
      "type": "question_answer",
      "id": "1",
      "attributes": {
        "thread_id": 201,
        "user_id": 4,
        "be_user_id": 1,
        "content": "",
        "content_html": null,
        "ip": "",
        "port": 0,
        "price": "10.00",
        "onlooker_unit_price": "2.00",
        "onlooker_price": "0.00",
        "onlooker_number": 0,
        "is_onlooker": true,
        "is_answer": 0,
        "is_approved": 1,
        "created_at": "2020-09-16T11:10:47+08:00",
        "updated_at": "2020-09-16T11:10:47+08:00",
        "expired_at": "2020-09-23T00:00:00+08:00"
      }
    }
  ],
  "meta": {
    "threadCount": 2,
    "pageCount": 1
  }
}
```
