### 实名认证接口

- **权限说明：** 自己
- **接口说明：** 实名认证接口
- **接口地址：** /api/users/real
- **请求方式：** PATCH

#### 请求参数

| 参数名称 | 类型   | 是否必须 | 描述     |
| :------- | :----- | :------- | :------- |
| identity | string | 是       | 身份证号 |
| realname | string | 是       | 姓名     |

请求示例：

```json
{
  "data": {
    "attributes": {
      "identity": "422828xxxxxxx1666",
      "realname": "张三"
    }
  }
}
```

#### 返回结果

| 参数名称 | 类型 | 出现要求 | 描述 |
| :------- | :--- | :------- | :--- |


#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 非 200

成功示例：

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "id": 1,
      "username": "username",
      "mobile": "mobile",
      "avatarUrl": "",
      "threadCount": 0,
      "registerIp": "",
      "lastLoginIp": "",
      "createdAt": "2019-11-19T15:25:31+08:00",
      "updatedAt": "2019-11-26T13:22:25+08:00"
    }
  }
}
```

失败示例：

```

```
