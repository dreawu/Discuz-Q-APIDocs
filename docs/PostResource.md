### 查询回复接口[单条]

- **接口说明：** 查询回复（点评）[单条]
- **接口地址：** /api/posts/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述     |
| :------- | :----: | :------: | :------- |
| include  | string |    否    | 关联数据 |

#### include 可关联的数据

| 关联名称                      | 模型  | 类型   | 是否默认 | 描述                   |
| :---------------------------- | :---- | :----- | :------: | :--------------------- |
| user                          | users | object |    是    | 回复的作者             |
| likedUsers                    | users | array  |    是    | 回复的作者             |
| commentPosts                  | posts | array  |    否    | 回复的评论列表         |
| commentPosts.user             | posts | object |    否    | 评论的作者             |
| commentPosts.user.groups      | posts | array  |    否    | 评论作者的用户组       |
| commentPosts.replyUser        | posts | object |    否    | 评论所回复的用户       |
| commentPosts.replyUser.groups | posts | array  |    否    | 评论所回复用户的用户组 |
| commentPosts.commentUser      | posts | object |    否    | 回复评论的关联用户  |
| commentPosts.mentionUsers     | posts | array  |    否    | 评论所 @ 的用户        |
| commentPosts.images           | posts | array  |    否    | 评论的图片             |
| images                        | posts | array  |    否    | 回复的图片             |
| attachments                   | posts | array  |    否    | 回复的附件             |

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

| 参数名称          | 类型      | 出现要求   | 描述                                              |
| :---------------- | :-------- | :--------- | :------------------------------------------------ |
| **data**          | object    |            | 基础数据                                          |
| type              | string    |            | 数据类型                                          |
| id                | int       |            | 数据 id                                           |
| **attributes**    | object    |            | 数据属性                                          |
| replyUserId       | int\|null |            | 所回复的用户 ID                                   |
| summary           | string    |            | 摘要                                              |
| content           | string    |            | 内容                                              |
| contentHtml       | string    |            | html 内容                                         |
| ip                | string    |            | 发布 ip                                           |
| replyCount        | int       |            | 回复数                                            |
| likeCount         | int       |            | 喜欢数                                            |
| createdAt         | datetime  |            | 创建时间                                          |
| updatedAt         | datetime  |            | 修改时间                                          |
| deletedAt         | datetime  | 在回收站时 | 删除时间                                          |
| isFirst           | bool      |            | 是否首帖                                          |
| isComment         | bool      |            | 是否是评论                                        |
| isApproved        | bool      |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isLiked           | bool      |            | 是否喜欢                                          |
| canEdit           | bool      |            | 是否有权编辑                                      |
| canApprove        | bool      |            | 是否有权审核                                      |
| canDelete         | bool      |            | 是否有权永久删除                                  |
| canHide           | bool      |            | 是否有权放入回收站                                |
| canLike           | bool      |            | 是否有权点赞                                      |
| isLiked           | bool      |            | 是否喜欢                                          |
| **relationships** | object    |            | 关联关系                                          |
| **included**      | object    |            | 关联数据                                          |

#### 返回示例

```json
{
  "data": {
    "type": "posts",
    "id": "2",
    "attributes": {
      "replyUserId": null,
      "content": "第一条回复",
      "contentHtml": "第一条回复",
      "replyCount": 8,
      "likeCount": 0,
      "createdAt": "2020-03-16T17:53:19+08:00",
      "updatedAt": "2020-04-27T11:50:57+08:00",
      "isApproved": 1,
      "canEdit": true,
      "canApprove": true,
      "canDelete": true,
      "canHide": true,
      "ip": "192.168.10.1",
      "isFirst": false,
      "isComment": false,
      "canLike": true
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      },
      "commentPosts": {
        "data": [
          {
            "type": "posts",
            "id": "42"
          },
          {
            "type": "posts",
            "id": "45"
          },
          {
            "type": "posts",
            "id": "46"
          },
          {
            "type": "posts",
            "id": "47"
          }
        ]
      },
      "images": {
        "data": []
      },
      "attachments": {
        "data": []
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "username",
        "avatarUrl": "avatarUrl",
        "isReal": true,
        "threadCount": 20,
        "followCount": 0,
        "fansCount": 0,
        "likedCount": 0,
        "signature": "",
        "usernameBout": 0,
        "follow": null,
        "status": 0,
        "loginAt": "2020-04-27T11:45:32+08:00",
        "joinedAt": null,
        "expiredAt": "2020-03-19T18:28:23+08:00",
        "createdAt": "2020-03-16T14:12:47+08:00",
        "updatedAt": "2020-04-27T17:16:22+08:00",
        "canEdit": true,
        "canDelete": true,
        "showGroups": true,
        "registerReason": "",
        "banReason": "",
        "originalMobile": "17301394502",
        "registerIp": "192.168.10.1",
        "lastLoginIp": "192.168.10.1",
        "identity": "",
        "realname": "realname",
        "mobile": "173****4502",
        "canWalletPay": true,
        "walletBalance": "997.80"
      }
    },
    {
      "type": "posts",
      "id": "42",
      "attributes": {
        "replyUserId": 1,
        "content": "Fantastic toolset AGP next-generation =sho4es=  solid state neural Personal Loan Account",
        "contentHtml": "Fantastic toolset AGP next-generation =sho4es=  solid state neural Personal Loan Account",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-04-24T14:38:30+08:00",
        "updatedAt": "2020-04-24T14:38:30+08:00",
        "isApproved": 1,
        "canEdit": true,
        "canApprove": true,
        "canDelete": true,
        "canHide": true,
        "ip": "192.168.10.1",
        "isFirst": false,
        "isComment": true
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "replyUser": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "mentionUsers": {
          "data": []
        },
        "images": {
          "data": []
        }
      }
    },
    {
      "type": "posts",
      "id": "45",
      "attributes": {
        "replyUserId": 1,
        "content": "Branding Crescent =sho4es=  Accounts hack input Dynamic Pitcairn Islands",
        "contentHtml": "Branding Crescent =sho4es=  Accounts hack input Dynamic Pitcairn Islands",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-04-26T14:41:34+08:00",
        "updatedAt": "2020-04-26T14:41:34+08:00",
        "isApproved": 1,
        "canEdit": true,
        "canApprove": true,
        "canDelete": true,
        "canHide": true,
        "ip": "192.168.10.1",
        "isFirst": false,
        "isComment": true
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "replyUser": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "mentionUsers": {
          "data": []
        },
        "images": {
          "data": []
        }
      }
    },
    {
      "type": "posts",
      "id": "46",
      "attributes": {
        "replyUserId": 1,
        "content": "Cape Verde Fresh Engineer =sho4es=  Future Pre-emptive Senior integrated plum",
        "contentHtml": "Cape Verde Fresh Engineer =sho4es=  Future Pre-emptive Senior integrated plum",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-04-26T14:41:35+08:00",
        "updatedAt": "2020-04-26T14:41:35+08:00",
        "isApproved": 1,
        "canEdit": true,
        "canApprove": true,
        "canDelete": true,
        "canHide": true,
        "ip": "192.168.10.1",
        "isFirst": false,
        "isComment": true
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "replyUser": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "mentionUsers": {
          "data": []
        },
        "images": {
          "data": []
        }
      }
    },
    {
      "type": "posts",
      "id": "47",
      "attributes": {
        "replyUserId": 1,
        "content": "synergize invoice =sho4es=  Licensed Soft Pizza Thailand indexing",
        "contentHtml": "synergize invoice =sho4es=  Licensed Soft Pizza Thailand indexing",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-04-26T14:41:36+08:00",
        "updatedAt": "2020-04-26T14:41:36+08:00",
        "isApproved": 1,
        "canEdit": true,
        "canApprove": true,
        "canDelete": true,
        "canHide": true,
        "ip": "192.168.10.1",
        "isFirst": false,
        "isComment": true
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "replyUser": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "mentionUsers": {
          "data": []
        },
        "images": {
          "data": []
        }
      }
    }
  ]
}
```
