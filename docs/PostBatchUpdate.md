### 修改回复接口[批量]

- **接口说明：** 修改回复[批量]
- **接口地址：** /api/posts/batch
- **请求方式：** PATCH

#### 请求参数

> 修改满足筛选条件的数据时，仅需在 meta['query'] 中传入筛选条件，在 meta['type'] 中传入操作类型
> approve 审核通过
> ignore 忽略
> delete 删除
> restore 还原

| 参数名称   |  类型  | 是否必须 | 描述                                              | 示例 |
| :--------- | :----: | :------: | :------------------------------------------------ | :--- |
| isApproved |  int   |    否    | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isDeleted  |  bool  |    否    | 是否删除（回收站）                                |
| isLiked    |  bool  |    否    | 是否喜欢                                          |
| message    | string |    否    | 操作原因                                          |

#### 请求示例

```json
// 修改部分数据
{
    "data": [
        {
            "type": "posts",
            "id": 100,
            "attributes": {
                "isApproved": 0
            }
        },
        {
            "type": "posts",
            "id": 2,
            "attributes": {
                "isApproved": 0
            }
        },
        {
            "type": "posts",
            "id": 3,
            "attributes": {
                "isApproved": 2
            }
        }
    ]
}

// 修改满足筛选条件的数据
{
    "meta": {
        "query": {
            "filter": {
                "user": 1
            }
        },
        "type": "ignore"
    }
}
```

#### 返回说明

- http 状态码 200

#### 返回结果

data 被修改的回复列表
meta 出现异常的回复列表

#### 返回示例

```json
{
  "data": [
    {
      "type": "posts",
      "id": "1",
      "attributes": {
        "content": "users Washington interface Extended copying == ivory == Computers Avon Colombia monitor Multi-lateral",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:23+08:00",
        "updatedAt": "2019-11-12T17:10:23+08:00",
        "isFirst": true,
        "isApproved": true,
        "isDeleted": true,
        "deletedAt": "2019-11-19T12:57:42+08:00",
        "isLiked": false
      }
    },
    {
      "type": "posts",
      "id": "2",
      "attributes": {
        "content": "Berkshire Legacy Public-key Bedfordshire Auto Loan Account == lime == Group incentivize exploit backing up port",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:27+08:00",
        "updatedAt": "2019-11-12T17:10:27+08:00",
        "isFirst": true,
        "isApproved": true,
        "isDeleted": true,
        "deletedAt": "2019-11-19T12:57:42+08:00",
        "isLiked": false
      }
    }
  ],
  "meta": [
    {
      "id": "3",
      "message": "model_not_found"
    },
    {
      "id": "4",
      "message": "model_not_found"
    }
  ]
}
```
