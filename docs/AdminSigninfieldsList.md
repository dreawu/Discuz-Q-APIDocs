获取后台扩展字段接口[列表]

- **接口说明：** 获取后台扩展字段接口[列表]
- **接口地址：** /api//admin/signinfields
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 值   | 描述 |
| :------- | :--- | :--- | :--- |
|          |      |      |      |

#### 请求示例

```
/api/admin/signinfields
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称               | 类型   | 出现要求 | 描述         |
| :--------------------- | :----- | :------- | :----------- |
| **data.attributes**    | object |          | 数据属性     |
| attributes.id          | bigint |          | 记录唯一编号 |
| attributes.name        | string |          | 名称         |
| attributes.sort        | int    |          | 字段排序     |
| attributes.type_desc   | string |          | 类型         |
| attributes.status      | int    |          | 是否启用     |
| attributes.fields_desc | string |          | 字段介绍     |
| attributes.required    | int    |          | 是否必须     |
| type                   | string |          | 数据类型     |
| id                     | string |          | 数据id       |

#### 返回示例

```json
{
    "data": [
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 52,
                "name": "单行文本框",
                "type": 0,
                "fields_ext": "",
                "fields_desc": "单行文本框",
                "sort": 1,
                "status": 1,
                "required": 0,
                "type_desc": "单行文本框"
            }
        },
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 53,
                "name": "图片",
                "type": 4,
                "fields_ext": "",
                "fields_desc": "",
                "sort": 2,
                "status": 1,
                "required": 1,
                "type_desc": "图片上传"
            }
        },
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 54,
                "name": "附件",
                "type": 5,
                "fields_ext": "",
                "fields_desc": "",
                "sort": 3,
                "status": 1,
                "required": 0,
                "type_desc": "附件上传"
            }
        },
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 55,
                "name": "11111",
                "type": 1,
                "fields_ext": "",
                "fields_desc": "",
                "sort": 4,
                "status": 1,
                "required": 0,
                "type_desc": "多行文本框"
            }
        },
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 56,
                "name": "2222",
                "type": 2,
                "fields_ext": "{\"options\":[{\"value\":\"1,1\",\"checked\":false},{\"value\":\"2,2\",\"checked\":false},{\"value\":\"3,3\",\"checked\":false}]}",
                "fields_desc": "",
                "sort": 5,
                "status": 1,
                "required": 0,
                "type_desc": "单选"
            }
        },
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 57,
                "name": "343",
                "type": 2,
                "fields_ext": "{\"options\":[{\"value\":\"233\",\"checked\":false}]}",
                "fields_desc": "",
                "sort": 6,
                "status": 1,
                "required": 0,
                "type_desc": "单选"
            }
        },
        {
            "type": "admin_sign_in",
            "id": "",
            "attributes": {
                "id": 58,
                "name": "223",
                "type": 0,
                "fields_ext": "",
                "fields_desc": "",
                "sort": 7,
                "status": -1,
                "required": 0,
                "type_desc": "单行文本框"
            }
        }
    ]
}
```