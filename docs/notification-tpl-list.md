### 系统消息模版列表

- **接口说明：** 系统消息模版列表
- **接口地址：** /api/notification/tpl
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述                       |
| :------- | :--- | :------: | :------------------------- |
| type     | int  |    否    | 默认 0 系统通知 1 微信通知 |

#### 请求示例

`{{host}}/api/notification/tpl?type=1`

#### 微信通知内置变量说明

| 模板名称 | 参数变量 | 参数说明 | 描述 | 推荐使用模板ID |
| :--- | :--- | :--- | :--- | :--- |
| **用户注册通知**  | ↘  |   |   |
| =>  | {$user_id}  | 注册人id  | 可用于站点第几名注册  |
| =>  | {$user_name}  | 用户名  | 注册人用户名  |
| =>  | {$user_mobile}  | 用户手机号  | 不带 * 的  |
| =>  | {$user_mobile_encrypt}  | 注册人手机号  | 带 * 的  |
| =>  | {$user_group}  | 注册人用户组  |   |
| =>  | {$joined_at}  | 付费加入时间  |   |
| =>  | {$expired_at}  | 付费到期时间  |   |
| =>  | {$site_name}  | 站点名称  |   |
| =>  | {$site_title}  | 站点标题  |   |
| =>  | {$site_introduction}  | 站点介绍  |   |
| =>  | {$site_mode}  | 站点模式  | 付费/免费 (用于提示用户"付费加入该站点")  |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **帐号解除禁用通知**  | ↘  |   |   |
| **帐号禁用通知**  | ↘  |   |   |
| **审核通过通知**  | ↘  |   |   |
| **审核拒绝通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  |   |
| =>  | {$user_name}  | 用户名  |   |
| =>  | {$user_mobile}  | 用户手机号  | 不带 * 的  |
| =>  | {$user_mobile_encrypt}  | 用户手机号  | 带 * 的  |
| =>  | {$user_change_status}  | 用户状态  | 改变的用户状态  |
| =>  | {$user_original_status}  | 用户状态  | 原用户状态  |
| =>  | {$reason}  | 原因  | 默认字符串'无'  |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **用户角色调整通知**  |   |   |   |
| =>  | {$user_id}  | 用户ID  |   |
| =>  | {$user_name}  | 用户名  | 被更改人的用户名  |
| =>  | {$user_mobile}  | 用户手机号  | 不带 * 的  |
| =>  | {$user_mobile_encrypt}  | 用户手机号  | 带 * 的  |
| =>  | {$group_original}  | 原用户组名  |   |
| =>  | {$group_change}  | 新用户组名  |   |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **内容修改通知**  | ↘  |   |   |
| **内容审核通过通知**  | ↘  |   |   |
| **内容审核不通过通知<br>(内容忽略通知)**  | ↘  |   |   |
| **内容删除通知**  | ↘  |   |   |
| **内容精华通知**  | ↘  |   |   |
| **内容置顶通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  | 帖子创建人ID  |
| =>  | {$user_name}  | 用户名  | 帖子创建人  |
| =>  | {$actor_name}  | 用户名  | 当前登录人/操作人(一般为管理员)  |
| =>  | {$message_change}  | 修改帖子的内容  | 对应操作时，该字段有效  |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 主题标题/首贴内容 <br>(如果有title是title，没有则是首帖内容)  |
| =>  | {$reason}  | 原因  | 默认字符串'无'  |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **内容点赞通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  | 点赞人用户ID  |
| =>  | {$user_name} | 点赞人姓名 | 当前登录人 |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 如果有标题则是标题内容，<br>没有则是首帖内容 |
| =>  | {$post_content}  | 帖子内容  |   |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **内容回复通知**  | ↘  |   |   |
| =>  | {$user_name}  | 用户名  | 回复人的用户名  |
| =>  | {$post_content}  | 回复内容  |   |
| =>  | {$reply_post}  | 被回复内容  | 1.回复主题时该内容等于thread_post_content值<br> 2.楼中楼回复时则是被回复内容<br> 3.后台审核通过帖子时，该内容等于thread_title值  |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 如果有标题则是标题内容，<br>没有则是首帖内容 |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **内容@通知**  | ↘  |   |   |
| =>  | {$user_name}  | 用户名  | 发送人姓名  |
| =>  | {$post_content}  | @源帖子内容  |   |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 如果有标题则是标题内容，<br>没有则是首帖内容 |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **内容支付通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  | 支付人用户ID  |
| =>  | {$user_name}  | 用户名  | 支付人  |
| =>  | {$order_sn}  | 订单编号  |   |
| =>  | {$payment_sn}  | 支付编号  |   |
| =>  | {$order_type_name}  | 订单支付类型  | 打赏/付费主题/付费用户组/问答回答收入/<br>问答围观收入/付费附件 (无“注册”类型)  |
| =>  | {$actual_amount}  | 实际获得金额  |   |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 如果有标题则是标题内容，<br>没有则是首帖内容 |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **分成收入通知**  | ↘  |   |   |
| =>  | {$user_name}  | 用户名  | 被更改人的用户名  |
| =>  | {$order_sn}  | 订单编号  |   |
| =>  | {$payment_sn}  | 支付编号  |   |
| =>  | {$order_type_name}  | 订单支付类型  | 注册/打赏/付费主题/付费附件  |
| =>  | {$boss_amount}  | 上级实际分成金额  |   |
| =>  | {$title}  | 主题标题/"注册站点"  | 如果是注册站点，该值是"注册站点" |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **提现通知**  | ↘  |   |   |
| **提现失败通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  | 提现人用户ID  |
| =>  | {$user_name}  | 用户名  | 提现人 |
| =>  | {$cash_sn}  | 提现交易编号  |   |
| =>  | {$cash_charge}  | 提现手续费  |   |
| =>  | {$cash_actual_amount}  | 提现实际到账金额  |   |
| =>  | {$cash_apply_amount}  | 提现申请金额  |   |
| =>  | {$cash_status}  | 提现结果  | 待审核/审核通过/审核不通过/<br>待打款/已打款/打款失败  |
| =>  | {$cash_mobile}  | 提现到账手机号码  |   |
| =>  | {$remark}  | 备注或原因  | 默认"无"  |
| =>  | {$trade_no}  | 交易号  |   |
| =>  | {$cash_created_at}  | 提现创建时间  |   |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **问答提问通知**  | ↘  |   |   |
| **问答过期通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  | 提问人用户ID  |
| =>  | {$user_name}  | 用户名  | 提问人姓名/匿名  |
| =>  | {$be_user_name}  | 用户名  | 被提问人  |
| =>  | {$question_price}  | 提问价格  | 也是解冻金额  |
| =>  | {$question_created_at}  | 提问创建时间  |   |
| =>  | {$question_expired_at}  | 提问过期时间  |   |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 如果有标题则是标题内容，<br>没有则是首帖内容 |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |
| **问答回答通知**  | ↘  |   |   |
| =>  | {$user_id}  | 用户ID  | 回答人用户ID  |
| =>  | {$user_name}  | 用户名  | 回答人姓名  |
| =>  | {$be_user_name}  | 用户名  | 被提问人  |
| =>  | {$question_content}  | 回答的内容  |   |
| =>  | {$question_price}  | 提问价格  |   |
| =>  | {$question_created_at}  | 提问创建时间  |   |
| =>  | {$question_expired_at}  | 提问过期时间  |   |
| =>  | {$thread_id}  | 主题ID  | 可用于跳转参数  |
| =>  | {$thread_title}  | 主题标题/首贴内容  | 如果有标题则是标题内容，<br>没有则是首帖内容 |
| =>  | {$notify_time}  | 通知时间  | 时间格式: 2020-12-xx xx:xx:xx  |
| =>  | {$site_domain}  | 站点域名  | https://discuzq.com  |

#### 返回说明

- 系统消息模版列表， http 状态码： 200

#### 返回结果

回复我的、点赞我的通知 data 字段说明

| 字段名    | 变量名                 |
| :-------- | :--------------------- |
| id        | 编号                   |
| status    | 状态 1 为正常， 0 关闭 |
| type_name | 列表名称               |
| title     | 通知标题               |
| conetn    | 通知内容               |
| vars      | 可使用的变量           |

#### 返回示例

```json
{
  "data": [
    {
      "type": "notification_tpls",
      "id": "1",
      "attributes": {
        "status": 1,
        "type_name": "新用户注册并加入后",
        "title": "欢迎加入{sitename}",
        "content": "{username}你好，你已经成为{sitename} 的{groupname} ，请您在发表言论时，遵守当地法律法规。祝你在这里玩的愉快。",
        "vars": {
          "{username}": "用户名",
          "{sitename}": "站点名称",
          "{groupname}": "用户组"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "2",
      "attributes": {
        "status": 1,
        "type_name": "注册审核通过通知",
        "title": "注册审核通知",
        "content": "{username}你好，你的注册申请已审核通过。",
        "vars": {
          "{username}": "用户名"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "3",
      "attributes": {
        "status": 1,
        "type_name": "注册审核不通过通知",
        "title": "注册审核通知",
        "content": "{username}你好，你的注册申请审核不通过，原因：{reason}",
        "vars": {
          "{username}": "用户名",
          "{reason}": "原因"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "4",
      "attributes": {
        "status": 1,
        "type_name": "内容审核不通过通知",
        "title": "内容审核通知",
        "content": "{username}你好，你的发布的内容 \"{content}\" 审核不通过，原因：{reason}",
        "vars": {
          "{username}": "用户名",
          "{content}": "内容",
          "{reason}": "原因"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "5",
      "attributes": {
        "status": 1,
        "type_name": "内容审核通过通知",
        "title": "内容审核通知",
        "content": "{username}你好，你的发布的内容 \"{content}\" 审核通过",
        "vars": {
          "{username}": "用户名",
          "{content}": "内容"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "6",
      "attributes": {
        "status": 1,
        "type_name": "内容删除通知",
        "title": "内容删除通知",
        "content": "{username}你好，你的发布的内容 \"{content} \" 已删除，原因：{reason}",
        "vars": {
          "{username}": "用户名",
          "{content}": "内容",
          "{reason}": "原因"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "7",
      "attributes": {
        "status": 1,
        "type_name": "内容精华通知",
        "title": "内容精华通知",
        "content": "{username}你好，你的发布的内容 \"{content}\" 已设为精华",
        "vars": {
          "{username}": "用户名",
          "{content}": "内容"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "8",
      "attributes": {
        "status": 1,
        "type_name": "内容置顶通知",
        "title": "内容置顶通知",
        "content": "{username}你好，你的发布的内容 \"{content}\" 已置顶",
        "vars": {
          "{username}": "用户名",
          "{content}": "内容"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "9",
      "attributes": {
        "status": 1,
        "type_name": "内容修改通知",
        "title": "内容修改通知",
        "content": "{username}你好，你的发布的内容 \"{content}\" 已被修改",
        "vars": {
          "{username}": "用户名",
          "{content}": "内容"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "10",
      "attributes": {
        "status": 1,
        "type_name": "帐号禁用通知",
        "title": "帐号禁用通知",
        "content": "{username}你好，你的账号已禁用，原因：{reason}",
        "vars": {
          "{username}": "用户名",
          "{reason}": "原因"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "11",
      "attributes": {
        "status": 1,
        "type_name": "用户解除禁用通知",
        "title": "解除禁用通知",
        "content": "{username}你好，你的账号已解除禁用",
        "vars": {
          "{username}": "用户名"
        }
      }
    },
    {
      "type": "notification_tpls",
      "id": "12",
      "attributes": {
        "status": 1,
        "type_name": "用户角色调整通知",
        "title": "角色调整通知",
        "content": "{username}你好，你的角色由{oldgroupname}变更为{newgroupname}",
        "vars": {
          "{username}": "用户名",
          "{oldgroupname}": "老用户组",
          "{newgroupname}": "新用户组"
        }
      }
    }
  ]
}
```
