### 管理员创建一码一用邀请码

- **接口说明：** 管理员创建一码一用邀请码
- **接口地址：** /api/invite
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述      |
| :------- | :--: | :------: | :-------- |
| group_id | int  |    是    | 用户组 id |

#### 请求示例

```json
{
  "data": {
    "type": "invite",
    "attributes": {
      "group_id": 1
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称              | 类型     | 出现要求 | 描述                                               |
| :-------------------- | :------- | :------- | :------------------------------------------------- |
| **data**              | object   |          | 基础数据                                           |
| type                  | string   |          | 数据类型                                           |
| id                    | int      |          | 数据 id                                            |
| **attributes**        | object   |          | 数据属性                                           |
| attributes.id         | int      |          | id                                                 |
| attributes.group_id   | int      |          | 用户组 id                                          |
| attributes.code       | string   |          | 邀请码                                             |
| attributes.dateline   | int      |          | 生效时间                                           |
| attributes.endtime    | datetime |          | 失效时间                                           |
| attributes.to_user_id | int      |          | 被邀请用户 id                                      |
| attributes.status     | tinyint  |          | 状态<br>0 失效<br>1 未使用<br>2 已使用<br>3 已过期 |

#### 返回示例

```json
{
  "data": {
    "type": "invite",
    "id": "15",
    "attributes": {
      "id": 15,
      "group_id": 1,
      "code": "LSO4md492aD5DOB4vzaKYa9kehaahQDr",
      "dateline": 1574330396,
      "endtime": 1574935196,
      "user_id": 0,
      "to_user_id": 0,
      "status": 0
    }
  }
}
```
