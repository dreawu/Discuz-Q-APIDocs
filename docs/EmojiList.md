### 表情[列表]

- **接口说明：** 表情[列表]
- **接口地址：** /api/emoji
- **请求方式：** GET

#### 请求参数

参数名称 |类型 |描述

#### 返回结果

| 参数名称            | 类型   | 出现要求 | 描述           |
| :------------------ | :----- | :------- | :------------- |
| type                | string | classify | 数据模型的类型 |
| id                  | int    | 创建成功 | 分类 id        |
| attributes          | object | 创建成功 | 数据模型的属性 |
| attributes.id       | int    | 创建成功 | id             |
| attributes.category | string | 创建成功 | 分类           |
| attributes.url      | string | 创建成功 | 表情所在路径   |
| attributes.code     | string | 创建成功 | 表情码         |
| attributes.order    | int    | 创建成功 | 排序           |

#### 返回说明

- 查询成功， http 状态码： 200

#### 返回示例

```json
{
  "data": [
    {
      "type": "emoji",
      "id": "1",
      "attributes": {
        "id": 1,
        "category": "qq",
        "url": "emoji/qq/yiwen.gif",
        "code": "",
        "order": 0
      }
    }
  ]
}
```
