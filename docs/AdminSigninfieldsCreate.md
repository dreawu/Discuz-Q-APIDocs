### 后台扩展字段创建

- **接口说明：** 后台扩展字段创建
- **接口地址：** /api/admin/signinfields
- **请求方式：** POST

#### 请求参数

| 字段名      | 变量名 | 必填 | 类型   | 描述     |
| :---------- | :----- | :--- | :----- | :------- |
| name        |        | 是   | string | 名称     |
| type        |        | 是   | int    | 类型     |
| fields_desc |        | 是   | string | 字段名称 |
| sort        |        | 是   | int    | 排序     |
| status      |        | 否   | int    | 是否启用 |
| required    |        | 否   | int    | 是否必填 |
| id          |        | 否   | int    |          |
| fields_ext  |        | 否   | string | 说明     |

```json
{
  "data":[
        {
            "type": "admin_sign_in",
            "attributes": {
                "name": "单行文本框",
                "type": 0,
                "fields_desc": "单行文本框",
                "sort": 1,
                "status": 1,
                "required": 0,
                "id": 52,
                "fields_ext": ""
            }
        },
        {
            "type": "admin_sign_in",
            "attributes": {
                "name": "图片",
                "type": 4,
                "fields_desc": "",
                "sort": 2,
                "status": 1,
                "required": 1,
                "id": 53,
                "fields_ext": ""
            }
        }
	]
}
```

#### 返回说明

- 返回当前创建成功数据， http 状态码： 201

#### 返回结果

|      | 参数名称               | 类型   | 出现要求 | 描述         |
| :--- | :--------------------- | :----- | :------- | :----------- |
|      | **data.attributes**    | object |          | 数据属性     |
|      | attributes.id          | bigint |          | 记录唯一编号 |
|      | attributes.name        | string |          | 名称         |
|      | attributes.sort        | int    |          | 字段排序     |
|      | attributes.type        | int    |          | 类型         |
|      | attributes.status      | int    |          | 是否启用     |
|      | attributes.fields_desc | string |          | 字段介绍     |
|      | attributes.fields_ext  | string |          | 扩展说明     |
|      | attributes.created_at  | date   |          | 创建时间     |
|      | attributes.updated_at  | date   |          | 更新时间     |
|      | type                   | string |          | 数据类型     |
|      | id                     | int    |          | 数据id       |

示例：

```json
{
    "data": {
        "type": "admin_sign_in",
        "id": "",
        "attributes": [
            {
                "id": 52,
                "name": "单行文本框",
                "type": 0,
                "fields_ext": "",
                "fields_desc": "单行文本框",
                "sort": 1,
                "status": 1,
                "required": 0,
                "created_at": "2021-02-05T12:33:48.000000Z",
                "updated_at": "2021-02-26T07:32:27.000000Z"
            },
            {
                "id": 53,
                "name": "图片",
                "type": 4,
                "fields_ext": "",
                "fields_desc": "",
                "sort": 2,
                "status": 1,
                "required": 1,
                "created_at": "2021-02-05T12:47:17.000000Z",
                "updated_at": "2021-03-04T08:53:22.000000Z"
            },
            {
                "id": 54,
                "name": "附件",
                "type": 5,
                "fields_ext": "",
                "fields_desc": "",
                "sort": 3,
                "status": 1,
                "required": 0,
                "created_at": "2021-02-05T12:47:17.000000Z",
                "updated_at": "2021-02-26T07:32:27.000000Z"
            }
        ]
    }
}
```