### 公众号创建自动回复

- **接口说明：** 公众号创建自动回复
- **接口地址：** /api/offiaccount/reply
- **请求方式：** POST

#### 请求参数

- get参数:
`{{host}}/api/offiaccount/reply?filter[type]=2`

- body参数:

| 参数名称 | 类型 | 名称 | 描述 | 是否必须 | 传输类型 |
| :--- | :--- | :--- | :--- | :--- | :--- |
| filter[type]  | int  | 数据类型 | 0自动回复1消息回复2关键词回复 | 是 | query |
| name | string | 规则名 |  | 是 | body |
| keyword | string | 关键词 |  | 是 | body |
| match_type | int | 匹配规则 | 0全匹配1半匹配 | 是 | body |
| reply_type | int | 消息回复类型 | 1文本2图片3语音4视频5图文 | 是 | body |
| content | string | 回复文本内容 |  | 是 | body |
| media_id | string | 素材ID |  | 是 | body |
| media_type | int | 素材类型 | 1图片2视频3语音4图文 | 是 | body |
| status | int | 是否开启 | 0关闭1开启 | 是 | body |

#### 请求示例

```json
{
    "data": {
        "type": "offiaccount_reply",
        "attributes": {
            "name": "{{$randomFirstName}}",
            "keyword": "{{$randomFirstName}}",
            "match_type": 1,
            "reply_type": 5,
            "content": "",
            "media_id": "ryUzCNaipU-iHQGbMFvy4ivplQqRd7Pvk63dZvq1o5E",
            "media_type": 4,
            "status": 1
        }
    }
}
```

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述 |
| :--- | :--- | :--- | :--- |
| name | string | 规则名 |  |
| keyword | string | 关键词 |  |
| match_type | int | 匹配规则 | 0全匹配1半匹配 |
| reply_type | int | 消息回复类型 | 1文本2图片3语音4视频5图文 |
| content | string | 回复文本内容 |  |
| media_id | string | 素材ID |  |
| media_type | int | 素材类型 | 1图片2视频3语音4图文 |
| type | int | 数据类型 | 0被关注回复1消息回复2关键词回复 |
| status | int | 是否开启 | 0关闭1开启 |
| created_at | string | 创建时间 |  |
| updated_at | string | 更新时间 |  |

#### 返回说明

- 成功，http 状态码 200
- 失败，http 状态码 500

#### 返回示例

```json
{
    "data": {
        "type": "offiaccount_reply",
        "id": "6",
        "attributes": {
            "name": "Amalia",
            "keyword": "Jaylon111",
            "match_type": 1,
            "reply_type": 5,
            "media_id": "ryUzCNaipU-iHQGbMFvy4ivplQqRd7Pvk63dZvq1o5E",
            "media_type": 4,
            "type": "2",
            "updated_at": "2020-07-02T17:36:56+08:00",
            "created_at": "2020-07-02T17:36:56+08:00"
        }
    }
}
```
