### 用户注册接口

- **接口说明：** 用户注册接口
- **接口地址：** /api/register
- **请求方式：** POST

#### 请求参数

| 参数名称        |  类型  | 是否必须 | 描述               |
| :-------------- | :----: | :------: | :----------------- |
| username        | string |    是    | 用户名             |
| password        | string |    是    | 密码               |
| token           | string |    否    | [微信公众号通过code参数换取token接口](wechat-user.md)返回的 token |
| ~~mobile~~      | ~~string~~ |    ~~否~~    | ~~手机号（已弃用）~~             |
| mobileToken     | string |    否    |  手机号登录时未绑定用户返回的token，用于注册并绑定手机号 [短信验证](sms-verify.md)|
| register_reason | string |    否    | 注册原因           |
| code            | string |    否    | 注册邀请码         |
| js_code         | string |    否    | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序登陆code，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/login/wx.login.html|
| iv              | string |    否    | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序用户数据数据加密算法的初始向量，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html|
| encryptedData   | string |    否    | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序用户包括敏感数据在内的完整用户信息的加密数据，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html |
| rebind    | int    | 否  | 重新绑定当前微信。  换绑：1，不换：0或者不传 |

#### 请求示例

```json
{
  "data": {
    "type": "users",
    "attributes": {
      "username": "username",
      "password": "password",
      "register_reason": "这是一条注册原因"
    }
  }
}
```

#### 返回说明

- 注册成功， http 状态码： 200
- 注册失败， http 状态码： 422
- 注册失败， http 状态码： 401
详见- [语言包说明](LanguagePack.md)

#### 返回结果

| 参数名称               | 类型     | 出现要求 | 描述        |
| :--------------------- | :------- | :------- | :---------- |
| **data**               | object   |          | 基础数据    |
| type                   | string   |          | 数据类型    |
| id                     | int      |          | 数据 id     |
| **attributes**         | object   |          | 数据属性    |
| attributes.username    | string   |          | 用户名      |
| attributes.nickname    | string   |          | 昵称        |
| attributes.mobile      | string   |          | 手机号      |
| attributes.unionId     | string   |          | union_id    |
| attributes.lastLoginIp | string   |          | 最后登陆 IP |
| attributes.createdAt   | datetime |          | 创建时间    |
| attributes.updatedAt   | datetime |          | 修改时间    |

#### 返回示例

成功示例：

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Q......P16l7srpANq3OzQkcjQrPO1nRKtUDGkYREEnGR-5fpw",
      "refresh_token": "def502005261a2......d861786ce8acc411ca83a0286526db35e67c937b607"
    }
  }
}
```

失败示例：

```json
{
  "errors": [
    {
      "status": "422",
      "code": "validation_error",
      "detail": ["用户名 已经存在。"],
      "source": {
        "pointer": "/data/attributes/username"
      }
    }
  ]
}
```
