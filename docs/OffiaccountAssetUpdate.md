### 公众号素材管理修改图文消息

- **接口说明：** 公众号素材管理修改图文消息
- **接口地址：** /api/offiaccount/asset
- **请求方式：** PATCH

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
| :--- | :--- | :--- | :--- | :--- |
| media_id  | string  | 是 | 永久素材ID | 要修改的ID |
| title  | string  | 是  | 标题  |   |
| thumb_media_id  | string | 是  | 封面图片素材ID  | 图文消息的封面图片素材id（必须是永久mediaID）  |
| content  | string  | 是  | 图文消息的具体内容  | 支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS  |
| author  | string  | 否  | 作者  |   |
| digest  | string  | 否  | 摘要  | 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空  |
| show_cover_pic  | int  | 否  | 是否显示封面  | 0为false，即不显示，1为true，即显示  |
| content_source_url  | string  | 否  | 图文消息的原文地址  | 即点击“阅读原文”后的URL  |

#### 请求示例

```json
{
    "data": {
        "type": "offiaccount_asset",
        "media_id": "Nvfpm344k2Zh1fm2SI08bYQwrUxGP6U-crHqiCuyd4s",
        "articles": {
           "title": "TITLE",
           "thumb_media_id": "THUMB_MEDIA_ID",
           "author": "AUTHOR",
           "digest": "DIGEST",
           "show_cover_pic": "SHOW_COVER_PIC(0 / 1)",
           "content": "CONTENT",
           "content_source_url": "CONTENT_SOURCE_URL"
        }
	  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :--- | :---- | :--- |
| errcode  | int  | 0成功，其余都是失败。（微信返回的数据）  |
| errmsg  | string  | 成功返回“ok”  |

#### 返回示例

- 正确

```json
{
    "data": {
        "type": "offiaccount_asset",
        "id": "1",
        "attributes": {
            "errcode": 0,
            "errmsg": "ok"
        }
    }
}
```

- 错误

```json
{
    "data": {
        "type": "offiaccount_asset",
        "id": "1",
        "attributes": {
            "errcode": 40007,
            "errmsg": "invalid media_id hint: [ldMK14l95]"
        }
    }
}
```
