### 被邀请用户列表

- **接口说明：** 被邀请用户列表
- **接口地址：** /api/invite/users
- **请求方式：** GET

#### 请求参数

| 参数名称         | 类型   | 是否必须 | 描述                                          |
| :--------------- | :----- | :------: | :-------------------------------------------- |
| filter[scale]    | int    |    否    | 查询邀请的用户里 0 没分成的 1 有分成的        |
| page[limit]      | int    |    否    | 分页数据条数                                  |
| page[number]     | int    |    否    | 页码                                          |
| sort             | string |    否    | 排序：固定值 正序 created_at 倒序 -created_at |
| filter[username] | string |    否    | 筛选被邀请的用户                              |

#### 请求示例

```
{{host}}/api/invite/users?filter[scale]=1&page[limit]=20&page[number]=1
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

| 参数名称     | 类型     | 描述                 |
| :----------- | :------- | :------------------- |
| pid          | int      | 上级 id(邀请人 ID)   |
| user_id      | int      | 用户 ID(被邀请人 ID) |
| invites_code | string   | 关联邀请码           |
| be_scale     | int      | 受邀时的分成比例     |
| level        | int      | 当前用户所处深度     |
| created_at   | datetime | 注册时间             |
| updated_at   | datetime | 最后活动时间         |

#### 返回示例

```json
{
  "links": {
    "first": "http://www.xxx.com/api/invite/users?filter%5Bscale%5D=1&page%5Blimit%5D=20",
    "last": "http://www.xxx.com/api/invite/users?filter%5Bscale%5D=1&page%5Blimit%5D=20"
  },
  "data": [
    {
      "type": "invite_user",
      "id": "1",
      "attributes": {
        "pid": 4,
        "user_id": 5,
        "invites_code": "pK0iMUDagTB5xxxxxxxxxSo1aFD7",
        "be_scale": 2,
        "level": 1,
        "updated_at": "2019-12-25T17:27:00+08:00",
        "created_at": "2019-12-25T17:27:00+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "5"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "5",
      "attributes": {
        "id": 5,
        "username": "三脚猫",
        "avatarUrl": "",
        "isReal": false,
        "threadCount": 5,
        "followCount": 0,
        "fansCount": 0,
        "likedCount": 0,
        "signature": "",
        "usernameBout": 0,
        "status": 0,
        "loginAt": "2020-08-05T22:46:20+08:00",
        "joinedAt": "2020-04-16T19:29:27+08:00",
        "expiredAt": null,
        "createdAt": "2020-04-16T19:29:27+08:00",
        "updatedAt": "2020-08-05T22:47:09+08:00",
        "canEdit": true,
        "canDelete": true,
        "showGroups": true,
        "registerReason": "",
        "banReason": "",
        "denyStatus": false,
        "originalMobile": "",
        "registerIp": "127.xxx.xxx.xxx",
        "registerPort": 0,
        "lastLoginIp": "127.0.0.1",
        "lastLoginPort": 62880,
        "identity": "",
        "realname": "",
        "mobile": "",
        "hasPassword": true,
        "canEditUsername": true
      }
    }
  ],
  "meta": {
    "total": 1,
    "pageCount": 1
  }
}
```
