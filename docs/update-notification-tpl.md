### 修改系统消息模版

- **接口说明：** 修改系统消息模版
- **接口地址：** /api/notification/tpl/{id}
- **请求方式：** PATCH

#### 请求参数

| 参数名称    | 类型   | 是否必须 | 描述                         |
| :---------- | :----- | :------: | :--------------------------- |
| status      | int    |    否    | 模版状态:1 开启 0 关闭       |
| title       | string |    否    | 模版标题 type=0 时传标题     |
| content     | int    |    否    | 模版内容 type=0 时传内容     |
| template_id | string |    否    | 当 tapy=1 微信通知时传这个值 |
| first_data  | string | 否  | 微信通知时会用到  |
| keywords_data  | array | 否  | keywords数组值<br>（这里不允许传字符串原因是会有英文","的存在）  |
| remark_data  | string | 否  | 微信通知时会用到  |
| color  | object | 否  | 使用方式查看下面示例，具体值要对应下标使用  |
| redirect_type  | int | 否  | 跳转类型：0无跳转 1跳转H5 2跳转小程序  |
| redirect_url  | string | 否  | 域名地址(跳转类型为1时使用),<br>默认不填时代码会根据不同类型通知跳转不同位置  |
| page_path  | string | 否  | 小程序路由(跳转类型为2时使用)  |

请求示例：

```json
{
    "data": [
        {
            "attributes": {
                "id": 1,
                "status": 1,
                "template_id": "t0wyNCUnL18MZnp5VtyIXUosOoRw0Z6SDB4y9BEjKhc",
                "first_data": "{$user_name}你好，你的角色有所变动",
                "keywords_data": [
                    "{$user,1,1,_change_status}哈哈{$user_name}？",
                    "{$post_content}@@@@,我在前面{$user_name}",
                    "AABBCC"
                ],
                "remark_data": "{$time_now} - {$user_name}昂昂昂昂",
                "color": {
                    "first_color": "{{$randomHexColor}}",
                    "keyword1_color": "{{$randomHexColor}}",
                    "keyword2_color": "{{$randomHexColor}}",
                    "remark_color": "{{$randomHexColor}}"
                },
                "redirect_type": 1,
                "redirect_url": "https://www.baidu.com",
                "page_path": "pages/notice/notice"
            }
        }
    ]
}
```

#### 返回说明

- 系统消息模版列表， http 状态码： 200

#### 返回结果

| 字段名    | 变量名                 |
| :-------- | :--------------------- |
| tpl_id | 数据库自增ID |
| status | 状态 1 为正常， 0 关闭 |
| type | 通知类型:0系统1微信2短信 |
| type_name | 类型名称 |
| title | 标题  |
| content | 内容 |
| template_id | 模板ID |
| first_data | first.DATA |
| keywords_data | keywords.DATA |
| remark_data | remark.DATA |
| color | data color |
| redirect_type | 跳转类型：0无跳转 1跳转H5 2跳转小程序 |
| redirect_url | 跳转地址 |
| page_path | 跳转路由 |
| disabled  | true不可以编辑的系统通知 false可编辑  |

#### 返回示例

```json
{
    "data": [
        {
            "type": "notification_tpls",
            "id": "0",
            "attributes": {
                "tpl_id": 1,
                "status": 1,
                "type": 0,
                "type_name": "新用户注册通知",
                "title": "欢迎加入{sitename}",
                "content": "{username}你好，你已经成为{sitename} 的{groupname} ，请你在发表言论时，遵守当地法律法规。祝你在这里玩的愉快。",
                "template_id": "",
                "template_variables": {
                    "{username}": "用户名",
                    "{sitename}": "站点名称",
                    "{groupname}": "用户组"
                },
                "first_data": "{$user_name}你好，你的角色有所变动",
                "keywords_data": [
                    "{$user，1，1，_change_status}哈哈{$user_name}？",
                    "{$post_content}@@@@，我在前面{$user_name}",
                    "AABBCC"
                ],
                "remark_data": "{$time_now} - {$user_name}昂昂昂昂",
                "color": {
                    "first_color": "#030d5b",
                    "keyword1_color": "#6f0c52",
                    "keyword2_color": "#2b660d",
                    "remark_color": "#594a58"
                },
                "redirect_type": 1,
                "redirect_url": "",
                "page_path": "",
                "disabled": false
            }
        }
    ]
}
```
