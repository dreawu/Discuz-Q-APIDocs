### 订单列表

- **接口说明：** 订单列表
- **接口地址：** /api/orders
- **请求方式：** GET

#### 请求参数

| 字段名   | 变量名                 | 必填 | 类型     | 描述                                                         |
| :------- | :--------------------- | :--- | :------- | :----------------------------------------------------------- |
| 排序参数 | sort                   | 否   | string   | 可选值：created_at、updated_at。                             |
| 筛选参数 | filter[status]         | 否   | int      | 筛选订单付款状态，可选值：0 待付款，1 已付款，3 支付失败，4 订单已过期，10 已退款订单，11 不处理的异常订单 |
| 筛选参数 | filter[user]           | 否   | int      | 传递 user_id,筛选某用户数据                                  |
| 筛选参数 | filter[order_sn]       | 否   | string   | 按订单号筛选                                                 |
| 筛选参数 | filter[username]       | 否   | string   | 按订单创建人筛选                                             |
| 筛选参数 | filter[payee_username] | 否   | string   | 按收款人用户名筛选                                           |
| 筛选参数 | filter[product]        | 否   | string   | 按商品名筛选（帖子内容）                                     |
| 筛选参数 | filter[start_time]     | 否   | datetime | 按订单创建时间范围筛选：开始时间                             |
| 筛选参数 | filter[end_time]       | 否   | datetime | 按订单创建时间范围筛选：最后时间                             |
| 关联参数 | include                | 否   | string   | 关联数据（包含 user，thread，thread.firstPost，group）       |

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                | 必填 | 类型     | 描述                                                         |
| :--------------------- | :-------------------- | :--- | :------- | :----------------------------------------------------------- |
| **links**              | object                | 是   | object   | 接口链接                                                     |
| **data.attributes**    | object                | 是   | object   | 数据属性                                                     |
| 订单编号               | attribute.order_sn    | 是   | string   | 订单唯一编号                                                 |
| 订单金额               | attribute.amount      | 是   | float    | 订单付款金额                                                 |
| 付款状态               | attributes.status     | 是   | int      | 付款状态，0 待付款，1 已付款 ，2 取消订单，3 支付失败，4 订单已过期，10 已退款订单，11 不处理的异常订单 |
| 订单类型               | attributes.type       | 是   | int      | 1：注册，2：打赏，3：付费主题，4：付费用户组，5：问答提问支付，6：问答围观付费, 7: 付费附件，20：文字帖红包，21：长文帖红包 |
| 主题 ID                | attributes.thread_id  | 是   | int      | 打赏的主题                                                   |
| 用户组 ID              | attributes.group_id   | 是   | int      | 付费用户组 id                                                |
| 订单标题               | attributes.title      | 是   | string   | 主题不为空时，标题不为空                                     |
| 更新时间               | attributes.updated_at | 是   | datetime | 更新时间                                                     |
| 创建时间               | attributes.created_at | 是   | datetime | 创建时间                                                     |
| **data.relationships** | object                | 否   | object   | 关联关系                                                     |
| **included**           | object                | 否   | object   | 关联数据                                                     |

#### 返回示例

```json
{
  "links": {
    "first": "http://discuz.test/api/order?page%5Blimit%5D=1&include=user%2Cthread",
    "next": "http://discuz.test/api/order?page%5Blimit%5D=1&page%5Boffset%5D=1&include=user%2Cthread",
    "last": "http://discuz.test/api/order?page%5Blimit%5D=1&page%5Boffset%5D=8&include=user%2Cthread"
  },
  "data": [
    {
      "type": "orders",
      "id": "9",
      "attributes": {
        "order_sn": "2019112210453097539952",
        "amount": "2.50",
        "status": 0,
        "type": 2,
        "title": "title",
        "thread_id": 2,
        "updated_at": "2019-11-22T10:45:30+08:00",
        "created_at": "2019-11-22T10:45:30+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "thread": {
          "data": {
            "type": "threads",
            "id": "2"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "mobile": "mobile",
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    },
    {
      "type": "threads",
      "id": "2",
      "attributes": {
        "title": "test",
        "price": "0.50",
        "viewCount": 0,
        "postCount": 0,
        "likeCount": 0,
        "createdAt": null,
        "updatedAt": null,
        "isApproved": true,
        "isSticky": false,
        "isEssence": false,
        "canApprove": false,
        "canSticky": false,
        "canEssence": false,
        "canDelete": false
      }
    }
  ]
}
```
