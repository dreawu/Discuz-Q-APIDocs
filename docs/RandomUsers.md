### 获取随机用户接口[列表]

- **接口说明：** 获取随机用户接口[列表]
- **接口地址：** /random/users
- **请求方式：** GET

#### 请求参数

#### 请求示例

```
/api/random/users
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称            | 类型   | 出现要求 | 描述       |
| :------------------ | :----- | :------- | :--------- |
| **data**            | object |          | 基础数据   |
| type                | string |          | 数据类型   |
| id                  | int    |          | 数据 id    |
| **attributes**      | object |          | 数据属性   |
| attributes.id       | string |          | 用户唯一ID |
| attributes.username | string |          | 用户名     |

#### 返回示例

```json
{
    "data": [
        {
            "type": "users",
            "id": "1",
            "attributes": {
                "id": 1,
                "username": "admincjw",
                "avatarUrl": ""
            }
        },
        {
            "type": "users",
            "id": "2",
            "attributes": {
                "id": 2,
                "username": "用户1",
                "avatarUrl": ""
            }
        },
        {
            "type": "users",
            "id": "3",
            "attributes": {
                "id": 3,
                "username": "用户2",
                "avatarUrl": ""
            }
        },
        {
            "type": "users",
            "id": "4",
            "attributes": {
                "id": 4,
                "username": "用户3",
                "avatarUrl": ""
            }
        }
    ]
}
```

