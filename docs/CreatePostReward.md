### 悬赏帖-采纳评论

- **接口说明：** 悬赏帖-采纳评论
- **接口地址：** /api/posts/reward
- **请求方式：** POST

#### 请求参数

| 参数名称  | 类型  | 是否必须 | 描述                |
| :-------- | :---: | :------: | :------------------ |
| thread_id |  int  |    是    | 帖子ID              |
| rewards   | float |    是    | 悬赏金额，必须大于0 |
| post_id   |  int  |    是    | 采纳的评论ID        |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "thread_id": 76,
      "rewards": 2,
      "post_id": 108
    }
  }
}
```

#### 返回说明

返回空， http 状态码： 204