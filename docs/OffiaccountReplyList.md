### 公众号获取回复列表

- **接口说明：** 公众号获取回复List
- **接口地址：** /api/offiaccount/reply
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
| :--- | :--- | :--- | :--- | :--- |
| filter[key_word]   | string  | 否  | 模糊查询  | 搜索关键词/规则名
| page[number]       | int  | 否  | 页码  |   |
| page[limit]        | int  | 否  | 条数  |   |

#### 请求示例

```
{{host}}/api/offiaccount/reply?filter[key_word]=n
```

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述 |
| :--- | :--- | :--- | :--- |
| name | string | 规则名 |  |
| keyword | string | 关键词 |  |
| match_type | int | 匹配规则 | 0全匹配1半匹配 |
| reply_type | int | 消息回复类型 | 1文本2图片3语音4视频5图文 |
| content | string | 回复文本内容 |  |
| media_id | string | 素材ID |  |
| media_type | int | 素材类型 | 1图片2视频3语音4图文 |
| type | int | 数据类型 | 0被关注回复1消息回复2关键词回复 |
| status | int | 是否开启 | 0关闭1开启 |
| created_at | string | 创建时间 |  |
| updated_at | string | 更新时间 |  |
| deleted_at | string | 删除时间 |  |

#### 返回说明

- 成功，http 状态码 200
- 失败，http 状态码 500

#### 返回示例

```json
{
    "links": {
        "first": "http://xxxxxxxx/api/reports?filter%5Bkey_word%5D=n",
        "last": "http://xxxxxxxx/api/reports?filter%5Bkey_word%5D=n"
    },
    "data": [
        {
            "type": "offiaccount_reply",
            "id": "4",
            "attributes": {
                "name": "Dina",
                "keyword": "Janick111",
                "match_type": 1,
                "reply_type": 5,
                "media_id": "ryUzCNaipU-iHQGbMFvy4ivplQqRd7Pvk63dZvq1o5E",
                "media_type": 4,
                "type": 2,
                "updated_at": "2020-06-23T20:01:24+08:00",
                "created_at": "2020-06-23T20:01:24+08:00"
            }
        },
        {
            "type": "offiaccount_reply",
            "id": "5",
            "attributes": {
                "name": "关注回复",
                "keyword": "attention",
                "match_type": 1,
                "reply_type": 5,
                "media_id": "ryUzCNaipU-iHQGbMFvy4ivplQqRd7Pvk63dZvq1o5E",
                "media_type": 4,
                "type": 0,
                "updated_at": "2020-06-23T20:01:24+08:00",
                "created_at": "2020-06-23T20:01:24+08:00"
            }
        }
    ],
    "meta": {
        "total": 2,
        "pageCount": 1
    }
}
```
