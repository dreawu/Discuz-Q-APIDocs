### 管理员邀请码[列表]

- **接口说明：** 针对管理员一码一用[列表]
- **接口地址：** /api/invite
- **请求方式：** GET

#### 请求参数

| 参数名称          | 类型   | 是否必须 | 描述                                                     |
|  :------------- | :--:   | :------: | :------------------------------------------------------- |
| filter[status]  | int    |    否    | 邀请码状态<br>0 失效<br>1 未使用<br>2 已使用<br>3 已过期 |
| page[number]    | int    |    否    | 页码                 |
| page[limit]     | int    |    否    | 单页数量             |

#### 返回结果

| 参数名称              | 类型   | 出现要求 | 描述                                               |
| :-------------------- | :----- | :------- | :------------------------------------------------- |
| type                  | string |          | 数据模型的类型                                     |
| id                    | int    |          | 分类 id                                            |
| attributes            | object |          | 数据模型的属性                                     |
| attributes.group_id   | int    |          | 用户组 id                                          |
| attributes.type       | int    |          | 类型：1普通会员邀请，2管理员邀请                       |
| attributes.code       | string |          | 邀请码                                             |
| attributes.dateline   | int    |          | 生效时间                                           |
| attributes.endtime    | int    |          | 失效时间                                           |
| attributes.user_id    | int    |          | 邀请人 id                                          |
| attributes.to_user_id | int    |          | 被邀请人 id                                        |
| attributes.status     | int    |          | 状态<br>0 失效<br>1 未使用<br>2 已使用<br>3 已过期 |

#### 返回说明

- 查询成功， http 状态码： 200

#### 返回示例

```json
{
  "links": {
    "first": "http://discuz.test/api/invite?filter%5Bstatus%5D=2",
    "last": "http://discuz.test/api/invite?filter%5Bstatus%5D=2"
  },
  "data": [
    {
      "type": "invite",
      "id": "6",
      "attributes": {
        "group_id": 11,
        "type": 2,
        "code": "WZx4NzMIA8UaI7wdwN1r1sgx0N3Z9FR9",
        "dateline": 1589272248,
        "endtime": 1589877048,
        "user_id": 1,
        "to_user_id": 0,
        "status": 2
      }
    },
    {
      "type": "invite",
      "id": "5",
      "attributes": {
        "group_id": 11,
        "type": 2,
        "code": "79Zt8yX3pLjHcp7tY2QYvafn7Ju7vatX",
        "dateline": 1589272247,
        "endtime": 1589877047,
        "user_id": 1,
        "to_user_id": 0,
        "status": 2
      }
    }
  ],
  "meta": {
    "total": 2,
    "pageCount": 1
  }
}
```
