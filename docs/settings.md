### 配置表设置接口

- **接口说明：** 设置接口
- **接口地址：** /api/settings
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型   | 描述     |
| :------- | :----- | :------- |
| key      | string | 字段名   |
| value    | string | 填入的值 |
| tag      | string | tag 类别 |

请求示例：

```json
{
  "data": [
    {
      "attributes": {
        "key": "site_mode",
        "value": "0",
        "tag": "default"
      }
    },
    {
      "attributes": {
        "key": "token",
        "value": "1",
        "tag": "qcloud"
      }
    }
  ]
}
```

现有字段值：

- 站点设置 set_site

| 字段名                | 功能对应           | tag            | 类型         | 值说明                                                                 | 参数值示例                              | 管理员<br>可见    |
| --------------------- | ------------------ | -------------- | ------------ | ---------------------------------------------------------------------- | --------------------------------------- | ----------------- |
| site_name             | 站点名称           | default        | string       | 展示在站点信息和 title                                                 | Discuz!Q                                |                   |
| site_title            | 站点标题           | default        | string       | 展示在站点 title                                                       | Discuz!Q                                |                   |
| site_keywords         | 站点关键词         | default        | string       | 展示在站点信息                                                         | 畅言,无阻                               |                   |
| site_introduction     | 站点介绍           | default        | string       | 展示在站点信息                                                         | 畅言无阻                                |                   |
| site_mode             | 站点模式           | default        | string       | public 公开模式 pay 付费模式                                           | pay                                     |                   |
| site_logo             | 站点 Logo          | default        | string       |                                                                        |                                         |                   |
| site_close            | 关闭站点           | default        | 单选         | 0 开启站点 1 关闭站点                                                  | 0                                       |                   |
| site_url              | 站点地址           | default        | 无需<br>配置 | 初始化站点时 C 端填写<br>末尾不要加斜线                                | discuz.com                              |                   |
| site_record           | 备案信息           | default        | string       | 站点的 ICP 备案编号                                                    | WQ12DISCUZ1                             |                   |
| site_stat             | 第三方统计         | default        | string       | 网站的第三方统计代码                                                   |                                         ||
| site_install          | 站点信息           | default        | string       | 站点安装时间                                                           | 2020-01-01 00:00:00                     |                   |
| site_pay_time         | 付费模式开启时间   | default        |              |                                                                        |                                         | ✓                 |
| site_price            | 加入价格           | default        | float        | 付费后加站价格                                                         | 39.9                                    | site_mode='pay'时 |
| site_expire           | 到期时间           | default        |              |                                                                        |                                         | site_mode='pay'时 |
| site_onlooker_price   | 站点围观价格       | default        | float        | 围观单价                                                               | 1                                       |                   |
| site_author_scale     | 作者比例           | default        | int          | 主题打赏分成比例,和站长比例加起来必须为 10,不填时默认为作者 10、平台 0 | 7                                       | ✓                 |
| site_master_scale     | 站长比例           | default        | int          | 主题打赏分成比例,和作者比例加起来必须为 10,不填时默认为作者 10、平台 0 | 3                                       | ✓                 |
| site_close_msg        | 关闭站点时提示信息 | default        | string       | 关闭后网站提示语                                                       | 该论坛已关闭。                          | site_close=1 时   |
| site_author.id        | 站长用户 ID        | default        | int          | 站长的用户 id                                                          | 1                                       |                   |
| site_author.username  | 站长用户 名称      | default        | string       | 站长的用户 名称                                                        | admin                                   |                   |
| site_author.avatar    | 站长用户 头像      | default        | string       | 站长的用户 头像                                                        | https://xxxxx.com/storage/avatars/1.png |                   |
| username_bout         | 用户名修改次数     | default        | int          | 允许每个用户的修改次数(管理员不受限制)默认值是 1                       |                                         |                   |
| site_header_logo      | 首页头部 LOGO      | default        |              |                                                                        |                                         |                   |
| site_background_image | 首页头部背景图     | default        |              |                                                                        |                                         |                   |
| miniprogram_video     | 微信小程序视频开关 | wx_miniprogram | int          | 开启后小程序允许发布视频、展示视频主题（0 关闭 1 开启）                | true                                    | ✓                 |
| site_pay_group_close  | 用户组购买开关     | default        | int          | 开启后永续购买用户组（0 关闭 1 开启）                                  |                                         | ✓                 |
| site_minimum_amount   | 最小自定义支付金额 | default        | float        |                                                                        |                                         |                   |
| site_open_sort | 开启智能排序 | default | int | 是否开启智能排序，0不开启，1开启 | 0 | |
| site_create_thread0 | 发布文字帖(普通帖) | default | int | 允许发布文字帖(普通帖)，0为不允许，1为允许，以下一样 | 1 | |
| site_create_thread1 | 发布帖子(长文帖) | default | int | 0为不允许，1为允许 | 1 | |
| site_create_thread2 | 发布视频帖 | default | int | 0为不允许，1为允许 | 1 | |
| site_create_thread3 | 发布图片帖 | default | int | 0为不允许，1为允许 | 1 | |
| site_create_thread4 | 发布语音帖 | default | int | 0为不允许，1为允许 | 1 | |
| site_create_thread5 | 发布问答帖(悬赏帖) | default | int | 0为不允许，1为允许 | 1 | |
| site_create_thread6 | 发布商品帖 | default | int | 0为不允许，1为允许 | 1 | |

- 注册设置 set_reg

| 字段名            | 功能对应         | tag     | 类型 | 值说明                                   | 参数值示例 | 管理员可见 |
| ----------------- | ---------------- | ------- | ---- | ---------------------------------------- | ---------- | ---------- |
| register_close    | 是否允许注册     | default | 单选 | 1 允许 0 不允许                          | 0          |            |
| register_type     | 注册模式         | default | 单选 | 0 用户名模式、1 手机号模式<br>2 无感模式 | 1          |            |
| register_captcha  | 注册验证码       | default | 单选 | 0 关闭 1 开启                            | 0          |            |
| register_validate | 是否开启注册审核 | default | 单选 | 1 开启 0 关闭                            | 0          |            |
| password_length   | 密码长度         | default | int  | 默认不填时是 6 位密码                    | 10         |            |
| password_strength | 密码强度         | default | 多选 | 0 数字 1 小写字母<br>2 符号 3 大写字母   | 0,1,2      |            |

- 第三方设置 passport

| 字段名                          | 功能对应                        | tag            | 类型   | 值说明            | 参数值示例 | 管理员可见 |
| ------------------------------- | ------------------------------- | -------------- | ------ | ----------------- | ---------- | ---------- |
| offiaccount_close               | 微信 h5 登陆开关                | wx_offiaccount |        | 0 关闭<br> 1 开启 | 0          |            |
| offiaccount_app_id              | 微信 h5 登陆                    | wx_offiaccount | 加密   | 使用              |            | ✓          |
| offiaccount_app_secret          | 微信 h5 登陆                    | wx_offiaccount | 加密   | 使用              |            | ✓          |
| offiaccount_server_config_token | 微信公众号服务器配置令牌(token) | wx_offiaccount | 加密   | 使用中            | g3G9Xo4jSM | ✓          |
| miniprogram_close               | 微信小程序登陆开关              | wx_miniprogram |        | 0 关闭<br> 1 开启 | 0          |            |
| miniprogram_app_id              | 微信小程序登陆                  | wx_miniprogram | 加密   | 使用              |            | ✓          |
| miniprogram_app_secret          | 微信小程序登陆                  | wx_miniprogram | 加密   | 使用              |            | ✓          |
| oplatform_close                 | 微信 pc 登陆开关                | wx_oplatform   |        | 0 关闭<br> 1 开启 | 0          |            |
| oplatform_app_id                | 微信开放平台                    | wx_oplatform   | 加密   | 使用              |            | ✓          |
| oplatform_app_secret            | 微信开放平台                    | wx_oplatform   | 加密   | 使用              |            | ✓          |
| oplatform_url                   | 微信 pc 登陆                    | wx_oplatform   | 不加密 | 使用              |            | ✓          |
| oplatform_app_token             | 微信 pc 登陆                    | wx_oplatform   | 不加密 | 使用              |            | ✓          |
| oplatform_app_aes_key           | 微信 pc 登陆                    | wx_oplatform   | 不加密 | 使用              |            | ✓          |

- 支付设置 paycenter

| 字段名             | 功能对应               | tag   | 类型 | 值说明          | 参数值示例 | 管理员可见 |
| ------------------ | ---------------------- | ----- | ---- | --------------- | ---------- | ---------- |
| wxpay_close        | 微信支付开关           | wxpay | 单选 | 1 允许 0 不允许 | 0          |            |
| wxpay_ios          | 关闭 IOS 微信支付      | wxpay |      |                 |            |            |
| mch_id             | 微信支付商户号         | wxpay |      | v1 使用         |            | ✓          |
| app_id             | 微信支付 app_id        | wxpay | 加密 | v1 使用         |            | ✓          |
| api_key            | 微信支付 api_key       | wxpay | 加密 | v1 使用         |            | ✓          |
| app_secret         | 微信支付 app_secret    | wxpay | 加密 | v1 使用         |            | ✓          |
| wxpay_mch_id       | 微信支付商户号         | wxpay | 加密 | 暂无使用        |            | ✓          |
| wxpay_app_id       | 微信支付 app_id        | wxpay | 加密 | 暂无使用        |            | ✓          |
| wxpay_api_key      | 微信支付 api_key       | wxpay | 加密 | 暂无使用        |            | ✓          |
| wxpay_app_secret   | 微信支付 app_secret    | wxpay | 加密 | 暂无使用        |            | ✓          |
| wxpay_close        | 微信支付开关           | wxpay | 单选 | 1 允许 0 不允许 | 0          |            |
| wxpay_mchpay_close | 微信企业付款到零钱开关 | wxpay |      | 1 开启 0 关闭   |            |            |

- 附件设置 set_attach

| 字段名           | 功能对应         | tag     | 类型   | 值说明  | 参数值示例       | 管理员可见 |
| ---------------- | ---------------- | ------- | ------ | ------- | ---------------- | ---------- |
| support_img_ext  | 支持的图片扩展名 | default | string | ,隔开   | png,gif,jpg      |            |
| support_file_ext | 支持的附件扩展名 | default | string | ,隔开   | doc,docx,pdf,zip |            |
| support_max_size | 支持最大大小     | default | int    | MB 单位 | 1                |            |

- 腾讯云设置 qcloud

| 字段名                    | 功能对应                 | tag    | 类型   | 值说明                                                                                                                                                                                                                  | 参数值示例          |
| ------------------------- | ------------------------ | ------ | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------- |
| qcloud_close              | 云 api 开关              | qcloud | 单选   | 1 允许 0 不允许                                                                                                                                                                                                         | 0                   |
| qcloud_app_id             | 云 api-app_id            | qcloud |        |
| qcloud_secret_id          | 云 api-secret_id         | qcloud | 加密   |
| qcloud_secret_key         | 云 api-secret_key        | qcloud | 加密   |
| qcloud_cms_image          | 云 api 图片安全开关      | qcloud | 单选   | true1 开启,false0 关闭                                                                                                                                                                                                  |
| qcloud_cms_text           | 云 api 内容安全开关      | qcloud | 单选   | true1 开启,false0 关闭                                                                                                                                                                                                  |
| qcloud_sms                | 短信开关                 | qcloud | 单选   | 0 关闭 1 开启                                                                                                                                                                                                           | 1                   |
| qcloud_sms_app_id         | 短信 app_id              | qcloud | 加密   |
| qcloud_sms_app_key        | 短信 app_key             | qcloud | 加密   |
| qcloud_sms_template_id    | 短信模板 id              | qcloud | 加密   |
| qcloud_sms_sign           | 短信签名                 | qcloud | 加密   |
| qcloud_cos                | 对象存储                 | qcloud | 单选   | true1 开启,false0 关闭                                                                                                                                                                                                  |
| qcloud_cos_cdn_url        | cos cdn 域名             | qcloud |        |                                                                                                                                                                                                                         |                     |
| qcloud_cos_bucket_name    | 名称                     | qcloud |        | test-1251011534                                                                                                                                                                                                         |
| qcloud_cos_bucket_area    | 地域                     | qcloud |        | ap-beijing                                                                                                                                                                                                              |
| qcloud_ci_url             | 数据万象处理域名         | qcloud |        | test-.picbj.myqcloud.com                                                                                                                                                                                                |
| qcloud_facdid             | 身份认证                 | qcloud | 单选   | true 1 开启,false 0 关闭                                                                                                                                                                                                |
| qcloud_facdid_region      | 人脸核身地域             | qcloud | 单选   | ap-beijing                                                                                                                                                                                                              |
| qcloud_vod                | 云点播开关               | qcloud | 单选   | 0 关闭 1 开启                                                                                                                                                                                                           | 1                   |
| qcloud_vod_transcode      | 转码模板                 | qcloud | int    | https://console.cloud.tencent.com/vod/video-process/template 中的模板名称 ID                                                                                                                                            | 100020              |
| qcloud_vod_cover_template | 截图模板 ID              | qcloud | int    | https://console.cloud.tencent.com/vod/video-process/template/screenshot 中的模板 ID，创建模板类型为”时间点截图“                                                                                                         |
| qcloud_vod_ext            | 视频扩展名               | qcloud | string | WMV、RM、MOV、MPEG、MP4、3GP、FLV、AVI、RMVB 等                                                                                                                                                                         | wmv,rm              |
| qcloud_vod_size           | 视频大小                 | qcloud | int    | 单位 MB                                                                                                                                                                                                                 | 1                   |
| qcloud_vod_taskflow_gif   | 动图封面任务流名称       | qcloud | string | https://console.cloud.tencent.com/vod/video-process/taskflow中创建的任务流，必须勾选“转动图”，选择合适的转动图gif模板并设置合适的时间段。如果填写了动图模板名称，则“截图模板”设置失效                                   | GifVideoCover       |
| qcloud_vod_url_key        | 云点播防盗链 Key         | qcloud | string | https://console.cloud.tencent.com/vod/distribute-play/domain 域名设置中“Key 防盗链”                                                                                                                                     | y35EzG              |
| qcloud_vod_url_expire     | 云点播防盗链签名有效期   | qcloud | int    | 单位秒。过期后该 URL 将不再有效，返回 403 响应码。考虑到机器之间可能存在时间差，防盗链 URL 的实际过期时间一般比指定的过期时间长 5 分钟，即额外给出 300 秒的容差时间。建议过期时间戳不要过短，确保视频有足够时间完整播放 | 3600                |
| qcloud_vod_watermark      | 视频水印模板 ID          | qcloud | int    | https://console.cloud.tencent.com/vod/video-process/template/watermark 中的模板 ID                                                                                                                                      | 1                   |
| qcloud_captcha_app_id     | 验证码 APPID             | qcloud | int    | 传输到腾讯云时， 此参数必须要强转(int)不然会报错!!!                                                                                                                                                                     | 848512336           |
| qcloud_captcha_secret_key | 验证码 SecretKey         | qcloud | string | 验证码必传                                                                                                                                                                                                              | K_HxHxTDonTA\*\*    |
| qcloud_captcha_ticket     | 验证码返回给用户的票据   | qcloud | string | https://cloud.tencent.com/document/product/1110/36926 只有开启验证码时传输                                                                                                                                              | ...V_CVW-U_Rypg\*\* |
| qcloud_captcha_randstr    | 验证票据需要的随机字符串 | qcloud | string | 只有开启验证码时传输                                                                                                                                                                                                    | @b19                |
| qcloud_faceid             | 实名认证开关             | qcloud | int    |                                                                                                                                                                                                                         |                     |
| qcloud_cos_doc_preview    | 是否开启文档预览         | qcloud | int    | 0 关 1 开                                                                                                                                                                                                               | 1                   |

- 提现设置 set_cash

| 字段名             | 功能对应           | tag  | 类型  | 值说明                                               | 参数值示例 | 管理员可见 |
| ------------------ | ------------------ | ---- | ----- | ---------------------------------------------------- | ---------- | ---------- |
| cash_interval_time | 提现间隔时间       | cash | int   | 每次提现间隔时间<br>1 天为 24 小时，0 或不填则不限制 | 1          | ✓          |
| cash_rate          | 提现手续费率       | cash | float | 提现手续费率（百分之）                               | 0.3        |            |
| cash_min_sum       | 单次提现最小金额   | cash | float | 用户每次提现的最小金额                               | 100        |            |
| cash_max_sum       | 单次提现最大金额   | cash | float | 用户每次提现的最大金额                               | 5000       | ✓          |
| cash_sum_limit     | 每日提现总金额上限 | cash | float | 所有用户提现的每日上限总金额                         | 5000       | ✓          |

- 水印设置 watermark

| 字段名             | 功能对应     | tag       | 类型   | 值说明     | 参数值示例          | 管理员可见 |
| ------------------ | ------------ | --------- | ------ | ---------- | ------------------- | ---------- |
| watermark          | 水印开关     | watermark | bool   | true/false | true                | ✓          |
| watermark_image    | 水印图       | watermark | string | 水印图路径 | watermark_image.png | ✓          |
| position           | 水印位置     | watermark | int    | 0 - 9      | 3                   | ✓          |
| horizontal_spacing | 水印水平边距 | watermark | int    | 0 - 9999   | 10                  | ✓          |
| vertical_spacing   | 水印垂直边距 | watermark | int    | 0 - 9999   | 10                  | ✓          |

- 位置服务

| 字段名     | 功能对应     | tag | 类型   | 值说明           | 参数值示例  | 管理员可见 |
| ---------- | ------------ | --- | ------ | ---------------- | ----------- | ---------- |
| lbs        | 位置服务     | lbs | bool   | 位置服务开关     | true        | x          |
| qq_lbs_key | 腾讯位置服务 | lbs | string | 腾讯位置服务 key | ABC-123-XYZ | x          |

- UCenter

| 字段名        | 功能对应     | tag     | 类型   | 值说明               | 参数值示例    | 管理员可见 |
| ------------- | ------------ | ------- | ------ | -------------------- | ------------- | ---------- |
| ucenter_url   | UCenter 地址 | ucenter | string | UCenter 配置的地址   | https://...   | ✓          |
| ucenter_key   | 通信秘钥     | ucenter | string | UCenter 配置的 key   | uonSweqDdqsda | ✓          |
| ucenter_appid | appid        | ucenter | string | UCenter 配置的 appid | 12            | ✓          |
| ucenter       | 开关         | ucenter | string | UCenter 配置的 key   | true          |            |

#### 返回结果

| 参数名称 | 类型 | 出现要求 | 描述 |
| :------- | :--- | :------- | :--- |


#### 返回说明

- 返回空， http 状态码： 204

#### 返回示例

```json

```
