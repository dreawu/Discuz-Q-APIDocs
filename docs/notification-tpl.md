### 获取指定系统消息模版

- **接口说明：** 获取指定系统消息模版
- **接口地址：** /api/notification/tpl/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述 |
| :------- | :--- | :------: | :--- |


#### 返回说明

- 系统消息模版列表， http 状态码： 200

#### 返回结果

| 字段名    | 变量名                 |
| :-------- | :--------------------- |
| id        | 编号                   |
| status    | 状态 1 为正常， 0 关闭 |
| type_name | 列表名称               |
| title     | 通知标题               |
| conetn    | 通知内容               |
| vars      | 可使用的变量           |
| template_id      | 模板 ID              |
| first_data       | first.DATA           |
| keywords_data    | keywords.DATA        |
| remark_data      | remark.DATA          |
| color            | data color           |
| redirect_type    | 跳转类型：0无跳转 1跳转H5 2跳转小程序 |
| redirect_url     | 跳转地址              |
| page_path        | 跳转路由              |

#### 返回示例

```json
{
  "data": {
    "type": "notification_tpls",
    "id": "1",
    "attributes": {
      "status": 1,
      "type_name": "新用户注册并加入后",
      "title": "欢迎加入{sitename}",
      "content": "{username}你好，你已经成为{sitename} 的{groupname} ，请您在发表言论时，遵守当地法律法规。祝你在这里玩的愉快。",
      "vars": {
        "{username}": "用户名",
        "{sitename}": "站点名称",
        "{groupname}": "用户组"
      },
      "template_id": "",
      "first_data": "",
      "keywords_data": [],
      "remark_data": "",
      "color": [],
      "redirect_type": "",
      "redirect_url": "",
      "page_path": ""
    }
  }
}
```
