### 创建举报数据

- **接口说明：** 创建举报数据
- **接口地址：** /api/reports
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
|:--|:--|:--|:--|:--
| user_id    | int  | 是  | 用户ID  | 举报人的用户ID
| thread_id  | int  | 否  | 主题ID  | 举报[主题/回复内容]都会传输
| post_id    | int  | 否  | 回复ID  |  如果举报的主题则不需要传输该ID
| type       | int  | 是  | 举报类型  | 0个人主页 1主题 2评论/回复
| reason     | string  | 是  | 举报理由  |  必传举报理由

#### 请求示例

```json
{
    "data": {
        "type": "reports",
        "attributes": {
          "user_id": 1,
          "thread_id": 32,
          "post_id": 68,
          "type": 2,
          "reason": "{{$randomFirstName}}"
        }
    }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| **data** | object  |   |  数据类型
| type     | string  |   |  数据类型
| id       | int     |   |  数据 id
**attributes**  | object  |   |
| user_id    | int  | 用户ID  |   举报人的用户ID
| thread_id  | int  | 主题ID  |  被举报的主题ID
| post_id    | int  | 回复ID  |  被举报的回复内容ID
| type       | int  | 举报类型  |  0个人主页 1主题 2评论/回复
| reason     | string  | 举报理由  |
| status     | int  | 举报状态  |  0未处理 1已处理
| created_at | string  | 举报创建时间  |
| updated_at | string  | 举报修改时间  |

#### 返回示例

```json
{
    "data": {
        "type": "reports",
        "id": "10",
        "attributes": {
            "user_id": 942,
            "thread_id": 253,
            "post_id": 981,
            "type": 0,
            "reason": "Rose",
            "status": 0,
            "created_at": "2020-04-28T16:07:27+08:00",
            "updated_at": "2020-04-28T16:07:27+08:00"
        }
    }
}
```
