### 删除通知[单个/批量]

- **接口说明：** 删除通知[单个/批量]
- **接口地址：** /api/notification/{id}
- **请求方式：** DELETE

#### 请求参数

#### 请求示例

```
/api/notification/20
/api/notification/11,12,13
```

#### 返回说明

- http 状态码 204

#### 返回结果

#### 返回示例
