### 查询用户组接口[单条]

- **接口说明：** 查询用户组接口[单条]
- **接口地址：** /api/groups
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型   | 描述                   |
| :------- | :----- | :--------------------- |
| id       | int    | 用户组 id              |
| include  | string | 查询结果需要关联的数据 |

#### 请求示例

```
/api/groups/7?include=permission

```

#### 返回结果

| 参数名称                                     | 类型   | 出现要求 | 描述                         |
| :------------------------------------------- | :----- | :------- | :--------------------------- |
| data                                         | object | 查询成功 | 主体数据                     |
| data.type                                    | string | classify | 数据模型的类型               |
| data.id                                      | int    | 查询成功 | 用户组 id                    |
| data.attributes                              | object | 查询成功 | 数据模型的属性               |
| data.attributes.id                           | int    | 查询成功 | 用户组 id                    |
| data.attributes.name                         | string | 查询成功 | 用户组名称                   |
| data.attributes.type                         | string | 查询成功 | 用户组类型                   |
| data.attributes.color                        | string | 查询成功 | 用户组颜色                   |
| data.attributes.icon                         | object | 查询成功 | 用户组图标
| data.attributes.isPaid                       | boolean| 查询成功 | 是否收费
| data.attributes.fee                          | float  | 查询成功 | 收费金额
| data.attributes.days                         | boolean| 查询成功 | 付费获得天数
| data.relationships                           | int    | 查询成功 | 与 included 里数据的关联关系 |
| data.relationships.groupPermission           | string | 查询成功 | 关联的类型                   |
| data.relationships.groupPermission.data      | object | 查询成功 | 主题总数                     |
| data.relationships.groupPermission.data.type | string | 查询成功 | 关联的类型                   |
| data.relationships.groupPermission.data.id   | int    | 查询成功 | 关联的 id                    |
| included                                     | array  | 查询成功 | 关联的数据                   |
| included.[].type                             | string | 查询成功 | 关联的数据类型               |
| included.[].id                               | int    | 查询成功 | 关联的数据 id                |
| included.[].attributes                       | object | 查询成功 | 数据的属性                   |
| included.[].attributes.group_id              | int    | 查询成功 | 关联的用户组 ID              |
| included.[].attributes.permission            | string | 查询成功 | 关联的用户组权限             |

#### 返回说明

- 查询成功， http 状态码： 200
- 修改失败， http 状态码： 404

#### 返回示例

```json
{
  "data": {
    "type": "groups",
    "id": "7",
    "attributes": {
      "id": 7,
      "name": "游客",
      "type": "",
      "color": "",
      "icon": ""
    },
    "relationships": {
      "groupPermission": {
        "data": [
          {
            "type": "groupPermission",
            "id": "3273"
          },
          {
            "type": "groupPermission",
            "id": "2342"
          }
        ]
      }
    }
  },
  "included": [
    {
      "type": "groupPermission",
      "id": "3273",
      "attributes": {
        "group_id": 7,
        "permission": "group.viewGroup"
      }
    },
    {
      "type": "groupPermission",
      "id": "2342",
      "attributes": {
        "group_id": 7,
        "permission": "groupPermission.updateGroupPermission"
      }
    }
  ]
}
```
