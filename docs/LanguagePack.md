### 语言包

**接口说明：** 语言包英文对照

- 异常处理的抛出错误

| code                                           | message                | static | detail |
| ---------------------------------------------- | ---------------------- | ------ | ------ |
| unknown_error                                  | 未知错误(公共错误名)   | 500    |
| category_not_found                             | 分类未找到             | 500    |
| censor_not_passed                              | 检查未通过             | 500    |
| sms_interval                                   | 短信间隔错误           |
| model_not_found                                | 模型未找到             | 404    |
| route_not_found                                | 路由未找到             | 404    |
| no_bind_user                                   | 未绑定用户             | 400    |
| rebind_mp_wechat                               | 更换微信公众号绑定             | 400    |
| sms_verify_error                               | 短信验证错误           |
| thread_count_fail                              | 主题数操作错误         |
| thread_behavior_fail                           | 主题状态异常           |
| thread_action_fail                             | 主题操作异常           |
| update_permission_error                        |                        | 404    |
| upload_error                                   | 上传图片失败           | 404    |
| file_not_allow                                 |
| scale_sum_not_10                               | 分成比例相加必须为 10  |
| cannot_delete_category_with_threads            | 无法删除存在主题的分类 |
| cannot_create_image_thread_without_attachments | 发表图片帖必须带有图片 |
| user_update_error                              | 修改信息失败           |
| uninitialized_pay_password                     | 未初始化支付密码       |
| invalid_emoji_path                             | 无效的表情目录         |
| cannot_create_thread_without_goods             | 发表商品帖必须带有商品  |
| post_goods_illegal                             | 商品不合法  |
| pc_qrcode_scanning_code                        | 扫码中 |
| pc_qrcode_time_out                             | 二维码已失效，扫码超时 |
| pc_qrcode_time_fail                            | 扫码登陆失败 |
| pc_qrcode_error                                | 二维码 code 错误 |

- 代码中的抛出错误

| code                                    | message                                        | static       | detail                      |
| --------------------------------------- | ---------------------------------------------- | ------------ | --------------------------- |
| upload_time_not_up                      | 一天仅允许修改一次头像                         | 404          |
| wallet_error                            |
| validation_error                        |  表单验证错误                                             | 422  | detail                      |
| permission_denied                       | 没有权限                                       | 401          |
| ban_user                                | 您的账号被禁用，无法访问本站点                 | 401          |
| register_validate                       | 帐号审核中，请审核通过后尝试                   | 401          |                             |
| register_close                          | 注册关闭                   | 401          |                             |
| validate_ignore                         | 帐号审核忽略，请联系管理员                   | 401          |                             |
| validate_reject                         | 帐号审核拒绝，请联系管理员                   | 401          |                             |
| user_deny                               | 您在对方的黑名单中                             | 401          |                             |
| not_authenticated                       |
| file_type_not_allow                     | 文件类型不允许                                 |
| file_size_not_allow                     | 文件大小不允许                                 |
| content_banned                          |
| login_failed                            | 帐号或密码错误                                 | 403          | 登录失败，您还可以尝试 N 次 |
| login_failures_times_toplimit           | 密码错误次数达到 5 次，请 15 分钟后再次尝试    | 402          |
| cannot_delete_category_with_threads     |
| Cannot delete the default group         |
| No Qcloud Service was specified.        |
| File must be readable.                  |
| trade_error                             | 交易错误                                       | 500          | detail                      |
| order_post_not_found                    | 订单错误                                       | 500          | 订单主题不存在              |
| order_type_error                        | 订单错误                                       | 500          | 订单类型有误                |
| order_create_failure                    | 订单错误                                       | 500          | 订单创建失败                |
| order_amount_error                      | 提现错误                                       | 500          | 订单金额有误                |
| operate_type_error                      | 钱包错误                                       | 500          | 操作类型不存在              |
| wallet_status_error                     | 钱包错误                                       | 500          | 钱包状态有误                |
| operate_forbidden                       | 提现审核错误                                   | 500          | 非法操作                    |
| available_amount_error                  | 提现错误                                       | 500          | 钱包可用金额不足            |
| cash_interval_time                      | 提现错误                                       | 500          | 提现处于限制间隔天数内      |
| cash_sum_limit                          | 提现错误                                       | 500          | 超出每日提现金额限制        |
| available_amount_error                  | 提现错误                                       | 500          | 钱包可用金额不足            |
| status_cash_freeze                      | 提现错误                                       | 500          | 钱包已冻结提现              |
| mobile_is_already_bind                  | 手机号码已存在                                 | 500          |
| user_has_mobile                         | 用户已绑定手机号                                 | 500          |
| post_not_comment                        | 不能回复，回复回帖的内容                       | 500          |
| post_not_fond                           | 未查询到该条回复                               | 500          |
| setting_fill_register_reason            | 注册时的注册原因必须必填                       | 500          |
| notification_is_missing_template_config | 微信推送信息不全                               | 500          |
| tencent_secret_key_error                | 腾讯云 Secretid 或 SecretKey 不正确            | 500          |
| tencent_vod_transcode_error             | 腾讯云云点播转码模板未设置                     | 500          |
| tencent_vod_subappid_error              | 腾讯云云点播子应用错误                         | 500          |
| pay_password_failures_times_toplimit    | 您输入的密码错误次数已超限，请在次日重试或重置 | 500          |
| bind_error                              | 小程序登录时，绑定用户被删除                   | 500          |
| wechat_mobile_unbound                   | 微信未绑定手机号                               | 500          |
| unable_to_get_location                  | 无法获取地理位置                               | 404          |
| qcloud_file_preview_unset               | 腾讯云对象存储文件预览未开启                              | 500          |

- 框架中的抛出错误

| code                                                          | message | static | detail |
| ------------------------------------------------------------- | ------- | ------ | ------ |
| No Socialite driver was specified.                            |
| Notification is missing toDatabase / toArray method.          |
| MariaDB version too low. You need at least MariaDB 10.0.5     |
| Notification is missing toArray method.                       |
| MySQL version too low. You need at least MySQL 5.6.           |
| Serializer must be an instance of [...]                       |
| Callback must be a callable or a 'Class@method' string.       |
| Policy not defined for [...]                                  |
| You must set the gate user with forUser()                     |
| Controller must be an instance of [...]                       |
| The search builder is undefined.                              |
| Route [$name] not found                                       |
| Invalid request handler: %s                                   |
| Cache store [{$name}] is not defined.                         |
| Driver [{\$config['driver']}] is not supported.               |
| not found user id                                             |
| A non-empty PSR-4 prefix must end with a namespace separator. |
| Unable to detect application namespace.                       |

### 特殊处理数据格式

- validation_error

> 用户注册需要弹出`detail`,用户登录就不需要弹出

```
{
    "errors": [
        {
            "status": "422",
            "code": "validation_error",
            "detail": [
                "密码 至少为 6 个字符。",
                "密码 格式不正确。"
            ],
            "source": {
                "pointer": "/data/attributes/password"
            }
        }
    ]
}
```
