### 公众号图文素材转站内帖子

- **接口说明：** 公众号图文素材转站内帖子
- **接口地址：** /api/offiaccount/transform
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 名称 | 描述 | 是否必须 | 传输类型 |
| :--- | :--- | :--- | :--- | :--- | :--- |
| media_id  | string  | 素材ID | 必须是图文素材ID | 是 | query |

#### 请求示例

```
{{host}}/api/offiaccount/transform?media_id=ryUzCNaipU-i******aNkzQcUF2brxQZwy7G4
```

#### 返回结果

| 参数名称                | 类型     | 出现要求   | 描述                                              |
| :---------------------- | :------- | :--------- | :------------------------------------------------ |
| **data**                | object   |            | 基础数据                                          |
| type                    | string   |            | 数据类型                                          |
| id                      | int      |            | 数据 id                                           |
| **attributes**          | object   |            | 数据属性                                          |
| type                    | int      |            | 文章类型(0 普通 1 长文 2 视频)                    |
| title                   | string   | 长文主题   | 标题                                              |
| price                   | float    | 长文主题   | 主题价格                                          |
| viewCount               | int      |            | 查看数                                            |
| postCount               | int      |            | 帖子数                                            |
| paidCount               | int      |            | 付费数                                            |
| rewardedCount           | int      |            | 打赏数                                            |
| createdAt               | datetime |            | 创建时间                                          |
| updatedAt               | datetime |            | 修改时间                                          |
| deletedAt               | datetime | 在回收站时 | 删除时间                                          |
| isApproved              | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isSticky                | bool     |            | 是否置顶                                          |
| isEssence               | bool     |            | 是否加精                                          |
| isFavorite              | bool     | 已收藏时   | 是否收藏                                          |
| paid                    | bool     | 付费主题   | 是否付费                                          |
| canViewPosts            | bool     |            | 是否有权查看详情                                  |
| canReply                | bool     |            | 是否有权回复                                      |
| canApprove              | bool     |            | 是否有权审核                                      |
| canSticky               | bool     |            | 是否有权置顶                                      |
| canEssence              | bool     |            | 是否有权加精                                      |
| canDelete               | bool     |            | 是否有权永久删除                                  |
| canHide                 | bool     |            | 是否有权放入回收站                                |
| canFavorite             | bool     |            | 是否有权收藏                                      |

#### 返回说明

- 成功，http 状态码 200
- 失败，http 状态码 500

#### 返回示例

```json
{
    "data": {
        "type": "threads",
        "id": "1723",
        "attributes": {
            "type": 1,
            "title": "试一下Markdown",
            "price": "0.00",
            "freeWords": 0,
            "viewCount": 0,
            "postCount": 1,
            "paidCount": 0,
            "rewardedCount": 0,
            "createdAt": "2020-08-04T11:45:31+08:00",
            "updatedAt": "2020-08-04T11:45:31+08:00",
            "isApproved": 0,
            "isSticky": false,
            "isEssence": false,
            "canViewPosts": true,
            "canReply": true,
            "canApprove": true,
            "canSticky": true,
            "canEssence": true,
            "canDelete": true,
            "canHide": true,
            "canEdit": true,
            "isDeleted": false,
            "canFavorite": true,
            "isFavorite": false
        }
    }
}
```
