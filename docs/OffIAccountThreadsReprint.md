### 站内帖子转公众号图文素材

- **接口说明：** 站内帖子转公众号图文素材
- **接口地址：** /api/offiaccount/reprint/{thread_id}
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 名称 | 描述 | 是否必须 | 传输类型 |
| :--- | :--- | :--- | :--- | :--- | :--- |
| query/{thread_id}  | int  | 主题ID | | 是 | query |

#### 请求示例

```
{{host}}/api/offiaccount/reprint/1560
```

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述 |
| :--- | :--- | :--- | :--- |
| title | string | 公众号标题 |  |
| content | string | 公众号内容 |  |
| media_id | string | 图文消息的素材ID | |

#### 返回说明

- 成功，http 状态码 200
- 失败，http 状态码 500

#### 返回示例

```json
{
    "title": "测试Markdown",
    "content": "<p><strong>粗体</strong></p>\n\n<p><em>斜体</em><code>代码块</code></p>",
    "media_id": "ryUzCNaipU-i**********pPBZ1aWxQ"
}
```
