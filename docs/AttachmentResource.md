### 获取附件接口[单条]

- **接口说明：** 如果是付费附件，接口不能直接调取，需要先访问主题详情接口获取附件数据，附件数据中的url参数会包含附件token，使用带token的url获取付费附件。
- **接口地址：** /api/attachments/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述           |
| :------- | :----: | :------: | :------------- |
| id       |  int   |    是    | 附件ID          |
|  t       | string |    是    | 付费附件token,访问主题详情页时会生成在附件地址上       |
| thumb    |  true  |    否    | 是否显示缩略图    |
| page     |  int   |    否    | 付费附件预览的页码。传入时会返回可预览文件指定页码的图片流。需要文件已付费且开启了COS。会使用COS文件预览能力。相关文档：https://cloud.tencent.com/document/product/436/45906 |
| isAttachment     |  int   |    否    | 1时Content-Disposition为attachment，可以弹出下载框。0或不传，Content-Disposition为inline，如果是mp3 mp4 jpg等会直接播放 |


#### 请求示例

```
# 获取缩略图
/api/attachments/1?thumb=true

# 获取原图
/api/attachments/1

# 下载附件
/api/attachments/1

# 预览附件
/api/attachments/1?t=123&page=1
```

#### 返回说明

帖子图片：直接显示图片  
帖子附件：下载附件（包括图片附件）
预览附件：X-Total-Page 总页数   image 预览生成的图片（base64格式）
