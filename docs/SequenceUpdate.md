### 创建智能排序条件

- **接口说明：** 创建智能排序条件
- **接口地址：** /api/sequence
- **请求方式：** POST

#### 请求参数

| 参数名称         |  类型  | 是否必须 | 描述                         |
| :--------------- | :----: | :------: | :--------------------------- |
| category_ids     | string |    是    | 添加的内容分类ID             |
| group_ids        | string |    是    | 添加的用户角色ID             |
| user_ids         | string |    是    | 添加的用户ID                 |
| topic_ids        | string |    是    | 添加的话题ID                 |
| thread_ids       | string |    是    | 添加的帖子ID                 |
| block_user_ids   | string |    是    | 排除的用户ID                 |
| block_topic_ids  | string |    是    | 排除的主题ID                 |
| block_thread_ids | string |    是    | 排除的帖子ID                 |
| site_open_sort   |  int   |    是    | 是否开启排序，1开启，0不开启 |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "category_ids": "1,2,3",
      "group_ids": "1,2,3",
      "user_ids": "1,2,3",
      "topic_ids": "1,2,3",
      "thread_ids": "1,2,3",
      "block_user_ids": "1,2,3",
      "block_topic_ids": "1,2,3",
      "block_thread_ids": "1,2,3"
    }
  }
}
```

#### 返回说明

返回空， http 状态码： 204