### 首页推荐内容接口

- **接口说明：** 首页推荐内容接口
- **接口地址：** /api/threads/recommend
- **请求方式：** GET

#### 请求示例

```
/api/threads/recommend
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称                            | 类型     | 出现要求 | 描述                                                         |
| :---------------------------------- | :------- | :------- | :----------------------------------------------------------- |
| **data**                            | object   |          | 基础数据                                                     |
| type                                | string   |          | 数据类型                                                     |
| id                                  | int      |          | 数据 id                                                      |
| **attributes**                      | object   |          | 数据属性                                                     |
| attributes.type                     | int      |          | 帖子类型：0 普通 1 长文 2 视频 3 图片 4 语音 5 问答帖 6 商品帖 |
| attributes.title                    | string   |          | 帖子标题                                                     |
| attributes.price                    | float    |          | 帖子付费价格                                                 |
| attributes.attachmentPrice          | float    |          | 帖子-附件的价格                                              |
| attributes.viewCount                | int      |          | 阅读数                                                       |
| attributes.postCount                | int      |          | 评论数                                                       |
| attributes.isRedPacket              | int      |          | 是否为红包，0不是，1是                                       |
| attributes.postContent              | string   |          | 帖子的内容，标题为空的话就用这个字段~要读帖子部分内容也从这里截取 |
| **attributes.questionTypeAndMoney** | object   |          | 子类，里面的字段和上面的一致                                 |
| id                                  | int      |          | 悬赏问答扩展表主键ID                                         |
| thread_id                           | int      |          | 悬赏/问答帖ID                                                |
| post_id                             | int      |          | 悬赏/问答帖主要内容ID                                        |
| type                                | int      |          | 类型，0为所有人可回答的悬赏帖，1为指定人回答的问答帖         |
| answer_id                           | int      |          | 被指定回答人的ID，可为空                                     |
| money                               | float    |          | 悬赏金额/问答金额                                            |
| remain_money                        | float    |          | 剩余金额                                                     |
| created_at                          | datetime |          | 创建时间                                                     |
| updated_at                          | datetime |          | 更新时间                                                     |
| expired_at                          | datetime |          | 过期时间                                                     |

#### 返回示例

```json
{
    "data": [
        {
            "type": "threads",
            "id": "30",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 6,
                "postCount": 4,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-21T11:43:41+08:00",
                "updatedAt": "2020-12-22T17:03:37+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 1,
                "postContent": "普通帖-带红包",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "canFavorite": false,
                "isFavorite": false
            }
        },
        {
            "type": "threads",
            "id": "31",
            "attributes": {
                "type": 1,
                "title": "长文帖-带红包",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 6,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-21T11:44:15+08:00",
                "updatedAt": "2020-12-21T11:44:15+08:00",
                "isApproved": 1,
                "isSticky": true,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 1,
                "postContent": "1112223334445556678899",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "canFavorite": false,
                "isFavorite": false
            }
        },
        {
            "type": "threads",
            "id": "50",
            "attributes": {
                "type": 0,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 1,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T19:51:04+08:00",
                "updatedAt": "2020-12-23T19:51:04+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 1,
                "postContent": "#红包帖# 这个普通帖即将是一个红包帖",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "canFavorite": false,
                "isFavorite": false
            }
        },
        {
            "type": "threads",
            "id": "51",
            "attributes": {
                "type": 1,
                "title": "这个长文帖即将是个红包帖",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 1,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T19:51:48+08:00",
                "updatedAt": "2020-12-23T19:51:48+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 1,
                "postContent": "#红包帖# 这是一个红包帖子喔",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "canFavorite": false,
                "isFavorite": false
            }
        },
        {
            "type": "threads",
            "id": "52",
            "attributes": {
                "type": 5,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 1,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T19:54:10+08:00",
                "updatedAt": "2020-12-23T19:54:10+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 0,
                "postContent": "#悬赏帖# 这即将是一个悬赏帖，啦啦啦啦啦啦啦啦啦。",
                "questionTypeAndMoney": {
                    "id": 4,
                    "thread_id": 52,
                    "post_id": 60,
                    "type": 0,
                    "answer_id": null,
                    "money": "5.00",
                    "remain_money": "5.00",
                    "created_at": "2020-12-23T12:00:00.000000Z",
                    "updated_at": "2020-12-23T12:00:03.000000Z",
                    "expired_at": "2020-12-30 20:00:07"
                },
                "album_content": "",
                "isDeleted": false,
                "onlookerState": true,
                "canFavorite": false,
                "isFavorite": false
            }
        },
        {
            "type": "threads",
            "id": "53",
            "attributes": {
                "type": 5,
                "title": "",
                "price": "0.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 2,
                "postCount": 1,
                "paidCount": 0,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T19:55:08+08:00",
                "updatedAt": "2020-12-23T19:55:08+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 0,
                "postContent": "#悬赏帖# 这也将会是一个悬赏帖，请注意！！111111",
                "questionTypeAndMoney": {
                    "id": 5,
                    "thread_id": 53,
                    "post_id": 61,
                    "type": 0,
                    "answer_id": null,
                    "money": "10.00",
                    "remain_money": "10.00",
                    "created_at": "2020-12-23T12:00:35.000000Z",
                    "updated_at": "2020-12-23T12:00:38.000000Z",
                    "expired_at": "2020-12-31 20:00:41"
                },
                "album_content": "",
                "isDeleted": false,
                "onlookerState": true,
                "canFavorite": false,
                "isFavorite": false
            }
        }
    ]
}
```
