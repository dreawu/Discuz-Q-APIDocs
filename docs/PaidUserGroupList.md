### 用户付费用户组列表

- **接口说明：** 用户付费用户组列表
- **接口地址：** /groups/paid
- **请求方式：** GET

#### 请求参数

| 字段名      | 变量名       | 必填 | 类型  | 描述                                       |
| :---------- | :----------- | :--- | :---- | :----------------------------------------- |
| 排序参数 | sort                   | 否   | string   | 可选值：created_at、updated_at。 |  
| 筛选参数 | filter[user]         | 否   | int      | 用户id,获取当前用户数据，请传递本用户 user_id  |
| 筛选参数 | filter[operator_id]         | 否   | int      | 操作人用户id  |
| 筛选参数 | filter[group_id]         | 否   | int      | 用户组id  |
| 筛选参数 | filter[order_id]         | 否   | int      | 订单id  |
| 筛选参数 | filter[delete_type]         | 否   | int      | 删除类型：0 正常；1 到期删除，2 管理员修改，3 用户复购删除 |
| 关联参数 | include                | 否   | string   | 关联数据（包含 user, group, operator, order）   


#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                | 必填 | 类型     | 描述                                            |
| :--------------------- | :-------------------- | :--- | :------- | :---------------------------------------------- |
| **data.attributes**    | object                | 是   | object   | 数据属性                                        |
| 用户id               | attributes.user_id   | 是   | string   | 购买用户组用户id                                    |
| 用户组id               | attributes.group_id   | 是   | string   | 购买的用户组id                                    |
| 订单id               | attributes.order_id   | 是   | string   | 购买关联订单id                                    |
| 操作人id               | attributes.operator_id   | 是   | string   | 添加用户组的管理人员id                                    |
| 删除类型              | attributes.delete_type   | 是   | string   | 删除类型：0 正常；1 到期删除，2 管理员修改，3 用户复购删除                                    |
| 到期时间               | attributes.expiration_time   | 是   | string   | 用户组到期时间                                    |
| 创建时间               | attributes.updated_at   | 是   | string   | 创建时间                                    |
| 修改时间              | attributes.created_at   | 是   | string   | 修改时间                                    |
| 删除时间               | attributes.deleted_at   | 是   | string   | 删除时间                                    |
| **data.relationships** | object                | 是   | object   | 关联关系                                        |
| **included**           | object                | 是   | object   | 关联数据（包含 user, group, operator, order） |

#### 返回示例

```json
{
    "links": {
        "first": "http://discuz.test/api/groups/paid?include=group&page%5Blimit%5D=1",
        "last": "http://discuz.test/api/groups/paid?include=group&page%5Blimit%5D=1"
    },
    "data": [
        {
            "type": "group_paid_user",
            "id": "1",
            "attributes": {
                "user_id": 1,
                "group_id": 11,
                "order_id": 2,
                "operator_id": 2,
                "delete_type": 0,
                "expiration_time": "2020-09-29T13:40:57+08:00",
                "updated_at": "2020-09-27T13:40:57+08:00",
                "created_at": "2020-09-27T13:40:57+08:00",
                "deleted_at": null
            },
            "relationships": {
                "group": {
                    "data": {
                        "type": "groups",
                        "id": "11"
                    }
                }
            }
        }
    ],
    "included": [
        {
            "type": "groups",
            "id": "11",
            "attributes": {
                "name": "付费用户组",
                "type": "",
                "color": "",
                "icon": "http://discuz.test/images/groups/group-10.svg",
                "default": false,
                "isDisplay": false,
                "isPaid": true,
                "fee": 1,
                "days": 2,
                "scale": 0.3,
                "is_subordinate": false,
                "is_commission": false
            }
        }
    ],
    "meta": {
        "total": 1,
        "pageCount": 1
    }
}
```
