### 修改用户组权限接口

- **接口说明：** 修改用户组权限接口
- **接口地址：** /api/permission
- **请求方式：** POST
- **注意：** 该接口会先删除该用户组下所有的权限，然后添加勾选的权限

#### 请求参数

| 参数名称    | 类型  | 是否必须 | 描述     |
| :---------- | :---: | :------: | :------- |
| permissions | array |    否    | 权限名称 |
| groupId     |  int  |    否    | 用户组id |

#### 增加的权限列表

| 权限                    | 名称                    |
| :---------------------- | :---------------------- |
| createThread.0.redPack  | 发布文字帖(普通帖)-红包 |
| createThread.0.position | 发布文字帖(普通帖)-位置 |
| createThread.1.redPack  | 发布帖子(长文帖)-红包   |
| createThread.1.position | 发布帖子(长文帖)-位置   |
| createThread.2.position | 发布视频帖-位置         |
| createThread.3.position | 发布图片帖-位置         |
| createThread.4.position | 发布语音帖-位置         |
| createThread.5.position | 发布问答帖-位置         |
| createThread.6.position | 发布商品帖-位置         |

#### **请求示例：**

```json
{
  "data": {
    "attributes": {
      "groupId": 10,
      "permissions": ["user.view", "user.edit"]
    }
  }
}
```

#### 返回结果

响应 204，无响应体


#### 返回说明

- 修改成功， http 状态码： 204
- 修改失败， http 状态码： !204
