### 普通用户创建邀请码

- **接口说明：** 普通用户创建邀请码
- **接口地址：** /api/userInviteCode
- **请求方式：** GET

#### 分成说明

- 金额最低分成规则
  1. 0.04 三人都可以分到
  2. 0.03 只分给了站长，上级不够分
  3. 0.02 都不够分，直接打给了作者

- 作者收到的打赏通知是实际到账金额数

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称  | 类型   | 出现要求 | 描述     |
| :-------- | :----- | :------- | :------- |
| **data**  | object |          | 基础数据 |
| data.type | string |          | 数据类型 |
| data.code | string |          | 邀请码   |

#### 返回示例

```json
{
  "data": {
    "type": "invite",
    "code": "eyJpdiI6Ikt6YWxyZUlKaVlPVkhEZVIzUFF3V3c9PSIsInZhbHVlIjoiS3FXSjlocG1mcEJkSll6dmo3YXFuUT09IiwibWFjIjoiYWE1OTI5ZjA0MWVkNjYzZGY1ZDkzNGVjNGQ0ZDdiYmQ1OGVjYWI0M2IwZDdlYWMxMjkxZjcyYTQ5NmYxODAwMyJ9"
  }
}
```
