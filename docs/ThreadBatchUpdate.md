### 修改主题接口[批量]

- **接口说明：** 修改主题[批量]
- **接口地址：** /api/threads/batch
- **请求方式：** PATCH

#### 请求参数

> 修改满足筛选条件的数据时，仅需在 meta['query'] 中传入筛选条件，在 meta['type'] 中传入操作类型
> approve 审核通过
> ignore 忽略
> delete 删除
> restore 还原

| 参数名称               |  类型  | 是否必须 | 描述                                              | 示例 |
| :--------------------- | :----: | :------: | :------------------------------------------------ | :--- |
| isApproved             |  int   |    否    | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| isSticky               |  bool  |    否    | 是否置顶                                          |
| isEssence              |  bool  |    否    | 是否加精                                          |
| isDeleted              |  bool  |    否    | 是否删除（回收站）                                |
| isFavorite             |  bool  |    否    | 是否收藏                                          |
| isSite                 |  bool  |    否    | 是否推荐到站点信息页                                          |
| relationships.category |  bool  |    否    | 分类关联关系                                      |
| message                | string |    否    | 操作原因                                          |

#### 请求示例

```json
// 修改部分数据
{
    "data": [
        {
            "type": "threads",
            "id": 100,
            "attributes": {
                "isApproved": true,
                "isSticky": true,
                "isEssence": false
            },
            "relationships": {
                "category": {
                    "data": {
                        "type": "categories",
                        "id": 4
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": 2,
            "attributes": {
                "isApproved": true,
                "isSticky": false,
                "isEssence": true
            },
            "relationships": {
                "category": {
                    "data": {
                        "type": "categories",
                        "id": 4
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": 3,
            "attributes": {
                "isApproved": true,
                "isSticky": true,
                "isEssence": false
            },
            "relationships": {
                "category": {
                    "data": {
                        "type": "categories",
                        "id": 4
                    }
                }
            }
        }
    ]
}

// 修改满足筛选条件的数据
{
    "meta": {
        "query": {
            "filter": {
            	"userId": 1
            }
        },
        "type": "approve"
    }
}
```

#### 返回说明

- http 状态码 200

#### 返回结果

data 被修改的主题列表
meta 出现异常的主题列表

#### 返回示例

```php
{
    "data": [
        {
            "type": "threads",
            "id": "4",
            "attributes": {
                "title": "",
                "price": "0.00",
                "viewCount": 0,
                "postCount": 3,
                "likeCount": 0,
                "createdAt": "2019-11-12T17:10:40+08:00",
                "updatedAt": "2019-11-13T14:37:08+08:00",
                "isApproved": true,
                "isSticky": false,
                "isEssence": false,
                "canFavorite": false
            }
        }
    ],
    "meta": [
        {
            "id": "1",
            "message": "model_not_found"
        },
        {
            "id": "2",
            "message": "model_not_found"
        },
        {
            "id": "3",
            "message": "permission_denied"
        }
    ]
}
```
