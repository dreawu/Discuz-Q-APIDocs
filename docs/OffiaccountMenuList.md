### 公众号获取菜单接口

- **接口说明：** 公众号获取菜单接口[单条]
- **接口地址：** /api/offiaccount/menu
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述 |
| :--- | :--- | :--- | :--- |

#### 请求示例

```
无
```

#### 返回结果

| 参数名称 | 类型 | 描述 | 关联字段 |
| :--- | :--- | :--- | :--- |
| type  | click  | key值则是触发数据库存储的关键字  | key  |
|   | view  | 跳转类型  | url  |
|   | miniprogram  | 小程序跳转  | appid、pagepath |
|   | media_id  | 接收对应素材  | media_id  |
|   | view_limited  | 接收图文消息  | media_id  |
|   | ~location_select~  | ~发送位置(暂时未用到可扩展)~  | ~key~  |
|   | ~pic_photo_or_album~  | ~拍照或者相册发图(暂时未用到可扩展)~  | ~key~  |
|   | ~scancode_waitmsg~  | ~扫码带提示(暂时未用到可扩展)~  | ~key~  |
|   | ~scancode_push~  | ~扫码推事件(暂时未用到可扩展)~  | ~key~  |
| sub_button  | array  | 二级菜单才会有  |   |

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回示例

```json
{
    "data": {
        "type": "offiaccount_menu",
        "id": "1",
        "attributes": {
            "menu": {
                "button": [
                    {
                        "type": "click",
                        "name": "今日歌曲",
                        "key": "attention",
                        "sub_button": []
                    },
                    {
                        "name": "菜单",
                        "sub_button": [
                            {
                                "type": "view",
                                "name": "搜索",
                                "url": "http://www.soso.com/",
                                "sub_button": []
                            },
                            {
                                "type": "miniprogram",
                                "name": "小程序跳转",
                                "url": "http://mp.weixin.qq.com",
                                "sub_button": [],
                                "appid": "appid",
                                "pagepath": "pages/home/index"
                            },
                            {
                                "type": "media_id",
                                "name": "media_id赞一下",
                                "sub_button": [],
                                "media_id": "ryUzCNaipU-dwq1d81gegrrt8-fdg1e89weqq3"
                            },
                            {
                                "type": "media_id",
                                "name": "image图片消息",
                                "sub_button": [],
                                "media_id": "ryUzCNaipU-dwq1d81gegrrt8-fdg1e89weqq3"
                            },
                            {
                                "type": "view_limited",
                                "name": "limited图文消息",
                                "sub_button": [],
                                "media_id": "ryUzCNaipU-dwq1d81gegrrt8-fdg1e89weqq3"
                            }
                        ]
                    },
                    {
                        "name": "加试",
                        "sub_button": [
                            {
                                "type": "location_select",
                                "name": "发送位置",
                                "key": "rselfmenu_2_0",
                                "sub_button": []
                            },
                            {
                                "type": "pic_photo_or_album",
                                "name": "拍照或者相册发图",
                                "key": "rselfmenu_1_1",
                                "sub_button": []
                            },
                            {
                                "type": "scancode_waitmsg",
                                "name": "扫码带提示",
                                "key": "rselfmenu_0_0",
                                "sub_button": []
                            },
                            {
                                "type": "scancode_push",
                                "name": "扫码推事件",
                                "key": "rselfmenu_0_1",
                                "sub_button": []
                            }
                        ]
                    }
                ]
            }
        }
    }
}
```
