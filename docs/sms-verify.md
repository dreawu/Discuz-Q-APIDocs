### 短信验证

- **接口说明：** 短信验证
- **接口地址：** /api/sms/verify
- **请求方式：** POST

#### 请求参数

| 参数名称                  |  类型   | 是否必须 | 描述                                                                                                                                                                                                      |
| :------------------------ | :-----: | :------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| mobile                    | string  |    是    | 手机号码                                                                                                                                                                                                  |
| code                      | string  |    是    | 验证码                                                                                                                                                                                                    |
| type                      | string  |    是    | 类型说明：<br>login 注册/登录<br>bind 绑定手机号<br>rebind 重绑手机号<br>reset_pwd 重设登录密码<br>reset_pay_pwd 重设支付密码<br>verify 验证手机号是否为当前用户（可不传手机号，但必须传 Token）          |
| password                  | string  |    否    | 类型为 reset_pwd 时必须                                                                                                                                                                                   |
| pay_password              | string  |    否    | 类型为 reset_pay_pwd 时必须                                                                                                                                                                               |
| pay_password_confirmation | string  |    否    | 类型为 reset_pay_pwd 时必须                                                                                                                                                                               |
| inviteCode                | string  |    否    | 类型为 login 时 可传邀请码进行邀请注册                                                                                                                                                                    |
| register                  |   int   |    否    | type为login时，传入本字段决定是否自动注册手机号为账号的用户（0 不注册 1 注册）默认不注册                                                                                                                                                             |
| token                     | string  |    否    |   手机号登陆时绑定微信传入。通过[微信公众号H5无感登陆](wechat-user.md)获取                                                                                                                                                          |
| inviteCode                | string  |    否    | 类型为 login 时 可传邀请码进行邀请注册                                                                                                                                                                    |
| js_code                   | string  |    否    | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序登陆 code，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/login/wx.login.html                                              |
| iv                        | string  |    否    | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序用户数据数据加密算法的初始向量，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html               |
| encryptedData             | string  |    否    | 小程序用户名密码登录时传入，会绑定小程序用户信息。<br>微信小程序用户包括敏感数据在内的完整用户信息的加密数据，https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "mobile": "mobile",
      "code": "29416",
      "type": "login"
    }
  }
}
```

#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 400  未绑定用户 no_bind_user

#### 返回结果 (成功) login 类型

| 参数名称                 | 类型   | 出现要求 | 描述           |
| :----------------------- | :----- | :------- | :------------- |
| type                     | string | token    | 数据模型的类型 |
| id                       | int    | 成功     | 分类 id        |
| attributes               | object | 成功     | 数据模型的属性 |
| attributes.token_type    | int    | 成功     | token 类型     |
| attributes.expires_in    | string | 成功     | 过期时间       |
| attributes.access_token  | string | 成功     | token          |
| attributes.refresh_token | string | 成功     | 刷新 token     |
#### 返回结果 (失败) login 类型

| 参数名称                   | 类型   | 出现要求 | 描述           |
| :----------------------- | :----- | :---- | :------------- |
| status                   | string |      | 错误码 400          |
| code                     | int    |      | 错误描述 no_bind_user        |
| token                    | object |      | 供注册、登陆时进行手机号绑定使用 |
| user                     | null   |      | 固定值null     |

#### 返回结果 (成功) bind reset_pwd reset_pay_pwd 类型

| 参数名称               | 类型   | 出现要求 | 描述           |
| :--------------------- | :----- | :------- | :------------- |
| type                   | string | token    | 数据模型的类型 |
| id                     | int    | 成功     | 用户 id        |
| attributes             | object | 成功     | 数据模型的属性 |
| attributes.username    | int    | 成功     | 用户名         |
| attributes.mobile      | string | 成功     | 手机号         |
| attributes.lastLoginIp | string | 成功     | 最后登录时间   |
| attributes.createdAt   | string | 成功     | 注册时间       |
| attributes.updatedAt   | string | 成功     | 更新时间       |

#### 返回示例

login 类型 示例：

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1QiLCJhbGci....hsL7aw50A",
      "refresh_token": "def50200c6613b478a7...3abb"
    }
  }
}
```
```
{
    "errors": [
        {
            "status": 400,
            "code": "no_bind_user",
            "token": "6zBp45V....EfqUvJz2",
            "user": null
        }
    ]
}
```

bind reset_pwd 类型 示例：

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "username": "username",
      "mobile": "mobile",
      "lastLoginIp": "",
      "createdAt": "2019-12-12T00:00:00+08:00",
      "updatedAt": "2019-12-12T00:00:00+08:00"
    }
  }
}
```

reset_pay_pwd 类型 示例：

```json
{
  "data": {
    "type": "users",
    "id": "53",
    "attributes": {
      "id": 53,
      "username": "username",
      "mobile": "mobile",
      "avatarUrl": "",
      "threadCount": 0,
      "followCount": 0,
      "fansCount": 0,
      "follow": null,
      "status": 0,
      "loginAt": null,
      "joinedAt": "2019-12-29T19:18:46+08:00",
      "expiredAt": null,
      "createdAt": "2019-12-29T19:18:46+08:00",
      "updatedAt": "2020-02-18T17:21:57+08:00",
      "canEdit": true,
      "canDelete": true,
      "canWalletPay": true,
      "registerReason": "",
      "banReason": "",
      "originalMobile": "mobile",
      "registerIp": "127.0.0.1",
      "lastLoginIp": "",
      "identity": "",
      "realname": ""
    }
  }
}
```
