### 获取相关主题的接口

- **接口说明：** 获取指定数量相关主题的接口，从按照 同话题阅读量排序>同分类阅读量排序>全站阅读量排序 的优先级获取的主题中随即获取指定数量的数据，有1小时缓存
- **接口地址：** /api/threads/relate/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述     |
| :------- | :----: | :------: | :------- |
| limit    | int    |    否    | 数据数量       |
| include  | string |    否    | 关联数据 |

#### include 可关联的数据

| 关联名称              | 模型        | 类型   | 是否默认 | 描述           |
| :-------------------- | :---------- | :----- | :------: | :------------- |
| user                  | users       | object |    是    | 发表用户       |
| firstPost             | posts       | object |    是    | 首帖           |
| firstPost.images      | attachments | array  |    否    | 首贴图片       |
| firstPost.attachments | attachments | array  |    否    | 首贴附件       |
| firstPost.likedUsers  | users       | array  |    否    | 点赞首贴的用户 |
| threadVideo           | thread-video       | object  |    否    | 主题视频 |
| category              | categories  | object |    否    | 主题分类       |

#### 请求示例

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

#### 返回示例

```json
{
  "data": {
    "type": "threads",
    "id": "1",
    "attributes": {
      "title": "",
      "price": "0.00",
      "attachment_price": "0.00",
      "viewCount": 28,
      "postCount": 4,
      "createdAt": "2020-02-12T16:23:29+08:00",
      "updatedAt": "2020-02-12T16:57:31+08:00",
      "isApproved": 1,
      "isSticky": false,
      "isEssence": false,
      "isLongArticle": false,
      "canViewPosts": false,
      "canReply": false,
      "canApprove": false,
      "canSticky": false,
      "canEssence": false,
      "canDelete": false,
      "canHide": false,
      "canFavorite": false
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      },
      "firstPost": {
        "data": {
          "type": "posts",
          "id": "1"
        }
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "username",
        "mobile": "mobile",
        "avatarUrl": "",
        "threadCount": 30,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": "2020-02-13T11:42:27+08:00",
        "joinedAt": "2019-12-16T19:41:17+08:00",
        "expiredAt": "2020-02-19T18:26:52+08:00",
        "createdAt": "2019-12-16T19:41:17+08:00",
        "updatedAt": "2020-02-13T11:42:27+08:00",
        "canEdit": false,
        "canDelete": false,
        "canWalletPay": true,
        "registerReason": "",
        "banReason": ""
      }
    },
    {
      "type": "posts",
      "id": "1",
      "attributes": {
        "replyUserId": null,
        "content": "Chief Unbranded Str::limit($thread->firstPost->con...",
        "contentHtml": "Chief Unbranded Str::limit($thread-&gt;firstPost-&gt;con...",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-02-12T16:23:29+08:00",
        "updatedAt": "2020-02-12T16:23:29+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canEdit": false,
        "canApprove": false,
        "canDelete": false,
        "canHide": false,
        "commentPosts": null,
        "canLike": false
      }
    }
  ]
}
```
