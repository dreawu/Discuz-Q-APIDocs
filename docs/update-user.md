### 用户资料修改

- **权限说明：** user.edit 修改 status 状态权限为 user.edit.status
- **接口说明：** 用户资料修改
- **接口地址：** /api/users/{id}
- **请求方式：** PATCH

#### 请求参数

| 参数名称                  |  类型  | 是否必须 | 描述                                                                                                          |
| :------------------------ | :----: | :------: | :------------------------------------------------------------------------------------------------------------ |
| password                  | string |    否    | 原密码，只有自己修改才需要传原密码，管理有权限的用户组不需要此参数;小程序注册的用户初始化密码时不需要传此参数 |
| newPassword               | string |    否    | 新密码                                                                                                        |
| password_confirmation     | string |    否    | 新密码确认密码                                                                                                |
| payPassword               | string |    否    | 支付密码，只有自己首次初始化支付密码可以不传 pay_password_token                                               |
| pay_password_confirmation | string |    否    | 支付密码确认密码                                                                                              |
| pay_password_token        | string |    否    | 修改支付密码需要通过[获取验证 token](ResetPayPassword.md)接口获取                                             |
| mobile                    | string |    否    | 手机号                                                                                                        |
| status                    |  int   |    否    | 用户状态 0 正常 1 禁用 2 审核中 3 审核拒绝 4 审核忽略                                                         |
| refuse_message            | string |    否    | 审核拒绝原因                                                                                                  |
| signature                 | string |    否    | 用户签名                                                                                                      |
| username                  | string |    否    | 用户名                                                                                                        |
| register_reason                  | string |    否    | 注册原因                                                                                                        |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "newPassword": "newPassword",
      "mobile": "mobile",
      "status": 1
    }
  }
}
```

#### status 修改时 触发通知表

| 原值     |   修改值   | 触发指定通知 ID | 通知名称         |
| :------- | :--------: | :-------------: | :--------------- |
| 0 正常   |   1 禁用   |       10        | 账号禁用通知     |
| 1 禁用   |   0 正常   |       11        | 账号解除禁用通知 |
| 2 审核中 |   0 正常   |        2        | 审核通过通知     |
| 2 审核中 | 3 审核拒绝 |        3        | 审核拒绝通知     |
| 2 审核中 | 4 审核忽略 |       无        | 不发送通知       |

#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 非 200

#### 返回结果

| 参数名称                         | 类型     | 出现要求 | 描述                                     |
| :------------------------------- | :------- | :------- | :--------------------------------------- |
| **data**                         | object   |          | 基础数据                                 |
| type                             | string   |          | 数据类型                                 |
| id                               | int      |          | 数据 id                                  |
| **attributes**                   | object   |          | 数据属性                                 |
| id                               | int      |          | 用户 id                                  |
| username                         | string   |          | 用户名                                   |
| mobile                           | string   |          | 手机号（脱敏）                           |
| avatarUrl                        | string   |          | 头像地址                                 |
| threadCount                      | int      |          | 主题数                                   |
| followCount                      | int      |          | 关注数                                   |
| fansCount                        | int      |          | 粉丝数                                   |
| follow                           | object   |          | 关注状态 0：未关注 1：已关注 2：互相关注 |
| status                           | int      |          | 状态                                     |
| signature                        | string   |          | 签名                                     |
| loginAt                          | datetime |          | 登录时间                                 |
| joinedAt                         | datetime |          | 加入时间                                 |
| expiredAt                        | datetime |          | 到期时间                                 |
| createdAt                        | datetime |          | 创建时间                                 |
| updatedAt                        | datetime |          | 修改时间                                 |
| canEdit                          | bool     |          | 是否可以编辑                             |
| canDelete                        | bool     |          | 是否可以删除                             |
| canWalletPay                     | bool     |          | 是否可以使用钱包支付                     |
| registerReason                   | string   |          | 注册理由                                 |
| banReason                        | string   |          | 禁用理由                                 |
| originalMobile                   | string   |          | 完整手机号                               |
| registerIp                       | string   |          | 注册 ip                                  |
| lastLoginIp                      | string   |          | 最后登录 ip                              |
| identity                         | string   |          | 注册理由                                 |
| realname                         | string   |          | 实际姓名                                 |
| walletBalance                    | float    | 当前用户 | 用户余额                                 |
| paid                             | bool     |          | 是否是付费用户                           |
| payTime                          | datetime |          | 支付时间                                 |
| unreadNotifications              | int      |          | 未读消息数                               |
| typeUnreadNotifications          | array    |          | 未读消息数明细                           |
| typeUnreadNotifications.replied  | array    |          | 未读回复消息数                           |
| typeUnreadNotifications.liked    | array    |          | 未读点赞消息数                           |
| typeUnreadNotifications.rewarded | array    |          | 未读打赏消息数                           |
| typeUnreadNotifications.system   | array    |          | 未读系统消息数                           |
| usernameBout                     | int  | 当前用户  | 用户名修改了几次  |
| canEditUsername  | bool | 当前用户  | 是否可以修改用户名  |

#### 返回示例

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "id": 1,
      "username": "username",
      "mobile": "mobile",
      "avatarUrl": "",
      "threadCount": 30,
      "followCount": 0,
      "fansCount": 0,
      "follow": null,
      "status": 0,
      "signature": "这是签名",
      "loginAt": "2020-02-13T11:42:27+08:00",
      "joinedAt": "2019-12-16T19:41:17+08:00",
      "expiredAt": "2020-02-19T18:26:52+08:00",
      "createdAt": "2019-12-16T19:41:17+08:00",
      "updatedAt": "2020-02-13T15:45:15+08:00",
      "canEdit": true,
      "canDelete": true,
      "canWalletPay": true,
      "registerReason": "",
      "banReason": "",
      "originalMobile": "mobile",
      "registerIp": "192.168.10.1",
      "lastLoginIp": "192.168.10.1",
      "identity": "",
      "realname": "",
      "paid": null,
      "payTime": null,
      "unreadNotifications": 8,
      "usernameBout": 0,
      "typeUnreadNotifications": {
        "replied": 8
      }
    }
  }
}
```
