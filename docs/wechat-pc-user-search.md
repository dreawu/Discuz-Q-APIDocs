### PC微信扫码登录查询用户扫码状态接口

- **接口说明：** pc 用户扫码状态查询
- **接口地址：** /api/oauth/wechat/web/user/search
- **请求方式：** get

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述                       |
| :-------- | :----- | :------: | :------------------------- |
| scene_str | string |    是    | 生成二维码接口返回的 token |

#### 请求示例

{
"scene_str": "QLsAV9yjwP6JQNCCx9Av4cOCtqIiJzpe25moGNAZ"
}

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |


#### 返回示例

已绑定用户直接返回用户 Token

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Qi......dj3H9CCSPib6MQtnaT6VNrw",
      "refresh_token": "def50200a26b6a9......10ccbf3c1694084c2d2d276"
    }
  }
}
```

未绑定用户返回 token

```json
{
  "errors": [
    {
      "status": 400,
      "code": "no_bind_user",
      "token": "jGdcUun***bmUcNuYG"
    }
  ]
}
```
