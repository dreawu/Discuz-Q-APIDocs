### 查询我点赞的帖子接口[列表]

- **接口说明：** 查询我点赞的帖子[列表]
- **接口地址：** /api/likes
- **请求方式：** GET

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述     |
| :------- | :----: | :------: | :------- |
| include  | string |    否    | 关联数据 |

#### 请求示例

```
// include  可选值
// user     作者（默认）
// thread   主题（默认）
/api/posts?include=user,thread
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 参数名称              | 类型     | 出现要求   | 描述                                              |
| :-------------------- | :------- | :--------- | :------------------------------------------------ |
| **links**             | object   |            | 接口链接                                          |
| **data**              | object   |            | 基础数据                                          |
| type                  | string   |            | 数据类型                                          |
| id                    | int      |            | 数据 id                                           |
| **attributes**        | object   |            | 数据属性                                          |
| attributes.content    | string   |            | 内容                                              |
| attributes.ip         | string   |            | 发布 ip                                           |
| attributes.replyCount | int      |            | 回复数                                            |
| attributes.likeCount  | int      |            | 喜欢数                                            |
| attributes.createdAt  | datetime |            | 创建时间                                          |
| attributes.updatedAt  | datetime |            | 修改时间                                          |
| attributes.deletedAt  | datetime | 在回收站时 | 删除时间                                          |
| attributes.isFirst    | bool     |            | 是否首帖                                          |
| attributes.isApproved | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| attributes.isLiked    | bool     |            | 是否喜欢                                          |
| **relationships**     | object   |            | 关联关系                                          |
| **included**          | object   |            | 关联数据                                          |

#### 返回示例

```json
{
  "links": {
    "first": "http://discuz.test/api/threads?page%5Blimit%5D=2",
    "next": "http://discuz.test/api/threads?page%5Blimit%5D=2&page%5Boffset%5D=2",
    "last": "http://discuz.test/api/threads?page%5Blimit%5D=2&page%5Boffset%5D=6"
  },
  "data": [
    {
      "type": "posts",
      "id": "8",
      "attributes": {
        "content": "Creative vertical == mint green == Berkshire Metical",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 2,
        "createdAt": "2019-11-12T17:10:43+08:00",
        "updatedAt": "2019-11-12T17:10:43+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canApprove": true,
        "canDelete": true,
        "canLike": true,
        "isLiked": true,
        "likedAt": "2019-11-25T14:45:46+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "thread": {
          "data": {
            "type": "threads",
            "id": "8"
          }
        }
      }
    },
    {
      "type": "posts",
      "id": "17",
      "attributes": {
        "content": "plug-and-play input Licensed == purple == Intelligent Gorgeous Web orange",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:49+08:00",
        "updatedAt": "2019-11-12T17:10:49+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canApprove": true,
        "canDelete": true,
        "canLike": true,
        "isLiked": true,
        "likedAt": "2019-11-25T10:45:45+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "thread": {
          "data": {
            "type": "threads",
            "id": "17"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "username",
        "mobile": "",
        "avatarUrl": null,
        "threadCount": null,
        "registerIp": null,
        "lastLoginIp": "127.0.0.1",
        "createdAt": "2019-10-11T00:00:00+08:00",
        "updatedAt": "2019-11-11T14:51:34+08:00"
      }
    },
    {
      "type": "threads",
      "id": "8",
      "attributes": {
        "title": "",
        "price": "0.00",
        "viewCount": 0,
        "postCount": 1,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:43+08:00",
        "updatedAt": "2019-11-12T17:10:43+08:00",
        "isApproved": 1,
        "isSticky": false,
        "isEssence": false,
        "canApprove": true,
        "canSticky": true,
        "canEssence": true,
        "canDelete": true,
        "canFavorite": true
      }
    },
    {
      "type": "threads",
      "id": "17",
      "attributes": {
        "title": "",
        "price": "0.00",
        "viewCount": 2,
        "postCount": 1,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:49+08:00",
        "updatedAt": "2019-11-22T11:02:52+08:00",
        "isApproved": 1,
        "isSticky": false,
        "isEssence": false,
        "canApprove": true,
        "canSticky": true,
        "canEssence": true,
        "canDelete": true,
        "canFavorite": true,
        "isFavorite": true,
        "favoriteAt": "2019-11-14T14:25:01+08:00"
      }
    }
  ],
  "meta": {
    "postCount": 8,
    "pageCount": 4
  }
}
```
