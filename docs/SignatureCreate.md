### 创建云点播前端上传签名

- **接口说明：** 创建云点播前端上传签名
- **接口地址：** /api/signature
- **请求方式：** GET

#### 请求参数

无

#### 请求示例

无

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

> 关联数据模型字段释义请参见相应文档

| 参数名称             | 类型   | 出现要求 | 描述     |
| :------------------- | :----- | :------- | :------- |
| **data**             | object |          | 基础数据 |
| type                 | string |          | 数据类型 |
| id                   | int    |          | 数据 id  |
| **attributes**       | object |          | 数据属性 |
| attributes.id        | int    |          | 固定值 1 |
| attributes.signature | int    |          | 签名     |

#### 返回示例

```json
{
  "data": {
    "type": "signature",
    "id": "1",
    "attributes": {
      "signature": "666"
    }
  }
}
```
