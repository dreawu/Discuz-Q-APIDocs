### 用户登录接口

- **接口说明：** 用户登录接口
- **接口地址：** /api/uc/login
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型    | 是否须 | 描述               |
| :------- | :------ | :----- | :----------------- |
| username | varchar | 是     | 用户名       |
| password | varchar | 是     | 密码               |
| questionid    | varchar | 否     | 问题 |
| answer  | varchar | 否     | 答案 |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "username": "username",
      "password": "password",
      "questionid": "",
      "answer": ""
    }
  }
}
```

#### 返回结果

| 参数名称      | 类型   | 出现要求 | 描述         |
| :------------ | :----- | :------- | :----------- |
| token_type    | string | 登录成功 | token 类型   |
| expires_in    | int    | 登录成功 | 时间         |
| access_token  | token  | 登录成功 | 用户标识     |
| refresh_token | token  | 登录成功 | 刷新用户标识 |

#### 返回说明

- 登录成功， http 状态码： 200
- 登录失败， http 状态码： 500

#### 返回示例

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Qi......dj3H9CCSPib6MQtnaT6VNrw",
      "refresh_token": "def50200a26b6a9......10ccbf3c1694084c2d2d276"
    }
  }
}
```

#### 失败示例

```json
{
  "error": "invalid_grant",
  "error_description": "The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.",
  "hint": "",
  "message": "The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client."
}
```
