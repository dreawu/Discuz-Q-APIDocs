### 查询话题接口[单条]

- **接口说明：** 查询话题接口[单条]
- **接口地址：** /api/topics/{id}
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型   | 描述                   |
| :------- | :----- | :--------------------- |
| id       | int    | 话题 id              |

#### 请求示例

```
/api/topics/7

```

#### 返回结果

| 参数名称                                     | 类型  | 描述                                 |
| :------------------------------------------- | :----- | :--------------------------- |
| data                                         | object  | 主体数据                     |
| data.type                                    | string  | 数据模型的类型，固定值topics               |
| data.id                                      | int     | 话题 id                    |
| data.attributes                              | object  | 数据模型的属性               |
| data.attributes. user_id                     | string  | 创建人user_id                   |
| data.attributes. content                     | string  | 话题名称                   |
| data.attributes. thread_count                | int     | 话题主题量                  |
| data.attributes. view_count                  | int     | 话题主题浏览量                                  |
| data.attributes. updated_at                  | datetime| 更新时间                             |
| data.attributes. created_at                  | datetime| 创建时间                             |


#### 返回说明

- 查询成功， http 状态码： 200
- 修改失败， http 状态码： 404

#### 返回示例

```json
{
    "data": {
        "type": "topics",
        "id": "3",
        "attributes": {
            "user_id": 5,
            "content": "2",
            "thread_count": 12,
            "view_count": 0,
            "updated_at": "2020-04-30T14:11:39+08:00",
            "created_at": "2020-04-28T16:00:59+08:00"
        }
    }
}
```
