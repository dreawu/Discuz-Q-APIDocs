### 查看用户钱包

- **接口说明：** 查看用户钱包
- **接口地址：** /api/wallet/user/{user_id}
- **请求方式：** GET

#### 请求参数

| 字段名  | 变量名  | 必填 | 类型 | 描述    |
| :------ | :------ | :--- | :--- | :------ |
| 用户 ID | user_id | 是   | int  | 用户 ID |

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                      | 必填 | 类型   | 描述                                                                 |
| :--------------------- | :-------------------------- | :--- | :----- | :------------------------------------------------------------------- |
| **data.attributes**    | object                      | 是   | object | 数据属性                                                             |
| 所属用户               | attributes.user_id          | 是   | int    | 所属用户 ID                                                          |
| 可用金额               | attributes.available_amount | 是   | float  | 用户钱包可用金额                                                     |
| 冻结金额               | attributes.freeze_amount    | 是   | float  | 用户钱包冻结金额，交易过程中冻结的资金                               |
| 钱包状态               | attributes.wallet_status    | 是   | int    | 钱包状态，0：表示正常；1：表示冻结提现，此状态下，用户无法申请提现。 |
| 提现税率               | attributes.cash_tax_ratio   | 是   | float  | 用户提现时的税率                                                     |
| **data.relationships** | object                      | 否   | object | 关联关系                                                             |
| **included**           | object                      | 否   | object | 关联数据                                                             |

示例：

```json
{
  "data": {
    "type": "user_wallet",
    "id": "",
    "attributes": {
      "user_id": 1,
      "available_amount": "35.00",
      "freeze_amount": "2.00",
      "wallet_status": 1,
      "cash_tax_ratio": 0.01
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "nickname": "",
        "mobile": "mobile",
        "unionId": null,
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    }
  ]
}
```
