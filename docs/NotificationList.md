### 通知列表

- **接口说明：** 通知列表
- **接口地址：** /api/notification
- **请求方式：** GET

#### 请求参数

| 参数名称     | 类型   | 是否必须 | 描述                                                                                                                       | 示例 |
| :----------- | :----- | :------: | :------------------------------------------------------------------------------------------------------------------------- | :-- |
| filter[type] | string |    否    | 通知类型<br>默认不传查全部<br>system 系统通知<br>replied 回复我的<br>liked 点赞我的<br>rewarded 支付我的 <br>related @我的<br>withdrawal 提现通知<br>questioned 问答通知 | rewarded,withdrawal (逗号拼接,支持传多个) |
| page[number]    | int    |    否    | 页码                 |
| page[limit]     | int    |    否    | 单页数量             |

#### 返回说明

- 返回通知列表， http 状态码： 200

#### 返回结果

| 字段名   | 变量名  | 必填 | 类型   | 描述         |
| :------- | :------ | :--- | :----- | :----------- |
| 通知编号 | id      | 是   | bigint | 消息 id      |
| 用户     | user_id | 是   | init   | 用户 id      |
| 阅读时间 | read_at | 是   | string | 用户阅读时间 |

```
// 回复我的、点赞我的通知 data 字段说明
thread_id       主题 ID
thread_title    标题（预留）
post_id         帖子 ID
post_content    帖子内容
user_id         用户 ID
user_name       用户名
user_avatar     用户头像

// 打赏我的通知 data 字段说明
@zz

```

#### 返回示例

| 字段               | 描述                   | 注意                          |
| ------------------ | ---------------------- | ----------------------------- |
| user_id            | 发送人 ID              |
| user_name          | 发送人姓名             |
| user_avatar        | 发送人头像             |
| read_at            | 阅读时间               |
| created_at         | 点赞/回复/@的时间      |
| thread_id          | 通过哪个主题 ID 操作的 |
| thread_title       | 该主题标题             |
| thread_created_at  | 该主题创建时间         |
| post_id            | 点赞/回复/@的 ID       |
| post_content       | 点赞/回复/@的内容      | 只有回复的通知这个字段不是空字符串,<br>空字符串前端做判断不展示白色部分的样式  |
| post_created_at    | 点赞/回复/@的时间      |                               |
| thread_username    | 主题创建人             |                               |
| thread_user_groups | 主题创建人的用户组名   |                               |
| amount             | 打赏金额               |                               |
| order_id           | 订单 ID                |                               |
| reply_post_id      | 楼中楼回复 ID          | 如果不是 0 说明是楼中楼(点评) |
| reply_post_user_id  | 楼中楼的用户ID  | 只有在`reply_post_id`不为0时会有值  |
| reply_post_user_name| 楼中楼的用户名  | 只有在`reply_post_id`不为0时会有值  |
| reply_post_content  | 楼中楼的内容  | 只有在`reply_post_id`不为0时会有值  |
| reply_post_created_at  | 楼中楼的创建时间  | 只有在`reply_post_id`不为0时会有值  |
| thread_is_approved  | 主题是否合法  | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略  |
| isReal              | 是否已读  |  |
| isScale             | 是否分成收入  | 如果是true财务通知的文案需要改一下 xx分成了我xx  |


>注意：以下情况指的都是 回复、@、点赞通知消息
1. 当回复的是主题详情页时，`post_id` 是主题评论id
2. 当回复的是主题评论时，上面的 `post_id` 变成了该数据的 `reply_post_id` ，该数据的 `post_id` 是当前楼中楼的评论id
>>两个页面的纬度不一样，所以 `post_id` 会跟着变更

<br>
<br>
<br>

- 点赞/回复/@通知 三个数据格式都一样:

```json
{
  "data": [
    {
      "type": "notification",
      "id": "44",
      "attributes": {
        "id": 44,
        "type": "replied",
        "user_id": 5,
        "user_name": "username",
        "user_avatar": "",
        "read_at": "2020-04-21T18:54:53+08:00",
        "created_at": "2020-04-21T18:52:38+08:00",
        "thread_id": 3,
        "thread_user_id": 1,
        "thread_title": "测试点赞通知哈2哈哈",
        "thread_created_at": "2020-04-17 14:22:07",
        "post_id": 58,
        "post_content": "回复你哈 回复人xuchen Configuration Dynamic Music",
        "reply_post_id": 0,
        "post_created_at": "2020-04-21 18:52:38",
        "thread_username": "admin",
        "isScale": true,
        "isReal": false,
      }
    }
  ]
}
```

- 支付通知示例:

| 字段               | 描述                   | 注意                          |
| ------------------ | ---------------------- | ----------------------------- |
| user_id            | 发送人 ID              |
| user_name          | 发送人姓名             |
| user_avatar        | 发送人头像             |
| read_at            | 阅读时间               |
| created_at         | 通知时间               |
| thread_id          | 通过哪个主题 ID 操作的 |
| order_type         | 交易类型  | 根据不同值展示文案不同: 1注册、2打赏、3付费主题
| thread_title       | 该主题标题             | 当是普通帖时该值为空
| thread_created_at  | 该主题创建时间         |
| content            | 打赏的主题内容            | 普通帖的内容，如果是主题帖就是标题内容
| thread_username    | 主题创建人             |                               |
| thread_user_groups | 主题创建人的用户组名   |                               |
| amount             | 打赏金额               |                               |
| order_id           | 订单 ID                |                               |
| reply_post_id      | 楼中楼  | 值一直等于0用于前端判断  |

```json
{
"data": [
      {
          "type": "notification",
          "id": "1697",
          "attributes": {
                "id": 57,
                "type": "rewarded",
                "user_id": 4,
                "read_at": "2020-04-23T15:11:16+08:00",
                "created_at": "2020-04-23T15:11:04+08:00",
                "amount": 0.1,
                "content": "测试点赞通知",
                "order_id": 27,
                "order_type": 3,
                "thread_id": 3,
                "thread_title": "",
                "thread_created_at": "2020-04-17 14:22:07",
                "user_name": "username",
                "user_avatar": "http://discuz.com/storage/avatars/4.png?1587089944",
                "thread_username": "username",
                "thread_user_groups": "管理员",
                "reply_post_id": 0
            }
      }
    ]
}
```

- 系统通知示例:

| 字段          | 描述                   | 注意 |
| ------------- | ---------------------- | ---- |
| user_id       | 发送人 ID              |
| user_name     | 发送人姓名             |
| user_avatar   | 发送人头像             |
| read_at       | 阅读时间               |
| created_at    | 通知时间               |
| title         | 通知标题               |
| content       | 通知内容               |
| raw.thread_id | 通过哪个主题 ID 操作的 |
| raw.is_first  | 是否是首贴             |
| raw.tpl_id    | 通知的模板 ID          |

```json
{
  "data": [
    {
      "type": "notification",
      "id": "14",
      "attributes": {
        "id": 14,
        "type": "system",
        "user_id": 1,
        "user_name": "",
        "user_avatar": "",
        "read_at": "2020-04-23T15:11:46+08:00",
        "created_at": "2020-04-20T10:42:48+08:00",
        "title": "内容修改通知",
        "content": "username你好，你发布的内容 \"测试点赞通知哈2@username\" 已被修改",
        "raw": {
          "id": 2,
          "thread_id": 3,
          "is_first": true,
          "tpl_id": 9
        }
      }
    }
  ]
}
```

- 提现通知示例:

| 字段           | 描述                   | 注意 |
| ------------- | ---------------------- | ---- |
| id            | 通知ID   |   |
| user_id       | 提现人ID               |
| user_name     | 提现人姓名             |
| user_avatar   | 提现人头像             |
| cash_actual_amount  | 实际提现金额  |
| cash_apply_amount  | 提现申请金额  |
| cash_status   | 提现状态  | 1：待审核，2：审核通过，3：审核不通过，4：待打款， 5，已打款， 6：打款失败
| remark        | 拒绝原因  |   |
| read_at       | 阅读时间               |
| created_at    | 提现时间               |

```json
{
"data": [
        {
            "type": "notification",
            "id": "104",
            "attributes": {
                "id": 104,
                "type": "withdrawal",
                "user_id": 4,
                "read_at": "2020-05-29T10:16:25+08:00",
                "created_at": "2020-05-28T08:18:58.000000Z",
                "wallet_cash_id": 35,
                "cash_actual_amount": "99.00",
                "cash_apply_amount": "100.00",
                "cash_status": 1,
                "remark": "",
                "user_name": "username",
                "user_avatar": "http://discuz.com/storage/avatars/4.png?1587089944",
                "thread_username": "",
                "thread_user_groups": ""
            }
        }
    ],
}
```
