### 上传头像接口

- **权限：** user.edit
- **接口说明：** 上传头像接口
- **接口地址：** /api/users/{id}/avatar
- **请求方式：** POST
- **请求体类型：** form-data

#### 请求参数

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |
| avatar   | file | 文件 |

请求示例：

```

```

#### 返回结果

| 参数名称  | 类型   | 描述     |
| :-------- | :----- | :------- |
| avatarUrl | string | 头像地址 |

#### 返回说明

成功示例：

```json
{
  "data": {
    "type": "users",
    "id": "1",
    "attributes": {
      "id": 1,
      "username": "username",
      "mobile": "mobile",
      "avatarUrl": "",
      "threadCount": 0,
      "lastLoginIp": "",
      "createdAt": "2019-11-19T15:25:31+08:00",
      "updatedAt": "2019-11-25T17:30:37+08:00"
    }
  }
}
```
