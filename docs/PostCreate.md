### 创建回复接口

- **接口说明：** 回复主题/回复其他回复
- **接口地址：** /api/posts
- **请求方式：** POST

#### 请求参数

| 参数名称  |  类型  | 是否必须 | 描述                       |
| :-------- | :----: | :------: | :------------------------- |
| threadId  |  int   |    是    | 主题 id                    |
| replyId   |  int   |    否    | 回复 id                    |
| content   | string |    是    | 内容                       |
| isComment |  bool  |    否    | 是否是回复的评论（楼中楼） |
| commentPostId  | int  | 否  | 回复评论的ID  |
| commentUserId  | int  | 否  | 回复评论的用户ID  |

#### 请求示例

```json
// 回复主题
{
    "data": {
        "type": "posts",
        "attributes": {
            "content": "{{$randomWords}} == {{$randomColor}} == {{$randomWords}}",
        },
        "relationships": {
            "thread": {
                "data": {
                    "type": "threads",
                    "id": "4"
                }
            },
            "attachments": {
                "data": [
                    {
                        "type": "attachments",
                        "id": 1
                    },
                    {
                        "type": "attachments",
                        "id": 2
                    }
                ]
            }
        }
    }
}

// 回复其它回复
{
    "data": {
        "type": "posts",
        "attributes": {
            "replyId": 33,
            "content": "{{$randomWords}} == {{$randomColor}} == {{$randomWords}}",
            "is_comment": true
        },
        "relationships": {
            "thread": {
                "data": {
                    "type": "threads",
                    "id": "4"
                }
            }
        }
    }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称               | 类型     | 出现要求   | 描述                                              |
| :--------------------- | :------- | :--------- | :------------------------------------------------ |
| **data**               | object   |            | 基础数据                                          |
| type                   | string   |            | 数据类型                                          |
| id                     | int      |            | 数据 id                                           |
| **attributes**         | object   |            | 数据属性                                          |
| attributes.summary     | string   |            | 摘要                                              |
| attributes.content     | string   |            | 内容                                              |
| attributes.contentHtml | string   |            | html 内容                                         |
| attributes.ip          | string   |            | 发布 ip                                           |
| attributes.replyCount  | int      |            | 回复数                                            |
| attributes.likeCount   | int      |            | 喜欢数                                            |
| attributes.createdAt   | datetime |            | 创建时间                                          |
| attributes.updatedAt   | datetime |            | 修改时间                                          |
| attributes.deletedAt   | datetime | 在回收站时 | 删除时间                                          |
| attributes.isFirst     | bool     |            | 是否首帖                                          |
| attributes.isApproved  | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略 |
| attributes.isLiked     | bool     |            | 是否喜欢                                          |
| attributes.isomment    | bool     |            | 是否是回复回帖的内容(楼中楼)                      |
| **relationships**      | object   |            | 关联关系                                          |
| **included**           | object   |            | 关联数据                                          |

#### 返回示例

```json
{
  "data": {
    "type": "Posts",
    "id": "33",
    "attributes": {
      "content": "Savings Account clicks-and-mortar == salmon == Ohio bus Investment Account Bulgarian Lev",
      "ip": "127.0.0.1",
      "replyCount": null,
      "likeCount": null,
      "createdAt": "2019-11-13T14:30:54+08:00",
      "updatedAt": "2019-11-13T14:30:54+08:00",
      "isFirst": false,
      "isApproved": false,
      "isLiked": false,
      "isomment": false
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      },
      "thread": {
        "data": {
          "type": "Threads",
          "id": "4"
        }
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "nickname": null,
        "mobile": "",
        "unionId": "",
        "lastLoginIp": "127.0.0.1",
        "createdAt": "2019-10-11T00:00:00+08:00",
        "updatedAt": "2019-11-11T14:51:34+08:00"
      }
    },
    {
      "type": "Threads",
      "id": "4",
      "attributes": {
        "title": "",
        "price": "0.00",
        "viewCount": 0,
        "postCount": 2,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:40+08:00",
        "updatedAt": "2019-11-13T14:30:54+08:00",
        "isApproved": true,
        "isSticky": false,
        "isEssence": false,
        "isFavorite": false
      }
    }
  ]
}
```
