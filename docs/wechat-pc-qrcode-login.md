### PC扫码有状态登陆

- **接口说明：** PC扫码有状态登陆
- **接口地址：** /api/oauth/wechat/qrcode/login/{session_token}
- **请求方式：** GET

#### 请求参数

| 参数名称    | 类型   | 是否必须  | 描述                  |
| :-------- | :----- | :------: | :-----------------   |

#### 请求示例

```
{{host}}/api/oauth/wechat/qrcode/login/XKqV8mE1aL1ydwq8caqybrdROlNcHO
```

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |
| pc_login  | bool  | PC扫码是否成功  |


#### 返回示例

- 成功

```json
{
    "pc_login": true
}
```

- 失败

```json
{
    "errors": [
        {
            "status": "500",
            "code": "扫码登陆失败"
        }
    ]
}
```
