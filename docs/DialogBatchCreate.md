### 创建会话接口[多条]

- **接口说明：** 创建会话[多条]
- **接口地址：** /api/dialog/batch
- **请求方式：** POST

#### 请求参数

| 参数名称                       |  类型  | 是否必须 | 描述                      |
| :----------------------------- | :----: | :------: | :------------------------ |
| **data**                       | object |    是    | 基础数据                  |
| type                           | string |    是    | 数据类型，固定值 dialog   |
| **attributes**                 | object |    是    | 数据属性                  |
| attributes. recipient_username | string |    是    | 用户名,用','分割 可传多个 |
| attributes. message_text       | string |    否    | 消息内容                  |

#### 请求示例

```json
{
  "data": {
    "type": "dialog",
    "attributes": {
      "recipient_username": "username",
      "message_text": "hello"
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

> 关联数据模型字段释义请参见相应文档

| 参数名称                      | 类型     | 出现要求 | 描述              |
| :---------------------------- | :------- | :------- | :---------------- |
| **data**                      | object   |          | 基础数据          |
| type                          | string   |          | 数据类型          |
| id                            | int      |          | 会话 id           |
| **attributes**                | object   |          | 数据属性          |
| attributes. dialog_message_id | int      |          | 最新一条消息的 ID |
| attributes. sender_user_id    | int      |          | 发送人 uid        |
| attributes. recipient_user_id | int      |          | 接收人 uid        |
| attributes.updated_at         | datetime |          | 更新时间          |
| attributes.created_at         | datetime |          | 创建时间          |

#### 返回示例

```json
{
  "data": [
    {
      "type": "dialog",
      "id": "5",
      "attributes": {
        "dialog_message_id": 0,
        "sender_user_id": 1,
        "recipient_user_id": 5,
        "updated_at": "2020-02-17T11:17:08+08:00",
        "created_at": "2020-02-17T11:17:08+08:00"
      }
    },
    {
      "type": "dialog",
      "id": "9",
      "attributes": {
        "dialog_message_id": 0,
        "sender_user_id": 1,
        "recipient_user_id": 6,
        "updated_at": "2020-02-17T18:12:45+08:00",
        "created_at": "2020-02-17T18:12:45+08:00"
      }
    }
  ]
}
```
