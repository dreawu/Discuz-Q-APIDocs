### 添加用户组

- **接口说明：** 添加用户组
- **接口地址：** /api/groups
- **请求方式：** POST

#### 请求参数
| 参数名称 | 类型   | 是否必须 | 描述       |
| :------- | :----- | :------- | :--------- |
| name     | string | 否       | 用户组名称 |
| type     | string | 否       | 类型       |
| color    | string | 否       | 颜色       |
| icon     | string | 否       | icon       |
| is_paid  | int | 否          | 是否付费用户组，0为免费,1为付费用户组         |
| fee     | float | 否       | 付费金额       |
| days     | int | 否       | 付费获得用户组权限天数       |

请求示例：
```
{
	"data":{
		"type":"groups",
		"attributes":{
			"name":"test",
			"default":"",
			"is_paid":1,
			"fee":0.001,
			"days":3
		}
	}
}
```

#### 返回说明

- 返回当前创建成功数据， http 状态码： 201

#### 返回结果

#### 返回示例

```json
{
    "data": {
        "type": "groups",
        "id": "35",
        "attributes": {
            "name": "test",
            "type": "",
            "color": "",
            "icon": "",
            "default": null,
            "isDisplay": false,
            "isPaid": true,
            "fee": 100,
            "days": 3
        }
    }
}
```
