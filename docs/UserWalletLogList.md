### 钱包动账记录

- **接口说明：** 钱包动账记录
- **接口地址：** /api/wallet/log
- **请求方式：** GET

#### 请求参数

| 字段名   | 变量名                  | 必填 | 类型     | 描述                                                         |
| :------- | :---------------------- | :--- | :------- | :----------------------------------------------------------- |
| 排序参数 | sort                    | 否   | string   | 可选值：created_at、updated_at。                             |
| 关联参数 | include                 | 否   | string   | 可选值:user 用户信息、userWallet 用户钱包信息、userWalletCash 提现记录信息、order 订单信息，sourceUser 分成来源用户信息 |
| 筛选参数 | filter[user]            | 否   | int      | 传递 user_id,筛选某用户数据                                  |
| 筛选参数 | filter[change_type]     | 否   | int      | 8 问答冻结，9问答返还冻结，10 提现冻结，11 提现成功，12 提现解冻，30 注册收入， 31 打赏收入，32 人工收入，33 分成打赏收入，34 注册分成收入，35 问答答题收入，36 问答围观收入， 50 人工支出，51 加入用户组支出，52 付费附件支出，41 打赏支出，60 付费主题收入，61 付费主题支出，62 分成付费主题收入，63 付费附件收入，64 付费附件分成收入，71 站点续费支出，81 问答提问支出，82 问答围观支出，100 文字帖红包支出，101 文字帖红包冻结，104文字帖订单异常返现，102 文字帖红包收入，103 文字帖冻结返还，110 长文帖红包支出，111 长文帖红包冻结，112 长文帖红包收入，113 长文帖冻结返还，114长文帖订单异常返现，120 悬赏问答收入， 121 悬赏帖过期-悬赏帖剩余悬赏金额返回，124问答帖订单异常返现 |
| 筛选参数 | filter[change_desc]     | 否   | sring    | 按描述筛选                                                   |
| 筛选参数 | filter[username]        | 否   | sring    | 按用户筛选                                                   |
| 筛选参数 | filter[start_time]      | 否   | datetime | 按提创建时间范围筛选：开始时间                               |
| 筛选参数 | filter[end_time]        | 否   | datetime | 按提创建时间范围筛选：最后时间                               |
| 筛选参数 | filter[source_username] | 否   | sring    | 模糊搜索来源用户名                                           |
| 筛选参数 | filter[source_user_id]  | 否   | int      | 筛选金额来源用户名 
| 筛选参数 | filter[change_type_exclude]  | 否   | int      | 排除变动类型，填写要排除的变动类型，可填写参数与 change_type 一致                                                                                                                                                                                                                                                      |

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                             | 必填 | 类型     | 描述             |
| :--------------------- | :--------------------------------- | :--- | :------- | :--------------- |
| **data.attributes**    | object                             | 是   | object   | 数据属性         |
| 记录 ID                | attributes.id                      | 是   | bigint   | 提现记录唯一编号 |
| 变动可用金额           | attributes.change_available_amount | 是   | float    | 变动可用金额     |
| 变动冻结金额           | attributes.change_freeze_amount    | 是   | float    | 变动冻结金额     |
| 变动类型               | attributes.change_type             | 是   | int      | 变动类型         |
| 变动描述               | attributes.change_desc             | 是   | string   | 变动描述         |
| 变动标题               | attributes.title                   | 是   | string   | 变动标题         |
| 更新时间               | attributes.updated_at              | 是   | datetime | 更新时间         |
| 创建时间               | attributes.created_at              | 是   | datetime | 创建时间         |
| **data.relationships** | object                             | 否   | object   | 关联关系         |
| **included**           | object                             | 否   | object   | 关联数据         |

示例：

```json
{
  "links": {
    "first": "http://discuz.test/api/wallet/log?page%5Blimit%5D=1&include=user%2CuserWallet",
    "next": "http://discuz.test/api/wallet/log?page%5Blimit%5D=1&page%5Boffset%5D=1&include=user%2CuserWallet",
    "last": "http://discuz.test/api/wallet/log?page%5Blimit%5D=1&page%5Boffset%5D=106&include=user%2CuserWallet"
  },
  "data": [
    {
      "type": "user_wallet_log",
      "id": "109",
      "attributes": {
        "id": 109,
        "change_available_amount": "-1.00",
        "change_freeze_amount": "1.00",
        "change_type": 10,
        "change_desc": "提现冻结",
        "updated_at": "2019-11-22T11:07:32+08:00",
        "created_at": "2019-11-22T11:07:32+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        },
        "userWallet": {
          "data": {
            "type": "user_wallet",
            "id": "1"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "mobile": "mobile",
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    },
    {
      "type": "user_wallet",
      "id": "1",
      "attributes": {
        "user_id": 1,
        "available_amount": "1.00",
        "freeze_amount": "33.00",
        "wallet_status": 0
      }
    }
  ]
}
```
