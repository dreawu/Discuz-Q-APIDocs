### 公众号素材管理获取列表

- **接口说明：** 公众号素材管理获取List
- **接口地址：** /api/offiaccount/asset
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
|:--|:--|:--|:--|:--
| filter[type]   | string  | 否  | 素材的类型  | 图片（image）、视频（video）、<br>语音（voice）、图文（news）
| page[number]   | int  | 否  | 页码  | 从全部素材的该偏移位置开始返回，可选，默认 0，0 表示从第一个素材 返回  |
| page[limit]    | int  | 否  | 条数  | 返回素材的数量，可选，默认 20, 取值在 1 到 20 之间  |

#### 请求示例

```
{{host}}/api/offiaccount/asset?filter[type]=image&page[limit]=10&page[number]=0
```

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述 |
| :--- | :--- | :--- | :--- |
| name | string | 文件名称 |  |
| title | string | 标题 |  图文消息的标题 |
| thumb_media_id | string | 封面图 |  图文消息的封面图片素材id（必须是永久mediaID） |
| author | string | 作者 |  |
| digest | string | 摘要  |  图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空 |
| content | string | 内容 |  图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS |
| url | string | URL |  图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL |
| show_cover_pic | string | 是否显示封面 |  0为false，即不显示，1为true，即显示 |
| content_source_url | string | 原文地址 |  图文消息的原文地址，即点击“阅读原文”后的URL |
| item_count | string | 素材的数量 |  本次调用获取的素材的数量 |
| total_count | string | 素材的总数 |  该类型的素材的总数 |
| update_time | string | 最后更新时间 |  这篇图文消息素材的最后更新时间 |

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回示例

```json
{
    "data": {
        "type": "offiaccount_asset",
        "id": "1",
        "attributes": {
            "item": [
                {
                    "media_id": "ryUzCNaipU-iHQGbMFvy4sHoopd-Ap5CiLoiNuyMAts",
                    "name": "offiaccount_9BSwJNHf.JPG",
                    "update_time": 1591168306,
                    "url": "",
                    "tags": []
                },
                {
                    "media_id": "wsSbTCj0WHabnrR69PxfCvtIGuJsLGycINVMfshjigI",
                    "name": "api_mpnews_cover.jpg",
                    "update_time": 1405652823,
                    "url": "",
                    "tags": []
                }
            ],
            "total_count": 36,
            "item_count": 10
        }
    }
}
```
