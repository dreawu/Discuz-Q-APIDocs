### 创建投票接口

- **接口说明：** 创建投票
- **接口地址：** /api/votes
- **请求方式：** POST

#### 请求参数


| 参数名称                                     | 类型  | 描述                                 |
| :------------------------------------------- | :----- | :--------------------------- |
| **data**                                     | object  | 主体数据                     |
| data.type                                    | string  | 数据模型的类型，固定值votes     |
| **data.attributes**                          | object  | 数据模型的属性               |
| data.attributes. name                        | string  | 投票名称                    |
| data.attributes. thread_id                   | int     | 主题ID                      |
| data.attributes. type                        | int     | 类型(0单选,1多选)            |
| data.attributes. total_count                 | int     | 投票总数量                   |
| data.attributes. start_at                    | datetime| 开始时间                     |
| data.attributes. end_at                      | datetime| 结束时间                     |
| **data.attributes. options**                 | object   | 选项                       |
| data.attributes. options .id                 | int      | 选项ID                      |
| data.attributes. options .content            | string   | 选项内容                     |

#### 请求示例

```
{
    "data": {
        "type": "votes",
        "attributes": {
            "thread_id": 0,
            "type": 0,
            "start_at": "2020-07-17 11:43:00",
            "end_at": "2020-07-18 11:43:00",
            "name": "投票1",
            "options": [
                {
                    "id": 0,
                    "content": "选项1"
                },
                {
                    "id": 0,
                    "content": "选项2"
                }
            ]
        }
    }
}
```


#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 500

#### 返回示例

```
{
    "data": {
        "type": "votes",
        "id": "1",
        "attributes": {
            "name": "投票1",
            "user_id": 3,
            "thread_id": 0,
            "type": 0,
            "total_count": 0,
            "start_at": "2020-07-17T11:43:00+08:00",
            "end_at": "2020-07-18T11:43:00+08:00",
            "updated_at": "2020-08-12T15:00:53+08:00",
            "created_at": "2020-08-12T15:00:53+08:00"
        },
        "relationships": {
            "options": {
                "data": [
                    {
                        "type": "vote-option",
                        "id": "1"
                    },
                    {
                        "type": "vote-option",
                        "id": "2"
                    }
                ]
            }
        }
    },
    "included": [
        {
            "type": "vote-option",
            "id": "1",
            "attributes": {
                "vote_id": 1,
                "content": "选项1",
                "count": 0,
                "updated_at": "2020-08-12T15:00:53+08:00",
                "created_at": "2020-08-12T15:00:53+08:00"
            }
        },
        {
            "type": "vote-option",
            "id": "2",
            "attributes": {
                "vote_id": 1,
                "content": "选项2",
                "count": 0,
                "updated_at": "2020-08-12T15:00:54+08:00",
                "created_at": "2020-08-12T15:00:54+08:00"
            }
        }
    ]
}
```