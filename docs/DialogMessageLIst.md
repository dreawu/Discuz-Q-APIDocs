### 获取会话消息列表

- **接口说明：** 获取会话消息列表，获取数据后会设置当前会话已读
- **接口地址：** /api/dialog/message
- **请求方式：** GET

#### 请求参数

| 参数名称          | 类型   | 是否必须 | 描述     |
| :---------------- | :----- | :------: | :------- |
| include           | string |    否    | 关联数据 |
| filter[dialog_id] | int    |    是    | 会话 ID  |
| page[number]      | int    |    否    | 页码     |
| page[limit]       | int    |    否    | 单页数量 |
| sort              | string |    否    | 创建时间正序createdAt，创建时间倒序-createdAt |

#### include 可关联的数据

| 关联名称    | 模型   | 类型   | 是否默认 | 描述   |
| :---------- | :----- | :----- | :------: | :----- |
| attachment  | attachments  | object |    是    | 附件（图片）   |
| user        | users  | object |    否    | 用户   |
| user.groups | groups | object |    否    | 用户组 |

#### 请求示例

```
/api/dialog/message?filter[dialog_id]=1&include=user
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

> 关联数据模型字段释义参见请参见相应文档

| 字段名                 | 变量名                  | 必填 | 类型     | 描述                  |
| :--------------------- | :---------------------- | :--- | :------- | :-------------------- |
| **links**              | object                  | 是   | object   | 接口链接              |
| **data.type**          | data.type               | 是   | string   | 固定值 dialog_message |
| **data.id**            | data.id                 | 是   | int      | 消息 ID               |
| **data.attributes**    | object                  | 是   | object   | 数据属性              |
| 消息 id                | id                      | 是   | int      | 消息 id               |
| 发送人 uid             | attribute. user_id      | 是   | int      | 发送人 uid            |
| 会话 id                | attribute. dialog_id    | 是   | int      | 会话 id               |
| 消息内容               | attribute. message_text | 是   | int      | 消息内容              |
| 更新时间               | attributes.updated_at   | 是   | datetime | 更新时间              |
| 创建时间               | attributes.created_at   | 是   | datetime | 创建时间              |
| **data.relationships** | object                  | 否   | object   | 关联关系              |
| **included**           | object                  | 否   | object   | 关联数据              |

#### 返回示例

```json
{
  "links": {
    "first": "DummySiteUrl/api/dialog/message?filter%5Bdialog_id%5D=1&include=user",
    "last": "DummySiteUrl/api/dialog/message?filter%5Bdialog_id%5D=1&include=user"
  },
  "data": [
    {
      "type": "dialog_message",
      "id": "1",
      "attributes": {
        "user_id": 2,
        "dialog_id": 1,
        "message_text": "11111",
        "updated_at": "2020-02-17T10:14:28+08:00",
        "created_at": "2020-02-17T10:14:31+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "2"
          }
        }
      }
    },
    {
      "type": "dialog_message",
      "id": "1",
      "attributes": {
        "user_id": 1,
        "dialog_id": 1,
        "message_text": "消息内容123",
        "updated_at": "2020-02-17T10:14:28+08:00",
        "created_at": "2020-02-17T10:14:31+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    },
    {
      "type": "dialog_message",
      "id": "1",
      "attributes": {
        "user_id": 1,
        "dialog_id": 1,
        "message_text": "消息内容123",
        "updated_at": "2020-02-17T10:14:28+08:00",
        "created_at": "2020-02-17T10:14:31+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    },
    {
      "type": "dialog_message",
      "id": "1",
      "attributes": {
        "user_id": 1,
        "dialog_id": 1,
        "message_text": "消息内容123",
        "updated_at": "2020-02-17T10:14:28+08:00",
        "created_at": "2020-02-17T10:14:31+08:00"
      },
      "relationships": {
        "user": {
          "data": {
            "type": "users",
            "id": "1"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "users",
      "id": "2",
      "attributes": {
        "id": 2,
        "username": "username",
        "mobile": "",
        "avatarUrl": "",
        "threadCount": 0,
        "followCount": 1,
        "fansCount": 1,
        "follow": null,
        "status": 0,
        "loginAt": "2020-02-06T19:48:34+08:00",
        "joinedAt": "2019-12-20T03:48:09+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-20T03:48:09+08:00",
        "updatedAt": "2020-02-06T19:52:14+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    },
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "username",
        "mobile": "",
        "avatarUrl": "DummySiteUrl/storage/avatars/1.png?1578395431",
        "threadCount": 0,
        "followCount": 4,
        "fansCount": 2,
        "follow": null,
        "status": 0,
        "loginAt": "2020-02-18T15:17:13+08:00",
        "joinedAt": "2019-12-19T13:51:19+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-19T13:51:20+08:00",
        "updatedAt": "2020-02-18T15:17:13+08:00",
        "canEdit": true,
        "canDelete": true,
        "registerReason": null,
        "originalMobile": "",
        "registerIp": "172.16.167.1",
        "lastLoginIp": "172.16.167.1",
        "identity": null,
        "realname": null
      }
    }
  ],
  "meta": {
    "total": 4,
    "size": 20
  }
}
```
