### 微信公众号登陆、注册、绑定接口

- **接口说明：** 微信通过 code 参数换取 token 接口。当调取接口时的headers中包含authorization,会根据登陆用户和当前微信的绑定情况进行绑定。
- **接口地址：** /api/oauth/wechat/user
- **请求方式：** GET

#### 请求参数

| 参数名称    | 类型   | 是否必须  | 描述                  |
| :-------- | :----- | :------: | :-----------------   |
| code      | string |    是    | 微信授权返回 code      |
| sessionId | string |    是    | 回调地址返回的参数      |
| state     | string |    否    | 微信授权返回 state<br>【注意】当测试调用该接口时，必传拼接微信回调参数     |
| inviteCode| string |    否    | 管理员生成的注册邀请码   |
| register  | int    |    否    | 是否自动注册。注册：1，不注册：0或者不传  |
| session_token  | string  | 否  | PC扫码登陆时，必传。参数由扫描二维码后在url中带入进页面  |
| rebind    | int  | 否  | 1换绑 0不换绑或者不传  |

#### 请求示例

```
/api/oauth/wechat/user?sessionId={sessionId}&code={code}&state={state}&inviteCode={inviteCode}&register={register}
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 400
- 返回code:no_bind_user时，表示微信未绑定用户，可以使用返回的token调用注册、登陆接口进行注册绑定、登陆绑定
- 返回code:rebind_mp_wechat时，说明前端请求要求重新绑定微信公众号，可以使用返回的token调用注册、登陆接口进行注册换绑、登陆换绑


#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |


#### 返回示例

已绑定用户直接返回用户 Token

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Qi......dj3H9CCSPib6MQtnaT6VNrw",
      "refresh_token": "def50200a26b6a9......10ccbf3c1694084c2d2d276"
    }
  }
}
```

未绑定用户返回 token

```json
{
  "errors": [
    {
      "status": 400,
      "code": "no_bind_user",
      "token": "jGdcUun***bmUcNuYG"
    }
  ]
}
```
