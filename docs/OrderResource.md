### 查询订单（单条）

- **接口说明：** 查询订单（单条）
- **接口地址：** /api/orders/{order_sn}
- **请求方式：** GET

#### 请求参数

| 字段名   | 变量名   | 必填 | 类型   | 描述         |
| :------- | :------- | :--- | :----- | :----------- |
| 订单编号 | order_sn | 是   | string | 订单唯一编号 |

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                | 必填 | 类型     | 描述                                                                                        |
| :--------------------- | :-------------------- | :--- | :------- | :------------------------------------------------------------------------------------------ |
| **data.attributes**    | object                | 是   | object   | 数据属性                                                                                    |
| 订单编号               | attributes.order_sn   | 是   | string   | 订单唯一编号                                                                                |
| 订单金额               | attributes.amount     | 是   | float    | 订单付款金额                                                                                |
| 付款状态               | attributes.status     | 是   | int      | 付款状态，0 待付款，1 已付款 ，2 取消订单，3 支付失败，4 订单已过期                         |
| 订单类型               | attributes.type       | 是   | int      | 1：注册，2：打赏，3：付费主题，4：付费用户组，5：问答提问支付，6：问答围观付费, 7: 付费附件 |
| 主题 ID                | attributes.thread_id  | 是   | int      | 打赏的主题                                                                                  |
| 用户组 ID              | attributes.group_id   | 是   | int      | 付费用户组 id                                                                               |
| 更新时间               | attributes.updated_at | 是   | datetime | 更新时间                                                                                    |
| 创建时间               | attributes.created_at | 是   | datetime | 创建时间                                                                                    |
| **data.relationships** | object                | 是   | object   | 关联关系                                                                                    |
| **included**           | object                | 是   | object   | 关联数据（包含 user，thread，thread.firstPost，group）                                      |

#### 返回示例

```json
{
  "data": {
    "type": "orders",
    "id": "4",
    "attributes": {
      "order_sn": "2019112113520654485453",
      "amount": "2.50",
      "status": 0,
      "type": 2,
      "thread_id": 2,
      "updated_at": "2019-11-21T13:52:06+08:00",
      "created_at": "2019-11-21T13:52:06+08:00"
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      },
      "thread": {
        "data": {
          "type": "threads",
          "id": "2"
        }
      }
    }
  },
  "relationships": {
    "user": {
      "data": {
        "type": "users",
        "id": "1"
      }
    },
    "thread": {
      "data": {
        "type": "threads",
        "id": "2"
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "nickname": "",
        "mobile": "mobile",
        "unionId": null,
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    },
    {
      "type": "threads",
      "id": "2",
      "attributes": {
        "title": "frgregre",
        "price": "0.50",
        "viewCount": 0,
        "postCount": 0,
        "likeCount": 0,
        "createdAt": null,
        "updatedAt": null,
        "isApproved": true,
        "isSticky": false,
        "isEssence": false
      }
    }
  ]
}
```
