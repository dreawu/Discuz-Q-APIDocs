### 创建主题视频接口

- **接口说明：** 创建主题视频接口，用作前端上传后回调。供后续视频清理使用
- **接口地址：** /api/thread/video
- **请求方式：** POST

#### 请求参数

| 参数名称  |  类型  | 是否必须 | 描述                       |
| :-------- | :----: | :------: | :------------------------- |
| file_id   | string |    是    | 视频上传后返回的 file_id   |
| file_name | string |    否    | 视频文件名称               |
| type      |  int   |    否    | 类型：0 视频（默认）1 音频 |

#### 请求示例

```json
{
  "data": {
    "type": "thread-video",
    "attributes": {
      "file_id": "1"
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称       | 类型   | 出现要求 | 描述                                    |
| :------------- | :----- | :------- | :-------------------------------------- |
| **links**      | object |          | 接口链接                                |
| **data**       | object |          | 基础数据                                |
| type           | string |          | 数据类型                                |
| id             | int    |          | 数据 id                                 |
| **attributes** | object |          | 数据属性                                |
| status         | int    |          | 视频状态 0:转码中 1 转码完成 2 转码失败 |
| reason         | string |          | 转码失败原因                            |
| file_name      | string |          | 视频文件名称                            |
| file_id        | string |          | 媒体文件唯一标识                        |
| width          | int    |          | 媒体文件宽                              |
| height         | int    |          | 媒体文件高                              |
| cover_url      | string |          | 媒体封面地址                            |

#### 返回示例

```json
{
  "data": {
    "type": "thread-video",
    "id": "68",
    "attributes": {
      "id": 1,
      "user_id": 1,
      "thread_id": 0,
      "status": 0,
      "reason": "",
      "file_name": "",
      "file_id": "1",
      "width": 1,
      "height": 1,
      "cover_url": "",
      "updated_at": "2020-04-20T14:50:10+08:00",
      "created_at": "2020-04-20T14:50:10+08:00"
    }
  }
}
```
