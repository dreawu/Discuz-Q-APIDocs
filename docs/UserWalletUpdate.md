### 更新用户钱包

- **接口说明：** 更新用户钱包
- **接口地址：** /api/wallet/user/{user_id}
- **请求方式：** PATCH

#### 请求参数

| 字段名   | 变量名         | 必填 | 类型  | 描述                                                                     |
| :------- | :------------- | :--- | :---- | :----------------------------------------------------------------------- |
| 所属用户 | user_id        | 是   | int   | 用户 id                                                                  |
| 操作类型 | operate_type   | 否   | int   | 1：增加余额操作，2：减少余额操作。                                       |
| 操作金额 | operate_amount | 否   | float | 给用户增加或减少的金额，都为大于零的数值。                               |
| 调整原因 | operate_reason | 否   | char  | 填写操作的备注或原因。                                                   |
| 钱包状态 | wallet_status  | 否   | int   | 标志用户钱包状态，0：表示正常；1：表示冻结提现，此状态下用户将无法提现。 |

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                      | 必填 | 类型   | 描述                                                                 |
| :--------------------- | :-------------------------- | :--- | :----- | :------------------------------------------------------------------- |
| **data.attributes**    | object                      | 是   | object | 数据属性                                                             |
| 所属用户               | attributes.user_id          | 是   | int    | 所属用户 ID                                                          |
| 可用金额               | attributes.available_amount | 是   | float  | 用户钱包可用金额                                                     |
| 冻结金额               | attributes.freeze_amount    | 是   | float  | 用户钱包冻结金额，交易过程中冻结的资金                               |
| 钱包状态               | attributes.wallet_status    | 是   | int    | 钱包状态，0：表示正常；1：表示冻结提现，此状态下，用户无法申请提现。 |
| **data.relationships** | object                      | 否   | object | 关联关系                                                             |
| **included**           | object                      | 否   | object | 关联数据                                                             |

示例：

```json
{
  "data": {
    "type": "user_wallet",
    "id": "1",
    "attributes": {
      "user_id": 1,
      "available_amount": "6.00",
      "freeze_amount": "28.00",
      "wallet_status": 0
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "mobile": "mobile",
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    }
  ]
}
```
