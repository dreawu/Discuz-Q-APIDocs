### 修改支付密码 - 获取验证 token

- **接口说明：** 获取验证 token
- **接口地址：** /api/users/pay-password/reset
- **请求方式：** POST

> 使用此接口返回的 TOKEN 可调用修[修改用户信息](update-user.md)接口修改支付密码

#### 请求参数

| 参数名称     |  类型  | 是否必须 | 描述     |
| :----------- | :----: | :------: | :------- |
| pay_password | string |    是    | 支付密码 |

#### 请求示例

```json
{
  "data": {
    "attributes": {
      "pay_password": "ccc"
    }
  }
}
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 500

#### 返回结果

| 参数名称  | 类型   | 出现要求 | 描述  |
| :-------- | :----- | :------- | :---- |
| sessionId | string |          | token |

#### 返回示例

```json
{
  "data": {
    "type": "sessions",
    "id": "jUSjovA7dFUMuCSOr1gi6Ygm0JPiXabIVLjAn2Gs",
    "attributes": {
      "sessionId": "jUSjovA7dFUMuCSOr1gi6Ygm0JPiXabIVLjAn2Gs"
    }
  }
}
```
