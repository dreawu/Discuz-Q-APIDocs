### 创建话题接口

- **接口说明：** 创建话题
- **接口地址：** /api/topics
- **请求方式：** POST

#### 请求参数


| 参数名称                                     | 类型  | 描述                                 |
| :------------------------------------------- | :----- | :--------------------------- |
| **data**                                     | object  | 主体数据                     |
| data.type                                    | string  | 数据模型的类型，固定值topics     |
| **data.attributes**                          | object  | 数据模型的属性               |
| data.attributes. content                     | string  | 话题名称                    |

#### 请求示例

```
{
    "data": {
        "type": "topics",
        "attributes": {
            "content": "话题"
        }
    }
}
```


#### 返回说明

- 成功， http 状态码： 200
- 失败， http 状态码： 500

#### 返回示例

```
{
    "data": {
        "type": "topics",
        "id": "1",
        "attributes": {
            "user_id": 1,
            "content": "话题",
            "thread_count": 0,
            "view_count": 0,
            "updated_at": "2020-08-13T17:42:26+08:00",
            "created_at": "2020-08-13T17:42:26+08:00"
        }
    }
}
```