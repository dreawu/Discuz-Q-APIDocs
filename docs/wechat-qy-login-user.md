### 企业微信登录

- **接口说明：** 企业微信通过 code 参数换取 token 接口
- **接口地址：** /api/oauth/wechat/qy/user
- **请求方式：** GET

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述               |
| :-------- | :----- | :------: | :----------------- |
| code      | string |    是    | 微信授权返回 code  |
| sessionId | string |    是    | 回调地址返回的参数 |
| state     | string |    否    | 微信授权返回 state |

```

/api/oauth/wechat/qy/user?sessionId={$sessionId}&code={code}&state={state}

```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 400

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |


#### 返回示例

```json
{
  "data": {
    "type": "token",
    "id": "1",
    "attributes": {
      "token_type": "Bearer",
      "expires_in": 2592000,
      "access_token": "eyJ0eXAiOiJKV1Qi......dj3H9CCSPib6MQtnaT6VNrw",
      "refresh_token": "def50200a26b6a9......10ccbf3c1694084c2d2d276"
    }
  }
}
```