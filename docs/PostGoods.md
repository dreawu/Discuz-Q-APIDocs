### 解析商品链接

- **接口说明：** 解析商品链接
- **接口地址：** /api/goods/analysis
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 名称 | 描述
|:--|:--|:--|:--|:--
| attributes.address | string  | 是  | 解析地址  | 可以是<br>淘宝/天猫/京东/拼多多H5/有赞/等链接<br>淘宝口令粘贴值/京东粘贴值H5域名/有赞粘贴值

#### 请求示例

```json
{
    "data": {
        "type": "analysis",
        "attributes": {
        	"address": "付致这段话₤V3kP1p8OMj3₤转移至👉🍑宝👈或點击链街https://m.tb.cn/h.VkpLqm2?sm=9d23c0 至瀏..覽..噐【韩国ins复古bf原宿风猫咪宽松短袖t恤男女学生ulzzang半袖上衣潮】"
        }
    }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称 | 类型 | 名称 | 描述
|:--|:--|:--|:--
| **data** | object  |   |  数据类型
| type     | string  |   |  数据类型
| id       | int     |   |  数据 id
**attributes**    | object  |   |
| user_id         | int  | 用户ID  |  发文用户ID
| post_id         | int  | 主题ID  |  主题ID
| platform_id     | string  | 平台商品ID  |  链接解析出各大平台商品ID
| title           | string  | 商品title  | 链接解析
| image_path      | string  | 商品封面图地址  | 链接解析
| price           | float  | 商品价格  | 链接解析
| type            | int  | 商品来源  | 0淘宝 1天猫 2京东 3拼多多H5 4有赞 <br>5淘宝口令粘贴值 6京东粘贴值H5域名 7有赞粘贴值
| status          | int  | 商品状态  |  0正常 1失效/下架
| ready_content   | string  | 预解析内容  | 用户填写的原链接  |
| detail_content  | string  | 解析详情页地址  | 最终解析出来的地址,可以直接访问  |
| created_at      | string  | 创建时间  |
| updated_at      | string  | 修改时间  |

#### 返回示例

```json
{
    "data": {
        "type": "post_goods",
        "id": "1",
        "attributes": {
            "user_id": 1,
            "post_id": 0,
            "platform_id": "565054639530",
            "title": "韩国ins复古bf原宿风猫咪宽松短袖t恤男女学生ulzzang半袖上衣潮",
            "image_path": "img.alicdn.com/imgextra/i4/TB1NG5dX_tYBeNjy1XdYXFXyVXa_M2.SS2_430x430q90.jpg",
            "price": "199.00",
            "type": 5,
            "status": 0,
            "ready_content": "付致这段话₤V3kP1p8OMj3₤转移至👉🍑宝👈或點击链街https://m.tb.cn/h.VkpLqm2?sm=9d23c0 至瀏..覽..噐【韩国ins复古bf原宿风猫咪宽松短袖t恤男女学生ulzzang半袖上衣潮】",
            "detail_content": "https://detail.tmall.com/item.htm?id=565054639530&ut_sk=1.XjLIohzYC0wDAIJxPL7iRS7/_21380790_1589511256554.Copy.1&sourceType=item&price=68&origin_price=199&suid=5A04DE8B-2FF0-40AC-8590-C85534D68990&un=b3fb6ac3779ca4ade8f55b3b63c56d8b&share_crt_v=1&spm=a2159r.13376460.0.0&sp_tk=4oKkVjNrUDFwOE9NajPigqQ=&cpp=1&shareurl=true&short_name=h.VkpLqm2"
        }
    }
}
```
