### 删除站点 logo 接口

- **权限：** setting.site
- **接口说明：** 删除站点 logo 接口
- **接口地址：** /api/settings/logo
- **请求方式：** DELETE

#### 请求参数

| 参数名称 |  类型  | 是否必须 | 描述                                                                                                                                          |
| :------- | :----: | :------: | :-------------------------------------------------------------------------------------------------------------------------------------------- |
| type     | string |    否    | 类型：<br/>logo 站点 logo （默认）<br>header_logo 站点头部 logo<br>background_image 站点背景图<br>watermark_image 水印图<br>favicon 站点 icon |

#### 请求示例

```json
{
  "type": "header_logo"
}
```

#### 返回结果

| 参数名称 | 类型   | 描述     |
| :------- | :----- | :------- |
| logo     | string | 头像地址 |

#### 返回说明

#### 返回示例

```json
{
  "data": {
    "type": "settings",
    "id": "1",
    "attributes": {
      "default": {
        "logo": ""
      }
    }
  }
}
```
