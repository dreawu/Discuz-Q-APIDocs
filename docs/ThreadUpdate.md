### 修改主题接口[单条]

- **接口说明：** 修改主题[单条]
- **接口地址：** /api/threads/{id}
- **请求方式：** PATCH

#### 请求参数

| 参数名称               |  类型  | 是否必须 | 描述                                                      | 示例 |
| :--------------------- | :----: | :------: | :-------------------------------------------------------- | :--- |
| isApproved             |  int   |    否    | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略         ||
| isSticky               |  bool  |    否    | 是否置顶                                                  ||
| isEssence              |  bool  |    否    | 是否加精                                                  ||
| isDeleted              |  bool  |    否    | 是否删除（回收站）                                        ||
| isFavorite             |  bool  |    否    | 是否收藏                                                  ||
| isSite                 | bool   |    否    | 是否推荐到站点信息页                                          ||
| relationships.category |  bool  |    否    | 分类关联关系                                              ||
| message                | string |    否    | 操作原因                                                  ||
| price                  | float  |    否    | 价格（长文、视频 可设置价格）                             ||
| attachment_price          | float  |    否    | 附件价格                  ||
| title                  | string |    否    | 标题（长文时必须）                                        ||
| file_name              | string |    否    | 视频文件名称 视频类型文章必填                             ||
| file_id                | string |    否    | 视频 file_id 视频类型文章必填                             ||
| longitude              | float  |    否    | 经度                                                      ||
| latitude               | float  |    否    | 纬度                                                      ||
| address                | string |    否    | 经纬度坐标对应的地址（如：广东省深圳市深南大道 10000 号） ||
| location               | string |    否    | 经纬度坐标对应的位置（如：腾讯大厦）                      ||
| is_draft | int | 是 | 是否草稿箱 ，0否 ，1是 ||

#### 请求示例

```json
{
  "data": {
    "type": "threads",
    "attributes": {
      "isApproved": false,
      "isSticky": true,
      "isEssence": true,
      "isDeleted": true,
      "price": "2.00",
      "title": "666",
      "file_name": "666",
      "file_id": "666",
      "message": "文章内容不合法",
	  "is_draft": 0
    },
    "relationships": {
      "category": {
        "data": {
          "type": "categories",
          "id": 4
        }
      }
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称          | 类型     | 出现要求   | 描述                                                      |
| :---------------- | :------- | :--------- | :-------------------------------------------------------- |
| **data**          | object   |            | 基础数据                                                  |
| type              | string   |            | 数据类型                                                  |
| id                | int      |            | 数据 id                                                   |
| **attributes**    | object   |            | 数据属性                                                  |
| title             | string   | 长文主题   | 标题                                                      |
| price             | float    | 长文主题   | 主题价格                                                  |
| viewCount         | int      |            | 查看数                                                    |
| postCount         | int      |            | 帖子数                                                    |
| longitude         | float    | 否         | 经度                                                      |
| latitude          | float    | 否         | 纬度                                                      |
| address           | string   | 否         | 经纬度坐标对应的地址（如：广东省深圳市深南大道 10000 号） |
| location          | string   | 否         | 经纬度坐标对应的位置（如：腾讯大厦）                      |
| createdAt         | datetime |            | 创建时间                                                  |
| updatedAt         | datetime |            | 修改时间                                                  |
| deletedAt         | datetime | 在回收站时 | 删除时间                                                  |
| isApproved        | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略         |
| isSticky          | bool     |            | 是否置顶                                                  |
| isEssence         | bool     |            | 是否加精                                                  |
| isFavorite        | bool     | 已收藏时   | 是否收藏                                                  |
| isSite            | bool     |            | 是否推荐到站点信息页                                      |
| isLongArticle     | bool     |            | 是否长文                                                  |
| paid              | bool     | 付费主题   | 是否付费                                                  |
| canViewPosts      | bool     |            | 是否有权查看详情                                          |
| canReply          | bool     |            | 是否有权回复                                              |
| canApprove        | bool     |            | 是否有权审核                                              |
| canSticky         | bool     |            | 是否有权置顶                                              |
| canEssence        | bool     |            | 是否有权加精                                              |
| canDelete         | bool     |            | 是否有权永久删除                                          |
| canHide           | bool     |            | 是否有权放入回收站                                        |
| canFavorite       | bool     |            | 是否有权收藏                                              |
| isDraft           | bool     |            | 是否草稿箱                                                |
| **relationships** | object   |            | 关联关系                                                  |
| **included**      | object   |            | 关联数据                                                  |

#### 返回示例

```json
{
  "data": {
    "type": "threads",
    "id": "25",
    "attributes": {
      "type": 2,
      "title": "",
      "price": 2,
      "viewCount": 13,
      "postCount": 1,
      "createdAt": "2020-03-10T16:31:54+08:00",
      "updatedAt": "2020-03-10T18:59:28+08:00",
      "isApproved": 1,
      "isSticky": false,
      "isEssence": false,
      "canViewPosts": true,
      "canReply": true,
      "canApprove": true,
      "canSticky": true,
      "canEssence": true,
      "canDelete": true,
      "canHide": true,
      "paid": false,
      "canFavorite": true,
	  "isDraft": true
    },
    "relationships": {
      "threadVideo": {
        "data": {
          "type": "thread-video",
          "id": "18"
        }
      }
    }
  },
  "included": [
    {
      "type": "thread-video",
      "id": "18",
      "attributes": {
        "id": 18,
        "user_id": 3,
        "thread_id": 25,
        "status": 0,
        "reason": "",
        "file_name": "666",
        "file_id": "666",
        "media_url": "",
        "cover_url": "",
        "updated_at": "2020-03-11T17:22:43+08:00",
        "created_at": "2020-03-10T16:31:54+08:00"
      }
    }
  ]
}
```
