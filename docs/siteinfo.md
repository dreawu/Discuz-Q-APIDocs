### 站点基本信息接口

- **接口说明：** 站点基本信息接口
- **接口地址：** /api/siteinfo
- **请求方式：** GET

#### 请求参数

#### 返回结果

| 参数名称                 | 类型   | 描述                 |
| :----------------------- | :----- | :------------------- |
| version                  | string | 程序版本号           |
| php_version              | string | 服务器 php 版本      |
| server_software          | string | 服务器信息           |
| server_os                | string | 服务器系统信息       |
| database_connection_name | string | 当前数据库连接配置   |
| ssl_installed            | string | 是否支持 https       |
| cache_driver             | string | 缓存驱动             |
| upload_size              | string | php.ini 最大上传大小 |
| db                       | string | 数据库版本号         |
| db_size                  | string | 数据库大小           |
| timezone                 | string | 时区                 |
| debug_mode               | bool   | 是否开启调试         |
| storage_dir_writable     | bool   | storage 目录是否可写 |
| cache_dir_writable       | bool   | cache 目录是否可写   |
| app_size                 | string | app 目录大小         |
| packages                 | array  | 使用的包列表         |
| unapprovedUsers          | int    | 未审核的用户数       |
| unapprovedThreads        | int    | 未审核的主题数       |
| unapprovedPosts          | int    | 未审核的回复数       |
| unapprovedMoneys         | int    | 未审核的提现申请数   |

#### 返回说明

- http 状态码： 200

#### 返回示例

```json
{
  "data": {
    "type": "siteinfo",
    "id": "1",
    "attributes": {
      "version": "5.0",
      "php_version": "Darwin / PHP v7.4.0",
      "server_software": "nginx/1.10.3",
      "server_os": "Darwin leiyu-MacBook-Air.local 19.0.0 Darwin Kernel Version 19.0.0: Wed Sep 25 20:18:50 PDT 2019; root:xnu-6153.11.26~2/RELEASE_X86_64 x86_64",
      "database_connection_name": "discuz.master",
      "ssl_installed": false,
      "cache_driver": "file",
      "upload_size": "20M",
      "db": "mysql/5.7.17",
      "db_size": "256 KB",
      "timezone": "Asia/Shanghai",
      "debug_mode": true,
      "storage_dir_writable": true,
      "cache_dir_writable": true,
      "app_size": "628 KB",
      "packages": [],
      "unapprovedThreads": 0,
      "unapprovedPosts": 0,
      "unapprovedMoneys": 0
    }
  }
}
```
