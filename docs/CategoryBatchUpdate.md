### 修改分类接口[多条]

- **接口说明：** 修改分类接口[多条]
- **接口地址：** /api/categories/batch
- **请求方式：** PATCH

#### 请求参数

| 参数名称    |  类型  | 是否必须 | 描述                                     |
| :---------- | :----: | :------: | :--------------------------------------- |
| name        | string |    是    | 分类名称                                 |
| description | string |    是    | 分类说明                                 |
| icon        | string |    否    | 分类图标 URL[可通过上传图片接口上传图标] |
| sort        |  int   |    否    | 显示顺序                                 |
| property    |  int   |    否    | 属性：0:正常 1:首页展示                  |

#### 请求示例

```json
{
  "data": [
    {
      "type": "categories",
      "id": 7,
      "attributes": {
        "name": "{{$randomWord}}",
        "description": "{{$randomWords}}",
        "sort": "{{$randomInt}}",
        "icon": "{{$randomAvatarImage}}"
      }
    },
    {
      "type": "categories",
      "id": 8,
      "attributes": {
        "name": "{{$randomWord}}",
        "description": "{{$randomWords}}",
        "sort": "{{$randomInt}}",
        "icon": "{{$randomAvatarImage}}"
      }
    },
    {
      "type": "categories",
      "id": 9,
      "attributes": {
        "name": "{{$randomWord}}",
        "description": "{{$randomWords}}",
        "sort": "{{$randomInt}}",
        "icon": "{{$randomAvatarImage}}"
      }
    }
  ]
}
```

#### 返回结果

| 参数名称               | 类型   | 出现要求 | 描述                    |
| :--------------------- | :----- | :------- | :---------------------- |
| **data**               | object |          | 基础数据                |
| type                   | string |          | 数据类型                |
| id                     | int    |          | 数据 id                 |
| **attributes**         | object |          | 数据属性                |
| attributes.id          | int    |          | 分类 id                 |
| attributes.name        | string |          | 分类名称                |
| attributes.icon        | string |          | 分类图标 URL            |
| attributes.description | string |          | 分类说明                |
| attributes.property    | int    |          | 属性：0:正常 1:首页展示 |
| attributes.sort        | int    |          | 显示顺序                |
| attributes.threadCount | int    |          | 主题总数                |

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

```json
{
  "data": [
    {
      "type": "categories",
      "id": "10",
      "attributes": {
        "name": "strategy",
        "description": "impactful Fully-configurable reboot Intelligent synergize",
        "icon": "",
        "sort": "665",
        "property": 0,
        "thread_count": 0,
        "ip": "127.0.0.1",
        "created_at": "2019-11-30T15:13:30+08:00",
        "updated_at": "2019-12-02T20:10:42+08:00"
      }
    }
  ],
  "meta": [
    {
      "id": 11,
      "message": {
        "name": ["名称 不能大于 10 个字符。"]
      }
    },
    {
      "id": 9,
      "message": "model_not_found"
    }
  ]
}
```
