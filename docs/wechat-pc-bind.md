### PC扫码绑定/换绑

- **接口说明：** PC扫码绑定/换绑
- **接口地址：** /api/oauth/wechat/pc/bind
- **请求方式：** GET

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述               |
| :-------- | :----- | :------: | :----------------- |
| session_token | string |    是    | 二维码携带的token值  |
| wechat_token  | string |    是    | 绑定时存储微信信息的token值 |

#### 请求示例

```
{{host}}/api/oauth/wechat/pc/bind?session_token=dWwzsWqC9mZXo2NeGp4C&wechat_token=lRoIJnKWtr4VihFjsSUpZpcu
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 500

#### 返回结果

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |
| bind  | bool  | true 成功，失败后直接抛出异常  |
| code  | string  | 绑定成功  |


#### 返回示例

绑定成功后

```json
{
    "bind": true,
    "code": "success_bind",
}
```

绑定失败异常

| 参数名称 | 类型 | 描述 |
| :------- | :--- | :--- |
| not_found_user_wechat  | code  | 未查询到微信信息  |
| not_found_user  | code  | 未查询到用户信息  |
| session_token_expired  | code  | session token已过期，重新生成二维码扫码(5分钟)  |

```json
{
    "errors": [
        {
            "status": "500",
            "code": "not_found_user_wechat"
        }
    ]
}
```
