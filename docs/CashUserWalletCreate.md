### 提现申请

- **接口说明：** 提现申请
- **接口地址：** /api/wallet/cash?include={user,userWallet,wechat}
- **请求方式：** POST

#### 请求参数

| 字段名       | 变量名            | 必填 | 类型   | 描述                                        |
| :----------- | :---------------- | :--- | :----- | :------------------------------------------ |
| 申请提现金额 | cash_apply_amount | 是   | float  | 申请提现金额                                |
| 提现方式     | cash_type         | 是   | int    | 提现方式：0：人工打款，1：付款到零钱        |
| 提现手机     | cash_mobile       | 否   | string | 当 cash_type = 0 时填写提现打款到的手机号码 |

```json
{
  "data": {
    "attritbutes": {
      "cash_apply_amount": 100,
      "cash_type": 0,
      "cash_mobile": "**********"
    }
  }
}
```

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 字段名                 | 变量名                        | 必填 | 类型   | 描述                                                                                 |
| :--------------------- | :---------------------------- | :--- | :----- | :----------------------------------------------------------------------------------- |
| **data.attributes**    | object                        | 是   | object | 数据属性                                                                             |
| 记录 ID                | attributes.id                 | 是   | bigint | 提现记录唯一编号                                                                     |
| 提现编号               | attributes.cash_sn            | 是   | bigint | 提现订单唯一编号                                                                     |
| 手续费                 | attributes.cash_charge        | 是   | float  | 提现手续费                                                                           |
| 实际提现金额           | attributes.cash_actual_amount | 是   | float  | 用户到账金额                                                                         |
| 提现申请金额           | attributes.cash_apply_amount  | 是   | float  | 用户提现申请金额                                                                     |
| 提现状态               | attributes.cash_status        | 是   | int    | 提现状态，1：待审核，2：审核通过，3：审核不通过，4：待打款， 5，已打款， 6：打款失败 |
| 提现方式               | attributes.cash_type          | 是   | int    | 提现方式                                                                             |
| 提现手机               | attributes.cash_mobile        | 是   | sring  | 提现打款到的手机号码                                                                 |
| 审核原因               | attributes.remark             | 是   | sring  | 审核不通过原因或备注,默认为空                                                        |
| **data.relationships** | object                        | 否   | object | 关联关系                                                                             |
| **included**           | object                        | 否   | object | 关联数据                                                                             |

示例：

```json
{
  "data": {
    "type": "user_wallet_cash",
    "id": "40",
    "attributes": {
      "id": 40,
      "user_id": 1,
      "cash_sn": "201911228652521021",
      "cash_charge": "0.01",
      "cash_actual_amount": 0.99,
      "cash_apply_amount": "1.00",
      "cash_status": 1,
      "cash_type": 0,
      "cash_mobile": "**********",
      "remark": null,
      "updated_at": "2019-11-22T11:07:32+08:00",
      "created_at": "2019-11-22T11:07:32+08:00"
    },
    "relationships": {
      "userWallet": {
        "data": {
          "type": "user_wallet",
          "id": "1"
        }
      },
      "user": {
        "data": {
          "type": "users",
          "id": "1"
        }
      }
    }
  },
  "included": [
    {
      "type": "user_wallet",
      "id": "1",
      "attributes": {
        "user_id": 1,
        "available_amount": "1.00",
        "freeze_amount": "33.00",
        "wallet_status": 0
      }
    },
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "username": "username",
        "mobile": "mobile",
        "lastLoginIp": "",
        "createdAt": "2019-11-16T12:47:45+08:00",
        "updatedAt": "2019-11-16T12:47:45+08:00"
      }
    }
  ]
}
```
