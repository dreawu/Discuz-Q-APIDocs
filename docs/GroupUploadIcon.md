### 上传用户组图标

- **接口说明：** 上传用户组图标
- **接口地址：** /api/groups/{id}/icon
- **请求方式：** POST

#### 请求参数

| 参数名称 | 类型 | 是否必须 | 描述 |
| :------- | :--- | :------- | :--- |
| icon     | file | 是       | icon |

#### 返回说明

- http 状态码：200

#### 返回结果

#### 返回示例

```json
{
  "data": {
    "type": "groups",
    "id": "10",
    "attributes": {
      "name": "普通会员",
      "type": "",
      "color": "",
      "icon": "http://example.com/storage/groups/group-10-xxxxxxxx.png",
      "default": true,
      "isDisplay": false,
      "isPaid": false,
      "fee": 0,
      "days": 0,
      "scale": 0,
      "is_subordinate": false,
      "is_commission": false
    }
  }
}
```
