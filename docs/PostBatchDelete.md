### 删除回复接口[批量]

- **接口说明：** 删除主题[批量]
- **接口地址：** /api/posts/batch/{ids}
- **请求方式：** DELETE

#### 请求参数

#### 请求示例

```
/api/posts/batch/11,12,13
```

#### 返回说明

- http 状态码 200

#### 返回结果

```
data  被删除的回复列表
meta  出现异常的回复列表
```

#### 返回示例

```json
{
  "data": [
    {
      "type": "posts",
      "id": "6",
      "attributes": {
        "content": "bandwidth Checking Account Personal Loan Account == lavender == Corner Supervisor Rapids",
        "ip": "127.0.0.1",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2019-11-12T17:10:42+08:00",
        "updatedAt": "2019-11-12T17:10:42+08:00",
        "isFirst": true,
        "isApproved": true,
        "isLiked": false
      }
    }
  ],
  "meta": [
    {
      "id": "4",
      "message": "model_not_found"
    },
    {
      "id": "5",
      "message": "model_not_found"
    }
  ]
}
```
