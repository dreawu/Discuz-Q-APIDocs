### 创建主题接口

- **接口说明：** 创建主题
- **接口地址：** /api/threads
- **请求方式：** POST

#### 请求参数

| 参数名称                  |  类型  | 是否必须 | 描述                                                      |
| :------------------------ | :----: | :------: | :-------------------------------------------------------- |
| title                     | string |    否    | 标题（发布帖子时必须）                                    |
| content                   | string |    是    | 内容（发布帖子时支持 Markdown 语法）                      |
| price                     | float  |    否    | 价格（发布帖子时可设置价格发布付费主题）                  |
| attachment_price          | float  |    否    | 附件价格                  |
| free_words                |  int   |    否    | 付费主题可免费阅读字数百分比(使用小数)                                    |
| type                      |  int   |    是    | 文章类型（0:文字 1:帖子 2:视频 3:图片 4:语音 5:问答 6:商品）    |
| longitude                 | float  |    否    | 经度                                                      |
| latitude                  | float  |    否    | 纬度                                                      |
| address                   | string |    否    | 经纬度坐标对应的地址（如：广东省深圳市深南大道 10000 号） |
| location                  | string |    否    | 经纬度坐标对应的位置（如：腾讯大厦）                      |
| file_id                   | string |    否    | 视频 file_id 视频类型文章必填                             |
| file_name                 | string |    否    | 视频文件名称                                              |
| cover_url                 | string |    否    | 视频封面地址                                              |
| media_url                 | string |    否    | 视频地址                                                  |
| is_anonymous              | bool   |    否    | 是否发布匿名帖  |
| relationships.category    | object |    是    | 主题分类关联关系                                          |
| relationships.attachments | object |    否    | 主题图片、附件关联关系                                    |
| relationships.question    | object |    否    | 问答帖子关联关系                                          |
| question.be_user_id          | int       | 是 | 回答人的 用户 id  |
| question.order_id            | string    | 是 | 支付成功后的 订单 id |
| question.price               | float     | 是 | 问答单价  |
| question.is_onlooker         | bool      | 是 | 是否允许围观 true允许 false不允许 |
| question.type | int | 是 | 0为面向所有人的悬赏帖，1为指定个人的问答帖 |
| question.expired_at | datetime | 否 | 悬赏帖过期时间，当为所有人的悬赏帖时，这里是必填 |
| post_goods_id         | int      | 否 | 商品ID |
| is_draft | int | 是 | 是否草稿箱 ，0否 ，1是 |

#### 请求示例

```json
{
  "data": {
    "type": "threads",
    "attributes": {
      "price": 10,
      "attachment_price": 10,
      "title": "title",
      "type": 5,
      "longitude": "116.397469",
      "latitude": "39.908821",
      "location": "北京市天安门",
      "content": "{{$randomWords}} == {{$randomColor}} == {{$randomWords}}",
      "is_anonymous": true
    },
    "relationships": {
      "category": {
        "data": {
          "type": "categories",
          "id": 6
        }
      },
      "attachments": {
        "data": [
          {
            "type": "attachments",
            "id": 1
          },
          {
            "type": "attachments",
            "id": 2
          }
        ]
      },
      "question": {
          "data": {
              "be_user_id": 1,
              "order_id": "20200918*****1554956",
              "price": 100,
              "is_onlooker": true
          }
      }
    }
  }
}
```

#### 返回说明

- 成功，http 状态码 201
- 失败，http 状态码 500

#### 返回结果

| 参数名称                | 类型     | 出现要求   | 描述                                                      |
| :---------------------- | :------- | :--------- | :-------------------------------------------------------- |
| **links**               | object   |            | 接口链接                                                  |
| **data**                | object   |            | 基础数据                                                  |
| type                    | string   |            | 数据类型                                                  |
| id                      | int      |            | 数据 id                                                   |
| **attributes**          | object   |            | 数据属性                                                  |
| type                    | int      |            | 文章类型(0 普通 1 长文 2 视频)                            |
| title                   | string   | 长文主题   | 标题                                                      |
| price                   | float    | 长文主题   | 主题价格                                                  |
| attachment_price        | float    |            | 附件价格                                                  |
| viewCount               | int      |            | 查看数                                                    |
| postCount               | int      |            | 帖子数                                                    |
| paidCount               | nt       |            | 付费数                                                    |
| rewardedCount           | nt       |            | 打赏数                                                    |
| longitude               | float    |            | 经度                                                      |
| latitude                | float    |            | 纬度                                                      |
| address                 | string   |            | 经纬度坐标对应的地址（如：广东省深圳市深南大道 10000 号） |
| location                | string   |            | 经纬度坐标对应的位置（如：腾讯大厦）                      |
| createdAt               | datetime |            | 创建时间                                                  |
| updatedAt               | datetime |            | 修改时间                                                  |
| deletedAt               | datetime | 在回收站时 | 删除时间                                                  |
| isApproved              | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略         |
| isSticky                | bool     |            | 是否置顶                                                  |
| isEssence               | bool     |            | 是否加精                                                  |
| isFavorite              | bool     | 已收藏时   | 是否收藏                                                  |
| paid                    | bool     | 付费主题   | 是否付费                                                  |
| canViewPosts            | bool     |            | 是否有权查看详情                                          |
| canReply                | bool     |            | 是否有权回复                                              |
| canApprove              | bool     |            | 是否有权审核                                              |
| canSticky               | bool     |            | 是否有权置顶                                              |
| canEssence              | bool     |            | 是否有权加精                                              |
| canDelete               | bool     |            | 是否有权永久删除                                          |
| canHide                 | bool     |            | 是否有权放入回收站                                        |
| canFavorite             | bool     |            | 是否有权收藏                                              |
| isDraft                 | int      |            | 是否草稿箱                                                |
| questionTypeAndMoney    | object   |            | 悬赏帖信息                                                |
| redPacket               | object   |            | 红包帖信息                                                |
| rewardedCount           | int      |            | 红包信息                                                  |
| **relationships**       | object   |            | 关联关系                                                  |
| thread-video. status    | int      |            | 视频状态 0:转码中 1 转码完成 2 转码失败                   |
| thread-video. reason    | string   |            | 转码失败原因                                              |
| thread-video. file_name | string   |            | 视频文件名称                                              |
| thread-video. file_id   | string   |            | 媒体文件唯一标识                                          |
| thread-video. media_url | string   |            | 媒体播放地址                                              |
| thread-video. cover_url | string   |            | 媒体封面地址                                              |
| **included**            | object   |            | 关联数据                                                  |

#### 返回示例

```json
{
  "data": {
    "type": "threads",
    "id": "25",
    "attributes": {
      "type": 2,
      "title": "",
      "price": "0.00",
      "attachment_price": "0.00",
      "viewCount": 1,
      "postCount": 1,
      "paidCount": 0,
      "rewardedCount": 0,
      "longitude": "116.397469",
      "latitude": "39.908821",
      "location": "北京市天安门",
      "createdAt": "2020-03-10T16:31:54+08:00",
      "updatedAt": "2020-03-10T16:31:54+08:00",
      "isApproved": 1,
      "isSticky": false,
      "isEssence": false,
      "canViewPosts": true,
      "canReply": false,
      "canApprove": false,
      "canSticky": false,
      "canEssence": false,
      "canDelete": false,
      "canHide": false,
      "canFavorite": false,
	  "isDraft": false,
	  "rewardedCount": 0
    },
    "relationships": {
      "user": {
        "data": {
          "type": "users",
          "id": "3"
        }
      },
      "firstPost": {
        "data": {
          "type": "posts",
          "id": "23"
        }
      },
      "threadVideo": {
        "data": {
          "type": "thread-video",
          "id": "18"
        }
      },
      "posts": {
        "data": []
      }
    }
  },
  "included": [
    {
      "type": "users",
      "id": "3",
      "attributes": {
        "id": 3,
        "username": "username",
        "avatarUrl": "",
        "threadCount": 19,
        "followCount": 0,
        "fansCount": 0,
        "follow": null,
        "status": 0,
        "loginAt": "2020-03-10T11:20:42+08:00",
        "joinedAt": "2020-03-09T16:39:28+08:00",
        "expiredAt": null,
        "createdAt": "2020-03-09T16:39:28+08:00",
        "updatedAt": "2020-03-10T17:03:44+08:00",
        "canEdit": false,
        "canDelete": false,
        "registerReason": "",
        "banReason": ""
      }
    },
    {
      "type": "posts",
      "id": "23",
      "attributes": {
        "replyUserId": null,
        "content": "abc",
        "contentHtml": "abc",
        "replyCount": 0,
        "likeCount": 0,
        "createdAt": "2020-03-10T16:31:54+08:00",
        "updatedAt": "2020-03-10T16:31:54+08:00",
        "isFirst": true,
        "isApproved": 1,
        "canEdit": false,
        "canApprove": false,
        "canDelete": false,
        "canHide": false,
        "canLike": false
      },
      "relationships": {
        "images": {
          "data": []
        },
        "attachments": {
          "data": []
        }
      }
    },
    {
      "type": "thread-video",
      "id": "18",
      "attributes": {
        "id": 18,
        "user_id": 3,
        "thread_id": 25,
        "status": 1,
        "reason": "source file err: file info lack width, height or color space",
        "file_name": "66",
        "file_id": "66",
        "media_url": "url",
        "cover_url": "url",
        "updated_at": "2020-03-10T18:26:31+08:00",
        "created_at": "2020-03-10T16:31:54+08:00"
      }
    }
  ]
}
```
