### QQ登录

- **接口说明：** qq登录
- **接口地址：** /api/oauth/qq
- **请求方式：** GET

#### 请求参数

| 参数名称 | 类型   | 描述                    | 是否必填 |
| :------- | :----- | :---------------------- | :------- |
| display | string | 用于展示的样式。不传则默认展示为PC下的样式。如果传入“mobile”，则展示为mobile端下的样式。 | 否       |
| code      | string | qq授权回调后返回参数 | 第一次请求不需要填，回调后必填 |
| sessionId | string | 回调地址返回的参数 |第一次请求不需要填，回调后必填 |
| state     | string | qq授权返回 state |第一次请求不需要填，回调后必填 |

请求示例：

```
/api/oauth/qq?display={display}

```
回调后示例：
```
api/oauth/qq?sessionId={sessionId}&display=mobile&code={code}&state={state}

```

#### 返回结果

#### 返回说明

- 成功， http 状态码： 302 直接跳转到qq授权页
- 失败， http 状态码： 非 200
