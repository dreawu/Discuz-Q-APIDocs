### 用户搜索接口

- **权限：** viewUserList
- **接口说明：** 用户搜索接口
- **接口地址：** /api/users
- **请求方式：** GET

#### 请求参数

| 参数名称           | 类型   | 是否必须 | 描述                                                                                   |
| :----------------- | :----- | :------: | :------------------------------------------------------------------------------------- |
| filter[id]         | int    |    否    | 用户 id                                                                                |
| filter[username]   | string |    否    | 用户名：多个用户名用半角逗号隔开<br>用户名前或后加星号可使用模糊搜索                   |
| filter[mobile]     | string |    否    | 用户手机号                                                                             |
| filter[status]     | int    |    否    | 状态：normal 正常 ban 禁用 mod 审核中 through 审核通过 refuse 审核拒绝 ignore 审核忽略 |
| filter[group_id][] | array  |    否    | 用户组                                                                                 |
| filter[isReal]     | string |    否    | 是否实名认证（yes/no）                                                                 |
| filter[wechat]     | string |    否    | 是否绑定微信（yes/no）                                                                 |
| filter[canBeAsked] | string |    否    | 是否允许被提问（yes/no）                                                               |
| page[limit]        | int    |    否    | 分页数据条数                                                                           |
| page[number]       | int    |    否    | 页码                                                                                   |
| sort               | string |    否    | 排序：固定值 正序 createdAt 倒序 -createdAt                                            |
| include            | string |    否    | 关联数据，默认 groups                                                                  |

#### 请求示例

```
/api/users?filter[username]=*d*
```

#### 返回说明

- 成功，http 状态码： 200
- 失败，http 状态码： 404

#### 返回结果

| 参数名称       | 类型     | 描述                           |
| :------------- | :------- | :----------------------------- |
| **links**      | object   | 接口链接                       |
| **data**       | object   | 基础数据                       |
| type           | string   | 数据类型                       |
| id             | int      | 数据 id                        |
| **attributes** | object   | 数据属性                       |
| id             | int      | 用户 id                        |
| username       | string   | 用户名                         |
| mobile         | string   | 脱敏手机号                     |
| avatarUrl      | string   | 头像地址                       |
| threadCount    | int      | 主题数                         |
| registerIp     | string   | 注册 ip                        |
| lastLoginIp    | string   | 最后登录 IP                    |
| status         | int      | 用户状态：<br>0 正常<br>1 禁用 |
| joinedAt       | datetime | 加入时间                       |
| expiredAt      | datetime | 到期时间                       |
| createdAt      | datetime | 注册时间                       |
| updatedAt      | datetime | 最后活动时间                   |
| originalMobile | string   | 完整手机号（管理员可见）       |

#### 返回示例

```json
{
  "links": {
    "first": "https://discuz.chat/api/users?filter%5Busername%5D=a%2A%2C%2Ad&page%5Blimit%5D=2",
    "next": "https://discuz.chat/api/users?filter%5Busername%5D=a%2A%2C%2Ad&page%5Blimit%5D=2&page%5Boffset%5D=2",
    "last": "https://discuz.chat/api/users?filter%5Busername%5D=a%2A%2C%2Ad&page%5Blimit%5D=2&page%5Boffset%5D=6"
  },
  "data": [
    {
      "type": "users",
      "id": "1",
      "attributes": {
        "id": 1,
        "username": "username",
        "mobile": "mobile",
        "avatarUrl": "",
        "threadCount": 17,
        "registerIp": "127.0.0.1",
        "lastLoginIp": "127.0.0.1",
        "status": 1,
        "joinedAt": null,
        "expiredAt": null,
        "createdAt": "2019-12-25T17:22:52+08:00",
        "updatedAt": "2019-12-27T16:15:20+08:00",
        "originalMobile": "mobilez"
      },
      "relationships": {
        "groups": {
          "data": [
            {
              "type": "groups",
              "id": "1"
            }
          ]
        }
      }
    },
    {
      "type": "users",
      "id": "2",
      "attributes": {
        "id": 2,
        "username": "username",
        "mobile": "mobile",
        "avatarUrl": "https://discuz.chat/storage/avatars/Le0Ixb2CErymqpkB.png",
        "threadCount": 0,
        "registerIp": "127.0.0.1",
        "lastLoginIp": "",
        "status": 0,
        "joinedAt": "2019-12-25T17:26:59+08:00",
        "expiredAt": null,
        "createdAt": "2019-12-25T17:27:00+08:00",
        "updatedAt": "2019-12-27T15:20:11+08:00",
        "originalMobile": "mobile"
      },
      "relationships": {
        "groups": {
          "data": [
            {
              "type": "groups",
              "id": "10"
            }
          ]
        }
      }
    }
  ],
  "included": [
    {
      "type": "groups",
      "id": "1",
      "attributes": {
        "name": "管理员",
        "type": "",
        "color": "",
        "icon": "",
        "default": 0
      }
    },
    {
      "type": "groups",
      "id": "10",
      "attributes": {
        "name": "普通会员",
        "type": "",
        "color": "",
        "icon": "",
        "default": 1
      }
    }
  ],
  "meta": {
    "total": 7,
    "size": 2
  }
}
```
