### 云点播回调接口

- **接口说明：** 云点播回调接口
- **接口地址：**/api /threads/notify/video
- **请求方式：** POST

#### 请求参数

| 参数名称  | 类型   | 是否必须 | 描述             |
| --------- | ------ | -------- | ---------------- |
| qvodtoken | string | 是       | 云点播回调校验码 |
| EventType | string | 是       | 事件类型         |

**请求示例**

http(s)://当前域名/api/threads/notify/video?qvodtoken=云点播回调校验码

#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果

| 参数名称 | 类型   | 出现要求 | 描述       |
| :------- | :----- | :------- | :--------- |
| status   | string |          | 返回状态码 |

示例：

```json
{
    "status": "SUCCESS"
}
```