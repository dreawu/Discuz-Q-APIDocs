### 微信小程序登录、注册、绑定接口

- **接口说明：** 微信小程序登录、注册、绑定接口。
开通微信开放平台并绑定微信公众号、小程序后，用户绑定过公众号则会自动绑定相同的账号。
当调取接口时的headers中包含authorization,会根据登陆用户和当前微信小程序的绑定情况进行绑定。
- **接口地址：** /api/oauth/wechat/miniprogram
- **请求方式：** POST

#### 请求参数

| 参数名称      | 类型   | 是否必填 | 描述                                                                                                                                                                                                                                                                                                                    |
| :------------ | :----- | :------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|    register   | int    | 否       | 是否自动注册。注册：1，不注册：0或者不传 |
|    code       | string | 否       | 注册邀请码 |
| js_code       | string | 是       | 通过 wx.login()获取的 code，文档地址[https://developers.weixin.qq.com/miniprogram/dev/api/open-api/login/wx.login.html](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/login/wx.login.html)                                                                                                              |
| iv            | string | 是       | 通过 wx.getUserInfo()获取的 iv，文档地址[https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html) wx.getUserInfo()默认获取英文用户资料，可传参数{'lang':'zh_CN'}获取简体中文           |
| encryptedData | string | 是       | 通过 wx.getUserInfo()获取的 encryptedData，文档地址[https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html)wx.getUserInfo()默认获取英文用户资料，可传参数{'lang':'zh_CN'}获取简体中文 |

请求示例：

```
{
	"data":
	{
		"attributes":{
			"register":1,
			"js_code":"001T0eQz",
			"iv":"pKByr",
			"encryptedData":"s5geLy225EqGq"
		}
	}
}

```

#### 返回结果

```
{
    "data": {
        "type": "token",
        "id": "1",
        "attributes": {
            "token_type": "Bearer",
            "expires_in": 2592000,
            "access_token": "eyJ0eXAiOiJKV1QiLCJhb...",
            "refresh_token": "def5020025dd5bd6f1ee..."
        }
    }
}
```
```
{
    "errors": [
        {
            "status": 400,
            "code": "no_bind_user",
            "token": "",
            "user": {
                "username": "username",
                "headimgurl": "headimgurl"
            }
        }
    ]
}
```

```


#### 返回说明

- 成功， http 状态码： 200 返回 discuzQ 登录态 access_token
- 失败， http 状态码： 400 no_bind_user, 返回登陆/注册接口绑定用的token，返回一些微信用户信息
