### 我的购买列表

- **接口说明：** 我的购买列表
- **接口地址：** /api/threads/paid
- **请求方式：** GET

#### 请求参数

| 字段名   | 变量名       | 必填 | 类型 | 描述             |
| :------- | :----------- | :--- | :--- | :--------------- |
| 分页参数 | page[number] | 是   | int  | 第几页           |
| 分页参数 | page[size]   | 是   | int  | 每页显示几条数据 |
#### 请示示例

```
http://192.168.2.232/api/threads/paid?page[number]=1&page[limit]=10
```



#### 返回说明

- 返回当前创建成功数据， http 状态码： 200

#### 返回结果(与接口api/threads的返回相似)

| 参数名称                            | 类型     | 出现要求   | 描述                                                         |
| :---------------------------------- | :------- | :--------- | :----------------------------------------------------------- |
| **links**                           | object   |            | 接口链接                                                     |
| **data**                            | object   |            | 基础数据                                                     |
| type                                | string   |            | 数据类型                                                     |
| id                                  | int      |            | 数据 id                                                      |
| **attributes**                      | object   |            | 数据属性                                                     |
| type                                | int      |            | 文章类型**0普通；1长文；2视频；3图片；4音频；5问答；6商品; 7专辑** |
| title                               | string   | 长文主题   | 标题                                                         |
| price                               | float    | 长文主题   | 主题价格                                                     |
| attachment_price                    | float    |            | 附件价格                                                     |
| viewCount                           | int      |            | 查看数                                                       |
| postCount                           | int      |            | 帖子数                                                       |
| paidCount                           | int      |            | 付费数                                                       |
| rewardedCount                       | int      |            | 打赏数                                                       |
| createdAt                           | datetime |            | 创建时间                                                     |
| updatedAt                           | datetime |            | 修改时间                                                     |
| deletedAt                           | datetime | 在回收站时 | 删除时间                                                     |
| isApproved                          | bool     |            | 是否合法（0/1/2）<br>0 不合法<br>1 正常<br>2 忽略            |
| isSticky                            | bool     |            | 是否置顶                                                     |
| isEssence                           | bool     |            | 是否加精                                                     |
| isFavorite                          | bool     | 已收藏时   | 是否收藏                                                     |
| isSite                              | bool     |            | 是否推荐到站点信息页                                         |
| paid                                | bool     | 付费主题   | 是否付费                                                     |
| isPaidAttachment                    | bool     | 付费附件   | 附件是否付费                                                 |
| canViewPosts                        | bool     |            | 是否有权查看详情                                             |
| canReply                            | bool     |            | 是否有权回复                                                 |
| canApprove                          | bool     |            | 是否有权审核                                                 |
| canSticky                           | bool     |            | 是否有权置顶                                                 |
| canEssence                          | bool     |            | 是否有权加精                                                 |
| canDelete                           | bool     |            | 是否有权永久删除                                             |
| canHide                             | bool     |            | 是否有权放入回收站                                           |
| canFavorite                         | bool     |            | 是否有权收藏                                                 |
| isRedPacket                         | int      |            | 是否为红包帖，0不是，1是                                     |
| postContent                         | string   |            | 帖子的内容字段，备用                                         |
| album_content                       | object   |            | 专辑帖的内容，存放的是专辑帖包含的主题的标题，该字段用于后台-内容管理-列表展示 |
| **questionTypeAndMoney**            | object   |            | 悬赏/问答帖信息                                              |
| id                                  | int      |            | 悬赏问答扩展表主键ID                                         |
| thread_id                           | int      |            | 悬赏/问答帖ID                                                |
| post_id                             | int      |            | 悬赏/问答帖主要内容ID                                        |
| type                                | int      |            | 类型，0为所有人可回答的悬赏帖，1为指定人回答的问答帖         |
| answer_id                           | int      |            | 被指定回答人的ID，可为空                                     |
| money                               | float    |            | 悬赏金额/问答金额                                            |
| remain_money                        | float    |            | 剩余金额                                                     |
| created_at                          | datetime |            | 创建时间                                                     |
| updated_at                          | datetime |            | 更新时间                                                     |
| expired_at                          | datetime |            | 过期时间                                                     |
| **relationships**                   | object   |            | 关联关系                                                     |
| **included**                        | object   |            | 关联数据                                                     |
| question_answer.be_user_id          | int      |            | 回答人的用户 ID                                              |
| question_answer.content             | string   |            | 回答的内容                                                   |
| question_answer.content_html        | string   |            | 回答的 html 内容                                             |
| question_answer.ip                  | string   |            | 回答人的 IP                                                  |
| question_answer.port                | int      |            | 回答人的端口                                                 |
| question_answer.price               | float    |            | 问答单价                                                     |
| question_answer.onlooker_unit_price | float    |            | 围观单价                                                     |
| question_answer.onlooker_price      | float    |            | 围观总价格                                                   |
| question_answer.onlooker_number     | int      |            | 围观总人数                                                   |
| question_answer.is_onlooker         | bool     |            | 是否允许围观 true 允许 false 不允许                          |
| question_answer.is_answer           | int      |            | 是否已回答 0 未回答 1 已回答 2 已过期                        |
| question_answer.expired_at          | string   |            | 问答过期时间                                                 |

示例：

```json
{
    "links": {
        "first": "http://www.dzqgit.com/api/threads/paid?isMobile=0",
        "last": "http://www.dzqgit.com/api/threads/paid?isMobile=0"
    },
    "data": [
        {
            "type": "threads",
            "id": "25",
            "attributes": {
                "type": 1,
                "title": "这是一个付费帖子",
                "price": "2.00",
                "attachmentPrice": "0.00",
                "freeWords": 0.1,
                "viewCount": 6,
                "postCount": 1,
                "paidCount": 1,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T11:23:44+08:00",
                "updatedAt": "2020-12-23T11:41:30+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": false,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 0,
                "postContent": "要花钱的喔1111111哈哈哈哈哈哈哈哈Hi好扫地机偶见覅感觉到房间卡带附件",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "paid": false,
                "isPaid": false,
                "canFavorite": false,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "56"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "4"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "10"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "4"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "26",
            "attributes": {
                "type": 1,
                "title": "这是一个付费帖子",
                "price": "5.00",
                "attachmentPrice": "0.00",
                "freeWords": 0.1,
                "viewCount": 6,
                "postCount": 1,
                "paidCount": 2,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T11:29:35+08:00",
                "updatedAt": "2020-12-23T11:41:10+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": false,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 0,
                "postContent": "还是要花钱的哟11111111111对符合规范估计都是破封建割据搜谱见覅价格低就给大家佛二极管那嘎达就的架构松紧的卡佛二级佛龛",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "paid": false,
                "isPaid": false,
                "canFavorite": false,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "57"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "3"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "28"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "3"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "27",
            "attributes": {
                "type": 3,
                "title": "",
                "price": "2.00",
                "attachmentPrice": "0.00",
                "freeWords": 0,
                "viewCount": 5,
                "postCount": 1,
                "paidCount": 1,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T11:33:00+08:00",
                "updatedAt": "2020-12-23T11:40:54+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": false,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 0,
                "postContent": "快来花钱吧！这是一条付费的图片帖",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "paid": false,
                "isPaid": false,
                "canFavorite": false,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "58"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "3"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "28"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "3"
                    }
                }
            }
        },
        {
            "type": "threads",
            "id": "28",
            "attributes": {
                "type": 1,
                "title": "还是一条付费帖子！",
                "price": "0.00",
                "attachmentPrice": "2.00",
                "freeWords": 0,
                "viewCount": 9,
                "postCount": 1,
                "paidCount": 1,
                "rewardedCount": 0,
                "longitude": "0.0000000",
                "latitude": "0.0000000",
                "address": "",
                "location": "",
                "createdAt": "2020-12-23T11:34:04+08:00",
                "updatedAt": "2020-12-23T11:40:36+08:00",
                "isApproved": 1,
                "isSticky": false,
                "isEssence": false,
                "isSite": false,
                "isAnonymous": false,
                "canBeReward": true,
                "canViewPosts": true,
                "canReply": false,
                "canApprove": false,
                "canSticky": false,
                "canEssence": false,
                "canDelete": false,
                "canHide": false,
                "canEdit": false,
                "isRedPacket": 0,
                "postContent": "哈互动覅返回is到花覅哦啊接讽德诵功快递费敬爱的价格可是公司多发几个技术架构时代峻峰说的就是讲故事的架构四大金刚时代峻峰",
                "questionTypeAndMoney": null,
                "album_content": "",
                "isDeleted": false,
                "isPaidAttachment": false,
                "canFavorite": false,
                "isFavorite": false
            },
            "relationships": {
                "firstPost": {
                    "data": {
                        "type": "posts",
                        "id": "59"
                    }
                },
                "lastPostedUser": {
                    "data": {
                        "type": "users",
                        "id": "3"
                    }
                },
                "category": {
                    "data": {
                        "type": "categories",
                        "id": "10"
                    }
                },
                "user": {
                    "data": {
                        "type": "users",
                        "id": "3"
                    }
                }
            }
        }
    ],
    "included": [
        {
            "type": "posts",
            "id": "56",
            "attributes": {
                "replyUserId": null,
                "summary": "<p>要花钱的...</p>",
                "summaryText": "要花钱的...",
                "content": "要花钱的...",
                "contentHtml": "<p>要花钱的...</p>",
                "replyCount": 0,
                "likeCount": 0,
                "createdAt": "2020-12-23T11:23:44+08:00",
                "updatedAt": "2020-12-23T11:23:44+08:00",
                "isApproved": 1,
                "canEdit": false,
                "canApprove": false,
                "canDelete": false,
                "canHide": false,
                "contentAttachIds": [],
                "isDeleted": false,
                "isFirst": true,
                "isComment": false,
                "canLike": false,
                "isLiked": false
            }
        },
        {
            "type": "posts",
            "id": "57",
            "attributes": {
                "replyUserId": null,
                "summary": "<p>还是要花钱的哟...</p>",
                "summaryText": "还是要花钱的哟...",
                "content": "还是要花钱的哟...",
                "contentHtml": "<p>还是要花钱的哟...</p>",
                "replyCount": 0,
                "likeCount": 0,
                "createdAt": "2020-12-23T11:29:35+08:00",
                "updatedAt": "2020-12-23T11:29:35+08:00",
                "isApproved": 1,
                "canEdit": false,
                "canApprove": false,
                "canDelete": false,
                "canHide": false,
                "contentAttachIds": [],
                "isDeleted": false,
                "isFirst": true,
                "isComment": false,
                "canLike": false,
                "isLiked": false
            }
        },
        {
            "type": "posts",
            "id": "58",
            "attributes": {
                "replyUserId": null,
                "summary": "<p>快来花钱吧！这是一条付费的图片帖</p>",
                "summaryText": "快来花钱吧！这是一条付费的图片帖",
                "content": "快来花钱吧！这是一条付费的图片帖",
                "contentHtml": "<p>快来花钱吧！这是一条付费的图片帖</p>",
                "replyCount": 0,
                "likeCount": 0,
                "createdAt": "2020-12-23T11:33:00+08:00",
                "updatedAt": "2020-12-23T11:33:00+08:00",
                "isApproved": 1,
                "canEdit": false,
                "canApprove": false,
                "canDelete": false,
                "canHide": false,
                "contentAttachIds": [],
                "isDeleted": false,
                "isFirst": true,
                "isComment": false,
                "canLike": false,
                "isLiked": false
            }
        },
        {
            "type": "posts",
            "id": "59",
            "attributes": {
                "replyUserId": null,
                "summary": "<p>哈互动覅返回is到花覅哦啊接讽德诵功快递费敬爱的价格可是公司多发几个技术架构时代峻峰说的就是讲故事的架构四大金刚时代峻峰</p>",
                "summaryText": "哈互动覅返回is到花覅哦啊接讽德诵功快递费敬爱的价格可是公司多发几个技术架构时代峻峰说的就是讲故事的架构四大金刚时代峻峰",
                "content": "哈互动覅返回is到花覅哦啊接讽德诵功快递费敬爱的价格可是公司多发几个技术架构时代峻峰说的就是讲故事的架构四大金刚时代峻峰",
                "contentHtml": "<p>哈互动覅返回is到花覅哦啊接讽德诵功快递费敬爱的价格可是公司多发几个技术架构时代峻峰说的就是讲故事的架构四大金刚时代峻峰</p>",
                "replyCount": 0,
                "likeCount": 0,
                "createdAt": "2020-12-23T11:34:04+08:00",
                "updatedAt": "2020-12-23T11:34:04+08:00",
                "isApproved": 1,
                "canEdit": false,
                "canApprove": false,
                "canDelete": false,
                "canHide": false,
                "contentAttachIds": [],
                "isDeleted": false,
                "isFirst": true,
                "isComment": false,
                "canLike": false,
                "isLiked": false
            }
        },
        {
            "type": "users",
            "id": "4",
            "attributes": {
                "id": 4,
                "username": "haha",
                "avatarUrl": "",
                "isReal": false,
                "threadCount": 2,
                "followCount": 0,
                "fansCount": 0,
                "likedCount": 0,
                "questionCount": 0,
                "signature": "",
                "usernameBout": 1,
                "status": 0,
                "loginAt": "2020-12-23T11:34:47+08:00",
                "joinedAt": "2020-12-15T11:28:43+08:00",
                "expiredAt": null,
                "createdAt": "2020-12-15T11:28:43+08:00",
                "updatedAt": "2020-12-23T11:39:02+08:00",
                "canEdit": false,
                "canDelete": false,
                "showGroups": false,
                "registerReason": "",
                "banReason": "",
                "denyStatus": false,
                "canBeAsked": true,
                "canEditUsername": false
            }
        },
        {
            "type": "users",
            "id": "3",
            "attributes": {
                "id": 3,
                "username": "newcjw",
                "avatarUrl": "",
                "isReal": false,
                "threadCount": 12,
                "followCount": 0,
                "fansCount": 0,
                "likedCount": 0,
                "questionCount": 0,
                "signature": "",
                "usernameBout": 0,
                "status": 0,
                "loginAt": "2020-12-23T11:32:19+08:00",
                "joinedAt": "2020-12-15T11:27:42+08:00",
                "expiredAt": null,
                "createdAt": "2020-12-15T11:27:42+08:00",
                "updatedAt": "2020-12-23T11:34:06+08:00",
                "canEdit": false,
                "canDelete": false,
                "showGroups": false,
                "registerReason": "",
                "banReason": "",
                "denyStatus": false,
                "canBeAsked": true,
                "canEditUsername": true
            }
        },
        {
            "type": "categories",
            "id": "10",
            "attributes": {
                "name": ".NET",
                "description": "sddddd",
                "icon": "",
                "sort": 4,
                "parentid": 0,
                "property": 0,
                "thread_count": 7,
                "ip": "127.0.0.1",
                "created_at": "2020-12-04T19:31:11+08:00",
                "updated_at": "2020-12-23T11:34:05+08:00",
                "canCreateThread": false,
                "checked": 0,
                "search_ids": "10,27",
                "children": [
                    {
                        "id": 27,
                        "name": ".NEI子类",
                        "description": "中转站",
                        "icon": "",
                        "sort": 8,
                        "property": 0,
                        "thread_count": 1,
                        "moderators": [
                            ""
                        ],
                        "ip": "127.0.0.1",
                        "parentid": 10,
                        "created_at": "2020-12-08T08:02:38.000000Z",
                        "updated_at": "2020-12-09T06:09:36.000000Z",
                        "canCreateThread": false,
                        "search_ids": 27,
                        "checked": 0
                    }
                ]
            }
        },
        {
            "type": "categories",
            "id": "28",
            "attributes": {
                "name": "HTML",
                "description": "11111",
                "icon": "",
                "sort": 5,
                "parentid": 0,
                "property": 0,
                "thread_count": 5,
                "ip": "127.0.0.1",
                "created_at": "2020-12-16T19:30:51+08:00",
                "updated_at": "2020-12-23T11:33:00+08:00",
                "canCreateThread": false,
                "checked": 0,
                "search_ids": "28",
                "children": []
            }
        }
    ],
    "meta": {
        "threadCount": 4,
        "pageCount": 1
    }
}
```
